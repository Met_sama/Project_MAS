cfg = {}

cfg.blips = false -- enable blips

cfg.seconds = 300 -- seconds to rob

cfg.cooldown = 600 -- time between robbaries

cfg.cops = 2 -- minimum cops online
cfg.permission = "bank.police" -- permission given to cops

cfg.banks = { -- list of banks
}