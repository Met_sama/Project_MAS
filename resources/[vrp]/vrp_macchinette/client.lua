vRP = Proxy.getInterface("vRP")

local Keys = {
  ["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57, 
  ["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177, 
  ["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
  ["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
  ["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
  ["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70, 
  ["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
  ["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
  ["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}


function DisplayNotification(string)
	SetTextComponentFormat("STRING")
	AddTextComponentString(string)
	DisplayHelpTextFromStringLabel(0, 0, 2, -1)
end

local macchinette = {
    {x = -38.14891052246,y = -1094.287109375,z = 26.422369003296},
	{x = 19.890424728394,y = -1114.2189941406,z = 29.797039031982},
	{x = 436.19476318359,y = -986.65313720703,z = 30.689674377441},
	{x = 2069.0124511718,y = 3341.6765136718,z = 47.57823562622},
	{x = 312.61108398438,y = -587.783203125,z = 43.291839599609},
	{x = 313.65417480469,y = -588.06079101563,z = 43.291839599609},
	{x = 322.3258972168,y = -600.22906494141,z = 43.291790008545},
	{x = 321.13470458984,y = -599.73968505859,z = 43.291790008545}
	
}
------------------------------------------------------------------------------------------MACCHINETTE------------------------------------------------------------------------------------------------------------------------------

Citizen.CreateThread(function()
    while true do
        Citizen.Wait(0)
        for k in pairs(macchinette) do
            DrawMarker(21, macchinette[k].x, macchinette[k].y, macchinette[k].z, 0, 0, 0, 0, 0, 0, 0.301, 0.301, 0.3001, 0, 255, 50, 200, 0, 0, 0, 0)
        end
    end
end)


Citizen.CreateThread(function()
    while true do
        Citizen.Wait(0)

        for k in pairs(macchinette) do

            local plyCoords = GetEntityCoords(GetPlayerPed(-1), false)
            local dist = Vdist(plyCoords.x, plyCoords.y, plyCoords.z, macchinette[k].x, macchinette[k].y, macchinette[k].z)
	        local ped = GetPlayerPed(-1)

            if dist <= 0.5 then
				DisplayNotification('~s~ Premi ~INPUT_CONTEXT~ per comprare dell\'~g~ acqua~s~ e un ~g~ sandwich ~s~ | Prezzo ~g~ 10$ ')
				
				if IsControlJustPressed(0, Keys['E']) then
						FreezeEntityPosition(ped, true)		
						TriggerServerEvent('cibo')
						vRP.notify("Comprando..")
			         	TriggerEvent('InteractSound_CL:PlayOnOne', 'SodaMachine', 1.0)	
						Citizen.Wait(3000)								
						local playerPed = GetPlayerPed(-1)
			        	RequestAnimDict('amb@medic@standing@kneel@base')  
				        while not HasAnimDictLoaded('amb@medic@standing@kneel@base') do
					    Citizen.Wait(0)
				        end
				        TaskPlayAnim(playerPed, 'amb@medic@standing@kneel@base', 'base', 3.0, 3.0, 2000, 0, 1, true, true, true)
						Citizen.Wait(700)
						ClearPedTasksImmediately(playerPed)
						vRP.notify("Aspetta ~r~60 secondi ~w~prima di fare altri esercizi.")	
						FreezeEntityPosition(ped, false)								
					end
				end			
           end
       end
   end)
------------------------------------------------------------------------------------------MACCHINETTE------------------------------------------------------------------------------------------------------------------------------





