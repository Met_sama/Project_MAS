MySQL = module("vrp_mysql", "MySQL")
local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")
local htmlEntities = module("vrp", "lib/htmlEntities")

vRPhv = {}
vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP","vrp_caveau")
HVclient = Tunnel.getInterface("vrp_caveau","vrp_caveau")
Tunnel.bindInterface("vrp_caveau",vRPhv)

VaultBancheServer = {
  ["banca_grande_01"] = {stato = false,or_angolo = 160.0,angolo = 360.0,time = 20,hash = 961976194,x = 253.730,y = 225.270,z = 101.875,px = 253.1919,py = 228.455,pz = 101.683},
  ["banca_grande_02"] = {stato = false,or_angolo = 250.0,angolo = 150.0,time = 20,hash = 746855201,x = 261.876,y = 221.831,z = 106.284,px = 261.607,py = 223.232,pz = 106.284},
  ---
  ["banca_piccola_01"] = {stato = false,or_angolo = 357.0,angolo = 265.0,time = 20,hash = -63539571,x = -2957.66,y = 481.937,z = 15.697,px =-2956.579,py = 481.756,pz = 15.697},
  --["banca_grande_03"] =  {stato = false,or_angolo = 0.0,angolo = 150.0,time = 20,hash = -1185205679,x = -105.041,y = 6472.76,z = 31.626,px = -105.579,py = 6471.955,pz =31.626},
  ["banca_piccola_03"] = {stato = false,or_angolo = 250.0,angolo = 150.0,time = 20,hash = 2121050683,x = 147.522,y = -1045.002,z = 29.368,px = 146.799,py = -1046.074,pz = 29.3680},
  ["banca_piccola_04"] = {stato = false,or_angolo = 250.0,angolo = 150.0,time = 20,hash = 2121050683,x = -353.220,y = -54.200,z = 49.036,px = -353.831,py = -55.316,pz = 49.036},
  ["banca_piccola_05"] = {stato = false,or_angolo = 297.0,angolo = 150.0,time = 20,hash = 2121050683,x = -1211.243,y = -335.375,z = 37.780,px = -1210.738,py = -336.556,pz = 37.781},
  ["banca_piccola_06"] = {stato = false,or_angolo = 250.0,angolo = 150.0,time = 20,hash = 2121050683,x = 311.817,y = -283.357,z = 54.164,px = 311.073,py = -284.406,pz = 54.164}
}
function vRPhv.AggiornaListaServer(lista)
  VaultBancheServer = lista
  HVclient.AggiornaListaClient(-1,{lista})
end

function vRPhv.ChiudiVaultServer(i)
  HVclient.ChiudiVaultClient(-1,{i})
end

function vRPhv.AllertaPolizia(x,y,z)
  vRP.sendServiceAlert({nil,"LSPD-Capitano",x,y,z,"Tentativo apertura caveau"})
end

AddEventHandler("vRP:playerSpawn", function(user_id, source, first_spawn)
  if first_spawn then
    SetTimeout(5000,function()
      HVclient.AggiornaListaClient(source,{VaultBancheServer})
    end)
  end
end)

function vRPhv.ControllaSePermesso()
  local user_id = vRP.getUserId({source})
  if user_id ~= nil then
    if vRP.hasPermission({user_id,"police.fine"}) then
      return true
    else
      vRPclient.notify(source,{"~r~Non permesso!"})
    end
  end
  return false
end