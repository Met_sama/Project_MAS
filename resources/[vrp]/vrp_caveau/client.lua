vRPhv = {}
Tunnel.bindInterface("vrp_caveau",vRPhv)
vRPserver = Tunnel.getInterface("vRP","vrp_caveau")
HVserver = Tunnel.getInterface("vrp_caveau","vrp_caveau")
vRP = Proxy.getInterface("vRP")

VaultBancheClient = {
    ["banca_grande_01"] = {stato = false,or_angolo = 160.0,angolo = 360.0,time = 20,hash = 961976194,x = 253.730,y = 225.270,z = 101.875,px = 253.1919,py = 228.455,pz = 101.683},
    ["banca_grande_02"] = {stato = false,or_angolo = 250.0,angolo = 150.0,time = 20,hash = 746855201,x = 261.876,y = 221.831,z = 106.284,px = 261.607,py = 223.232,pz = 106.284},
    ---
    ["banca_piccola_01"] = {stato = false,or_angolo = 357.0,angolo = 265.0,time = 20,hash = -63539571,x = -2957.66,y = 481.937,z = 15.697,px =-2956.579,py = 481.756,pz = 15.697},
    --["banca_grande_03"] =  {stato = false,or_angolo = 0.0,angolo = 150.0,time = 20,hash = -1185205679,x = -105.041,y = 6472.76,z = 31.626,px = -105.579,py = 6471.955,pz =31.626},
    ["banca_piccola_03"] = {stato = false,or_angolo = 250.0,angolo = 150.0,time = 20,hash = 2121050683,x = 147.522,y = -1045.002,z = 29.368,px = 146.799,py = -1046.074,pz = 29.3680},
    ["banca_piccola_04"] = {stato = false,or_angolo = 250.0,angolo = 150.0,time = 20,hash = 2121050683,x = -353.220,y = -54.200,z = 49.036,px = -353.831,py = -55.316,pz = 49.036},
    ["banca_piccola_05"] = {stato = false,or_angolo = 297.0,angolo = 150.0,time = 20,hash = 2121050683,x = -1211.243,y = -335.375,z = 37.780,px = -1210.738,py = -336.556,pz = 37.781},
    ["banca_piccola_06"] = {stato = false,or_angolo = 250.0,angolo = 150.0,time = 20,hash = 2121050683,x = 311.817,y = -283.357,z = 54.164,px = 311.073,py = -284.406,pz = 54.164}
}
selectVault = nil

Citizen.CreateThread(function()
	while true do
    local playerPos = GetEntityCoords(GetPlayerPed(PlayerId(), true))
        for i,dati in pairs(VaultBancheClient) do
            if dati.stato then
                if (Vdist(playerPos.x, playerPos.y, playerPos.z, dati.x,dati.y,dati.z) < 25.0) then
                    local VaultDoor = GetClosestObjectOfType(dati.x,dati.y,dati.z, 25.0, dati.hash, 0, 0, 0)
                    if GetEntityHeading(VaultDoor) ~= dati.angolo then
                        SetEntityHeading(VaultDoor,dati.angolo)
                        FreezeEntityPosition(VaultDoor, true)
                    end
                end
            end
        end
	  Citizen.Wait(0)
    end
end)

Citizen.CreateThread(function()
	while true do
    local playerPos = GetEntityCoords(GetPlayerPed(PlayerId(), true))
        for i,dati in pairs(VaultBancheClient) do
            if not dati.stato then
                if (Vdist(playerPos.x, playerPos.y, playerPos.z, dati.x,dati.y,dati.z) < 25.0) then
                    local VaultDoor = GetClosestObjectOfType(dati.x,dati.y,dati.z, 25.0, dati.hash, 0, 0, 0)
                    FreezeEntityPosition(VaultDoor, true)
                end
            end
        end
	  Citizen.Wait(0)
    end
end)

function mycb(success, timeremaining)
	if success then
        vRP.notify({'Successo! '..timeremaining..'secondi rimasti.'})
        vRPhv.ApriVault(selectVault)
		TriggerEvent('mhacking:hide')
	else
        vRP.notify({'Fallito!'})
        local porta = VaultBancheClient[selectVault]
        HVserver.AllertaPolizia({porta.x,porta.y,porta.z})
		TriggerEvent('mhacking:hide')
	end
end

function vRPhv.ApriVault(i)
    local porta = VaultBancheClient[i]
    porta.stato = true
    HVserver.AggiornaListaServer({VaultBancheClient})
end

function vRPhv.ChiudiVault(i)
    local porta = VaultBancheClient[i]
    if porta.stato then
        porta.stato = false
        HVserver.AggiornaListaServer({VaultBancheClient})
        Citizen.Wait(1000)
        HVserver.ChiudiVaultServer({i})
    else
        vRP.notify({"~r~Caveau già chiuso!"})
    end
end

function vRPhv.ChiudiVaultClient(i)
    local porta = VaultBancheClient[i]
    local VaultDoor = GetClosestObjectOfType(porta.x,porta.y,porta.z, 25.0, porta.hash, 0, 0, 0)
    SetEntityHeading(VaultDoor,porta.or_angolo)
end

function vRPhv.AggiornaListaClient(lista)
    VaultBancheClient = lista
end

Citizen.CreateThread(function()
	while true do
    local playerPos = GetEntityCoords(GetPlayerPed(PlayerId(), true))
        for i,dati in pairs(VaultBancheClient) do
            if dati.stato then
                --DrawMarker(1, dati.px, dati.py, dati.pz-1, 0, 0, 0, 0, 0, 0, 0.7,0.7,0.6, 0,100,0, 255, 0, 0, 2, 0, 0, 0, 0)
                if (Vdist(playerPos.x, playerPos.y, playerPos.z, dati.px,dati.py,dati.pz) < 1.0) then
                    DisplayNotification("Premi ~INPUT_PICKUP~ per ~b~chiudere il caveau")
                    if IsControlJustReleased(0, 38) then
                        HVserver.ControllaSePermesso({},function(perm)
                            if perm then
                                vRPhv.ChiudiVault(i)
                            end
                        end)
                    end
                end
            end
        end
	  Citizen.Wait(0)
    end
end)

Citizen.CreateThread(function()
	while true do
    local playerPos = GetEntityCoords(GetPlayerPed(PlayerId(), true))
        for i,dati in pairs(VaultBancheClient) do
            if not dati.stato then
                --DrawMarker(1, dati.px, dati.py, dati.pz-1, 0, 0, 0, 0, 0, 0, 0.7,0.7,0.6, 0,100,0, 255, 0, 0, 2, 0, 0, 0, 0)
                if (Vdist(playerPos.x, playerPos.y, playerPos.z, dati.px,dati.py,dati.pz) < 1.0) then
                    DisplayNotification("Premi ~INPUT_DETONATE~ per ~b~hackerare")
                    if IsControlJustReleased(0, 47) then
                        selectVault = i
                        TriggerEvent("mhacking:show")
                        TriggerEvent("mhacking:start",7,dati.time,mycb)
                    end
                end
            end
        end
	  Citizen.Wait(0)
    end
end)

function DisplayNotification(string)
	SetTextComponentFormat("STRING")
	AddTextComponentString(string)
    DisplayHelpTextFromStringLabel(0, 0, 1, -1)
end