Tunnel = module("vrp", "lib/Tunnel")
Proxy = module("vrp", "lib/Proxy")

vRP = Proxy.getInterface("vRP")

vRPclient = Tunnel.getInterface("vRP", "vrp_pets_server")

local pets = {
	["cane"] = {
		name = "cane",
		price = 9000
	},
	["gatto"] = {
		name = "gatto",
		price = 7500
	},
	["lupo"] = {
		name = "lupo",
		price = 12000
	},
	["coniglio"] = {
		name = "coniglio",
		price = 4000
	},
	["husky"] = {
		name = "Husky",
		price = 15000
	},
	["maiale"] = {
		name = "maiale",
		price = 15000
	},
	["barboncino"] = {
		name = "barboncino",
		price = 11000
	},
	["carlino"] = {
		name = "carlino",
		price = 14000
	},
	["retriever"] = {
		name = "retriever",
		price = 16000
	},
	["pastore"] = {
		name = "pastore",
		price = 15000
	},
	["terrier"] = {
		name = "terrier",
		price = 14000
	}
}

RegisterServerEvent("vrp_pets:animalname")
AddEventHandler("vrp_pets:animalname", function (source, method)
	local _source = source
	local player = vRP.getUserId({_source})
	MySQL.Async.fetchAll('SELECT * FROM vrp_users WHERE id = @id', {['@id'] = player}, function(result)
		if method == "callPet" then
			TriggerClientEvent("vrp_pets:callPet", _source, result[1].pet)
		elseif method == "orders" then
			TriggerClientEvent("vrp_pets:orders", _source, result[1].pet)
		end
	end)
end)

RegisterServerEvent("vrp_pets:dead")
AddEventHandler("vrp_pets:dead", function()
	local _source = source
	local player = vRP.getUserId({_source})
	
	MySQL.Async.execute('UPDATE vrp_users SET pet = NULL WHERE id = @id', {['@id'] = player})
end)

RegisterServerEvent('vrp_pets:startHarvest')
AddEventHandler('vrp_pets:startHarvest', function()
	local _source = source
	local player = vRP.getUserId({_source})
	vRP.tryGetInventoryItem({player, 'croquettes', 1})
end)

RegisterServerEvent('vrp_pets:takeanimal')
AddEventHandler('vrp_pets:takeanimal', function (source, petid, price)
	local _source = source
	local player = vRP.getUserId({_source})
	local playerMoney = vRP.getMoney({player})

	if playerMoney >= price then
		vRP.tryPayment({player, price})
		MySQL.Async.execute('UPDATE vrp_users SET pet = @pet WHERE id = @id', {['@id'] = player, ['@pet'] = petid})
		vRPclient.notify(_source, {"Hai comprato un " .. pets[petid].name .. " perr " .. price})
	else
		vRPclient.notify(_source, {"~r~Non hai abbastanza soldi!"})
	end
end)

RegisterServerEvent("vrp_pets:buypet")
AddEventHandler("vrp_pets:buypet", function ()
	local _source = source
	local player = vRP.getUserId({_source})
	local menudata = {}
	
	menudata.name = "Negozio di animali"
	menudata.css = "align = 'top-left'"

	for k, v in pairs(pets) do
		menudata[v.name] = {function (choice)
			TriggerEvent("vrp_pets:takeanimal", _source, k, v.price)
			vRP.closeMenu({_source})
		end, "$" .. v.price}
	end
	
	vRP.openMenu({_source, menudata})
end)

RegisterServerEvent("vrp_pets:petMenu")
AddEventHandler("vrp_pets:petMenu", function (status, come, isInVehicle)
	local _source = source
	local player = vRP.getUserId({_source})
	local menudata = {}

	menudata.name = "Gestisci animali domestici"
	menudata.css = {align = 'top-left'}

	if come == 1 then
		menudata["Dai Crocchette"] = {function (choice)
			local data = vRP.getUserDataTable({player})
			TriggerClientEvent("vrp_pets:givefood", _source, data.inventory)
			vRP.closeMenu({_source})
		end, "Fame :" .. status .. "%"}
		menudata["Attacca o separa il tuo animale domestico"] = {function (choice)
			TriggerClientEvent("vrp_pets:attachdetach", _source)
			vRP.closeMenu({_source})
		end}

		if isInVehicle then
			menudata["Dì al tuo animale domestico di scendere dalla macchina"] = {function (choice)
				TriggerClientEvent("vrp_pets:enterleaveveh", _source)
				vRP.closeMenu({_source})
			end}
		else
			menudata["Dì al tuo animale domestico di salire in macchina"] = {function (choice)
				TriggerClientEvent("vrp_pets:enterleaveveh", _source)
				vRP.closeMenu({_source})
			end}
		end

		menudata["Dai un ordine"] = {function (choice)
			TriggerEvent("vrp_pets:animalname", _source, "orders")
			vRP.closeMenu({_source})
		end}
	else
		menudata["Chiama il tuo animale domestico"] = {function (choice)
			if come == 0 then
				TriggerEvent("vrp_pets:animalname", _source, "callPet")
				vRP.closeMenu({_source})
			end
		end}
	end

	vRP.openMenu({_source, menudata})
end)

RegisterServerEvent("vrp_pets:ordersMenu")
AddEventHandler("vrp_pets:ordersMenu", function (data, model, inanimation)
	local _source = source
	local player = vRP.getUserId({_source})
	local menudata = {}

	menudata.name = "Ordini Animali domestici"
	menudata.css = {align = 'top-left'}
	
	if not inanimation then
		if model ~= 1462895032 then
			menudata["Cerca la palla"] = {function (choice) -- balle
				TriggerClientEvent("vrp_pets:findball", _source)
				vRP.closeMenu({_source})
			end}
		end
		menudata["Seguimi"] = {function (choice) -- pied
			TriggerClientEvent("vrp_pets:followme", _source)
			vRP.closeMenu({_source})
		end}
		menudata["Di 'all'animale di entrare in casa"] = {function (choice) -- niche
			TriggerClientEvent("vrp_pets:goHome", _source)
			vRP.closeMenu({_source})
		end}
		
		if (data == "cane") then
			menudata["Sedersi"] = {function (choice) -- assis
				TriggerClientEvent("vrp_pets:seat", _source, 1)
				vRP.closeMenu({_source})
			end}
			menudata["Sdraiarsi"] = {function (choice) -- coucher
				TriggerClientEvent("vrp_pets:laydown", _source, 1)
				vRP.closeMenu({_source})
			end}
		end
		if (data == "gatto") then
			menudata["Sdraiarsi"] = {function (choice) -- coucher2
				TriggerClientEvent("vrp_pets:laydown", _source, 2)
				vRP.closeMenu({_source})
			end}
		end
		if (data == "lupo") then
			menudata["Sdraiarsi"] = {function (choice) -- coucher3
				TriggerClientEvent("vrp_pets:laydown", _source, 3)
				vRP.closeMenu({_source})
			end}
		end
		if (data == "carlino") then
			menudata["Sedersi"] = {function (choice) -- assis2
				TriggerClientEvent("vrp_pets:seat", _source, 2)
				vRP.closeMenu({_source})
			end}
		end
		if (data == "retriever") then
			menudata["Sedersi"] = {function (choice) -- assis3
				TriggerClientEvent("vrp_pets:seat", _source, 3)
				vRP.closeMenu({_source})
			end}
		end
	else
		menudata["Sollevare"] = {function (choice) -- debout
			TriggerClientEvent("vrp_pets:standup", _source)
			vRP.closeMenu({_source})
		end}
	end

	vRP.openMenu({_source, menudata})
end)