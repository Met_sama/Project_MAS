--[[
    FiveM Scripts
    Copyright C 2018  Sighmir

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    at your option any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
]]


vRP = Proxy.getInterface("vRP")
vRPg = Proxy.getInterface("vRP_garages")

function deleteVehiclePedIsIn()
  local v = GetVehiclePedIsIn(GetPlayerPed(-1),false)
  SetVehicleHasBeenOwnedByPlayer(v,false)
  Citizen.InvokeNative(0xAD738C3085FE7E11, v, false, true) -- set not as mission entity
  SetVehicleAsNoLongerNeeded(Citizen.PointerValueIntInitialized(v))
  Citizen.InvokeNative(0xEA386986E786A54F, Citizen.PointerValueIntInitialized(v))
end

local camionshop = {
	opened = false,
	title = "Shop Veicoli",
	currentmenu = "main",
	lastmenu = nil,
	currentpos = nil,
	selectedbutton = 0,
	marker = { r = 0, g = 155, b = 255, a = 200, type = 1 },
	menu = {
		x = 0.1,
		y = 0.08,
		width = 0.2,
		height = 0.04,
		buttons = 10,
		from = 1,
		to = 10,
		scale = 0.4,
		font = 0,
		["main"] = {
			title = "La Mano - Showroom",
			name = "main",
			buttons = {
				{name = "Veicoli", description = ""},
			--	{name = "Motociclette", description = ""},
			}
		},
		["vehicles"] = {
			title = "Veicoli",
			name = "Veicoli",
			buttons = {
				{name = "camion", description = ''},
		--{name = "coupes", description = ''},
		--{name = "sedans", description = ''},
		--{name = "sports", description = ''},
		--{name = "sportsclassics", description = ''},
		--{name = "supers", description = ''},
		--{name = "muscle", description = ''},
		--{name = "offroad", description = ''},
		--{name = "suvs", description = ''},
		--{name = "vans", description = ''},
				--{name = "cycles", description = ''},
			}
		},
		["camion"] = {
			title = "camion",
			name = "camion",
			buttons = {
				{name = "Burrito (125kg)", costs = 60000, description = {}, model = "gburrito2"},
				{name = "Paradise (150kg)", costs = 65000, description = {}, model = "paradise"},
				{name = "Rumpo(250kg)", costs = 80000, description = {}, model = "rumpo"},
				{name = "Surfer (100kg)", costs = 50000, description = {}, model = "surfer"},
				{name = "Youga(150kg)", costs = 65000, description = {}, model = "youga"},
				{name = "Mule(500kg)", costs = 220000, description = {}, model = "mule"},
      --	{name = "Mule 4(750kg)", costs = 275000, description = {}, model = "mule4"},				
				{name = "Benson(750kg)", costs = 310000, description = {}, model = "benson"},
				{name = "Bison 2(65kg)", costs = 28000, description = {}, model = "bison2"},
				{name = "Speedo(150kg)", costs = 60000, description = {}, model = "speedo"},
				{name = "Journey(25kg)", costs = 18000, description = {}, model = "journey"},
				{name = "Pounder(1000kg)", costs = 440000, description = {}, model = "pounder"},
				{name = "Pounder n.2 (2000kg)", costs = 700000, description = {}, model = "pounder2"},
				{name = "Camper(35kg)", costs = 25000, description = {}, model = "camper"},
				{name = "Burrito3(125kg)", costs = 55000, description = {}, model = "burrito3"},
				{name = "Range Rover Evoque(80kg)", costs = 570000, description = {}, model = "evoque"},
				{name = "Renault Kangoo(110kg)", costs = 63500, description = {}, model = "kangoo"},
				{name = "Porsche Cayenne(80kg)", costs = 420000, description = {}, model = "pct18"},
				{name = "2018 Ford F-250 XLT(90kg)", costs = 350000, description = {}, model = "18f250"},
				{name = "2018 Ford Velociraptor(90kg)", costs = 330000, description = {}, model = "raptor2017"},
				{name = "Renault Master(325kg)", costs = 168000, description = {}, model = "renmaster"},
				{name = "Alfa Romeo Stelvio(80kg)", costs = 560000, description = {}, model = "stelvio"},
				{name = "Vapid Scout(80kg)", costs = 45000, description = {}, model = "scout"},		
				{name = "Blindato", costs = 10000, description = {}, model = "riot"},		
				{name = "Blindato 2", costs = 10000, description = {}, model = "riot2"},		
				{name = "SUV Polizia", costs = 10000, description = {}, model = "pranger"},		
				{name = "Ambulanza", costs = 10000, description = {}, model = "ambulance"},		
				{name = "Furgone Polizia", costs = 10000, description = {}, model = "policet"},		
				{name = "Suv Polizia", costs = 10000, description = {}, model = "polscout"},
				{name = "Suv Sceriffo", costs = 10000, description = {}, model = "sherscout"},
												
				
				
				
			}
		},
		["coupes"] = {
			title = "coupes",
			name = "coupes",
			buttons = {
				{name = "Cognoscenti Cabrio", costs = 180000, description = {}, model = "cogcabrio"},
				{name = "Exemplar", costs = 200000, description = {}, model = "exemplar"},
				{name = "F620", costs = 80000, description = {}, model = "f620"},
				{name = "Felon", costs = 90000, description = {}, model = "felon"},
				{name = "Felon GT", costs = 95000, description = {}, model = "felon2"},
				{name = "Jackal", costs = 60000, description = {}, model = "jackal"},
				{name = "Oracle", costs = 80000, description = {}, model = "oracle"},
				{name = "Oracle XS", costs = 82000, description = {}, model = "oracle2"},
				{name = "Sentinel", costs = 90000, description = {}, model = "sentinel"},
				{name = "Sentinel XS", costs = 60000, description = {}, model = "sentinel2"},
				{name = "Windsor", costs = 800000, description = {}, model = "windsor"},
				{name = "Windsor Drop", costs = 850000, description = {}, model = "windsor2"},
				{name = "Zion", costs = 60000, description = {}, model = "zion"},
				{name = "Zion Cabrio", costs = 65000, description = {}, model = "zion2"},
			}
		},
		["sports"] = {
			title = "sports",
			name = "sports",
			buttons = {
				{name = "9F", costs = 120000, description = {}, model = "ninef"},
				{name = "9F Cabrio", costs = 130000, description = {}, model = "ninef2"},
				{name = "Alpha", costs = 150000, description = {}, model = "alpha"},
				{name = "Banshee", costs = 105000, description = {}, model = "banshee"},
				{name = "Bestia GTS", costs = 610000, description = {}, model = "bestiagts"},
				{name = "Blista Compact", costs = 42000, description = {}, model = "blista"},
				{name = "Buffalo", costs = 35000, description = {}, model = "buffalo"},
				{name = "Buffalo S", costs = 96000, description = {}, model = "buffalo2"},
				{name = "Carbonizzare", costs = 195000, description = {}, model = "carbonizzare"},
				{name = "Comet", costs = 100000, description = {}, model = "comet2"},
				{name = "Coquette", costs = 138000, description = {}, model = "coquette"},
				{name = "Drift Tampa", costs = 995000, description = {}, model = "tampa2"},
				{name = "Feltzer", costs = 130000, description = {}, model = "feltzer2"},
				{name = "Furore GT", costs = 448000, description = {}, model = "furoregt"},
				{name = "Fusilade", costs = 36000, description = {}, model = "fusilade"},
				{name = "Jester", costs = 240000, description = {}, model = "jester"},
				{name = "Jester(Racecar)", costs = 350000, description = {}, model = "jester2"},
				{name = "Kuruma", costs = 95000, description = {}, model = "kuruma"},
				{name = "Lynx", costs = 1735000, description = {}, model = "lynx"},
				{name = "Massacro", costs = 275000, description = {}, model = "massacro"},
				{name = "Massacro(Racecar)", costs = 385000, description = {}, model = "massacro2"},
				{name = "Omnis", costs = 701000, description = {}, model = "omnis"},
				{name = "Penumbra", costs = 24000, description = {}, model = "penumbra"},
				{name = "Rapid GT", costs = 140000, description = {}, model = "rapidgt"},
				{name = "Rapid GT Convertible", costs = 150000, description = {}, model = "rapidgt2"},
				{name = "Schafter V12", costs = 140000, description = {}, model = "schafter3"},
				{name = "Sultan", costs = 12000, description = {}, model = "sultan"},
				{name = "Surano", costs = 110000, description = {}, model = "surano"},
				{name = "Tropos", costs = 816000, description = {}, model = "tropos"},
				{name = "Verkierer", costs = 695000, description = {}, model = "verlierer2"},
			}
		},
		["sportsclassics"] = {
			title = "sportsclassics",
			name = "sportsclassics",
			buttons = {
				{name = "Casco", costs = 680000, description = {}, model = "casco"},
				{name = "Coquette Classic", costs = 665000, description = {}, model = "coquette2"},
				{name = "JB 700", costs = 350000, description = {}, model = "jb700"},
				{name = "Pigalle", costs = 400000, description = {}, model = "pigalle"},
				{name = "Stinger", costs = 850000, description = {}, model = "stinger"},
				{name = "Stinger GT", costs = 875000, description = {}, model = "stingergt"},
				{name = "Stirling GT", costs = 975000, description = {}, model = "feltzer3"},
				{name = "Z-Type", costs = 950000, description = {}, model = "ztype"},
			}
		},
		["supers"] = {
			title = "supers",
			name = "supers",
			buttons = {
				{name = "Adder", costs = 1000000, description = {}, model = "adder"},
				{name = "Banshee 900R", costs = 565000, description = {}, model = "banshee2"},
				{name = "Bullet", costs = 155000, description = {}, model = "bullet"},
				{name = "Cheetah", costs = 650000, description = {}, model = "cheetah"},
				{name = "Entity XF", costs = 795000, description = {}, model = "entityxf"},
				{name = "ETR1", costs = 199500, description = {}, model = "sheava"},
				{name = "FMJ", costs = 1750000, description = {}, model = "fmj"},
				{name = "Infernus", costs = 440000, description = {}, model = "infernus"},
				{name = "Osiris", costs = 1950000, description = {}, model = "osiris"},
				{name = "RE-7B", costs = 2475000, description = {}, model = "le7b"},
				{name = "Reaper", costs = 1595000, description = {}, model = "reaper"},
				{name = "Sultan RS", costs = 795000, description = {}, model = "sultanrs"},
				{name = "T20", costs = 2200000, description = {}, model = "t20"},
				{name = "Turismo R", costs = 500000, description = {}, model = "turismor"},
				{name = "Tyrus", costs = 2550000, description = {}, model = "tyrus"},
				{name = "Vacca", costs = 240000, description = {}, model = "vacca"},
				{name = "Voltic", costs = 150000, description = {}, model = "voltic"},
				{name = "X80 Proto", costs = 2700000, description = {}, model = "prototipo"},
				{name = "Zentorno", costs = 725000, description = {}, model = "zentorno"},
			}
		},
		["muscle"] = {
			title = "muscle",
			name = "muscle",
			buttons = {
				{name = "Blade", costs = 160000, description = {}, model = "blade"},
				{name = "Buccaneer", costs = 29000, description = {}, model = "buccaneer"},
				{name = "Chino", costs = 225000, description = {}, model = "chino"},
				{name = "Coquette BlackFin", costs = 695000, description = {}, model = "coquette3"},
				{name = "Dominator", costs = 35000, description = {}, model = "dominator"},
				{name = "Dukes", costs = 62000, description = {}, model = "dukes"},
				{name = "Gauntlet", costs = 32000, description = {}, model = "gauntlet"},
				{name = "Hotknife", costs = 90000, description = {}, model = "hotknife"},
				{name = "Faction", costs = 36000, description = {}, model = "faction"},
				{name = "Nightshade", costs = 585000, description = {}, model = "nightshade"},
				{name = "Picador", costs = 9000, description = {}, model = "picador"},
				{name = "Sabre Turbo", costs = 15000, description = {}, model = "sabregt"},
				{name = "Tampa", costs = 375000, description = {}, model = "tampa"},
				{name = "Virgo", costs = 195000, description = {}, model = "virgo"},
				{name = "Vigero", costs = 21000, description = {}, model = "vigero"},
			}
		},
		["offroad"] = {
			title = "offroad",
			name = "offroad",
			buttons = {
				{name = "Bifta", costs = 75000, description = {}, model = "bifta"},
				{name = "Blazer", costs = 8000, description = {}, model = "blazer"},
				{name = "Brawler", costs = 715000, description = {}, model = "brawler"},
				{name = "Bubsta 6x6", costs = 249000, description = {}, model = "dubsta3"},
				{name = "Dune Buggy", costs = 20000, description = {}, model = "dune"},
				{name = "Rebel", costs = 22000, description = {}, model = "rebel2"},
				{name = "Sandking", costs = 38000, description = {}, model = "sandking"},
				{name = "The Liberator", costs = 550000, description = {}, model = "monster"},
				{name = "Trophy Truck", costs = 550000, description = {}, model = "trophytruck"},
			}
		},
		["suvs"] = {
			title = "suvs",
			name = "suvs",
			buttons = {
				{name = "Baller", costs = 90000, description = {}, model = "baller"},
				{name = "Cavalcade", costs = 60000, description = {}, model = "cavalcade"},
				{name = "Grabger", costs = 35000, description = {}, model = "granger"},
				{name = "Huntley S", costs = 195000, description = {}, model = "huntley"},
				{name = "Landstalker", costs = 58000, description = {}, model = "landstalker"},
				{name = "Radius", costs = 32000, description = {}, model = "radi"},
				{name = "Rocoto", costs = 85000, description = {}, model = "rocoto"},
				{name = "Seminole", costs = 30000, description = {}, model = "seminole"},
				{name = "XLS", costs = 253000, description = {}, model = "xls"},
			}
		},
		["vans"] = {
			title = "vans",
			name = "vans",
			buttons = {
				{name = "Bison", costs = 30000, description = {}, model = "bison"},
				{name = "Bobcat XL", costs = 23000, description = {}, model = "bobcatxl"},
				{name = "Gang Burrito", costs = 65000, description = {}, model = "gburrito"},
				{name = "Journey", costs = 15000, description = {}, model = "journey"},
				{name = "Minivan", costs = 30000, description = {}, model = "minivan"},
				{name = "Paradise", costs = 25000, description = {}, model = "paradise"},
				{name = "Rumpo", costs = 13000, description = {}, model = "rumpo"},
				{name = "Surfer", costs = 11000, description = {}, model = "surfer"},
				{name = "Youga", costs = 16000, description = {}, model = "youga"},
			}
		},
		["sedans"] = {
			title = "sedans",
			name = "sedans",
			buttons = {
				{name = "Asea", costs = 1000000, description = {}, model = "asea"},
				{name = "Asterope", costs = 1000000, description = {}, model = "asterope"},
				{name = "Cognoscenti", costs = 1000000, description = {}, model = "cognoscenti"},
				{name = "Cognoscenti(Armored)", costs = 1000000, description = {}, model = "cognoscenti2"},
				{name = "Cognoscenti 55", costs = 1000000, description = {}, model = "cog55"},
				{name = "Cognoscenti 55(Armored", costs = 1500000, description = {}, model = "cog552"},
				{name = "Fugitive", costs = 24000, description = {}, model = "fugitive"},
				{name = "Glendale", costs = 200000, description = {}, model = "glendale"},
				{name = "Ingot", costs = 9000, description = {}, model = "ingot"},
				{name = "Intruder", costs = 16000, description = {}, model = "intruder"},
				{name = "Premier", costs = 10000, description = {}, model = "premier"},
				{name = "Primo", costs = 9000, description = {}, model = "primo"},
				{name = "Primo Custom", costs = 9500, description = {}, model = "primo2"},
				{name = "Regina", costs = 8000, description = {}, model = "regina"},
				{name = "Schafter", costs = 65000, description = {}, model = "schafter2"},
				{name = "Stanier", costs = 10000, description = {}, model = "stanier"},
				{name = "Stratum", costs = 10000, description = {}, model = "stratum"},
				{name = "Stretch", costs = 30000, description = {}, model = "stretch"},
				{name = "Super Diamond", costs = 250000, description = {}, model = "superd"},
				{name = "Surge", costs = 38000, description = {}, model = "surge"},
				{name = "Tailgater", costs = 55000, description = {}, model = "tailgater"},
				{name = "Warrener", costs = 120000, description = {}, model = "warrener"},
				{name = "Washington", costs = 15000, description = {}, model = "washington"},
			}
		},
		["motorcycles"] = {
			title = "Motociclette",
			name = "Motociclette",
			buttons = {
				{name = "Akuma", costs = 9000, description = {}, model = "AKUMA"},
				{name = "Bagger", costs = 5000, description = {}, model = "bagger"},
				{name = "Bati 801", costs = 15000, description = {}, model = "bati"},
				{name = "Bati 801RR", costs = 15000, description = {}, model = "bati2"},
				{name = "BF400", costs = 95000, description = {}, model = "bf400"},
				{name = "Carbon RS", costs = 40000, description = {}, model = "carbonrs"},
				{name = "Cliffhanger", costs = 225000, description = {}, model = "cliffhanger"},
				{name = "Daemon", costs = 5000, description = {}, model = "daemon"},
				{name = "Double T", costs = 12000, description = {}, model = "double"},
				{name = "Enduro", costs = 48000, description = {}, model = "enduro"},
				{name = "Faggio", costs = 4000, description = {}, model = "faggio2"},
				{name = "Gargoyle", costs = 120000, description = {}, model = "gargoyle"},
				{name = "Hakuchou", costs = 82000, description = {}, model = "hakuchou"},
				{name = "Hexer", costs = 15000, description = {}, model = "hexer"},
				{name = "Innovation", costs = 90000, description = {}, model = "innovation"},
				{name = "Lectro", costs = 700000, description = {}, model = "lectro"},
				{name = "Nemesis", costs = 12000, description = {}, model = "nemesis"},
				{name = "PCJ-600", costs = 9000, description = {}, model = "pcj"},
				{name = "Ruffian", costs = 9000, description = {}, model = "ruffian"},
				{name = "Sanchez", costs = 7000, description = {}, model = "sanchez"},
				{name = "Sovereign", costs = 90000, description = {}, model = "sovereign"},
				{name = "Thrust", costs = 75000, description = {}, model = "thrust"},
				{name = "Vader", costs = 9000, description = {}, model = "vader"},
				{name = "Vindicator", costs = 600000, description = {}, model = "vindicator"},
			}
		},
	}
}
local fakecar = {model = '', car = nil}
local camionshop_locations = {
{entering = {2072.27734375,3318.4875488281,47.58939743042}, inside = {2060.8110351563,3421.5434570313,44.41365814209}, outside = {2104.4177246094,3335.3447265625,45.547649383545}},
}

local camionshop_blips ={}
local inrangeofcamionshop = false
local currentlocation = nil
local boughtcar = false

function vehSR_drawTxt(text,font,centre,x,y,scale,r,g,b,a)
	SetTextFont(font)
	SetTextProportional(0)
	SetTextScale(scale, scale)
	SetTextColour(r, g, b, a)
	SetTextDropShadow(0, 0, 0, 0,255)
	SetTextEdge(1, 0, 0, 0, 255)
	SetTextDropShadow()
	SetTextOutline()
	SetTextCentre(centre)
	SetTextEntry("STRING")
	AddTextComponentString(text)
	DrawText(x , y)
end

function vehSR_IsPlayerInRangeOfcamionshop()
	return inrangeofcamionshop
end

local firstspawn = 0
AddEventHandler('playerSpawned', function(spawn)
if firstspawn == 0 then
	--326 car blip 227 225
	vehSR_ShowcamionshopBlips(true)
	firstspawn = 1
end
end)

function vehSR_ShowcamionshopBlips(bool)
	if bool and #camionshop_blips == 0 then
		for station,pos in pairs(camionshop_locations) do
			local loc = pos
			pos = pos.entering
			local blip = AddBlipForCoord(pos[1],pos[2],pos[3])
			-- 60 58 137
			SetBlipSprite(blip,632)
			BeginTextCommandSetBlipName("STRING")
			AddTextComponentString("Concessionario Camion")
			EndTextCommandSetBlipName(blip)
			SetBlipAsShortRange(blip,true)
			SetBlipAsMissionCreatorBlip(blip,true)
			table.insert(camionshop_blips, {blip = blip, pos = loc})
		end
		Citizen.CreateThread(function()
			while #camionshop_blips > 0 do
				Citizen.Wait(0)
				local inrange = false
				for i,b in ipairs(camionshop_blips) do
					if IsPlayerWantedLevelGreater(GetPlayerIndex(),0) == false and camionshop.opened == false and IsPedInAnyVehicle(vehSR_LocalPed(), true) == false and  GetDistanceBetweenCoords(b.pos.entering[1],b.pos.entering[2],b.pos.entering[3],GetEntityCoords(vehSR_LocalPed())) < 5 then
						DrawMarker(39,b.pos.entering[1],b.pos.entering[2],b.pos.entering[3]-0.2,0,0,0,0,0,0,0.5,0.3,0.5,0,155,255,150,0,true,0,true)
						vehSR_drawTxt("Premi ~g~ENTER~s~ per comprare ~b~il veicolo",0,1,0.5,0.8,0.6,255,255,255,255)
						currentlocation = b
						inrange = true
					end
				end
				inrangeofcamionshop = inrange
			end
		end)
	elseif bool == false and #camionshop_blips > 0 then
		for i,b in ipairs(camionshop_blips) do
			if DoesBlipExist(b.blip) then
				SetBlipAsMissionCreatorBlip(b.blip,false)
				Citizen.InvokeNative(0x86A652570E5F25DD, Citizen.PointerValueIntInitialized(b.blip))
			end
		end
		camionshop_blips = {}
	end
end

function vehSR_f(n)
	return n + 0.0001
end

function vehSR_LocalPed()
	return GetPlayerPed(-1)
end

function vehSR_try(f, catch_f)
	local status, exception = pcall(f)
	if not status then
		catch_f(exception)
	end
end
function vehSR_firstToUpper(str)
    return (str:gsub("^%l", string.upper))
end
--local veh = nil
function vehSR_OpenCreator()
	boughtcar = false
	local ped = vehSR_LocalPed()
	local pos = currentlocation.pos.inside
	FreezeEntityPosition(ped,true)
	SetEntityVisible(ped,false)
	local g = Citizen.InvokeNative(0xC906A7DAB05C8D2B,pos[1],pos[2],pos[3],Citizen.PointerValueFloat(),0)
	SetEntityCoords(ped,pos[1],pos[2],g)
	SetEntityHeading(ped,pos[4])
	camionshop.currentmenu = "main"
	camionshop.opened = true
	camionshop.selectedbutton = 0
end
local vehicle_price = 0
function vehSR_CloseCreator(vehicle,veh_type)
	Citizen.CreateThread(function()
		local ped = vehSR_LocalPed()
		if not boughtcar then
			local pos = currentlocation.pos.entering
			SetEntityCoords(ped,pos[1],pos[2],pos[3])
			FreezeEntityPosition(ped,false)
			SetEntityVisible(ped,true)
		else
			deleteVehiclePedIsIn()
			vRP.teleport({2095.9428710938,3314.3498535156,45.090263366699})
			vRPg.spawnBoughtVehicle({veh_type, vehicle})
			SetEntityVisible(ped,true)
			FreezeEntityPosition(ped,false)
		end
		camionshop.opened = false
		camionshop.menu.from = 1
		camionshop.menu.to = 10
	end)
end

function vehSR_drawMenuButton(button,x,y,selected)
	local menu = camionshop.menu
	SetTextFont(menu.font)
	SetTextProportional(0)
	SetTextScale(menu.scale, menu.scale)
	if selected then
		SetTextColour(0, 0, 0, 255)
	else
		SetTextColour(255, 255, 255, 255)
	end
	SetTextCentre(0)
	SetTextEntry("STRING")
	AddTextComponentString(button.name)
	if selected then
		DrawRect(x,y,menu.width,menu.height,255,255,255,255)
	else
		DrawRect(x,y,menu.width,menu.height,0,0,0,150)
	end
	DrawText(x - menu.width/2 + 0.005, y - menu.height/2 + 0.0028)
end


function vehSR_drawMenuTitle(txt,x,y)
local menu = camionshop.menu
	SetTextFont(2)
	SetTextProportional(0)
	SetTextScale(0.5, 0.5)
	SetTextColour(255, 255, 255, 255)
	SetTextEntry("STRING")
	AddTextComponentString(txt)
	DrawRect(x,y,menu.width,menu.height,0,0,0,150)
	DrawText(x - menu.width/2 + 0.005, y - menu.height/2 + 0.0028)
end
function vehSR_tablelength(T)
  local count = 0
  for _ in pairs(T) do count = count + 1 end
  return count
end
function vehSR_Notify(text)
SetNotificationTextEntry('STRING')
AddTextComponentString(text)
DrawNotification(false, false)
end

function vehSR_drawMenuRight(txt,x,y,selected)
	local menu = camionshop.menu
	SetTextFont(menu.font)
	SetTextProportional(0)
	SetTextScale(menu.scale, menu.scale)
	if selected then
		SetTextColour(0, 0, 0, 255)
	else
		SetTextColour(255, 255, 255, 255)
	end
	SetTextCentre(0)
	SetTextEntry("STRING")
	AddTextComponentString(txt)
	DrawText(x + menu.width/2 - 0.06, y - menu.height/2 + 0.0028)
end
local backlock = false
Citizen.CreateThread(function()
	local last_dir
	while true do
		Citizen.Wait(0)
		if IsControlJustPressed(1,201) and vehSR_IsPlayerInRangeOfcamionshop() then
			if camionshop.opened then
				vehSR_CloseCreator("","")
			else
				vehSR_OpenCreator()
			end
		end
		if camionshop.opened then
			local ped = vehSR_LocalPed()
			local menu = camionshop.menu[camionshop.currentmenu]
			vehSR_drawTxt(camionshop.title,1,1,camionshop.menu.x,camionshop.menu.y,1.0, 255,255,255,255)
			vehSR_drawMenuTitle(menu.title, camionshop.menu.x,camionshop.menu.y + 0.08)
			vehSR_drawTxt(camionshop.selectedbutton.."/"..vehSR_tablelength(menu.buttons),0,0,camionshop.menu.x + camionshop.menu.width/2 - 0.0385,camionshop.menu.y + 0.067,0.4, 255,255,255,255)
			local y = camionshop.menu.y + 0.12
			buttoncount = vehSR_tablelength(menu.buttons)
			local selected = false

			for i,button in pairs(menu.buttons) do
				if i >= camionshop.menu.from and i <= camionshop.menu.to then

					if i == camionshop.selectedbutton then
						selected = true
					else
						selected = false
					end
					vehSR_drawMenuButton(button,camionshop.menu.x,y,selected)
					if button.costs ~= nil then
						if camionshop.currentmenu == "camion" or camionshop.currentmenu == "coupes" or camionshop.currentmenu == "sedans" or camionshop.currentmenu == "sports" or camionshop.currentmenu == "sportsclassics" or camionshop.currentmenu == "supers" or camionshop.currentmenu == "muscle" or camionshop.currentmenu == "offroad" or camionshop.currentmenu == "suvs" or camionshop.currentmenu == "vans" or camionshop.currentmenu == "industrial" or camionshop.currentmenu == "cycles" or camionshop.currentmenu == "motorcycles" then
							vehSR_drawMenuRight(button.costs.."$",camionshop.menu.x,y,selected)
						else
							vehSR_drawMenuButton(button,camionshop.menu.x,y,selected)
						end
					end
					y = y + 0.04
					if camionshop.currentmenu == "camion" or camionshop.currentmenu == "coupes" or camionshop.currentmenu == "sedans" or camionshop.currentmenu == "sports" or camionshop.currentmenu == "sportsclassics" or camionshop.currentmenu == "supers" or camionshop.currentmenu == "muscle" or camionshop.currentmenu == "offroad" or camionshop.currentmenu == "suvs" or camionshop.currentmenu == "vans" or camionshop.currentmenu == "industrial" or camionshop.currentmenu == "cycles" or camionshop.currentmenu == "motorcycles" then
						if selected then
							if fakecar.model ~= button.model then
								if DoesEntityExist(fakecar.car) then
									Citizen.InvokeNative(0xEA386986E786A54F, Citizen.PointerValueIntInitialized(fakecar.car))
								end
								local pos = currentlocation.pos.inside
								local hash = GetHashKey(button.model)
								RequestModel(hash)
								local timer = 0
								while not HasModelLoaded(hash) and timer < 255 do
									Citizen.Wait(1)
									vehSR_drawTxt("Loading...",0,1,0.5,0.5,1.5,255,255-timer,255-timer,255)
									RequestModel(hash)
									timer = timer + 1
								end
								if timer < 255 then
									local veh = CreateVehicle(hash,pos[1],pos[2],pos[3],pos[4],false,false)
									while not DoesEntityExist(veh) do
										Citizen.Wait(1)
										vehSR_drawTxt("Loading...",0,1,0.5,0.5,1.5,255,255-timer,255-timer,255)
									end
									FreezeEntityPosition(veh,true)
									SetEntityInvincible(veh,true)
									SetVehicleDoorsLocked(veh,4)
									--SetEntityCollision(veh,false,false)
									TaskWarpPedIntoVehicle(vehSR_LocalPed(),veh,-1)
									for i = 0,24 do
										SetVehicleModKit(veh,0)
										RemoveVehicleMod(veh,i)
									end
									fakecar = { model = button.model, car = veh}
								else
									timer = 0
									while timer < 50 do
										Citizen.Wait(1)
										vehSR_drawTxt("Failed!",0,1,0.5,0.5,1.5,255,0,0,255)
										timer = timer + 1
									end
									if last_dir then
										if camionshop.selectedbutton < buttoncount then
											camionshop.selectedbutton = camionshop.selectedbutton +1
											if buttoncount > 10 and camionshop.selectedbutton > camionshop.menu.to then
												camionshop.menu.to = camionshop.menu.to + 1
												camionshop.menu.from = camionshop.menu.from + 1
											end
										else
											last_dir = false
											camionshop.selectedbutton = camionshop.selectedbutton -1
											if buttoncount > 10 and camionshop.selectedbutton < camionshop.menu.from then
												camionshop.menu.from = camionshop.menu.from -1
												camionshop.menu.to = camionshop.menu.to - 1
											end
										end
									else
										if camionshop.selectedbutton > 1 then
											camionshop.selectedbutton = camionshop.selectedbutton -1
											if buttoncount > 10 and camionshop.selectedbutton < camionshop.menu.from then
												camionshop.menu.from = camionshop.menu.from -1
												camionshop.menu.to = camionshop.menu.to - 1
											end
										else
											last_dir = true
											camionshop.selectedbutton = camionshop.selectedbutton +1
											if buttoncount > 10 and camionshop.selectedbutton > camionshop.menu.to then
												camionshop.menu.to = camionshop.menu.to + 1
												camionshop.menu.from = camionshop.menu.from + 1
											end
										end
									end
								end
							end
						end
					end
					if selected and IsControlJustPressed(1,201) then
						vehSR_ButtonSelected(button)
					end
				end
			end
			if IsControlJustPressed(1,202) then
				vehSR_Back()
			end
			if IsControlJustReleased(1,202) then
				backlock = false
			end
			if IsControlJustPressed(1,188) then
				last_dir = false
				if camionshop.selectedbutton > 1 then
					camionshop.selectedbutton = camionshop.selectedbutton -1
					if buttoncount > 10 and camionshop.selectedbutton < camionshop.menu.from then
						camionshop.menu.from = camionshop.menu.from -1
						camionshop.menu.to = camionshop.menu.to - 1
					end
				end
			end
			if IsControlJustPressed(1,187)then
				last_dir = true
				if camionshop.selectedbutton < buttoncount then
					camionshop.selectedbutton = camionshop.selectedbutton +1
					if buttoncount > 10 and camionshop.selectedbutton > camionshop.menu.to then
						camionshop.menu.to = camionshop.menu.to + 1
						camionshop.menu.from = camionshop.menu.from + 1
					end
				end
			end
		end

	end
end)


function vehSR_round(num, idp)
  if idp and idp>0 then
    local mult = 10^idp
    return math.floor(num * mult + 0.5) / mult
  end
  return math.floor(num + 0.5)
end
function vehSR_ButtonSelected(button)
	local ped = GetPlayerPed(-1)
	local this = camionshop.currentmenu
	local btn = button.name
	if this == "main" then
		if btn == "Veicoli" then
			vehSR_OpenMenu('vehicles')
		elseif btn == "Motociclette" then
			vehSR_OpenMenu('motorcycles')
		end
	elseif this == "vehicles" then
		if btn == "sports" then
			vehSR_OpenMenu('sports')
		elseif btn == "sedans" then
			vehSR_OpenMenu('sedans')
		elseif btn == "camion" then
			vehSR_OpenMenu('camion')
		elseif btn == "coupes" then
			vehSR_OpenMenu('coupes')
		elseif btn == "sportsclassics" then
			vehSR_OpenMenu("sportsclassics")
		elseif btn == "supers" then
			vehSR_OpenMenu('supers')
		elseif btn == "muscle" then
			vehSR_OpenMenu('muscle')
		elseif btn == "offroad" then
			vehSR_OpenMenu('offroad')
		elseif btn == "suvs" then
			vehSR_OpenMenu('suvs')
		elseif btn == "vans" then
			vehSR_OpenMenu('vans')
		end
	elseif this == "camion" or this == "coupes" or this == "sedans" or this == "sports" or this == "sportsclassics" or this == "supers" or this == "muscle" or this == "offroad" or this == "suvs" or this == "vans" or this == "industrial" then
		TriggerServerEvent('camion_SR:CheckMoneyForVeh',button.model,button.costs, "car")
    elseif this == "cycles" or this == "motorcycles" then
		TriggerServerEvent('camion_SR:CheckMoneyForVeh',button.model,button.costs, "bike")
	end
end

RegisterNetEvent('camion_SR:CloseMenu')
AddEventHandler('camion_SR:CloseMenu', function(vehicle, veh_type)
	boughtcar = true
	vehSR_CloseCreator(vehicle,veh_type)
end)

function vehSR_OpenMenu(menu)
	fakecar = {model = '', car = nil}
	camionshop.lastmenu = camionshop.currentmenu
	if menu == "vehicles" then
		camionshop.lastmenu = "main"
	elseif menu == "bikes"  then
		camionshop.lastmenu = "main"
	elseif menu == 'race_create_objects' then
		camionshop.lastmenu = "main"
	elseif menu == "race_create_objects_spawn" then
		camionshop.lastmenu = "race_create_objects"
	end
	camionshop.menu.from = 1
	camionshop.menu.to = 10
	camionshop.selectedbutton = 0
	camionshop.currentmenu = menu
end


function vehSR_Back()
	if backlock then
		return
	end
	backlock = true
	if camionshop.currentmenu == "main" then
		vehSR_CloseCreator("","")
	elseif camionshop.currentmenu == "camion" or camionshop.currentmenu == "coupes" or camionshop.currentmenu == "sedans" or camionshop.currentmenu == "sports" or camionshop.currentmenu == "sportsclassics" or camionshop.currentmenu == "supers" or camionshop.currentmenu == "muscle" or camionshop.currentmenu == "offroad" or camionshop.currentmenu == "suvs" or camionshop.currentmenu == "vans" or camionshop.currentmenu == "industrial" or camionshop.currentmenu == "cycles" or camionshop.currentmenu == "motorcycles" then
		if DoesEntityExist(fakecar.car) then
			Citizen.InvokeNative(0xEA386986E786A54F, Citizen.PointerValueIntInitialized(fakecar.car))
		end
		fakecar = {model = '', car = nil}
		vehSR_OpenMenu(camionshop.lastmenu)
	else
		vehSR_OpenMenu(camionshop.lastmenu)
	end

end

function vehSR_stringstarts(String,Start)
   return string.sub(String,1,string.len(Start))==Start
end