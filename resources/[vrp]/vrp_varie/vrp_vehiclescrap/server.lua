local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")

vRP = Proxy.getInterface("vRP")

local scrapprices = {
	{id = 0, price = 36}, --compacts
	{id = 1, price = 40}, --sedans
	{id = 2, price = 52}, --SUV's
	{id = 3, price = 64}, --coupes
	{id = 4, price = 50}, --muscle
	{id = 5, price = 65}, --sport classic
	{id = 6, price = 72}, --sport
	{id = 7, price = 110}, --super
	{id = 8, price = 22}, --motorcycle
	{id = 9, price = 38}, --offroad
	{id = 10, price = 44}, --industrial
	{id = 11, price = 34}, --utility
	{id = 12, price = 34}, --vans
	{id = 13, price = 40}, --bicycles
	{id = 14, price = 20}, --boats
	{id = 15, price = 82}, --helicopter
	{id = 16, price = 90}, --plane
	{id = 17, price = 29}, --service
	{id = 18, price = 50}, --emergency
	{id = 19, price = 62}, --military
	{id = 20, price = 34} --commercial
}



-- GROUPS
-- WHO HAVE ACCESS TO SCRAP VEHICLES
local groups = {"user"};


RegisterServerEvent("vRP_VehicleScrap:getVehPrice")
AddEventHandler("vRP_VehicleScrap:getVehPrice", function(class)
	for k, price in pairs(scrapprices) do
		if class == price.id then
			vehPrice = 55
			TriggerClientEvent("setVehPrice", -1, vehPrice)
		end
	end
end)

RegisterServerEvent("vRP_VehicleScrap:SellVehicle")
AddEventHandler("vRP_VehicleScrap:SellVehicle", function(vehPrice)
	local user_id = vRP.getUserId({source})
    vRP.giveBankMoney({user_id,vehPrice})
end)

RegisterServerEvent('vRP_VehicleScrap:Mechanic')
AddEventHandler('vRP_VehicleScrap:Mechanic', function(triggerevent)
	local source = source
    local user_id = vRP.getUserId({source})
    for k,v in ipairs(groups) do
		if vRP.hasGroup({user_id,v}) then
      		TriggerClientEvent(triggerevent, source)
    	else
     		TriggerClientEvent("pNotify:SendNotification", source,{text = "Non sei un meccanico", type = "error", queue = "global", timeout = 2000, layout = "centerRight",animation = {open = "gta_effects_fade_in", close = "gta_effects_fade_out"},killer = true})
     	end
    end
end)
