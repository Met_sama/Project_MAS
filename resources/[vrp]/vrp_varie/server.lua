-- Settings
local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")


vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP","vRP_varie")

Armi = {}

RegisterServerEvent("ritira_telefono")
AddEventHandler("ritira_telefono", function ()
  local user_id = vRP.getUserId({source})

  if vRP.hasGroup({user_id, "telefono"}) then
  vRPclient.notify(source,{"~r~Possiedi già il cellulare."})
  else
  vRP.addUserGroup({user_id, "telefono"})
  vRPclient.notify(source,{"~b~Hai ritirato il cellulare."})
  end
end)