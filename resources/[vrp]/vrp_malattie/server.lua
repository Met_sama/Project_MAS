MySQL = module("vrp_mysql", "MySQL")
local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")
local htmlEntities = module("vrp", "lib/htmlEntities")

vRPma = {}
vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP","vRP_malattie")
MAclient = Tunnel.getInterface("vRP_malattie","vRP_malattie")
Tunnel.bindInterface("vRP_malattie",vRPma)

local cfg = module("vrp_malattie", "cfg/medicine")

for k,v in pairs(cfg.medicine) do
	vRP.defInventoryItem({k,v.name,v.desc,v.choices,v.weight})
end

AddEventHandler("vRP:playerSpawn", function(user_id, source, first_spawn)
	local player = source
	SetTimeout(35000,function()
	  local custom = nil
	   vRP.getUData({user_id,"vRP:malattia",function(value)
		if value ~= nil then
		  custom = json.decode(value)
		  if custom ~= nil then
			MAclient.MalattiaClient(player,{custom,true})
		   else
			local val = math.random(1,25)
            if val == 2 then
				vRP.GiveRandomMalattia({user_id})
			end
		   end
		  else
			local val = math.random(1,25)
            if val == 21 then
				vRP.GiveRandomMalattia({user_id})
			end
		end
	 end})
  end)
end)


