local cfg = {}

cfg.medicine = {
	["raffreddorecura"] = {
    name = "Cura Raffreddore",
    desc = "Medicina che cura il raffreddore.",
    choices = function(args)
	  local menu = {}
      menu["Ingoia"] = {function(player,choice)
  	    local user_id = vRP.getUserId({player})
  	    if user_id ~= nil then
  	      if vRP.tryGetInventoryItem({user_id,"raffreddorecura",1,false}) then
			vRPclient.notify(player,{"~g~Ingoiando..."})
					
			local seq = {
				{"mp_player_inteat@burger", "mp_player_int_eat_burger_enter",1},
				{"mp_player_inteat@burger", "mp_player_int_eat_burger",1},
				{"mp_player_inteat@burger", "mp_player_int_eat_burger_fp",1},
				{"mp_player_inteat@burger", "mp_player_int_eat_exit_burger",1}
			}
					
			vRPclient.playAnim(player,{true,seq,false})
			 SetTimeout(10000,function()
				vRP.setUData({user_id,"vRP:malattia",json.encode()})
				MAclient.MalattiaClient(player,{"raffreddore",false})
			 end)
			vRPclient.notify(player,{"~g~Malattia in fase di cura"})
  		    vRP.closeMenu({player})
  		  end
  	    end
  	  end}
	  return menu
    end,
	 weight = 0.1
	},
	["febbrecura"] = {
    name = "Paracetamolo",
    desc = "Paracetamolo EG",
    choices = function(args)
	  local menu = {}
      menu["Ingoia"] = {function(player,choice)
  	    local user_id = vRP.getUserId({player})
  	    if user_id ~= nil then
  	      if vRP.tryGetInventoryItem({user_id,"febbrecura",1,false}) then
			vRPclient.notify(player,{"~g~Ingoiando qualcosa.."})
			  
			local seq = {
				{"mp_player_inteat@burger", "mp_player_int_eat_burger_enter",1},
				{"mp_player_inteat@burger", "mp_player_int_eat_burger",1},
				{"mp_player_inteat@burger", "mp_player_int_eat_burger_fp",1},
				{"mp_player_inteat@burger", "mp_player_int_eat_exit_burger",1}
			}
			vRPclient.playAnim(player,{true,seq,false})

			 SetTimeout(10000,function()
			  	vRP.setUData({user_id,"vRP:malattia",json.encode()})
				MAclient.MalattiaClient(player,{"febbre",false})
			 end)

			vRPclient.notify(player,{"~g~Malattia in fase di cura..."})
  		    vRP.closeMenu({player})
  		  end
  	    end
  	  end}
	  return menu
    end,
	 weight = 0.1
	},
	["raffreddorecura"] = {
		name = "Sciroppo della nonna",
		desc = "Usa questo per curare il raffreddore",
		choices = function(args)
		  local menu = {}
		  menu["Bevi"] = {function(player,choice)
			  local user_id = vRP.getUserId({player})
			  if user_id ~= nil then
				if vRP.tryGetInventoryItem({user_id,"raffreddorecura",1,false}) then
				vRPclient.notify(player,{"~g~Bevendo..."})
				  
				local seq = {
					{"mp_player_inteat@burger", "mp_player_int_eat_burger_enter",1},
					{"mp_player_inteat@burger", "mp_player_int_eat_burger",1},
					{"mp_player_inteat@burger", "mp_player_int_eat_burger_fp",1},
					{"mp_player_inteat@burger", "mp_player_int_eat_exit_burger",1}
				}
				vRPclient.playAnim(player,{true,seq,false})
	
				 SetTimeout(10000,function()
					  vRP.setUData({user_id,"vRP:malattia",json.encode()})
					MAclient.MalattiaClient(player,{"raffreddore",false})
				 end)
	
				vRPclient.notify(player,{"~g~Malattia curata..."})
				  vRP.closeMenu({player})
				end
			  end
			end}
		  return menu
		end,
	  weight = 0.1
	},
	["emicraneacura"] = {
		name = "Pastiglia 2.0",
		desc = "Usa questa pastiglia per curare l'emicranea",
		choices = function(args)
		  local menu = {}
		  menu["Bevi"] = {function(player,choice)
			  local user_id = vRP.getUserId({player})
			  if user_id ~= nil then
				if vRP.tryGetInventoryItem({user_id,"emicraneacura",1,false}) then
				vRPclient.notify(player,{"~g~Bevendo..."})
				  
				local seq = {
					{"mp_player_inteat@burger", "mp_player_int_eat_burger_enter",1},
					{"mp_player_inteat@burger", "mp_player_int_eat_burger",1},
					{"mp_player_inteat@burger", "mp_player_int_eat_burger_fp",1},
					{"mp_player_inteat@burger", "mp_player_int_eat_exit_burger",1}
				}
				vRPclient.playAnim(player,{true,seq,false})
	
				 SetTimeout(10000,function()
					  vRP.setUData({user_id,"vRP:malattia",json.encode()})
					MAclient.MalattiaClient(player,{"emicranea",false})
				 end)
	
				vRPclient.notify(player,{"~g~Malattia curata..."})
				  vRP.closeMenu({player})
				end
			  end
			end}
		  return menu
		end,
	  weight = 0.1
	},
	["hivcura"] = {
		name = "Cura HIV",
		desc = "Usa questo per curare l'HIV",
		choices = function(args)
		  local menu = {}
		  menu["Bevi"] = {function(player,choice)
			  local user_id = vRP.getUserId({player})
			  if user_id ~= nil then
				if vRP.tryGetInventoryItem({user_id,"hivcura",1,false}) then
				vRPclient.notify(player,{"~g~Bevendo..."})
				  
				local seq = {
					{"mp_player_inteat@burger", "mp_player_int_eat_burger_enter",1},
					{"mp_player_inteat@burger", "mp_player_int_eat_burger",1},
					{"mp_player_inteat@burger", "mp_player_int_eat_burger_fp",1},
					{"mp_player_inteat@burger", "mp_player_int_eat_exit_burger",1}
				}
				vRPclient.playAnim(player,{true,seq,false})
	
				 SetTimeout(10000,function()
					  vRP.setUData({user_id,"vRP:malattia",json.encode()})
					MAclient.MalattiaClient(player,{"hiv",false})
				 end)
	
				vRPclient.notify(player,{"~g~Malattia curata..."})
				  vRP.closeMenu({player})
				end
			  end
			end}
		  return menu
		end,
	  weight = 0.1
	}
}

return cfg