vRPma = {}
Tunnel.bindInterface("vRP_malattie",vRPma)
vRPserver = Tunnel.getInterface("vRP","vRP_malattie")
MAserver = Tunnel.getInterface("vRP_malattie","vRP_malattie")
vRP = Proxy.getInterface("vRP")

--Malattie--
raffreddore = false
febbre = false
raffreddore = false
emicranea = false
hiv = false
---Tempo---
min = 15
max = 30
---Altro---
emicraneasound = 0

function vRPma.MalattiaClient(malattia,flag)
	if malattia == "raffreddore" then
		raffreddore = flag 
	elseif malattia == "febbre" then
		febbre = flag
	elseif malattia == "raffreddore" then
		raffreddore = flag
	elseif malattia == "emicranea" then
		emicranea = flag
	elseif malattia == "hiv" then
		hiv = flag
	end 
end

Citizen.CreateThread(function()
  while true do
		if raffreddore then
			SetPedRagdollOnCollision(PlayerPedId(),true)
			SetPedToRagdoll(PlayerPedId())
		else
			SetPedRagdollOnCollision(PlayerPedId(),false)
	  end
		Citizen.Wait(math.random(min,max)*1000)
  end
end)

Citizen.CreateThread(function()
	while true do
	  if febbre then
		ShakeGameplayCam('HAND_SHAKE', 6.0)
		 Citizen.Wait(math.random(1,2)*1000)
		ShakeGameplayCam('SMALL_EXPLOSION_SHAKE', 0.1)
	  end
	  Citizen.Wait(math.random(min,max)*1000)
   end
end)

local Dict1 = "amb@code_human_wander_idles@female@idle_a"
local Name1 = "idle_b_sneeze"

Citizen.CreateThread(function()
  while true do
	if raffreddore then
			while not HasAnimDictLoaded(Dict1) do
				RequestAnimDict(Dict1)
				Citizen.Wait(100)
			end
		local Duration1 = GetAnimDuration(Dict1, Name1)
		TaskPlayAnim(GetPlayerPed(PlayerId()), 1.0, -1, -1, 50, 0, 0, 0, 0)
		TaskPlayAnim(GetPlayerPed(PlayerId()), Dict1, Name1, 1.0, -1, -1, 50, 0, 0, 0, 0)
			Citizen.Wait(Duration1*1000)
		ClearPedSecondaryTask(GetPlayerPed(PlayerId()))
		ClearPedTasks(GetPlayerPed(PlayerId()))
	  end
	 Citizen.Wait(math.random(min,max)*1000)
  end
end)

Citizen.CreateThread(function()
  while true do
		if emicranea then
			local effect = "Rampage"
			emicraneasound = 1000
			StartScreenEffect(effect, 0, true)
			 Citizen.Wait(3000)
			StopScreenEffect(effect)
		end
		Citizen.Wait(math.random(min,max)*1000)
	end
end)
Citizen.CreateThread(function()
  while true do
		if emicraneasound > 0 then
			emicraneasound = emicraneasound - 200
			PlaySound(-1, "Hit_1", "LONG_PLAYER_SWITCH_SOUNDS", 0, 0, 1)
		end
		Citizen.Wait(emicraneasound)
	end
end)

Citizen.CreateThread(function()
  while true do
    if hiv then
      DoScreenFadeOut(1000)
        while not IsScreenFadedOut() do
          Wait(0)
        end
      ragdoll = true
      Citizen.Wait(math.random(3,6)*1000)
        if IsScreenFadedOut() then
          DoScreenFadeIn(1000)
        end
      Citizen.Wait(2000)
      ragdoll = false
		end
		Citizen.Wait(math.random(min,max)*100000)
  end
end)

Citizen.CreateThread(function()
  while true do
    Citizen.Wait(0)
    if ragdoll then
      SetPedToRagdoll(PlayerPedId())
    end
  end
end)

RegisterNetEvent("vRPm:MalattiaClient")
AddEventHandler("vRPm:MalattiaClient", function(cura)
    vRRma.MalattiaClient(cura,true)
end)

RegisterNetEvent("vRPm:CuraClient")
AddEventHandler("vRPm:CuraClient", function(cura)
    vRRma.MalattiaClient(cura,false)
end)
