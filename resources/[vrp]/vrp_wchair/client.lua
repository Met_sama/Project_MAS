vRPwc = {}
Tunnel.bindInterface("vrp_wchair",vRPwc)
vRPserver = Tunnel.getInterface("vRP","vrp_wchair")
WCserver = Tunnel.getInterface("vrp_wchair","vrp_wchair")
vRP = Proxy.getInterface("vRP")

SpawnClientWC = {}

creata = false
inmano = false
seduto = false
att = false

function vRPwc.AggiornaListaWC(list)
  SpawnClientWC = list
  for i,data in pairs(SpawnClientWC) do
    vRPwc.SyncThreadWC(data.WCnet)
    Wait(2000)
  end
end

function vRPwc.SpawnWithData()
if not creata then
  creata = true
  RequestModel(GetHashKey("prop_wheelchair_01"))
    while not HasModelLoaded(GetHashKey("prop_wheelchair_01")) do
      Citizen.Wait(100)
    end

  local plyCoords = GetOffsetFromEntityInWorldCoords(GetPlayerPed(PlayerId()), 0.0, 0.0, 0.0)
  local wheelchair = CreateObject(GetHashKey("prop_wheelchair_01"), plyCoords.x, plyCoords.y, plyCoords.z, 1, 1, 1)
  PlaceObjectOnGroundProperly(wheelchair)
  local objnetid = ObjToNet(wheelchair)
  SetNetworkIdExistsOnAllMachines(objnetid, true)
  NetworkSetNetworkIdDynamic(objnetid, true)
  SetNetworkIdCanMigrate(objnetid, false)
  Citizen.Wait(2000)
  WCserver.SyncThreadWCServer({objnetid})
else
  DisplayNotification('Sedia già creata!')
end
end

function vRPwc.SyncClient(list)
  SpawnClientWC = list
end

function vRPwc.SyncThreadWC(netobj)

  local wheelchair = NetToObj(netobj)
  local dati = {WCnet = netobj, inmano = false, seduto = false}
  local serverdatainmano = false
  local serverdataseduto = false
  table.insert(SpawnClientWC, dati)
  
  Citizen.CreateThread(function()
    while true do
      local playerPos = GetEntityCoords(GetPlayerPed(-1), true)
      local pos = GetEntityCoords(wheelchair, true)

      for i,data in pairs(SpawnClientWC) do
        if data.WCnet == netobj then
          local serverdatainmano = data.inmano
          local serverdataseduto = data.seduto
        end
      end 

      if (Vdist(playerPos.x, playerPos.y, playerPos.z, pos.x,pos.y,pos.z) < 2.0) and not IsPedSittingInAnyVehicle(GetPlayerPed(PlayerId())) and not inmano and not seduto then
        if IsControlJustReleased(0, 38) and not IsPedSittingInAnyVehicle(GetPlayerPed(-1)) then
          for i,data in pairs(SpawnClientWC) do
            if data.WCnet == netobj then
              serverdatainmano = data.inmano
            end
          end 
          if not serverdatainmano then
            WCserver.SyncInManoServer({netobj,true})
            NetworkRequestControlOfEntity(wheelchair)
            while not NetworkHasControlOfEntity(wheelchair) do
              Citizen.Wait(1)
            end
            AttachEntityToEntity(wheelchair, GetPlayerPed(PlayerId()), GetPedBoneIndex(GetPlayerPed(PlayerId()), 18905), 0.76,-0.2,0.00999,-138.6,-250.99,-12.7, 1, 1, 0, 1, 0, 1)    
            
            inmano = true
            att = false
            Wait(500)
          end
        end
      end
      if inmano and not seduto then
        if IsControlJustReleased(0, 38) then
          inmano = false
          ClearPedSecondaryTask(GetPlayerPed(PlayerId()))
          DetachEntity(wheelchair)  
          WCserver.SyncInManoServer({netobj,false})      
        elseif IsControlJustReleased(0, 74) then
          local x,y,z,objsel = GetObjInFront(10)
          if objsel ~= 0 then
            inmano = false
            att = true
            ClearPedSecondaryTask(GetPlayerPed(PlayerId()))
            DetachEntity(wheelchair) 
            WCserver.SyncInManoServer({netobj,false})
            Wait(100)
            local Model = GetEntityModel(objsel)
            local d1,d2 = GetModelDimensions(Model)
            AttachEntityToEntity(wheelchair, objsel, 0, 0.0, d1["y"]-0.5, d1["z"]+0.63, 0.0, 0.0, 0.0, 1, 1, 1, 1, 1, 1)
            --AttachEntityToEntity(wheelchair, objsel, 0, 0.0,-3.5,0.0, 0.0,0.0,0.0, 1, 1, 0, 1, 0, 1)    
          end
        end
      ---------------------------------------------------------------
        if IsPedRagdoll(GetPlayerPed(PlayerId())) then
          inmano = false
          ClearPedSecondaryTask(GetPlayerPed(PlayerId()))
          DetachEntity(wheelchair)    
          WCserver.SyncInManoServer({netobj,false})
        end
        if IsPedSittingInAnyVehicle(GetPlayerPed(PlayerId())) then
          inmano = false
          ClearPedSecondaryTask(GetPlayerPed(PlayerId()))
          DetachEntity(wheelchair) 
          WCserver.SyncInManoServer({netobj,false})  
        end
      end

      if (Vdist(playerPos.x, playerPos.y, playerPos.z, pos.x,pos.y,pos.z) < 2.0) and not IsPedSittingInAnyVehicle(GetPlayerPed(PlayerId())) and not inmano and not seduto then
        DisplayNotification('Premi ~INPUT_DETONATE~ per sederti')
        if IsControlJustReleased(0, 47) and not IsPedSittingInAnyVehicle(GetPlayerPed(-1)) then
          for i,data in pairs(SpawnClientWC) do
            if data.WCnet == netobj then
              serverdataseduto = data.seduto
            end
          end 
          if not serverdataseduto then
            AttachEntityToEntity(GetPlayerPed(PlayerId()), wheelchair, 0, 0.0,0.043,0.02, 14.0,0.0,180.0, 1, 1, 0, 1, 0, 1)    
            seduto = true
            WCserver.SyncSedutoServer({netobj,true})
            Wait(500)
          end
        end
      end
      if IsControlJustReleased(0, 47) and seduto then
        seduto = false
        WCserver.SyncSedutoServer({netobj,false})
        ClearPedSecondaryTask(GetPlayerPed(PlayerId()))
        ClearPedTasks(GetPlayerPed(PlayerId()))
        AttachEntityToEntity(GetPlayerPed(PlayerId()), wheelchair, 0, 0.0,-0.3,0.02, 14.0,0.0,180.0, 1, 1, 0, 1, 0, 1)    
        DetachEntity(GetPlayerPed(PlayerId()))
      end
      Citizen.Wait(0)
    end
  end)

end

Direct = "anim@heists@box_carry@"
Nome = "idle"

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(0)
		if inmano then
			while not HasAnimDictLoaded(Direct) do
				RequestAnimDict(Direct)
				Citizen.Wait(100)
      end
      
			if not IsEntityPlayingAnim(PlayerPedId(), Direct, Nome, 3) then
        TaskPlayAnim(GetPlayerPed(PlayerId()), 1.0, -1, -1, 50, 0, 0, 0, 0) -- 50 = 32 + 16 + 2
        TaskPlayAnim(GetPlayerPed(PlayerId()), Direct, Nome, 1.0, -1, -1, 50, 0, 0, 0, 0)
			end

			DisablePlayerFiring(PlayerId(), true)
			DisableControlAction(0,25,true) -- disable aim
			DisableControlAction(0, 44,  true) -- INPUT_COVER
			DisableControlAction(0,37,true) -- INPUT_SELECT_WEAPON
			SetCurrentPedWeapon(GetPlayerPed(-1), GetHashKey("WEAPON_UNARMED"), true)
				
		end
	end
end)

Direct2 = "amb@prop_human_seat_chair@male@generic@idle_a"
Nome2 = "idle_c"

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(0)
		if seduto then
			while not HasAnimDictLoaded(Direct2) do
				RequestAnimDict(Direct2)
				Citizen.Wait(100)
      end
      
			if not IsEntityPlayingAnim(PlayerPedId(), Direct2, Nome2, 3) then
        TaskPlayAnim(GetPlayerPed(PlayerId()), 1.0, -1, -1, 1, 0, 0, 0, 0) -- 50 = 32 + 16 + 2
        TaskPlayAnim(GetPlayerPed(PlayerId()), Direct2, Nome2, 1.0, -1, -1, 1, 0, 0, 0, 0)
			end

			DisablePlayerFiring(PlayerId(), true)
			DisableControlAction(0,25,true) -- disable aim
			DisableControlAction(0, 44,  true) -- INPUT_COVER
			DisableControlAction(0,37,true) -- INPUT_SELECT_WEAPON
			SetCurrentPedWeapon(GetPlayerPed(-1), GetHashKey("WEAPON_UNARMED"), true)
				
		end
	end
end)


function DisplayNotification(string)
  SetTextComponentFormat("STRING")
  AddTextComponentString(string)
  DisplayHelpTextFromStringLabel(0, 0, 1, -1)
end

function DrawText2D(x,y,text,scale)
	SetTextColour(12, 50, 201, 255)
	SetTextFont(0)
	SetTextScale(scale, scale)
	SetTextWrap(0.0, 1.0)
	SetTextCentre(false)
	SetTextDropshadow(2, 2, 0, 0, 0)
	SetTextEdge(1, 255, 255, 255, 205)
	SetTextEntry("STRING")
	AddTextComponentString("~h~"..text)
	DrawText(x, y)
end

function GetObjInFront(flag)
  local plyCoords = GetEntityCoords(GetPlayerPed(PlayerId()), false)
  local plyOffset = GetOffsetFromEntityInWorldCoords(GetPlayerPed(PlayerId()), 0.0, 1.2, 0.0)

  local xn = plyOffset.x 
  local yn = plyOffset.y 
  local zn = plyOffset.z - 1

  local rayHandle = StartShapeTestCapsule(plyCoords.x, plyCoords.y, plyCoords.z+2, xn, yn, zn, 0.3, flag, GetPlayerPed(PlayerId()), 7)
  local _, _, coords1, _, veh = GetShapeTestResult(rayHandle)
  local x,y,z = table.unpack(coords1)
  return x,y,z,veh
end