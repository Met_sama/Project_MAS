MySQL = module("vrp_mysql", "MySQL")
local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")
local htmlEntities = module("vrp", "lib/htmlEntities")

vRPwc = {}
vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP","vrp_wchair")
WCclient = Tunnel.getInterface("vrp_wchair","vrp_wchair")
Tunnel.bindInterface("vrp_wchair",vRPwc)

SpawnWC = {}

function isAllowedToChange(player)
    if player == 0 then
        return true
    end
    local admins =  {
        "steam:1100001171eae66",   -- Stronght


    }
    local allowed = false
    for i,id in ipairs(admins) do
        for x,pid in ipairs(GetPlayerIdentifiers(player)) do
            --if debugprint then print('admin id: ' .. id .. '\nplayer id:' .. pid) end
            if string.lower(pid) == string.lower(id) then
                allowed = true
            end
        end
    end
    return allowed
end

RegisterCommand("swchair", function(source, args, raw)
    if isAllowedToChange(source) then
    	WCclient.SpawnWithData(source,{})
    end
end)

function vRPwc.SyncThreadWCServer(netobj)
	WCclient.SyncThreadWC(-1,{netobj})
	local dati = {WCnet = netobj, inmano = false, seduto = false}
    table.insert(SpawnWC, dati)
    WCclient.SyncClient(-1,{SpawnWC})
end

function vRPwc.SyncInManoServer(netobj,val)
    for i,data in pairs(SpawnWC) do
        if data.WCnet == netobj then
            data.inmano = val
            WCclient.SyncClient(-1,{SpawnWC})
        end
    end
end

function vRPwc.SyncSedutoServer(netobj,val)
    for i,data in pairs(SpawnWC) do
        if data.WCnet == netobj then
            data.seduto = val
            WCclient.SyncClient(-1,{SpawnWC})
        end
    end
end

AddEventHandler("vRP:playerSpawn",function(user_id, source, first_spawn)
	SetTimeout(18000, function()
		WCclient.AggiornaListaWC(source,{SpawnWC})
	end)
end)