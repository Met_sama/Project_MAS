--bind client tunnel interface
vRPbm = {}
Tunnel.bindInterface("vRP_basic_menu",vRPbm)
vRPserver = Tunnel.getInterface("vRP","vRP_basic_menu")
HKserver = Tunnel.getInterface("vrp_hotkeys","vRP_basic_menu")
BMserver = Tunnel.getInterface("vRP_basic_menu","vRP_basic_menu")
vRP = Proxy.getInterface("vRP")

trailer = nil
vehv1 = nil 
vehv2 = nil 
vehv3 = nil 
vehv4 = nil 
vehv5 = nil 
vehv6 = nil 


function vRPbm.TrailerSpawn()
    local mhash = GetHashKey("TR2")

	  while not HasModelLoaded(mhash) do
	    RequestModel(mhash)
	    Citizen.Wait(10)
      end
      
	local x,y,z = table.unpack(GetEntityCoords(PlayerPedId(),true))
	local car = CreateVehicle(mhash, x+5,y,z+0.5, 0.0, true, false)
	trailer = car
	  SetVehicleOnGroundProperly(car)
	  SetEntityInvincible(car,false)
end

function vRPbm.TrailerInsertSlot(slot)
    if slot == 1 then
      vehv1 = vRPbm.GetVehicleInDirection()
	  AttachEntityToEntity(vehv1, trailer, GetPedBoneIndex(vehv1, 28252), 0.0, 4.5, 3.0, 0.0, 0.0, 0.0, 1, 1, 0, 1, 0, 1)
    elseif slot == 2 then
      vehv2 = vRPbm.GetVehicleInDirection()
      AttachEntityToEntity(vehv2, trailer, GetPedBoneIndex(vehv2, 28252), 0.0, 0.0, 3.0, 0.0, 0.0, 0.0, 1, 1, 0, 1, 0, 1)
    elseif slot == 3 then
      vehv3 = vRPbm.GetVehicleInDirection()
	  AttachEntityToEntity(vehv3, trailer, GetPedBoneIndex(vehv3, 28252), 0.0, -4.5, 3.0, 0.0, 0.0, 0.0, 1, 1, 0, 1, 0, 1)
    elseif slot == 4 then
      vehv4 = vRPbm.GetVehicleInDirection()
	  AttachEntityToEntity(vehv4, trailer, GetPedBoneIndex(vehv4, 28252), 0.0, 4.5, 1.0, 0.0, 0.0, 0.0, 1, 1, 0, 1, 0, 1)  
    elseif slot == 5 then
      vehv5 = vRPbm.GetVehicleInDirection()
	  AttachEntityToEntity(vehv5, trailer, GetPedBoneIndex(vehv5, 28252), 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1, 1, 0, 1, 0, 1)
    elseif slot == 6 then
      vehv6 = vRPbm.GetVehicleInDirection()
      AttachEntityToEntity(vehv6, trailer, GetPedBoneIndex(vehv6, 28252), 0.0, -4.5, 1.0, 0.0, 0.0, 0.0, 1, 1, 0, 1, 0, 1)
    end
end

function vRPbm.TrailerRemoveSlot(slot)
    if slot == 1 then
      AttachEntityToEntity(vehv1, trailer, GetPedBoneIndex(vehv1, 28252), 0.0, 6.0, 3.0, 0.0, 0.0, 0.0, 1, 1, 0, 1, 0, 1)
      DetachEntity(vehv1, true, true)
      vehv1 = nil
    elseif slot == 2 then
      AttachEntityToEntity(vehv2, trailer, GetPedBoneIndex(vehv2, 28252), 0.0, 6.0, 3.0, 0.0, 0.0, 0.0, 1, 1, 0, 1, 0, 1)
      DetachEntity(vehv2, true, true)
      vehv2 = nil
    elseif slot == 3 then
      AttachEntityToEntity(vehv3, trailer, GetPedBoneIndex(vehv3, 28252), 0.0, 6.0, 3.0, 0.0, 0.0, 0.0, 1, 1, 0, 1, 0, 1)
      DetachEntity(vehv3, true, true)
      vehv3 = nil
    elseif slot == 4 then
      AttachEntityToEntity(vehv4, trailer, GetPedBoneIndex(vehv4, 28252), 0.0, 6.0, 1.0, 0.0, 0.0, 0.0, 1, 1, 0, 1, 0, 1)  
      DetachEntity(vehv4, true, true)
      vehv4 = nil
    elseif slot == 5 then
      AttachEntityToEntity(vehv5, trailer, GetPedBoneIndex(vehv5, 28252), 0.0, 6.0, 1.0, 0.0, 0.0, 0.0, 1, 1, 0, 1, 0, 1)
      DetachEntity(vehv5, true, true)
      vehv5 = nil
    elseif slot == 6 then
      AttachEntityToEntity(vehv6, trailer, GetPedBoneIndex(vehv6, 28252), 0.0, 6.0, 1.0, 0.0, 0.0, 0.0, 1, 1, 0, 1, 0, 1)
      DetachEntity(vehv6, true, true)
      vehv6 = nil
    end
end

function vRPbm.GetVehicleInDirection()
	local x,y,z = table.unpack(GetEntityCoords(PlayerPedId(),true))
    
	local vehv = GetClosestVehicle(x+0.0001,y+0.0001,z+0.0001, 30+0.0001, 0, 8192+4096+4+2+1)  -- boats, helicos
	  
	if not IsEntityAVehicle(vehv) then 
	  vehv = GetClosestVehicle(x+0.0001,y+0.0001,z+0.0001, 30+0.0001, 0, 4+2+1) -- cars
	end
return vehv
end


function vRPbm.lockpickVehicle(wait,any)
	Citizen.CreateThread(function()
		local pos = GetEntityCoords(GetPlayerPed(-1))
		local entityWorld = GetOffsetFromEntityInWorldCoords(GetPlayerPed(-1), 0.0, 20.0, 0.0)

		local rayHandle = CastRayPointToPoint(pos.x, pos.y, pos.z, entityWorld.x, entityWorld.y, entityWorld.z, 10, GetPlayerPed(-1), 0)
		local _, _, _, _, vehicleHandle = GetRaycastResult(rayHandle)
		if DoesEntityExist(vehicleHandle) then
		  if GetVehicleDoorsLockedForPlayer(vehicleHandle,PlayerId()) or any then
			local prevObj = GetClosestObjectOfType(pos.x, pos.y, pos.z, 10.0, GetHashKey("prop_weld_torch"), false, true, true)
			if(IsEntityAnObject(prevObj)) then
				SetEntityAsMissionEntity(prevObj)
				DeleteObject(prevObj)
			end
			StartVehicleAlarm(vehicleHandle)
			TaskStartScenarioInPlace(GetPlayerPed(-1), "WORLD_HUMAN_WELDING", 0, true)
			Citizen.Wait(wait*1000)
			SetVehicleDoorsLocked(vehicleHandle, 1)
			for i = 1,64 do 
				SetVehicleDoorsLockedForPlayer(vehicleHandle, GetPlayerFromServerId(i), false)
			end 
			ClearPedTasksImmediately(GetPlayerPed(-1))
			
			vRP.notify({"~g~Veicolo sbloccato."})
			
			-- ties to the hotkey lock system
			local plate = GetVehicleNumberPlateText(vehicleHandle)
			HKserver.lockSystemUpdate({1, plate})
			HKserver.playSoundWithinDistanceOfEntityForEveryone({vehicleHandle, 10, "unlock", 1.0})
		  else
			vRP.notify({"~g~Veicolo già sbloccato."})
		  end
		else
			vRP.notify({"~r~Troppo lontano dal veicolo."})
		end
	end)
end

function vRPbm.spawnVehicle(model) 
    -- load vehicle model
    local i = 0
    local mhash = GetHashKey(model)
    while not HasModelLoaded(mhash) and i < 1000 do
	  if math.fmod(i,100) == 0 then
	    vRP.notify({"~b~Caricamento modello."})
	  end
      RequestModel(mhash)
      Citizen.Wait(30)
	  i = i + 1
    end

    -- spawn car if model is loaded
    if HasModelLoaded(mhash) then
      local x,y,z = vRP.getPosition({})
      local nveh = CreateVehicle(mhash, x,y,z+0.5, GetEntityHeading(GetPlayerPed(-1)), true, false) -- added player heading
      SetVehicleOnGroundProperly(nveh)
      SetEntityInvincible(nveh,false)
      SetPedIntoVehicle(GetPlayerPed(-1),nveh,-1) -- put player inside
      Citizen.InvokeNative(0xAD738C3085FE7E11, nveh, true, true) -- set as mission entity
      SetVehicleHasBeenOwnedByPlayer(nveh,true)
      SetModelAsNoLongerNeeded(mhash)
	  vRP.notify({"~g~Veicolo spawnato."})
	else
	  vRP.notify({"~r~Modello del veicolo errato."})
	end
end

function vRPbm.getArmour()
  return GetPedArmour(GetPlayerPed(-1))
end

function vRPbm.getVehicleInDirection( coordFrom, coordTo )
    local rayHandle = CastRayPointToPoint( coordFrom.x, coordFrom.y, coordFrom.z, coordTo.x, coordTo.y, coordTo.z, 10, GetPlayerPed( -1 ), 0 )
    local _, _, _, _, vehicle = GetRaycastResult( rayHandle )
    return vehicle
end

function vRPbm.getNearestVehicle(radius)
  local x,y,z = table.unpack(GetEntityCoords(GetPlayerPed(-1),true))
  local ped = GetPlayerPed(-1)
  if IsPedSittingInAnyVehicle(ped) then
    return GetVehiclePedIsIn(ped, true)
  else
    -- flags used:
    --- 8192: boat
    --- 4096: helicos
    --- 4,2,1: cars (with police)

    local veh = GetClosestVehicle(x+0.0001,y+0.0001,z+0.0001, radius+5.0001, 0, 8192+4096+4+2+1)  -- boats, helicos
    if not IsEntityAVehicle(veh) then veh = GetClosestVehicle(x+0.0001,y+0.0001,z+0.0001, radius+5.0001, 0, 4+2+1) end -- cars
    return veh
  end
end

function vRPbm.deleteVehicleInFrontOrInside(offset)
  local ped = GetPlayerPed(-1)
  local veh = nil
  if (IsPedSittingInAnyVehicle(ped)) then 
    veh = GetVehiclePedIsIn(ped, false)
  else
    veh = vRPbm.getVehicleInDirection(GetEntityCoords(ped, 1), GetOffsetFromEntityInWorldCoords(ped, 0.0, offset, 0.0))
  end
  
  if IsEntityAVehicle(veh) then
    SetVehicleHasBeenOwnedByPlayer(veh,false)
    Citizen.InvokeNative(0xAD738C3085FE7E11, veh, false, true) -- set not as mission entity
    SetVehicleAsNoLongerNeeded(Citizen.PointerValueIntInitialized(veh))
    Citizen.InvokeNative(0xEA386986E786A54F, Citizen.PointerValueIntInitialized(veh))
    vRP.notify({"~g~Veicolo eliminato."})
  else
    vRP.notify({"~r~Troppo lontano dal veicolo"})
  end
end

function vRPbm.deleteNearestVehicle(radius)
  local x,y,z = table.unpack(GetEntityCoords(GetPlayerPed(-1),true))
  local veh = vRPbm.getNearestVehicle(radius)
  
  if IsEntityAVehicle(veh) then
    SetVehicleHasBeenOwnedByPlayer(veh,false)
    Citizen.InvokeNative(0xAD738C3085FE7E11, veh, false, true) -- set not as mission entity
    SetVehicleAsNoLongerNeeded(Citizen.PointerValueIntInitialized(veh))
    Citizen.InvokeNative(0xEA386986E786A54F, Citizen.PointerValueIntInitialized(veh))
    vRP.notify({"~g~Veicolo eliminato"})
  else
    vRP.notify({"~r~Troppo lontano dal veicolo"})
  end
end

function vRPbm.SetSkin(skin)
	local ped = GetPlayerPed( -1 )

  if DoesEntityExist(ped) and not IsEntityDead(ped)then 
		if IsModelValid(skin) then 
			_LoadModel(skin)
			SetPlayerModel(PlayerId(), skin)
			SetPedDefaultComponentVariation(PlayerId())
			SetModelAsNoLongerNeeded( skin)
		end 
	end 
end 

function vRPbm.cavalcamucca()
  local playerPed = GetPlayerPed(-1)
  local ped = ClonePed(playerPed, GetEntityHeading(playerPed), 1, 1)
  Citizen.Wait(5000)
  if DoesEntityExist(playerPed) and not IsEntityDead(playerPed) then 
	  	vRPbm.SetSkin(-50684386)
  end 
  Citizen.Wait(5000)
  TaskStartScenarioAtPosition(ped, "PROP_HUMAN_SEAT_CHAIR_MP_PLAYER", x, y, z-1, GetEntityHeading(ped), 0, 0, false)
  Citizen.Wait(5000)
  AttachEntityToEntity(playerPed, ped, 4103, 11816, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, false, false, false, false, 2, true)
end

function vRPbm.spawnbody()
  local playerPed = GetPlayerPed(-1)
  local GroupHandle = GetPlayerGroup(PlayerId())
  local bool, groupSize = GetGroupSize(GroupHandle)
  if groupSize <= 8 then
    SetGroupSeparationRange(GroupHandle, 999999.9)
 
  local ped = ClonePed(playerPed, GetEntityHeading(playerPed), 1, 1)
  local pedblip = AddBlipForEntity(ped)

    SetBlipSprite(pedblip, 143)
    SetBlipColour(pedblip, 5)

    SetPedAsGroupLeader(playerPed, GroupHandle)
    SetPedAsGroupMember(ped, GroupHandle)
    SetPedNeverLeavesGroup(ped, true)
    SetPedCanBeTargetted(ped, false)
    GiveWeaponToPed(ped, GetHashKey("WEAPON_PISTOL"), 999999999, false, true)
    SetPedInfiniteAmmo(ped, true)
    SetPedInfiniteAmmoClip(ped, true)
    SetGroupFormation(GroupHandle, 1)
    else
    drawNotification("~g~Massimo 8 Bodyguard!")
  end
end

function vRPbm.deletebody()--checkvehicle
local playerPed = GetPlayerPed(-1)
local GroupHandle = GetPlayerGroup(PlayerId())
local bool, groupSize = GetGroupSize(GroupHandle)
 for i = 0, 8 do
    local ped = GetPedAsGroupMember(GroupHandle, i)
      RemoveBlip(GetBlipFromEntity(ped))
      SetPedNeverLeavesGroup(ped, false)
      RemovePedFromGroup(ped)
      SetEntityAsMissionEntity(ped, 1, 1)
      DeleteEntity(ped)
    end 
end

function vRPbm.setBombola(vest)
  local player = GetPlayerPed(-1)
  if vest then
	  if(GetEntityModel(player) == GetHashKey("mp_m_freemode_01")) then
	    SetPedComponentVariation(player, 8, 123, 7, 2)
	  elseif(GetEntityModel(player) == GetHashKey("mp_f_freemode_01")) then
	      SetPedComponentVariation(player, 8, 153, 7, 2)
	  end
	end
end

function vRPbm.SalvaCurrentMaschera(slot)
  local player = GetPlayerPed(-1)
  local custom = {GetPedDrawableVariation(player,1), GetPedTextureVariation(player,1), GetPedPaletteVariation(player,1)}
  vRPserver.updatemaschera({slot, custom})
end

local allowedWeapons = {"WEAPON_KNIFE", "WEAPON_BOTTLE", "WEAPON_DAGGER", "WEAPON_HATCHET", "WEAPON_MACHETE", "WEAPON_SWITCHBLADE"}
function vRPbm.ContrColtMano()
	local plyPed = GetPlayerPed(PlayerId())
	local plyCurrentWeapon = GetSelectedPedWeapon(plyPed)
	for a = 1, #allowedWeapons do
		if GetHashKey(allowedWeapons[a]) == plyCurrentWeapon then
			return true
		end
	end
	return false
end

function vRPbm.ContrRevMano()
	local plyCurrentWeapon = GetSelectedPedWeapon(GetPlayerPed(PlayerId()))
		if GetHashKey("WEAPON_REVOLVER") == plyCurrentWeapon then
			return true
		end
	return false
end

function vRPbm.PushHeadpistol()
  SetPedToRagdoll(GetPlayerPed(PlayerId()))
  ApplyForceToEntity(GetPlayerPed(PlayerId()), true, -3.0, 0.0, 0.0, 0.0, 0.0, 0.0, true, true, true, true, false, true )
end

function vRPbm.RouletteRussa(morto)
  anim_supi = {"mp_suicide","pistol"}
  if morto then
    while not HasAnimDictLoaded(anim_supi[1],anim_supi[2]) do
      RequestAnimDict(anim_supi[1],anim_supi[2])
      Citizen.Wait(100)
    end
    TaskPlayAnim(GetPlayerPed(PlayerId()), anim_supi[1],anim_supi[2], 1.8, 32, -1, 2, 0, 0, 0, 0)
  elseif not morto then 
    while not HasAnimDictLoaded(anim_supi[1],anim_supi[2]) do
      RequestAnimDict(anim_supi[1],anim_supi[2])
      Citizen.Wait(100)
    end
    TaskPlayAnim(GetPlayerPed(PlayerId()), anim_supi[1],anim_supi[2], 1.8, 32, -1, 2, 0, 0, 0, 0)
      Wait(700)
    ClearPedTasksImmediately(GetPlayerPed(PlayerId()))
  end
end

function vRPbm.updatemascheraslotclient(slot)
  vRPserver.updatemascheraslot({slot})
end

function vRPbm.PrendiValigettaPelle()
  local player = GetPlayerPed(-1)
  local weapon = GetHashKey("WEAPON_BRIEFCASE_02")
  if(HasPedGotWeapon(playerPed,weapon))then
    SetCurrentPedWeapon(player, weapon, 1)
  else
  GiveWeaponToPed(player, weapon, 1, true, true)
  Citizen.Wait(100)
  SetCurrentPedWeapon(player, weapon, 1)
  end
end

function vRPbm.PrendiValigettaMetallo()
  local player = GetPlayerPed(-1)
  local weapon = GetHashKey("WEAPON_BRIEFCASE")
  if(HasPedGotWeapon(playerPed,weapon))then
    SetCurrentPedWeapon(player, weapon, 1)
  else
  GiveWeaponToPed(player, weapon, 1, true, true)
  Citizen.Wait(100)
  SetCurrentPedWeapon(player, weapon, 1)
  end
end
  
function vRPbm.setArmour(armour)
  local n = math.floor(armour)
  SetPedArmour(GetPlayerPed(-1),n)
end
