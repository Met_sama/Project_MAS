
other2 = nil
forz = false
playerStillDragged2 = false

RegisterNetEvent("dr:drag2")
AddEventHandler('dr:drag2', function(pl)
    other2 = pl
    forz = not forz
end)

RegisterNetEvent("dr:undrag2")
AddEventHandler('dr:undrag2', function(pl)
    forz = false
end)

Citizen.CreateThread(function()
    while true do
        if forz and other2 ~= nil then
            local ped2 = GetPlayerPed(GetPlayerFromServerId(other2))
            local myped2 = GetPlayerPed(-1)
            AttachEntityToEntity(myped2, ped2, 4103, 11816, 2.0, 2.0, 0.0, 0.0, 180.0, 0.0, true, false, false, false, 2, true)
            playerStillDragged2 = true
        else
            if(playerStillDragged2) then
                DetachEntity(GetPlayerPed(-1), true, false)
                --ApplyForceToEntity(ped2, 1, 40.0, coords.y*2, 4.0, 0.0, 0.0, 0.0, 1, false, true, true, true, true)
                playerStillDragged2 = false
            end
        end
        Citizen.Wait(0)
    end
end)

other = nil
drag = false
playerStillDragged = false

RegisterNetEvent("dr:drag")
AddEventHandler('dr:drag', function(pl)
    other = pl
    drag = not drag
end)

RegisterNetEvent("dr:undrag")
AddEventHandler('dr:undrag', function(pl)
    drag = false
end)

Citizen.CreateThread(function()
    while true do
        if drag and other ~= nil then
            local ped = GetPlayerPed(GetPlayerFromServerId(other))
            local myped = GetPlayerPed(-1)
            AttachEntityToEntity(myped, ped, 4103, 11816, 0.54, 0, 0.0, 0.0, 0.0, 0.0, false, false, false, false, 2, true)
            playerStillDragged = true
        else
            if(playerStillDragged) then
                DetachEntity(GetPlayerPed(-1), true, false)
                playerStillDragged = false
            end
        end
        Citizen.Wait(0)
    end
end)