local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")
local htmlEntities = module("vrp", "lib/htmlEntities")

vRPbm = {}
vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP","vRP_basic_menu")
BMclient = Tunnel.getInterface("vRP_basic_menu","vRP_basic_menu")
vRPbsC = Tunnel.getInterface("vRP_barbershop","vRP_basic_menu")
Tunnel.bindInterface("vrp_basic_menu",vRPbm)

local Lang = module("vrp", "lib/Lang")
local cfg = module("vrp", "cfg/base")
local lang = Lang.new(module("vrp", "cfg/lang/"..cfg.lang) or {})

-- LOG FUNCTION
function vRPbm.logInfoToFile(file,info)
  file = io.open(file, "a")
  if file then
    file:write(os.date("%c").." => "..info.."\n")
  end
  file:close()
end
-- MAKE CHOICES
--toggle service
local choice_uber = {function(player,choice)
  local user_id = vRP.getUserId({player})
  local service = "uber"
  if user_id ~= nil then
    if vRP.hasGroup({user_id,service}) then
	  vRP.removeUserGroup({user_id,service})
	  if vRP.hasMission({player}) then
		vRP.stopMission({player})
	  end
      vRPclient.notify(player,{"~r~Non sei più un Uber"})
	else
	  vRP.addUserGroup({user_id,service})
      vRPclient.notify(player,{"~g~Ora sei un Uber"})
	end
  end
end, "Diventa un UBER"}

-- teleport waypoint
local choice_tptowaypoint = {function(player,choice)
  TriggerClientEvent("TpToWaypoint", player)
end, "Teletrasportati sul marker"}

-- fix barbershop green hair for now
local ch_fixhair = {function(player,choice)
    local custom = {}
    local user_id = vRP.getUserId({player})
    vRP.getUData({user_id,"vRP:head:overlay",function(value)
	  if value ~= nil then
	    custom = json.decode(value)
        vRPbsC.setOverlay(player,{custom,true})
	  end
	end})
end, "Fixa i capelli"}

--toggle blips
local ch_blips = {function(player,choice)
  --print("PLAYER-BLIP-TOGGLE("..GetPlayerName(player)..") = True"))
  TriggerClientEvent("showBlips", player)
end, "Toggle blips."}

local spikes = {}
local ch_spikes = {function(player,choice)
	local user_id = vRP.getUserId({player})
	BMclient.isCloseToSpikes(player,{},function(closeby)
		if closeby and (spikes[player] or vRP.hasPermission({user_id,"admin.spikes"})) then
		  BMclient.removeSpikes(player,{})
		  spikes[player] = false
		elseif closeby and not spikes[player] and not vRP.hasPermission({user_id,"admin.spikes"}) then
		  vRPclient.notify(player,{"~r~Tu puoi mettere solo una striscia chiodata!"})
		elseif not closeby and spikes[player] and not vRP.hasPermission({user_id,"admin.spikes"}) then
		  vRPclient.notify(player,{"~r~È possibile distribuire solo un set di Strisce chiodate"})
		elseif not closeby and (not spikes[player] or vRP.hasPermission({user_id,"admin.spikes"})) then
		  BMclient.setSpikesOnGround(player,{})
		  spikes[player] = true
		end
	end)
end, "Togli striscia chiodata"}

local ch_sprites = {function(player,choice)
  TriggerClientEvent("showSprites", player)
end, "Leva."}

local ch_deleteveh = {function(player,choice)
  BMclient.deleteVehicleInFrontOrInside(player,{5.0})
end, "Elimina la macchina piu vicina."}

--client function
local ch_crun = {function(player,choice)
  vRP.prompt({player,"Function:","",function(player,stringToRun) 
    stringToRun = stringToRun or ""
	TriggerClientEvent("RunCode:RunStringLocally", player, stringToRun)
  end})
end, "Run client function."}

--server function
local ch_srun = {function(player,choice)
  vRP.prompt({player,"Function:","",function(player,stringToRun) 
    stringToRun = stringToRun or ""
	TriggerEvent("RunCode:RunStringRemotelly", stringToRun)
  end})
end, "Run server function."}

--police weapons // comment out the weapons if you dont want to give weapons.
--local ch_polweapons = {function(player,choice)
 -- BMclient.polweapons(player)
--end,"Fai rifornimento dalla macchina."}

--store money
local choice_store_money = {function(player, choice)
  local user_id = vRP.getUserId({player})
  if user_id ~= nil then
    local amount = vRP.getMoney({user_id})
    if vRP.tryPayment({user_id, amount}) then -- unpack the money
      vRP.giveInventoryItem({user_id, "money", amount, true})
    end
  end
end, "Metti i tuoi soldi nell' inventario."}

--medkit storage
local emergency_medkit = {}
emergency_medkit["Take"] = {function(player,choice)
	local user_id = vRP.getUserId({player}) 
	vRP.giveInventoryItem({user_id,"medkit",25,true})
	vRP.giveInventoryItem({user_id,"pills",25,true})
end}

--heal me
local emergency_heal = {}
emergency_heal["Heal"] = {function(player,choice)
	local user_id = vRP.getUserId({player}) 
	vRPclient.setHealth(player,{1000})
end}

--loot corpse
local choice_loot = {function(player,choice)
  local user_id = vRP.getUserId({player})
  if user_id ~= nil then
    vRPclient.getNearestPlayer(player,{10},function(nplayer)
      local nuser_id = vRP.getUserId({nplayer})
      if nuser_id ~= nil then
        vRPclient.isInComa(nplayer,{}, function(in_coma)
          if in_coma then
			local revive_seq = {
			  {"amb@medic@standing@tendtodead@enter","enter",1},
			  {"amb@medic@standing@tendtodead@idle_a","idle_c",1},
			  {"amb@medic@standing@tendtodead@exit","exit",1}
			}
  			vRPclient.playAnim(player,{false,revive_seq,false}) -- anim
            SetTimeout(15000, function()
              local ndata = vRP.getUserDataTable({nuser_id})
              if ndata ~= nil then
			    if ndata.inventory ~= nil then -- gives inventory items
				  vRP.clearInventory({nuser_id})
                  for k,v in pairs(ndata.inventory) do 
			        vRP.giveInventoryItem({user_id,k,v.amount,true})
	              end
				end
			  end
			  local nmoney = vRP.getMoney({nuser_id})
			  if vRP.tryPayment({nuser_id,nmoney}) then
			    vRP.giveMoney({user_id,nmoney})
			  end
            end)
			vRPclient.stopAnim(player,{false})
          else
            vRPclient.notify(player,{lang.emergency.menu.revive.not_in_coma()})
          end
        end)
      else
        vRPclient.notify(player,{lang.common.no_player_near()})
      end
    end)
  end
end,"Loota il cadavere piu vicino"}

-- hack player
local ch_hack = {function(player,choice)
  -- get nearest player
  local user_id = vRP.getUserId({player})
  if user_id ~= nil then
    vRPclient.getNearestPlayer(player,{25},function(nplayer)
      if nplayer ~= nil then
        local nuser_id = vRP.getUserId({nplayer})
        if nuser_id ~= nil then
          -- prompt number
		  local nbank = vRP.getBankMoney({nuser_id})
          local amount = math.floor(nbank*0.01)
		  local nvalue = nbank - amount
		  if math.random(1,100) == 1 then
			vRP.setBankMoney({nuser_id,nvalue})
            vRPclient.notify(nplayer,{"Hacked ~r~".. amount .."$."})
		    vRP.giveInventoryItem({user_id,"dirty_money",amount,true})
		  else
            vRPclient.notify(nplayer,{"~g~Hacking attempt failed."})
            vRPclient.notify(player,{"~r~Hacking attempt failed."})
		  end
        else
          vRPclient.notify(player,{lang.common.no_player_near()})
        end
      else
        vRPclient.notify(player,{lang.common.no_player_near()})
      end
    end)
  end
end,"Hack closest player."}

-- mug player
local ch_mug = {function(player,choice)
  -- get nearest player
  local user_id = vRP.getUserId({player})
  if user_id ~= nil then
    vRPclient.getNearestPlayer(player,{10},function(nplayer)
      if nplayer ~= nil then
        local nuser_id = vRP.getUserId({nplayer})
        if nuser_id ~= nil then
          -- prompt number
		  local nmoney = vRP.getMoney({nuser_id})
          local amount = nmoney
		  if math.random(1,3) == 1 then
            if vRP.tryPayment({nuser_id,amount}) then
              vRPclient.notify(nplayer,{"Mugged ~r~"..amount.."$."})
		      vRP.giveInventoryItem({user_id,"dirty_money",amount,true})
            else
              vRPclient.notify(player,{lang.money.not_enough()})
            end
		  else
            vRPclient.notify(nplayer,{"~g~Mugging attempt failed."})
            vRPclient.notify(player,{"~r~Mugging attempt failed."})
		  end
        else
          vRPclient.notify(player,{lang.common.no_player_near()})
        end
      else
        vRPclient.notify(player,{lang.common.no_player_near()})
      end
    end)
  end
end, "Mug closest player."}

-- drag player
local ch_drag = {function(player,choice)
  -- get nearest player
  local user_id = vRP.getUserId({player})
  if user_id ~= nil then
    vRPclient.getNearestPlayer(player,{10},function(nplayer)
      if nplayer ~= nil then
        local nuser_id = vRP.getUserId({nplayer})
        if nuser_id ~= nil then
		  vRPclient.isHandcuffed(nplayer,{},function(handcuffed)
			if handcuffed then
				TriggerClientEvent("dr:drag", nplayer, player)
			else
				vRPclient.notify(player,{"Il player non è ammanettato."})
			end
		  end)
        else
          vRPclient.notify(player,{lang.common.no_player_near()})
        end
      else
        vRPclient.notify(player,{lang.common.no_player_near()})
      end
    end)
  end
end, "Trasporta il player piu vicino"}

local ch_distintivo = {function(player,choice)
  local user_id = vRP.getUserId({player})
  if user_id ~= nil then
    BMclient.MostraDistintivoProp(player,{})
    --vRPclient.playAnim(player,{false,{{"mp_arresting","a_uncuff",1}},false})
  end
end}

-- player check
local choice_player_check = {function(player,choice)
  vRPclient.getNearestPlayer(player,{5},function(nplayer)
    local nuser_id = vRP.getUserId({nplayer})
    if nuser_id ~= nil then
      vRPclient.notify(nplayer,{lang.police.menu.check.checked()})
      vRPclient.getWeapons(nplayer,{},function(weapons)
        -- prepare display data (money, items, weapons)
        local money = vRP.getMoney({nuser_id})
        local items = ""
        local data = vRP.getUserDataTable({nuser_id})
        if data and data.inventory then
          for k,v in pairs(data.inventory) do
            local item_name = vRP.getItemName({k})
            if item_name then
              items = items.."<br />"..item_name.." ("..v.amount..")"
            end
          end
        end

        local weapons_info = ""
        for k,v in pairs(weapons) do
          weapons_info = weapons_info.."<br />"..k.." ("..v.ammo..")"
        end

        vRPclient.setDiv(player,{"police_check",".div_police_check{ background-color: rgba(0,0,0,0.75); color: white; font-weight: bold; width: 500px; padding: 10px; margin: auto; margin-top: 150px; }",lang.police.menu.check.info({money,items,weapons_info})})
        -- request to hide div
        vRP.request({player, lang.police.menu.check.request_hide(), 1000, function(player,ok)
          vRPclient.removeDiv(player,{"police_check"})
        end})
      end)
    else
      vRPclient.notify(player,{lang.common.no_player_near()})
    end
  end)
end, lang.police.menu.check.description()}

-- player store weapons
local choice_store_weapons = {function(player, choice)
  local user_id = vRP.getUserId({player})
  if user_id ~= nil then
    vRPclient.getWeapons(player,{},function(weapons)
      for k,v in pairs(weapons) do
        -- convert weapons to parametric weapon items
        vRP.giveInventoryItem({user_id, "wbody|"..k, 1, true})
        if v.ammo > 0 then
          vRP.giveInventoryItem({user_id, "wammo|"..k, v.ammo, true})
        end
      end

      -- clear all weapons
      vRPclient.giveWeapons(player,{{},true})
    end)
  end
end, lang.police.menu.store_weapons.description()}

-- armor item
vRP.defInventoryItem({"body_armor","Body Armor","Intact body armor.",
function(args)
  local choices = {}

  choices["Equip"] = {function(player,choice)
    local user_id = vRP.getUserId({player})
    if user_id ~= nil then
      if vRP.tryGetInventoryItem({user_id, "body_armor", 1, true}) then
		BMclient.setArmour(player,{100,true})
        vRP.closeMenu({player})
      end
    end
  end}

  return choices
end,
5.00})

-- store armor
local choice_store_armor = {function(player, choice)
  local user_id = vRP.getUserId({player})
  if user_id ~= nil then
    BMclient.getArmour(player,{},function(armour)
      if armour > 95 then
        vRP.giveInventoryItem({user_id, "body_armor", 1, true})
        -- clear armor
	    BMclient.setArmour(player,{0,false})
	  else
	    vRPclient.notify(player, {"~r~L'armatura danneggiata non può essere immagazzinata!"})
      end
    end)
  end
end, "Conservare l'armatura intatta nell'inventario."}

local unjailed = {}
function jail_clock(target_id,timer)
  local target = vRP.getUserSource({tonumber(target_id)})
  local users = vRP.getUsers({})
  local online = false
  for k,v in pairs(users) do
	if tonumber(k) == tonumber(target_id) then
	  online = true
	end
  end
  if online then
    if timer > 0 then
	  vRPclient.notify(target, {"~r~Tempo rimanente: " .. timer .. " minuto(i)."})
      vRP.setUData({tonumber(target_id),"vRP:jail:time",json.encode(timer)})
	  SetTimeout(60*1000, function()
		for k,v in pairs(unjailed) do -- check if player has been unjailed by cop or admin
		  if v == tonumber(target_id) then
	        unjailed[v] = nil
		    timer = 0
		  end
		end
		vRP.setHunger({tonumber(target_id), 0})
		vRP.setThirst({tonumber(target_id), 0})
	    jail_clock(tonumber(target_id),timer-1)
	  end) 
    else 
	  BMclient.loadFreeze(target,{true})
	  SetTimeout(15000,function()
		BMclient.loadFreeze(target,{false})
	  end)
	  vRPclient.teleport(target,{424.52294921875,-979.03723144532,30.710632324218}) -- teleport to outside jail
	  vRPclient.setHandcuffed(target,{false})
      vRPclient.notify(target,{"~b~ID liberato."})
	  vRP.setUData({tonumber(target_id),"vRP:jail:time",json.encode(-1)})
    end
  end
end

function vRPbm.payPhoneNumber(user_id,phone)
  local player = vRP.getUserSource({user_id})
  local directory_name = vRP.getPhoneDirectoryName({user_id, phone})
  if directory_name == "unknown" then
	directory_name = phone
  end
  vRP.prompt({player,"Importo da inviare a "..directory_name..":","0",function(player,transfer)
	if transfer ~= nil and transfer ~= "" and tonumber(transfer)>0 then 
	  vRP.getUserByPhone({phone, function(target_id)
	    local my_bank = vRP.getBankMoney({user_id}) - tonumber(transfer)
		if target_id~=nil then
          if my_bank >= 0 then
		    local target = vRP.getUserSource({target_id})
			if target ~= nil then
			  vRP.setBankMoney({user_id,my_bank})
              vRPclient.notify(player,{"~g~Hai trasferito ~r~$"..transfer.." ~g~a ~b~"..directory_name})
			  local target_bank = vRP.getBankMoney({target_id}) + tonumber(transfer)
			  vRP.setBankMoney({target_id,target_bank})
			  vRPbm.logInfoToFile("mpayLog.txt",user_id .. " mobile paid "..target_id.." the amount of " .. transfer .. ", user bank post-payment for "..user_id.." equals $"..my_bank.." and for "..user_id.." equals $"..target_bank)
			  vRP.getUserIdentity({user_id, function(identity)
		        local my_directory_name = vRP.getPhoneDirectoryName({target_id, identity.phone})
			    if my_directory_name == "unknown" then
		          my_directory_name = identity.phone
			    end
                vRPclient.notify(target,{"~g~Hai ricevuto ~y~$"..transfer.." ~g~da ~b~"..my_directory_name})
			  end})
              vRP.closeMenu({player})
			else
			  vRPclient.notify(player,{"~r~Non è possibile effettuare pagamenti a giocatori offline."})
			end
          else
            vRPclient.notify(player,{lang.money.not_enough()})
          end
		else
		  vRPclient.notify(player,{"~r~Quel numero di telefono sembra non valido."})
		end
	  end})
	else
	  vRPclient.notify(player,{"~r~Il valore deve essere maggiore di 0."})
	end
  end})
end

-- mobilepay
local ch_mobilepay = {function(player,choice) 
	local user_id = vRP.getUserId({player})
	local menu = {}
	menu.name = lang.phone.directory.title()
	menu.css = {top = "75px", header_color = "rgba(0,0,255,0.75)"}
    menu.onclose = function(player) vRP.openMainMenu({player}) end -- nest menu
	menu[">Inserisci numero"] = {
	  -- payment function
	  function(player,choice) 
	    vRP.prompt({player,"Numero Telefonico:","000-0000",function(player,phone)
	      if phone ~= nil and phone ~= "" then 
		    vRPbm.payPhoneNumber(user_id,phone)
		  else
		    vRPclient.notify(player,{"~r~Devi digitare un numero di telefono."})
		  end
	    end})
	  end,"Inserisci il numero manualmente."}
	local directory = vRP.getPhoneDirectory({user_id})
	for k,v in pairs(directory) do
	  menu[k] = {
	    -- payment function
	    function(player,choice) 
		  vRPbm.payPhoneNumber(user_id,v)
	    end
	  ,v} -- number as description
	end
	vRP.openMenu({player, menu})
end,"Trasferisci dei soldi tramite il telefono."}

-- dynamic jail
local ch_jail = {function(player,choice) 
  vRPclient.getNearestPlayers(player,{15},function(nplayers) 
	local user_list = ""
    for k,v in pairs(nplayers) do
	  user_list = user_list .. "[" .. vRP.getUserId({k}) .. "]" .. GetPlayerName(k) .. " | "
    end 
	if user_list ~= "" then
	  vRP.prompt({player,"Player vicini:" .. user_list,"",function(player,target_id) 
	    if target_id ~= nil and target_id ~= "" then 
	      vRP.prompt({player,"Pena (in minuti):","1",function(player,jail_time)
			if jail_time ~= nil and jail_time ~= "" then 
	          local target = vRP.getUserSource({tonumber(target_id)})
			  if target ~= nil then
		        if tonumber(jail_time) > 999 then
  			      jail_time = 999
		        end
		        if tonumber(jail_time) < 1 then
		          jail_time = 1
		        end
		  
                vRPclient.isHandcuffed(target,{}, function(handcuffed)  
                  if handcuffed then 
					BMclient.loadFreeze(target,{true})
					SetTimeout(15000,function()
					  BMclient.loadFreeze(target,{false})
					end)
				    vRPclient.teleport(target,{459.5500793457,-994.46508789063,23.914855957031}) -- teleport to inside jail
				    vRPclient.notify(target,{"~r~Sei stato arrestato."})
				    vRPclient.notify(player,{"~b~Hai arrestato un player."})
				    vRP.setHunger({tonumber(target_id),0})
				    vRP.setThirst({tonumber(target_id),0})
				    jail_clock(tonumber(target_id),tonumber(jail_time))
					local user_id = vRP.getUserId({player})
					vRPbm.logInfoToFile("jailLog.txt",user_id .. " ha arrestato "..target_id.." per " .. jail_time .. " minutes")
			      else
				    vRPclient.notify(player,{"~r~Questo player non è ammanettato"})
			      end
			    end)
			  else
				vRPclient.notify(player,{"~r~Questo id è invalido."})
			  end
			else
			  vRPclient.notify(player,{"~r~Il tempo di prigione non può essere vuoto."})
			end
	      end})
        else
          vRPclient.notify(player,{"~r~Nessun ID giocatore selezionato."})
        end 
	  end})
    else
      vRPclient.notify(player,{"~r~Nessun giocatore vicino."})
    end 
  end)
end,"Invia un giocatore vicino alla prigione."}

-- penitenziario
local ch_jail2 = {function(player,choice) 
  vRPclient.getNearestPlayers(player,{15},function(nplayers) 
	local user_list = ""
    for k,v in pairs(nplayers) do
	  user_list = user_list .. "[" .. vRP.getUserId({k}) .. "]" .. GetPlayerName(k) .. " | "
    end 
	if user_list ~= "" then
	  vRP.prompt({player,"Player vicini:" .. user_list,"",function(player,target_id) 
	    if target_id ~= nil and target_id ~= "" then 
	      vRP.prompt({player,"Pena (in minuti):","1",function(player,jail_time)
			if jail_time ~= nil and jail_time ~= "" then 
	          local target = vRP.getUserSource({tonumber(target_id)})
			  if target ~= nil then
		        if tonumber(jail_time) > 999 then
  			      jail_time = 999
		        end
		        if tonumber(jail_time) < 1 then
		          jail_time = 1
		        end
		  
                vRPclient.isHandcuffed(target,{}, function(handcuffed)  
                  if handcuffed then 
					BMclient.loadFreeze(target,{true})
					SetTimeout(15000,function()
					  BMclient.loadFreeze(target,{false})
					end)
				    vRPclient.teleport(target,{1800.548828125,2482.6721191406,-122.69355773926}) -- teleport to inside jail
				    vRPclient.notify(target,{"~r~Sei stato arrestato."})
				    vRPclient.notify(player,{"~b~Hai arrestato un player."})
				    vRP.setHunger({tonumber(target_id),0})
				    vRP.setThirst({tonumber(target_id),0})
				    jail_clock(tonumber(target_id),tonumber(jail_time))
					local user_id = vRP.getUserId({player})
					vRPbm.logInfoToFile("jailLog.txt",user_id .. " ha arrestato e mandato al penitenziario "..target_id.." per " .. jail_time .. " minutes")
			      else
				    vRPclient.notify(player,{"~r~Questo player non è ammanettato"})
			      end
			    end)
			  else
				vRPclient.notify(player,{"~r~Questo id è invalido."})
			  end
			else
			  vRPclient.notify(player,{"~r~Il tempo di prigione non può essere vuoto."})
			end
	      end})
        else
          vRPclient.notify(player,{"~r~Nessun ID giocatore selezionato."})
        end 
	  end})
    else
      vRPclient.notify(player,{"~r~Nessun giocatore vicino."})
    end 
  end)
end,"Invia un giocatore vicino alla prigione."}


-- dynamic unjail
local ch_unjail = {function(player,choice) 
	vRP.prompt({player,"Player ID:","",function(player,target_id) 
	  if target_id ~= nil and target_id ~= "" then 
		vRP.getUData({tonumber(target_id),"vRP:jail:time",function(value)
		  if value ~= nil then
		  custom = json.decode(value)
			if custom ~= nil then
			  local user_id = vRP.getUserId({player})
			  if tonumber(custom) > 0 or vRP.hasPermission({user_id,"admin.easy_unjail"}) then
	            local target = vRP.getUserSource({tonumber(target_id)})
				if target ~= nil then
	              unjailed[target] = tonumber(target_id)
				  vRPclient.notify(player,{"~g~Sarai rilasciato tra poco."})
				  vRPclient.notify(target,{"~g~Qualcuno ha abbassato la tua pena"})
				  vRPbm.logInfoToFile("jailLog.txt",user_id .. " freed "..target_id.." from a " .. custom .. " minutes sentence")
				else
				  vRPclient.notify(player,{"~r~Questo id è invalido."})
				end
			  else
				vRPclient.notify(player,{"~r~ID non in cella."})
			  end
			end
		  end
		end})
      else
        vRPclient.notify(player,{"~r~Questo id è invalido"})
      end 
	end})
end,"Libera un giocatore in prigione."}

-- (server) called when a logged player spawn to check for vRP:jail in user_data
AddEventHandler("vRP:playerSpawn", function(user_id, source, first_spawn) 
  local target = vRP.getUserSource({user_id})
  SetTimeout(35000,function()
    local custom = {}
    vRP.getUData({user_id,"vRP:jail:time",function(value)
	  if value ~= nil then
	    custom = json.decode(value)
	    if custom ~= nil then
		  if tonumber(custom) > 0 then
			BMclient.loadFreeze(target,{true})
			SetTimeout(15000,function()
			  BMclient.loadFreeze(target,{false})
			end)
            vRPclient.setHandcuffed(target,{true})
            vRPclient.teleport(target,{1641.5477294922,2570.4819335938,45.564788818359}) -- teleport inside jail
            vRPclient.notify(target,{"~r~Pena finita"})
			vRP.setHunger({tonumber(user_id),0})
			vRP.setThirst({tonumber(user_id),0})
			vRPbm.logInfoToFile("jailLog.txt",user_id.." è stato rimandato in prigione per " .. custom .. " minuti per completare la sua sentenza")
		    jail_clock(tonumber(user_id),tonumber(custom))
		  end
	    end
	  end
	end})
  end)
end)

-- improved handcuff
local ch_handcuff = {function(player,choice)
  vRPclient.getNearestPlayer(player,{10},function(nplayer)
    local nuser_id = vRP.getUserId({nplayer})
    if nuser_id ~= nil then
      vRPclient.toggleHandcuff(nplayer,{})
	  local user_id = vRP.getUserId({player})
	  vRPbm.logInfoToFile("jailLog.txt",user_id .. " ammanettato "..nuser_id)
      vRP.closeMenu({nplayer})
    else
      vRPclient.notify(player,{lang.common.no_player_near()})
    end
  end)
end,lang.police.menu.handcuff.description()}

-- admin god mode
local gods = {}
function task_god()
  SetTimeout(10000, task_god)

  for k,v in pairs(gods) do
    vRP.setHunger({v, 0})
    vRP.setThirst({v, 0})

    local player = vRP.getUserSource({v})
    if player ~= nil then
      vRPclient.setHealth(player, {200})
    end
  end
end
task_god()

local ch_godmode = {function(player,choice)
  local user_id = vRP.getUserId({player})
  if user_id ~= nil then
    if gods[player] then
	  gods[player] = nil
	else
	  gods[player] = user_id
	end
  end
end, "Leva la godmode agli admin."}

local player_lists = {}
local ch_userlist = {function(player,choice)
  local user_id = vRP.getUserId({player})
  if user_id ~= nil then
    if player_lists[player] then -- hide
      player_lists[player] = nil
      vRPclient.removeDiv(player,{"user_list"})
    else -- show
      local content = "<span class=\"id\">ID</span><span class=\"pseudo\">NICKNAME</span><span class=\"name\">ROLEPLAY NAME</span><span class=\"job\">PROFESSION</span>"
      local count = 0
	  local users = vRP.getUsers({})
      for k,v in pairs(users) do
        count = count+1
        local source = vRP.getUserSource({k})
        vRP.getUserIdentity({k, function(identity)
		  if source ~= nil then
            content = content.."<br /><span class=\"id\">"..k.."</span><span class=\"pseudo\">"..vRP.getPlayerName({source}).."</span>"
            if identity then
              content = content.."<span class=\"name\">"..htmlEntities.encode(identity.firstname).." "..htmlEntities.encode(identity.name).."</span><span class=\"job\">"..vRP.getUserGroupByType({k,"job"}).."</span>"
            end
          end
		  
          -- check end
          count = count-1
          if count == 0 then
            player_lists[player] = true
            local css = [[
              .div_user_list{ 
                margin: auto; 
				text-align: left;
                padding: 8px; 
                width: 650px; 
                margin-top: 100px; 
                background: rgba(50,50,50,0.0); 
                color: white; 
                font-weight: bold; 
                font-size: 1.1em;
              } 
              .div_user_list span{ 
				display: inline-block;
				text-align: center;
              } 
              .div_user_list .id{ 
                color: rgb(255, 255, 255);
                width: 45px; 
              }
              .div_user_list .pseudo{ 
                color: rgb(66, 244, 107);
                width: 145px; 
              }
              .div_user_list .name{ 
                color: rgb(92, 170, 249);
                width: 295px; 
              }
			  .div_user_list .job{ 
                color: rgb(247, 193, 93);
                width: 145px; 
			  }
            ]]
            vRPclient.setDiv(player,{"user_list", css, content})
          end
		end})
      end
    end
  end
end, "Leva lista utenti."}

-- spawn vehicle
local ch_spawnveh = {function(player,choice) 
	vRP.prompt({player,"Modello veicolo:","",function(player,model)
	  if model ~= nil and model ~= "" then 
	    BMclient.spawnVehicle(player,{model})
	  else
		vRPclient.notify(player,{"~r~Devi digitare un modello di veicolo."})
	  end
	end})
end,"Crea un modello di veicolo."}
--cavalcamucca()
local ch_body = {function(player,choice) 
  local user_id = vRP.getUserId({player})
  if user_id ~= nil then
  BMclient.spawnbody(player)
  end
end}

local ch_cavalcamucca = {function(player,choice) 
  local user_id = vRP.getUserId({player})
  if user_id ~= nil then
    BMclient.cavalcamucca(player)
  end
end}

local choice_arrenditi = {function(player,choice) 
  local user_id = vRP.getUserId({player})
  if user_id ~= nil then
    vRPclient.getNearestPlayer(player,{10},function(nplayer)
      local nuser_id = vRP.getUserId({nplayer})
      vRPclient.isInComa(player,{}, function(in_coma)
        if in_coma then
          if not vRP.hasPermission({user_id,"nophone"}) then
            local docs = vRP.getUsersByPermission({"ems.whitelisted"})
            if #docs == 0 then 
              if nuser_id == nil then
                vRPclient.killComa(player,{})
              elseif vRP.hasPermission({nuser_id,"police.jail"}) then
                vRPclient.notify(player,{"~r~Vicino a te c'è un agente di polizia."})
              else
                vRPclient.killComa(player,{})
              end
            else
              vRPclient.notify(player,{"~g~Ci sono "..#docs.." medici/o online"})
            end
          else
            vRPclient.killComa(player,{})
          end
        else
          vRPclient.notify(player,{"~r~Non sei in coma."})
        end
      end)
    end)
  end
end}

local ch_telefono = {function(player,choice) 
  local user_id = vRP.getUserId({player})
  if user_id ~= nil then
  vRPclient.getNearestPlayer(player,{10},function(nplayer)
  local nuser_id = vRP.getUserId({nplayer})
  if nuser_id ~= nil then
  vRP.request({nplayer,"Il player ti vuole sequestrare i dispositivi elettronici?", 30, function(nplayer,ok)
  if ok then
  vRP.removeUserGroup({nuser_id,"telefono"})
  vRPclient.notify(nplayer,{"~b~Vai alla centrale di polizia per ritirare il cellulare."})
  vRPclient.notify(player,{"~b~Il player ti ha dato i dispositivi elettronici."})
  else
  vRPclient.notify(player,{"~r~Il player si è ribellato."})
  end
  end})
  else vRPclient.notify(player,{lang.common.no_player_near()})
  end
  end)
  end
end,"Sequestra dispositivi elettronici"}

local ch_delbody = {function(player,choice) 
  local user_id = vRP.getUserId({player})
  if user_id ~= nil then
  BMclient.deletebody(player)
end
end}--  choice_telecamera
--           vRPbm.setBombola(vest)
local ch_telecamera = {function(player,choice) 
local user_id = vRP.getUserId({player})
  if user_id ~= nil then
  local src = player
  TriggerClientEvent("Cam:ToggleCam", src)
  end
end}

local ch_ombrello = {function(player,choice) 
  local user_id = vRP.getUserId({player})
    if user_id ~= nil then
      local src = player
      TriggerClientEvent("LSLI:ToggleUmbrella", src)
   end
end}

local ch_binocolo = {function(player,choice) 
  local user_id = vRP.getUserId({player})
    if user_id ~= nil then
      local src = player
      TriggerClientEvent("LSLI:ToggleBinocolo", src)
   end
end}

local choice_toglifascetta = {function(player,choice) 
  local user_id = vRP.getUserId({player})
    if user_id ~= nil then
      BMclient.ContrColtMano(player,{}, function(coltello)
        if coltello then
          vRPclient.getNearestPlayer(player,{10},function(nplayer)
			local nuser_id = vRP.getUserId({nplayer})
            if nuser_id ~= nil then
              vRPclient.isHandcuffed(nplayer,{}, function(handcuffed)  -- check handcuffed
                if handcuffed then        
                  TriggerClientEvent("dr:drag", nplayer, player)
                  vRPclient.playAnim(player,{false,{{"mp_arresting","a_uncuff",1}},false})
                  vRPclient.playAnim(nplayer,{false,{{"mp_arresting","b_uncuff",1}},false})
                  SetTimeout(4200, function()
                    TriggerClientEvent("dr:undrag", nplayer, player)
                    vRPclient.toggleHandcuff(nplayer,{})
                  end)
                else
                  vRPclient.notify(player,{"~r~Il player non è ammanettato"})
                end
              end)
            else
              vRPclient.notify(player,{"~r~Nessun player vicino"})
            end
          end)
        else
          vRPclient.notify(player,{"~r~Non hai niente di tagliente"})
        end
      end)
   end
end}

local choice_rouletterussa = {function(player,choice) 
  local user_id = vRP.getUserId({player})
    if user_id ~= nil then
      BMclient.ContrRevMano(player,{}, function(rev)
        if rev then
          local x = math.random(1,20)
          if x == 3 then
            BMclient.RouletteRussa(player,{true})
            SetTimeout(2100, function()
              BMclient.PushHeadpistol(player)
              vRPclient.setHealth(player,{5})
            end)
          else
            --BMclient.RouletteRussa(player,{false})
            vRPclient.notify(player,{"~r~Salvo"})
          end
        else
          vRPclient.notify(player,{"~r~Non hai il revolver in mano"})
        end
      end)
    end
end}

local ch_bombola = {function(player,choice) 
  local user_id = vRP.getUserId({player})
    if user_id ~= nil then
    BMclient.setBombola(player,{true})
    vRPclient.notify(player,{"~r~RICORDATI CHE ESSERE IN ACQUA PER INDOSSARE LA BOMBOLA"})
  end
end}

local ch_velmetallo = {function(player,choice) 
  local user_id = vRP.getUserId({player})
    if user_id ~= nil then
    BMclient.PrendiValigettaMetallo(player)
  end
end}

local ch_velpelle = {function(player,choice) 
  local user_id = vRP.getUserId({player})
    if user_id ~= nil then
    BMclient.PrendiValigettaPelle(player)
  end
end}

local ch_notizia = {function(player,choice) 
  local user_id = vRP.getUserId({player})
  if user_id ~= nil then
    --TriggerClientEvent("alert:Send", source, "POTATO22")
    vRP.prompt({player,"Testo:","",function(player,textin) 
      if textin ~= nil and textin ~= "" then 
        local options = {
          text = textin,
          type = "warning",
          timeout = 35000,
          layout = "centerRight",
          queue = "right"
        }
        TriggerClientEvent("pNotify:SendNotification", -1, options)
      else
        vRPclient.notify(player,{"~r~Non ha scritto niente.."})
      end
    end})
  end
end,"Manda annuncio."}

local ch_microfono = {function(player,choice) 
local user_id = vRP.getUserId({player})
  if user_id ~= nil then
  local src = player
  TriggerClientEvent("Mic:ToggleMic", src)
  end
end}

local ch_bmicrofono = {function(player,choice) 
  local user_id = vRP.getUserId({player})
  if user_id ~= nil then
    TriggerClientEvent("Mic:ToggleBMic", player)
  end
end}

local choice_hostage = {function(player,choice) 
  local user_id = vRP.getUserId({player})
  if user_id ~= nil then
    TriggerEvent("HP:StartHostage",player)
  end
end}

--[[local ch_rompipenna = {function(player,choice) 
local user_id = vRP.getUserId({player})
  if user_id ~= nil then
    if vRP.tryGetInventoryItem({user_id, "pennablu_2", 1, true}) then
      --vRPclient.playAnim(player,{false,{{"@indirizzo","animazione",1}},false})
      vRP.giveInventoryItem({user_id, "methblu", 2, true})
    end
  end
end}]]--
---------------------------------------------------------------

local ch_maschere_1_salva = {function(player,choice) 
  local user_id = vRP.getUserId({player})
  if user_id ~= nil then
  BMclient.SalvaCurrentMaschera(player, {1})
  vRPclient.notify(player,{"~g~Slot 1 salvato"})
  end
end}

local ch_maschere_2_salva = {function(player,choice) 
  local user_id = vRP.getUserId({player})
  if user_id ~= nil then
  BMclient.SalvaCurrentMaschera(player, {2})
  vRPclient.notify(player,{"~g~Slot 2 salvato"})
  end
end}

local ch_maschere_3_salva = {function(player,choice) 
  local user_id = vRP.getUserId({player})
  if user_id ~= nil then
  BMclient.SalvaCurrentMaschera(player, {3})
  vRPclient.notify(player,{"~g~Slot 3 salvato"})
  end
end}
-----------------------------------------------------------------------
local ch_maschere_1_equip = {function(player,choice) 
  local user_id = vRP.getUserId({player})
  if user_id ~= nil then
    BMclient.updatemascheraslotclient(player, {1})
    vRPclient.notify(player,{"~b~Slot 1 equipaggiato"})
  end
end}

local ch_maschere_2_equip = {function(player,choice) 
  local user_id = vRP.getUserId({player})
  if user_id ~= nil then
    BMclient.updatemascheraslotclient(player, {2})
    vRPclient.notify(player,{"~b~Slot 2 equipaggiato"})
  end
end}

local ch_maschere_3_equip = {function(player,choice) 
  local user_id = vRP.getUserId({player})
  if user_id ~= nil then
    BMclient.updatemascheraslotclient(player, {3})
    vRPclient.notify(player,{"~b~Slot 3 equipaggiato"})
  end
end}



-----------------------------------------------------------------------

local choice_maschere = {function(player,choice)
	local user_id = vRP.getUserId({player})
	local menu = {}
	menu.name = "Maschere"
	menu.css = {top = "75px", header_color = "rgba(0,0,255,0.75)"}
    menu.onclose = function(player) vRP.openMainMenu({player}) end -- nest menu
	
    	
	if vRP.hasPermission({user_id,"player.loot"}) then
    menu["Salva M.1"] = ch_maschere_1_salva -- Send a nearby handcuffed player to jail with prompt for choice and user_list
    end
  
  if vRP.hasPermission({user_id,"player.loot"}) then
    menu["Salva M.2"] = ch_maschere_2_salva -- Send a nearby handcuffed player to jail with prompt for choice and user_list
  end
 	  
--  if vRP.hasPermission({user_id,"player.loot"}) then
--    menu["Salva slot 3"] = ch_maschere_3_salva -- Send a nearby handcuffed player to jail with prompt for choice and user_list
--  end
  
  
	if vRP.hasPermission({user_id,"player.loot"}) then
    menu["Equipaggia M.1"] = ch_maschere_1_equip -- Send a nearby handcuffed player to jail with prompt for choice and user_list
    end
  
  if vRP.hasPermission({user_id,"player.loot"}) then
    menu["Equipaggia M.2"] = ch_maschere_2_equip -- Send a nearby handcuffed player to jail with prompt for choice and user_list
  end
 	  
--  if vRP.hasPermission({user_id,"player.loot"}) then
--    menu["Equip slot 3"] = ch_maschere_3_equip -- Send a nearby handcuffed player to jail with prompt for choice and user_list
--  end
 
	vRP.openMenu({player, menu})
end}


local player_azioni = {function(player,choice)
	local user_id = vRP.getUserId({player})
	local menu = {}
		menu.name = "Azioni"
		menu.css = {top="75px",header_color="rgba(99,34,0,0.90)"}
    menu.onclose = function(player) vRP.openMainMenu({player}) end
    

    
--
    if vRP.hasPermission({user_id,"giornalista.service"}) then
      menu["Telecamera"] = ch_telecamera -- transforms money in wallet to money in inventory to be stored in houses and cars
    end

    if vRP.hasPermission({user_id,"giornalista.service"}) then
      menu["Microfono"] = ch_microfono -- transforms money in wallet to money in inventory to be stored in houses and cars
    end

    if vRP.hasPermission({user_id,"giornalista.service"}) then
      menu["Giraffa"] = ch_bmicrofono -- transforms money in wallet to money in inventory to be stored in houses and cars
    end

    if vRP.hasPermission({user_id,"giornalista.service"}) then
      menu["Annuncio"] = ch_notizia -- transforms money in wallet to money in inventory to be stored in houses and cars
    end
--
    if vRP.hasPermission({user_id,"mission.delivery.fish"}) then
      menu["Bombola d'ossigeno"] = ch_bombola -- transforms money in wallet to money in inventory to be stored in houses and cars
    end
  
    if vRP.hasPermission({user_id,"player.store_money"}) then
      menu["Valigetta di metallo"] = ch_velmetallo -- transforms money in wallet to money in inventory to be stored in houses and cars
    end

    if vRP.hasPermission({user_id,"player.store_money"}) then
      menu["Valigetta di pelle"] = ch_velpelle -- transforms money in wallet to money in inventory to be stored in houses and cars
    end

		vRP.openMenu({player,menu})  
end}

local ch_player_menu = {function(player,choice)
	local user_id = vRP.getUserId({player})
	local menu = {}
	menu.name = "Player"
	menu.css = {top = "75px", header_color = "rgba(0,0,255,0.75)"}
    menu.onclose = function(player) vRP.openMainMenu({player}) end -- nest menu choice_togliphone

    if vRP.hasPermission({user_id,"player.store_money"}) then
      menu["Taglia fascetta"] = choice_toglifascetta -- transforms money in wallet to money in inventory to be stored in houses and cars
    end

    if vRP.hasPermission({user_id,"player.store_money"}) then
      menu["Sequestra telefono"] = ch_telefono -- transforms money in wallet to money in inventory to be stored in houses and cars
    end
	
  --  if vRP.hasPermission({user_id,"player.store_money"}) then
  --    menu["Roulette Russa"] = choice_rouletterussa -- transforms money in wallet to money in inventory to be stored in houses and cars
  --  end	
	    
    if vRP.hasPermission({user_id,"player.loot"}) then
      menu["Azioni"] = player_azioni -- transforms money in wallet to money in inventory to be stored in houses and cars
    end

    if vRP.hasPermission({user_id,"player.fix_haircut"}) then
      menu["Fixa i capelli"] = ch_fixhair -- just a work around for barbershop green hair bug while I am busy
    end
	
 --   if vRP.hasPermission({user_id,"player.noclip"}) then---Togliere il NO per riattivare
 --     menu["Lista utenti"] = ch_userlist -- a user list for players with vRP ids, player name and identity names only.
 --   end
	
    if vRP.hasPermission({user_id,"player.store_weapons"}) then
      menu["Immagazzina armi"] = choice_store_weapons -- store player weapons, like police store weapons from vrp
    end
	
	if vRP.hasPermission({user_id,"player.store_weapons"}) then ---ch_player_menu
      menu["Immagazzina soldi"] = choice_store_money -- transforms money in wallet to money in inventory to be stored in houses and cars
  end
	
    if vRP.hasPermission({user_id,"player.store_armor"}) then
      menu["Conserva le armature"] = choice_store_armor -- store player armor
    end
	
    if vRP.hasPermission({user_id,"player.check"}) then
      menu["Ispeziona"] = choice_player_check -- checks nearest player inventory, like police check from vrp
    end
    
    if vRP.hasPermission({user_id,"player.loot"}) then
      menu["Loot"] = choice_loot -- take the items of nearest player in coma
    end
	
    if vRP.hasPermission({user_id,"player.loot"}) then
      menu["Maschere"] = choice_maschere -- take the items of nearest player in coma
    end

	vRP.openMenu({player, menu})
end}

-- REGISTER MAIN MENU CHOICES
vRP.registerMenuBuilder({"main", function(add, data)
  local user_id = vRP.getUserId({data.player})
  if user_id ~= nil then
    local choices = {}
	
    if vRP.hasPermission({user_id,"gang.permessi"}) then
      choices["Illegale"] = ch_illegale -- opens player submenu
    end	
	
    if vRP.hasPermission({user_id,"player.player_menu"}) then
      choices["Player"] = ch_player_menu -- opens player submenu
    end

    add(choices)
  end
end})

local choice_putinveh = {function(player,choice)
  vRPclient.getNearestPlayer(player,{10},function(nplayer)
    local nuser_id = vRP.getUserId(nplayer)
    if nuser_id ~= nil then
      vRPclient.isHandcuffed(nplayer,{}, function(handcuffed)  -- check handcuffed
        if handcuffed then
          vRPclient.putInNearestVehicleAsPassenger(nplayer, {5})
        else
          vRPclient.notify(player,{lang.police.not_handcuffed()})
        end
      end)
    else
      vRPclient.notify(player,{lang.common.no_player_near()})
    end
  end)
end,lang.police.menu.putinveh.description()}

local choice_getoutveh = {function(player,choice)
  vRPclient.getNearestPlayer(player,{10},function(nplayer)
    local nuser_id = vRP.getUserId(nplayer)
    if nuser_id ~= nil then
      vRPclient.isHandcuffed(nplayer,{}, function(handcuffed)  -- check handcuffed
        if handcuffed then
          vRPclient.ejectVehicle(nplayer, {})
        else
          vRPclient.notify(player,{lang.police.not_handcuffed()})
        end
      end)
    else
      vRPclient.notify(player,{lang.common.no_player_near()})
    end
  end)
end,lang.police.menu.getoutveh.description()}

local ch_forza = {function(player,choice) 
  local user_id = vRP.getUserId({player})
  if user_id ~= nil then
    vRPclient.isHandcuffed(player,{}, function(handcuffed)  -- check handcuffed
      if not handcuffed then
        vRPclient.getNearestPlayer(player,{5},function(nplayer)
          local nuser_id = vRP.getUserId({nplayer})
          if nuser_id ~= nil then
            vRPclient.notify(player,{"~r~Ricorda che dura solo 60 secondi.."})
            TriggerClientEvent("dr:drag2", nplayer, player)
            vRPclient.playAnim(player,{true,{{"switch@trevor@under_pier","loop_trevor",1}},true})
            vRPclient.playAnim(nplayer,{false,{{"switch@trevor@under_pier","loop_ped",1}},true})
            SetTimeout(60000, function()
              vRPclient.stopAnim(nplayer,{true})
              vRPclient.stopAnim(nplayer,{false})
              TriggerClientEvent("dr:undrag2", nplayer, player)
              vRPclient.stopAnim(player,{false})
              SetTimeout(2000, function() 
                vRPclient.stopAnim(player,{true})
              end)
            end)
          end
         end)
        else
        vRPclient.notify(player,{"~r~Sei con le manette.."})
      end
    end)
  end
end}

local ch_spezzacollo = {function(player,choice) 
  local user_id = vRP.getUserId({player})
  if user_id ~= nil then
    vRPclient.getNearestPlayer(player,{2},function(nplayer)
      local nuser_id = vRP.getUserId({nplayer})
      if nuser_id ~= nil then
        TriggerClientEvent("dr:drag", nplayer, player)
        vRPclient.playAnim(player,{true,{{"mp_common_heist","grab_kill_drop",1}},false})
        vRPclient.playAnim(nplayer,{false,{{"mp_common_heist","hit_grab_kill_drop",1}},false})
        TriggerClientEvent("dr:undrag", nplayer, player)
        SetTimeout(1900, function()
          vRPclient.setHealth(nplayer,{5})
        end)
      end
    end)
  end
end}

local ch_poterimenu = {function(player,choice)
	local user_id = vRP.getUserId({player})
	local menu = {}
	menu.name = "Poteri"
	menu.css = {top = "75px", header_color = "rgba(0,0,255,0.75)"}
  menu.onclose = function(player) vRP.openPoliceMenu({player}) end-- nest menu
	
    	
	if vRP.hasPermission({user_id,"admin.deleteveh"}) then
    menu["La forza"] = ch_forza -- Send a nearby handcuffed player to jail with prompt for choice and user_list
  end

  if vRP.hasPermission({user_id,"admin.deleteveh"}) then
    menu["Spezzacollo"] = ch_spezzacollo -- Send a nearby handcuffed player to jail with prompt for choice and user_list
  end

	vRP.openMenu({player, menu})
end}

-- RESGISTER ADMIN MENU CHOICES
vRP.registerMenuBuilder({"admin", function(add, data)
  local user_id = vRP.getUserId({data.player})
  if user_id ~= nil then
    local choices = {}--ch_cavalcamucca
	
  if vRP.hasPermission({user_id,"admin.deleteveh"}) then
    choices["Poteri"] = ch_poterimenu -- Delete nearest vehicle (Fixed pull request https://github.com/Sighmir/vrp_basic_menu/pull/11/files/419405349ca0ad2a215df90cfcf656e7aa0f5e9c from benjatw)
  end

  if vRP.hasPermission({user_id,"admin.deleteveh"}) then
    choices[".Elimina veicolo"] = ch_deleteveh -- Delete nearest vehicle (Fixed pull request https://github.com/Sighmir/vrp_basic_menu/pull/11/files/419405349ca0ad2a215df90cfcf656e7aa0f5e9c from benjatw)
  end

	if vRP.hasPermission({user_id,"admin.spawnveh"}) then
      choices["Spawna veicolo"] = ch_spawnveh -- Spawn a vehicle model
	end
	
	if vRP.hasPermission({user_id,"admin.godmode"}) then
      choices["Vita infinita"] = ch_godmode -- Toggles admin godmode (Disable the default admin.god permission to use this!) 
	end
	
  if vRP.hasPermission({user_id,"player.blips"}) then
    choices["Blip"] = ch_blips -- turn on map blips and sprites
  end
	
  if vRP.hasPermission({user_id,"player.sprites"}) then
    choices["Sprites"] = ch_sprites -- turn on only name sprites
  end

	if vRP.hasPermission({user_id,"player.tptowaypoint"}) then
      choices["Teletrasportati al marker"] = choice_tptowaypoint -- teleport user to map blip
	end

	if vRP.hasPermission({user_id,"admin.spikes"}) then
    choices["BodyGuard"] = ch_body -- Toggle spikes
  end
	
	if vRP.hasPermission({user_id,"admin.spikes"}) then
    choices["Del-Bodyguard"] = ch_delbody -- Toggle spikes
  end	
    add(choices)
  end
end})

local ch_venite = {function(player,choice)
  local user_id = vRP.getUserId({player})
  if user_id ~= nil then
    vRPclient.playAnim(player,{true,{{"swat","come",1}},false})
  end
end}

local ch_fermi = {function(player,choice)
  local user_id = vRP.getUserId({player})
  if user_id ~= nil then
    vRPclient.playAnim(player,{true,{{"swat","freeze",1}},false})
  end
end}

local ch_andavan = {function(player,choice)
  local user_id = vRP.getUserId({player})
  if user_id ~= nil then
    vRPclient.playAnim(player,{true,{{"swat","go_fwd",1}},false})
  end
end}

local ch_regroup = {function(player,choice)
  local user_id = vRP.getUserId({player})
  if user_id ~= nil then
    vRPclient.playAnim(player,{true,{{"swat","rally_point",1}},false})
  end
end}

local ch_capito = {function(player,choice)
  local user_id = vRP.getUserId({player})
  if user_id ~= nil then
    vRPclient.playAnim(player,{true,{{"swat","understood",1}},false})
  end
end}

local ch_vaidietro = {function(player,choice)
  local user_id = vRP.getUserId({player})
  if user_id ~= nil then
    vRPclient.playAnim(player,{true,{{"swat","you_back",1}},false})
  end
end}

local ch_vaiavanti = {function(player,choice)
  local user_id = vRP.getUserId({player})
  if user_id ~= nil then
    vRPclient.playAnim(player,{true,{{"swat","you_fwd",1}},false})
  end
end}

local ch_vaisinistra = {function(player,choice)
  local user_id = vRP.getUserId({player})
  if user_id ~= nil then
    vRPclient.playAnim(player,{true,{{"swat","you_left",1}},false})
  end
end}

local ch_vaidestra = {function(player,choice)
  local user_id = vRP.getUserId({player})
  if user_id ~= nil then
    vRPclient.playAnim(player,{true,{{"swat","you_right",1}},false})--
  end
end}

local ch_salutomil = {function(player,choice)
  local user_id = vRP.getUserId({player})
  if user_id ~= nil then
    vRPclient.playAnim(player,{true,{{"mp_player_int_uppersalute","mp_player_int_salute",1}},false})-- "mp_player_int_uppersalute","mp_player_int_salute"
  end
end}
--- "mazza.anim"
local ch_polianim = {function(player,choice)
	local user_id = vRP.getUserId({player})
	local menu = {}
	menu.name = "Animazioni"
	menu.css = {top = "75px", header_color = "rgba(23,44,0,0.75)"}
  menu.onclose = function(player) vRP.openPoliceMenu({player}) end
	
    	
	if vRP.hasPermission({user_id,"police.easy_jail"}) then
    menu["01 - VENITE"] = ch_venite-- Send a nearby handcuffed player to jail with prompt for choice and user_list
    end
  
  if vRP.hasPermission({user_id,"police.easy_jail"}) then
    menu["02 - STOP"] = ch_fermi -- Send a nearby handcuffed player to jail with prompt for choice and user_list
  end
 	  
  if vRP.hasPermission({user_id,"police.easy_jail"}) then
    menu["03 - LET'S GO"] = ch_andavan -- Send a nearby handcuffed player to jail with prompt for choice and user_list
  end

	if vRP.hasPermission({user_id,"police.easy_jail"}) then
    menu["04 - RE-GROUP"] = ch_regroup -- Un jails chosen player if he is jailed (Use admin.easy_unjail as permission to have this in admin menu working in non jailed players)
  end

  if vRP.hasPermission({user_id,"police.easy_jail"}) then
    menu["05 - OK!"] = ch_capito -- Un jails chosen player if he is jailed (Use admin.easy_unjail as permission to have this in admin menu working in non jailed players)
  end

  if vRP.hasPermission({user_id,"police.easy_jail"}) then
    menu["06 - DIETRO"] = ch_vaidietro -- Un jails chosen player if he is jailed (Use admin.easy_unjail as permission to have this in admin menu working in non jailed players)
  end

  if vRP.hasPermission({user_id,"police.easy_jail"}) then
    menu["07 - AVANTI"] = ch_vaiavanti -- Un jails chosen player if he is jailed (Use admin.easy_unjail as permission to have this in admin menu working in non jailed players)
  end

  if vRP.hasPermission({user_id,"police.easy_jail"}) then
    menu["08 - LEFT!"] = ch_vaisinistra -- Un jails chosen player if he is jailed (Use admin.easy_unjail as permission to have this in admin menu working in non jailed players)
  end
  
  if vRP.hasPermission({user_id,"police.easy_jail"}) then
    menu["09 - RIGHT!"] = ch_vaidestra -- Un jails chosen player if he is jailed (Use admin.easy_unjail as permission to have this in admin menu working in non jailed players)
  end

  if vRP.hasPermission({user_id,"police.easy_jail"}) then
    menu["10 - SALUTO!"] = ch_salutomil -- Un jails chosen player if he is jailed (Use admin.easy_unjail as permission to have this in admin menu working in non jailed players)
  end
	
	vRP.openMenu({player, menu})
end}

-- REGISTER POLICE MENU CHOICES
vRP.registerMenuBuilder({"police", function(add, data)
  local user_id = vRP.getUserId({data.player})
  if user_id ~= nil then
    local choices = {}-- ch_askchest

  
  if vRP.hasPermission({user_id,"police.easy_jail"}) then ---ch_player_menu
  choices["Comandi"] = ch_polianim -- transforms money in wallet to money in inventory to be stored in houses and cars
  end

	if vRP.hasPermission({user_id,"police.easy_fine"}) then
     choices["Multa"] = ch_fine -- Fines closeby player
  end
  
	if vRP.hasPermission({user_id,"police.easy_fine"}) then
     choices["Arresta"] = ch_jail -- Fines closeby player
  end  
 
	if vRP.hasPermission({user_id,"police.easy_fine"}) then
     choices["Arresta[PENITENZIARIO]"] = ch_jail2 -- Fines closeby player
  end   
	
	if vRP.hasPermission({user_id,"police.easy_fine"}) then--ch_polweapons
     choices["Ammanetta"] = ch_handcuff -- Toggle cuffs AND CLOSE MENU for nearby player
  end
	
	if vRP.hasPermission({user_id,"police.2spikes"}) then
      choices["Strisce chiodate"] = ch_spikes -- Toggle spikes
    end
	
	if vRP.hasPermission({user_id,"police.freeze"}) then -- Freezes a player in position
      choices["Blocca/freeze"] = choice_freeze
    end
	
  if vRP.hasPermission({user_id,"police.drag"}) then
    choices["Trasporta player"] = ch_drag -- Drags closest handcuffed player
  end
      
    add(choices)
  end
end})

local ch_illegale = {function(player,choice)
	local user_id = vRP.getUserId({player})
	local menu = {}
	menu.name = "Illegale"
	menu.css = {top = "75px", header_color = "rgba(23,44,0,0.75)"}
    menu.onclose = function(player) vRP.openMainMenu({player}) end -- nest menu
	
    	
         if vRP.hasPermission(user_id,"gang.permessi") then
            menu[lang.police.menu.handcuff.title()] = ch_handcuff
          end
		  
          if vRP.hasPermission(user_id,"gang.permessi") then
            menu[lang.police.menu.drag.title()] = ch_drag
          end			  

          if vRP.hasPermission(user_id,"gang.permessi") then
            menu[lang.police.menu.putinveh.title()] = choice_putinveh
          end

          if vRP.hasPermission(user_id,"gang.permessi") then
            menu[lang.police.menu.getoutveh.title()] = choice_getoutveh
          end
	
	vRP.openMenu({player, menu})
end}

-- vRP.registerMenuBuilder({"phone", function(add) 
--    local choices = {} 
	
--    choices["MPay"] = ch_mobilepay -- transfer money through phone
	
--    add(choices)
-- end}) 



