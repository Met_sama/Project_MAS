MySQL = module("vrp_mysql", "MySQL")
local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")
local htmlEntities = module("vrp", "lib/htmlEntities")

vRPbu = {}
vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP","vRP_business")
BUclient = Tunnel.getInterface("vRP_business","vRP_business")
Tunnel.bindInterface("vRP_business",vRPbu)


MySQL.createCommand("vRP/busi_tables",[[
CREATE TABLE IF NOT EXISTS vrp_business(
    business VARCHAR(255),
	user_id INTEGER,
	CONSTRAINT pk_business PRIMARY KEY(business)
);
]])

MySQL.createCommand("vRP/get_new_business","SELECT user_id FROM vrp_business WHERE business = @business")
MySQL.createCommand("vRP/set_new_business","UPDATE vrp_business SET user_id = @user_id WHERE business = @business")
MySQL.createCommand("vRP/insert_user_business","INSERT IGNORE INTO vrp_business(business,user_id) VALUES(@business,@user_id)")

MySQL.execute("vRP/busi_tables")

local blips = {
--	["concessionario"] = {title="Concessionario Santa Claus Vergine", prezzo= 1500000, gruppo= "Titolare del Concessionario", x=-32.658653259277,y=-1106.7951660156,z=26.422348022461},
	
	["burgershot"] = {title="Burger Shot", prezzo= 350000, gruppo= "Burger Shot", x=-1183.3422851562,y=-884.23431396484,z=13.742436408996},
		
	["bennys"] = {title="Bennys", prezzo= 750000, gruppo= "Bennys", x=-205.43008422852,y=-1308.2648925782,z=31.291624069214}
			
--	["armeriadowntown"] = {title="Armeria Downtown", prezzo= 1500000, gruppo= "Titolare dell' ArmeriaDowntown", x=14.07980632782,y=-1106.1878662109,z=29.797027587891}
}

for nome,_ in pairs(blips) do
	local user_id = 0
	MySQL.execute("vRP/insert_user_business", {business = nome, user_id = json.encode(user_id)})
end

function vRPbu.ControlloMoney(name,titolo,gruppo,prezzo)
	local user_id = vRP.getUserId({source})
	local player = vRP.getUserSource({user_id})
	local gruppov = vRP.getUserGroupByType({user_id,"business"})
	if gruppov == "" then 
	  GetStato(name, function(p_id)
		if p_id ~= user_id then
		   if p_id == 0 then
			local titolo = blips[name].title
			local gruppo = blips[name].gruppo
			local prezzo = blips[name].prezzo
        	if vRP.tryBankMoney({user_id,prezzo}) then
				vRP.addUserGroup({user_id,gruppo})
			--	TriggerClientEvent('chatMessage', -1, 'BUSINESS', { 30, 70, 255 },"^1"..GetPlayerName(player).."^2 è diventato ^3"..gruppo.."^2 per ^1"..prezzo.."^2$")
				print(GetPlayerName(player).." è diventato "..gruppo.." per "..prezzo.."$")
				SetStato(name,user_id)
				vRPclient.notify(player,{"Congratulazioni per aver comprato ~b~"..titolo.." ~h~ricorda di pagare le spese varie!."})
	  		 else
			 vRPclient.notify(player,{"~r~Non hai abbastanza soldi"})
	  		end
			else
		  vRPclient.notify(player,{"~r~Business già comprato"})
		  end
		else
		vRPclient.notify(player,{"~r~Questo business è già tuo"})
	 end
  end)
else
	vRPclient.notify(player,{"~r~Sei già "..gruppov..""})
end
end

function GetStato(business, cbr)
	local task = Task(cbr, {false})
  
	MySQL.query("vRP/get_new_business", {business = business}, function(rows, affected)
	  if #rows > 0 then
		task({rows[1].user_id})
	  else
		task()
	  end
	end)
end
  
function SetStato(business,user_id)
  MySQL.execute("vRP/set_new_business", {business = business, user_id = json.encode(user_id)})
end

function DisativaBusiness(user_id)
	local gruppo = vRP.getUserGroupByType({user_id,"business"})
	local name = vRP.getGroupName({gruppo})
	SetStato(name,0)
end

RegisterServerEvent('paycheck:tax')
AddEventHandler('paycheck:tax', function()
		local user_id = vRP.getUserId({source})
		
--burgerking		
	if vRP.hasPermission({user_id,"burgershot.business"}) then
		if vRP.tryBankMoney({user_id, 500}) then
			vRPclient.notify(source,{"Spese: $500"})
		else
			vRPclient.notify(source,{"~r~Non puoi pagare le spese, ~b~hai appena perso il tuo business..."})	
			DisativaBusiness(user_id)
			vRP.removeUserGroup({user_id,"Burger Shot"})
		end	
	end	
--bennys	
	if vRP.hasPermission({user_id,"bennys.business"}) then
		if vRP.tryBankMoney({user_id, 1500}) then
			vRPclient.notify(source,{"Spese: $500"})
		else
			vRPclient.notify(source,{"~r~Non puoi pagare le spese, ~b~hai appena perso il tuo business..."})	
			DisativaBusiness(user_id)
			vRP.removeUserGroup({user_id,"Burger Shot"})
		end	
	end
end)

local choice_vendibusiness = {function(player,choice) 
	local user_id = vRP.getUserId({player})
	if user_id ~= nil then
	  local gruppo = vRP.getUserGroupByType({user_id,"business"})
		  vRPclient.getNearestPlayers(player,{15},function(nplayers) 
			  local user_list = ""
				  for k,v in pairs(nplayers) do
				  user_list = user_list .. "[" .. vRP.getUserId({k}) .. "]" .. GetPlayerName(k) .. " | "
				  end 
			  if user_list ~= "" then
				  vRP.prompt({player,"Player vicini:" .. user_list,"",function(player,target_id) 
					  if target_id ~= nil and target_id ~= "" then 
						  local target = vRP.getUserSource({tonumber(target_id)})
						  if target ~= nil then
							vRP.prompt({player,"Prezzo da offrire:","",function(player,costo2) 
								local costo = tonumber(costo2)
							  	vRP.request({target, "Il player "..GetPlayerName(player).." ti vuole vendere il suo business ("..gruppo..") per "..costo, 60, function(nplayer,ok)
								  if ok then 
									if vRP.tryBankMoney({target_id, costo}) then
									  vRP.removeUserGroup({user_id,gruppo})
									  vRP.addUserGroup({tonumber(target_id),gruppo})
									  vRPclient.notify(player,{"~g~Il player ha accettato"})
									  vRPclient.notify(target,{"~g~Complimenti per l'acquisto.."})
									else
									  vRPclient.notify(target,{"~r~Non hai abbastanza soldi.."})
									  vRPclient.notify(player,{"~r~Il player non ha abbastanza soldi.."})
									end
								  else
									  vRPclient.notify(player,{"~r~Il player ha rifiutato"})
								  end
								end})
							end})
						end
					end
				end})
			  else
			  vRPclient.notify(player,{"~r~Nessun player vicino.."})
			end
		end)
	end
  end}

local choice_abbandona = {function(player,choice) 
  local user_id = vRP.getUserId({player})
  if user_id ~= nil then
		vRP.request({player, "Sei sicuro di voler chiudere il tuo business?", 60, function(player,ok)
			if ok then 
			local gruppo = vRP.getUserGroupByType({user_id,"business"})
			local name = vRP.getGroupName({gruppo})
				vRP.removeUserGroup({user_id,gruppo})
				SetStato(name,0)
				vRP.openMainMenu({player})
			end
		end})
  end
end}

local business_menu = {function(player,choice)
	local user_id = vRP.getUserId({player})
	local menu = {}
		menu.name = "Business"
		menu.css = {top="75px",header_color="rgba(0,0,0,0.90)"}
		menu.onclose = function(player) vRP.openMainMenu({player}) end

			if vRP.hasPermission({user_id,"propr.aut"}) then
				menu["Chiudi business"] = choice_abbandona
			end

			if vRP.hasPermission({user_id,"propr.aut"}) then
				menu["Vendi business"] = choice_vendibusiness
			end

		vRP.openMenu({player,menu})
end}

vRP.registerMenuBuilder({"main", function(add, data)
  local player = data.player

  local user_id = vRP.getUserId({player})
  if user_id ~= nil then
    local choices = {}

    if vRP.hasPermission({user_id,"propr.aut"}) then
      choices["Business"] = business_menu
		end
		
		add(choices)
  end
end})


AddEventHandler("vRP:playerSpawn",function(user_id, source, first_spawn)
	if first_spawn then
		for nome, data in pairs(blips) do
			BUclient.AggBlip(source,{data.x,data.y,data.z,375,60,data.title})
		end
	end
end)
