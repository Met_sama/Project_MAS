vRPbu = {}
Tunnel.bindInterface("vRP_business",vRPbu)
vRPserver = Tunnel.getInterface("vRP","vRP_business")
BUserver = Tunnel.getInterface("vRP_business","vRP_business")
vRP = Proxy.getInterface("vRP")

local blips = {
	["burgershot"] = {title="Burger Shot", prezzo= 350000, gruppo= "Burger Shot", x=-1183.3422851562,y=-884.23431396484,z=13.742436408996},
	["bennys"] = {title="Bennys", prezzo= 750000, gruppo= "Bennys", x=-205.43008422852,y=-1308.2648925782,z=31.291624069214}
}

function vRPbu.AggBlip(x,y,z,id,colour,title)
    local blip = AddBlipForCoord(x, y, z)
    SetBlipSprite(blip, id)
	SetBlipScale(blip, 0.8)
    SetBlipDisplay(blip, 4)
	SetBlipColour(blip, colour)
    SetBlipAsShortRange(blip, true)
	BeginTextCommandSetBlipName("STRING")
    AddTextComponentString(title)
    EndTextCommandSetBlipName(blip)
end


Citizen.CreateThread(function ()
	while true do
		Citizen.Wait(0)
		for nome, data in pairs(blips) do
			if GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)), data.x,data.y,data.z, true ) < 1 then
				DrawSpecialText("Premi [~g~E~s~] per comprare "..data.title.." al prezzo di ~r~"..data.prezzo.."$")
				if(IsControlJustPressed(1, 38)) then
					BUserver.ControlloMoney({nome})
				end
			end
		end
	end
end)

function DrawSpecialText(m_text, showtime)
	SetTextEntry_2("STRING")
	AddTextComponentString(m_text)
	DrawSubtitleTimed(showtime, 1)
end