local cfg = module("cfg/casse")
local casse = cfg.casse

local function build_client_chest(source)
  local user_id = vRP.getUserId(source)
  if user_id ~= nil then
    for k,v in pairs(casse) do
      local nome,perm,x,y,z = table.unpack(v)

      local function chestcom_enter()
        local user_id = vRP.getUserId(source)
        if user_id ~= nil then
          if vRP.hasPermission(user_id,perm) then
            vRP.openChest(source, nome.."_comune", 60000,false,nil,nil)
          end
        end
      end

      local function chestcom_leave()
        vRP.closeMenu(source)
        vRP.closeMenu(source)
        SetTimeout(200, function()
          vRP.closeMenu(source)
          vRP.closeMenu(source)
        end)
      end

      --vRPclient.addBlip(source,{x,y,z,351,46,"Cassa Comune"})
      vRPclient.addMarker(source,{x,y,z-1,0.7,0.7,0.5,0,255,125,125,150})

      vRP.setArea(source,"vRP:chestcomune"..k,x,y,z,2,1.5,chestcom_enter,chestcom_leave,true)
    end
  end
end

AddEventHandler("vRP:playerSpawn",function(user_id, source, first_spawn)
  if first_spawn then
    build_client_chest(source)
  end
end)

