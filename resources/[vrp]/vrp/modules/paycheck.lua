local Proxy = module("vrp", "lib/Proxy")

RegisterServerEvent('paycheck:salary')
AddEventHandler('paycheck:salary', function()
  	local user_id = vRP.getUserId(source)
	--Polizia
	if vRP.hasPermission(user_id,"police.stipendiorecluta") then
		vRP.giveBankMoney(user_id,450)
		vRPclient.notifyPicture(source,{"CHAR_BLANK_ENTRY",1,"Centrale di Polizia",false,"Payday: ~g~$450"})
	end		
	if vRP.hasPermission(user_id,"police.stipendioagente") then
		vRP.giveBankMoney(user_id,500)
		vRPclient.notifyPicture(source,{"CHAR_BLANK_ENTRY",1,"Centrale di Polizia",false,"Payday: ~g~$450"})
	end		
	if vRP.hasPermission(user_id,"police.stipendiosceriffo") then
		vRP.giveBankMoney(user_id,570)
		vRPclient.notifyPicture(source,{"CHAR_BLANK_ENTRY",1,"Centrale di Polizia",false,"Payday: ~g~$570"})
	end		
	if vRP.hasPermission(user_id,"police.stipendioufficiale") then
		vRP.giveBankMoney(user_id,650)
		vRPclient.notifyPicture(source,{"CHAR_BLANK_ENTRY",1,"Centrale di Polizia",false,"Payday: ~g~$650"})
	end			
	if vRP.hasPermission(user_id,"police.stipendiotenente") then
		vRP.giveBankMoney(user_id,700)
		vRPclient.notifyPicture(source,{"CHAR_BLANK_ENTRY",1,"Centrale di Polizia",false,"Payday: ~g~$800"})
	end		
	if vRP.hasPermission(user_id,"police.stipendiodetective") then
		vRP.giveBankMoney(user_id,900)
		vRPclient.notifyPicture(source,{"CHAR_BLANK_ENTRY",1,"Centrale di Polizia",false,"Payday: ~g~$900"})
	end			
	if vRP.hasPermission(user_id,"police.stipendiovicecapitano") then
		vRP.giveBankMoney(user_id,1600)
		vRPclient.notifyPicture(source,{"CHAR_BLANK_ENTRY",1,"Centrale di Polizia",false,"Payday: ~g~$1600"})
	end		
	if vRP.hasPermission(user_id,"police.stipendiocapitano") then
		vRP.giveBankMoney(user_id,1600)
		vRPclient.notifyPicture(source,{"CHAR_BLANK_ENTRY",1,"Centrale di Polizia",false,"Payday: ~g~$1600"})
	end			
	
	
    --Ems
	if vRP.hasPermission(user_id,"stipendio.infermiere") then
		vRP.giveBankMoney(user_id,300)
		vRPclient.notifyPicture(source,{"CHAR_BLANK_ENTRY",1,"Ospedale",false,"Payday: ~g~$300"})
	end
	if vRP.hasPermission(user_id,"stipendio.specializzando") then
		vRP.giveBankMoney(user_id,450)
		vRPclient.notifyPicture(source,{"CHAR_BLANK_ENTRY",1,"Ospedale",false,"Payday: ~g~$450"})
	end	
	if vRP.hasPermission(user_id,"stipendio.dottore") then
		vRP.giveBankMoney(user_id,600)
		vRPclient.notifyPicture(source,{"CHAR_BLANK_ENTRY",1,"Ospedale",false,"Payday: ~g~$600"})
	end		
	if vRP.hasPermission(user_id,"stipendio.direttoresanitario") then
		vRP.giveBankMoney(user_id,730)
		vRPclient.notifyPicture(source,{"CHAR_BLANK_ENTRY",1,"Ospedale",false,"Payday: ~g~$730"})
	end			
	if vRP.hasPermission(user_id,"stipendio.direttoresanitario") then
		vRP.giveBankMoney(user_id,850)
		vRPclient.notifyPicture(source,{"CHAR_BLANK_ENTRY",1,"Ospedale",false,"Payday: ~g~$850"})
	end		
	if vRP.hasPermission(user_id,"stipendio.direttoregenerale") then
		vRP.giveBankMoney(user_id,1000)
		vRPclient.notifyPicture(source,{"CHAR_BLANK_ENTRY",1,"Ospedale",false,"Payday: ~g~$1000"})
	end	
	
	
	if vRP.hasPermission(user_id,"weazel.mic") then
		vRP.giveBankMoney(user_id,300)
		vRPclient.notifyPicture(source,{"CHAR_BLANK_ENTRY",1,"Weazel News",false,"Payday: ~g~$300"})
	end	
	
	
	--Lavori
	if vRP.hasPermission(user_id,"repair.paycheck") then
		vRP.giveBankMoney(user_id,300)
		vRPclient.notifyPicture(source,{"CHAR_BLANK_ENTRY",1,"Banca",false,"Payday: ~g~$300"})
	end
	if vRP.hasPermission(user_id,"delivery.paycheck") then
		vRP.giveBankMoney(user_id,200)
		vRPclient.notifyPicture(source,{"CHAR_BLANK_ENTRY",1,"Banca",false,"Payday: ~g~$200"})
	end
end)
