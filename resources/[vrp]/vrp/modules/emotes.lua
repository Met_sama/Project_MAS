
-- this module define the emotes menu

local cfg = module("cfg/emotes")
local lang = vRP.lang

local effetti = cfg.effetti
local emotes = cfg.emotes
local camminata = cfg.camminata
---------------------------------------
local function ch_effetti(player,choice)
  local effe = effetti[choice]
  if effe then
    vRPclient.playScreenEffect(player,{effe,7})
  end
end
local function ch_camminata(player,choice)
 local cammin = camminata[choice]
 if cammin then
  vRPclient.playMovement(player,{cammin,false,false,false,false})
 end
end
-- add emotes menu to main menu

vRP.registerMenuBuilder("admin", function(add, data)
  local choices = {}
    
  choices["AdminOnlyEffetti"] = {function(player,choice)
    vRP.buildMenu("effe", {player = player}, function(menu)
      menu.name = "Effetti"
      menu.css = {top="75px",header_color="rgba(255, 144, 0)"}
      menu.onclose = function(player) vRP.openMainMenu(player) end
     local user_id = vRP.getUserId(player)
     if user_id ~= nil then
      -- add emotes to the emote menu
      for k,v in pairs(effetti) do
        if vRP.hasPermission(user_id,"admin.deleteveh") then
          menu[k] = {ch_effetti}
        end
      end
    end

  vRP.openMenu(player,menu)
end)
end}
add(choices)
end)

vRP.registerMenuBuilder("main", function(add, data)
  local choices = {}

  choices["Camminata"] = {function(player,choice)
    vRP.buildMenu("camm", {player = player}, function(menu)
      menu.name = "Stile di camminata"
      menu.css = {top="75px",header_color="rgba(255, 144, 0)"}
      menu.onclose = function(player) vRP.openMainMenu(player) end
      
     local user_id = vRP.getUserId(player)
     if user_id ~= nil then
      -- add emotes to the emote menu
      for k,v in pairs(camminata) do
        if vRP.hasPermission(user_id,"player.store_money") then
          menu[k] = {ch_camminata}
        end
      end
    end

  menu["Z - STOP ANIMAZIONI"] = {function(player,choice)
    vRPclient.resetMovement(player,{false})
  end, "Ferma tutte el animazioni"}

  vRP.openMenu(player,menu)
end)
end}

    choices["Animazioni"] = {function(player,choice)
      vRP.buildMenu("anim", {player = player}, function(menu)
        menu.name = "Animazioni"
        menu.css = {top="75px",header_color="rgba(255, 144, 0)"}
        menu.onclose = function(player) vRP.openMainMenu(player) end

    menu["Z - STOP ANIMAZIONI"] = {function(player,choice)
      vRPclient.stopAnim(player,{true}) -- upper
      vRPclient.stopAnim(player,{false})
    end, lang.emotes.clear.description()}
  
    vRP.openMenu(player,menu)
  end)
end}
      add(choices)
end)


vRP.registerMenuBuilder("anim", function(add, data)
  local choices = {}
  


  for nome,dati in pairs(emotes) do
    choices[nome] = {function(player, choice)
    -- build emotes menu
      local menu = {name=nome,css={top="75px",header_color="rgba(214,217,32,0.72)"},onclose = function(player) vRP.openAnimMenu(player) end}
      local user_id = vRP.getUserId(player)

      if user_id ~= nil then
        -- add emotes to the emote menu
        for name,data in pairs(dati) do
          if vRP.hasPermissions(user_id, data.permissions or {}) then
            menu[name] = {function(player,choice)
              local emote = emotes[nome]
              local emote2 = emote[name]
              if emote2 then
                vRPclient.playAnim(player,{emote2[1],emote2[2],emote2[3]})
              end
            end}
          end
        end
      end
      vRP.openMenu(player,menu)
    end}
  end
  choices["Cerca"] = {function(player,choice)
    vRP.prompt(player,"Cerca","",function(player, anim)
      for nome,dati in pairs(emotes) do
        for name,data in pairs(dati) do
          if name == anim or string.lower(name) == anim then
            if data then
              vRPclient.playAnim(player,{data[1],data[2],data[3]})
            else
              vRPclient.notify(player,{"~r~Questa animazione non esiste"})
            end
            break
          end
        end
      end
    end)
  end, "Cerca animazione"}
  add(choices)
end)
