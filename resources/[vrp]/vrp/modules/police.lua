
-- this module define some police tools and functions
local lang = vRP.lang
local cfg = module("cfg/police")

-- police records

-- insert a police record for a specific user
--- line: text for one line (can be html)
function vRP.insertPoliceRecord(user_id, line)
  if user_id ~= nil then
    vRP.getUData(user_id, "vRP:police_records", function(data)
      local records = data..line.."<br />"
      vRP.setUData(user_id, "vRP:police_records", records)
    end)
  end
end

-- police PC
--[[
RegisterServerEvent("vRP:Spari_2")
AddEventHandler("vRP:Spari_2", function(strada,sex,x,y,z)
  vRP.sendServiceAlert(nil, "LSPD-Spari",x,y,z,"~b~Colpi d'arma da fuoco sparati da un/una "..sex.." sulla strada, "..strada)
end)
RegisterServerEvent("vRP:Spari_3")
AddEventHandler("vRP:Spari_3", function(strada,strada2,sex,x,y,z)
  vRP.sendServiceAlert(nil, "LSPD-Spari",x,y,z,"~b~Colpi d'arma da fuoco sparati da un/una "..sex.." sulla strada, "..strada..", "..strada2)
end)
]]--

local menu_pc = {name=lang.police.pc.title(),css={top="75px",header_color="rgba(0,125,255,0.75)"}}

-- search identity by registration
local function ch_searchreg(player,choice)
  vRP.prompt(player,lang.police.pc.searchreg.prompt(),"",function(player, reg)
    vRP.getUserByRegistration(reg, function(user_id)
      if user_id ~= nil then
        vRP.getUserIdentity(user_id, function(identity)
          if identity then
            -- display identity and business
            local name = identity.name
            local firstname = identity.firstname
            local age = identity.age
            local phone = identity.phone
            local registration = identity.registration
            local bname = ""
            local bcapital = 0
            local home = ""
            local number = ""
            local business2 = vRP.getUserGroupByType(user_id,"business")

            vRP.getUserBusiness(user_id, function(business)
              if business then
                bname = business.name
                bcapital = business.capital
              end

              vRP.getUserAddress(user_id, function(address)
                if address then
                  home = address.home
                  number = address.number
                end

                local content = lang.police.identity.info({name,firstname,age,registration,phone,home,number,business2,user_id})
                vRPclient.setDiv(player,{"police_pc",".div_police_pc{ background-color: rgba(0,0,0,0.75); color: white; font-weight: bold; width: 500px; padding: 10px; margin: auto; margin-top: 150px; }",content})
              end)
            end)
          else
            vRPclient.notify(player,{lang.common.not_found()})
          end
        end)
      else
        vRPclient.notify(player,{lang.common.not_found()})
      end
    end)
  end)
end

-- show police records by registration
local function ch_show_police_records(player,choice)
  vRP.prompt(player,lang.police.pc.searchreg.prompt(),"",function(player, reg)
    vRP.getUserByRegistration(reg, function(user_id)
      if user_id ~= nil then
        vRP.getUData(user_id, "vRP:police_records", function(content)
          vRPclient.setDiv(player,{"police_pc",".div_police_pc{ background-color: rgba(0,0,0,0.75); color: white; font-weight: bold; width: 500px; padding: 10px; margin: auto; margin-top: 150px; }",content})
        end)
      else
        vRPclient.notify(player,{lang.common.not_found()})
      end
    end)
  end)
end

-- delete police records by registration
local function ch_delete_police_records(player,choice)
  vRP.prompt(player,lang.police.pc.searchreg.prompt(),"",function(player, reg)
    vRP.getUserByRegistration(reg, function(user_id)
      if user_id ~= nil then
        vRP.setUData(user_id, "vRP:police_records", "")
        vRPclient.notify(player,{lang.police.pc.records.delete.deleted()})
      else
        vRPclient.notify(player,{lang.common.not_found()})
      end
    end)
  end)
end

-- close business of an arrested owner
local function ch_closebusiness(player,choice)
  vRPclient.getNearestPlayer(player,{5},function(nplayer)
    local nuser_id = vRP.getUserId(nplayer)
    if nuser_id ~= nil then
      vRP.getUserIdentity(nuser_id, function(identity)
        vRP.getUserBusiness(nuser_id, function(business)
          if identity and business then
            vRP.request(player,lang.police.pc.closebusiness.request({identity.name,identity.firstname,business.name}),15,function(player,ok)
              if ok then
                vRP.closeBusiness(nuser_id)
                vRPclient.notify(player,{lang.police.pc.closebusiness.closed()})
              end
            end)
          else
            vRPclient.notify(player,{lang.common.no_player_near()})
          end
        end)
      end)
    else
      vRPclient.notify(player,{lang.common.no_player_near()})
    end
  end)
end

-- track vehicle
local function ch_trackveh(player,choice)
  vRP.prompt(player,lang.police.pc.trackveh.prompt_reg(),"",function(player, reg) -- ask reg
    vRP.getUserByRegistration(reg, function(user_id)
      if user_id ~= nil then
        vRP.prompt(player,lang.police.pc.trackveh.prompt_note(),"",function(player, note) -- ask note
          -- begin veh tracking
          vRPclient.notify(player,{lang.police.pc.trackveh.tracking()})
          local seconds = math.random(cfg.trackveh.min_time,cfg.trackveh.max_time)
          SetTimeout(seconds*1000,function()
            local tplayer = vRP.getUserSource(user_id)
            if tplayer ~= nil then
              vRPclient.getAnyOwnedVehiclePosition(tplayer,{reg},function(ok,x,y,z)
                if ok then -- track success
                  vRP.sendServiceAlert(nil, cfg.trackveh.service,x,y,z,lang.police.pc.trackveh.tracked({reg,note}))
                else
                  vRPclient.notify(player,{lang.police.pc.trackveh.track_failed({reg,note})}) -- failed
                end
              end)
            else
              vRPclient.notify(player,{lang.police.pc.trackveh.track_failed({reg,note})}) -- failed
            end
          end)
        end)
      else
        vRPclient.notify(player,{lang.common.not_found()})
      end
    end)
  end)
end

local function ch_trackveh2(player,choice)
  vRP.prompt(player,lang.police.pc.trackveh.prompt_reg(),"",function(player, reg) -- ask reg
    vRP.getUserByRegistration(reg, function(user_id)
      if user_id ~= nil then
        vRP.prompt(player,lang.police.pc.trackveh.prompt_note(),"",function(player, note) -- ask note
          -- begin veh tracking
          vRPclient.notify(player,{lang.police.pc.trackveh.tracking2()})
          local seconds = math.random(cfg.trackveh.min_timelent,cfg.trackveh.max_timelent)
          SetTimeout(seconds*1000,function()
            local tplayer = vRP.getUserSource(user_id)
            if tplayer ~= nil then
              vRPclient.getAnyOwnedVehiclePosition(tplayer,{},function(ok,x,y,z)
                if ok then -- track success
                  vRP.sendServiceAlert(nil, cfg.trackveh.service,x,y,z,lang.police.pc.trackveh.tracked({reg,note}))
                else
                  vRPclient.notify(player,{lang.police.pc.trackveh.track_failed({reg,note})}) -- failed
                end
              end)
            else
              vRPclient.notify(player,{lang.police.pc.trackveh.track_failed({reg,note})}) -- failed
            end
          end)
        end)
      else
        vRPclient.notify(player,{lang.common.not_found()})
      end
    end)
  end)
end

menu_pc[lang.police.pc.searchreg.title()] = {ch_searchreg,lang.police.pc.searchreg.description()}
menu_pc[lang.police.pc.trackveh.title()] = {ch_trackveh,lang.police.pc.trackveh.description()}
menu_pc[lang.police.pc.records.show.title()] = {ch_show_police_records,lang.police.pc.records.show.description()}
menu_pc[lang.police.pc.records.delete.title()] = {ch_delete_police_records, lang.police.pc.records.delete.description()}
menu_pc[lang.police.pc.closebusiness.title()] = {ch_closebusiness,lang.police.pc.closebusiness.description()}

menu_pc.onclose = function(player) -- close pc gui
  vRPclient.removeDiv(player,{"police_pc"})
end

local function pc_enter(source,area)
  local user_id = vRP.getUserId(source)
  if user_id ~= nil and vRP.hasPermission(user_id,"police.pc") then
    vRP.openMenu(source,menu_pc)
  end
end

local function pc_leave(source,area)
  vRP.closeMenu(source)
end

function vRP.setSequestro(user_id,vehicle,sequestro)
   MySQL.execute("vRP/set_sequestro", {user_id = user_id, vehicle = vehicle, sequestro = sequestro})
end

-- main menu choices
local choice_sequestro = {function(player,choice) 
  local user_id = vRP.getUserId(player)
    if user_id ~= nil then
      vRPclient.getNearestVehicle2(player,{5},function(veh,name,targa)
        if veh then
          if string.sub(targa, 1, 1) == "P" then
 --           vRPclient.controllaPosizione(player,{532.146,-168.722,54.831,5},function(inpos)
 --             if inpos then
                vRP.request(player,"Sequestrare: "..name.." - Reg: "..targa,30,function(player,ok)
                  if ok then
                    local newtarga = string.gsub(targa, "P ", "")
                    vRP.getUserByRegistration(newtarga, function(nuser_id)
                      if nuser_id then
                        vRP.setSequestro(tonumber(nuser_id),string.lower(name),true)
                        vRPclient.notify(player,{"~g~Veicolo sequestrato"})
                        local nplayer = vRP.getUserSource(tonumber(nuser_id))
                        if nplayer then
                          vRPclient.notify(nplayer,{"~b~Il tuo veicolo ~g~"..name.."~b~ è stato sequestrato"})
                        end
                      else
                        vRPclient.notify(player,{"~r~"..newtarga.." stranamente non registrata...(Reporta questo errore)"})
                      end
                end)
              else
                vRPclient.notify(player,{"~r~Devi trovarti nella zona di sequestro.."})
              end
            end)
          else
            vRPclient.notify(player,{"~r~Macchina non registrata"})
          end
        else
          vRPclient.notify(player,{"~r~Nessuna macchina vicina"})
        end
      end)
    end
end,"Sequestra veicolo vicino a te."}

local choice_disequestro = {function(player,choice) 
  local user_id = vRP.getUserId(player)
  if user_id ~= nil then
    vRPclient.getNearestPlayer(player,{10},function(nplayer)
      if nplayer ~= nil then
        local nuser_id = vRP.getUserId(nplayer)
        if nuser_id ~= nil then 
          vRP.prompt(player,"Macchina da restituire","",function(player, name)
            vRP.setSequestro(nuser_id,name,false)
            vRPclient.notify(player,{"~g~Veicolo restituito"})
            vRPclient.notify(nplayer,{"~b~Il tuo veicolo ~g~"..name.."~b~ ti è stato restituito"})
          end)
        end
      else
        vRPclient.notify(player,{"~r~Nessun player vicino"})
      end
    end)
  end
end,"Restituisci il veicolo"}

local choice_disequestro_2 = {function(player,choice) 
  local user_id = vRP.getUserId(player)
  if user_id ~= nil then
    vRP.prompt(player,"Inserisci [user_id]","",function(player,nuser_id) 
      local nuser_id = nuser_id
      local nplayer = vRP.getUserSource(nuser_id)
      if nuser_id ~= nil and nuser_id ~= "" then 
        vRP.prompt(player,"Macchina da restituire","",function(player, name)
          vRP.setSequestro(nuser_id,name,false)
          vRPclient.notify(player,{"Veicolo restituito"})
          if nplayer then
            vRPclient.notify(nplayer,{"Il tuo veicolo "..name.." ti è stato restituito"})
          end
        end)
      end
    end)
  end
end,"Restituisci il veicolo"}

display_css_red = [[
  .div_money{
    position: absolute;
    top: 50px;
    right: 10px;
    font-size: 30px;
    font-family: Pricedown;
    color: #FFFFFF;
    text-shadow: rgb(0, 0, 0) 1px 0px 0px, rgb(0, 0, 0) 0.533333px 0.833333px 0px, rgb(0, 0, 0) -0.416667px 0.916667px 0px, rgb(0, 0, 0) -0.983333px 0.133333px 0px, rgb(0, 0, 0) -0.65px -0.75px 0px, rgb(0, 0, 0) 0.283333px -0.966667px 0px, rgb(0, 0, 0) 0.966667px -0.283333px 0px;
  }
  
  .div_bmoney{
    position: absolute;
    top: 80px;
    right: 10px;
    font-size: 30px;
    font-family: Pricedown;
    color: #FF3300;
    text-shadow: rgb(0, 0, 0) 1px 0px 0px, rgb(0, 0, 0) 0.533333px 0.833333px 0px, rgb(0, 0, 0) -0.416667px 0.916667px 0px, rgb(0, 0, 0) -0.983333px 0.133333px 0px, rgb(0, 0, 0) -0.65px -0.75px 0px, rgb(0, 0, 0) 0.283333px -0.966667px 0px, rgb(0, 0, 0) 0.966667px -0.283333px 0px;
  }

  .div_money .symbol{
    content: url('https://i.imgur.com/X0wRE77.png'); 
  }
    
  .div_bmoney .symbol{
    content: url('https://i.imgur.com/j72svBc.png');
  }
]]

display_css = [[
  .div_money{
    position: absolute;
    top: 50px;
    right: 10px;
    font-size: 30px;
    font-family: Pricedown;
    color: #FFFFFF;
    text-shadow: rgb(0, 0, 0) 1px 0px 0px, rgb(0, 0, 0) 0.533333px 0.833333px 0px, rgb(0, 0, 0) -0.416667px 0.916667px 0px, rgb(0, 0, 0) -0.983333px 0.133333px 0px, rgb(0, 0, 0) -0.65px -0.75px 0px, rgb(0, 0, 0) 0.283333px -0.966667px 0px, rgb(0, 0, 0) 0.966667px -0.283333px 0px;
  }
  .div_bmoney{
    position: absolute;
    top: 80px;
    right: 10px;
    font-size: 30px;
    font-family: Pricedown;
    color: #FFFFFF;
    text-shadow: rgb(0, 0, 0) 1px 0px 0px, rgb(0, 0, 0) 0.533333px 0.833333px 0px, rgb(0, 0, 0) -0.416667px 0.916667px 0px, rgb(0, 0, 0) -0.983333px 0.133333px 0px, rgb(0, 0, 0) -0.65px -0.75px 0px, rgb(0, 0, 0) 0.283333px -0.966667px 0px, rgb(0, 0, 0) 0.966667px -0.283333px 0px;
  }
    .div_money .symbol{
    content: url('https://i.imgur.com/X0wRE77.png'); 
   }
  
  .div_bmoney .symbol{
    content: url('https://i.imgur.com/glLG7gH.png');
  
  }
]]

local choice_bloccaconto = {function(player,choice) 
  local user_id = vRP.getUserId(player)
    if user_id ~= nil then
      vRPclient.getNearestPlayers(player,{15},function(nplayers)
      usrList = ""
        for k,v in pairs(nplayers) do
          usrList = usrList .. "[" .. vRP.getUserId(k) .. "]" .. GetPlayerName(k) .. " | "
        end
        vRP.prompt(player,"Inserisci [user_id] (Player vicini): " .. usrList .. "","",function(player,nuser_id_p) 
          local nuser_id = tonumber(nuser_id_p)
          local nplayer = vRP.getUserSource(nuser_id)
            if nuser_id ~= nil and nuser_id ~= "" then 
              vRP.addUserGroup(nuser_id,"nowithdraw")
              vRPclient.removeDiv(nplayer,{"bmoney"})
              vRPclient.setDiv(nplayer,{"bmoney",display_css_red,lang.money.bdisplay({format_num(vRP.getBankMoney(nuser_id),0)})})
              vRPclient.notify(nplayer,{"~r~Il tuo conto è stato bloccato"})
            end
        end)
      end)
    end
end,"Blocca il conto di un player in modo che non possa prelevare e trasferire i soldi che ha in banca."}

local choice_sbloccaconto = {function(player,choice) 
  local user_id = vRP.getUserId(player)
    if user_id ~= nil then
      vRPclient.getNearestPlayers(player,{15},function(nplayers)
      usrList = ""
        for k,v in pairs(nplayers) do
          usrList = usrList .. "[" .. vRP.getUserId(k) .. "]" .. GetPlayerName(k) .. " | "
        end
        vRP.prompt(player,"Inserisci [user_id] (Player vicini): " .. usrList .. "","",function(player,nuser_id_p) 
          local nuser_id = tonumber(nuser_id_p)
          local nplayer = vRP.getUserSource(nuser_id)
            if nuser_id ~= nil and nuser_id ~= "" then 
              vRP.removeUserGroup(nuser_id,"nowithdraw")
              vRPclient.removeDiv(nplayer,{"bmoney"})
              vRPclient.setDiv(nplayer,{"bmoney",display_css,lang.money.bdisplay({format_num(vRP.getBankMoney(nuser_id),0)})})
              vRPclient.notify(nplayer,{"~g~Il tuo conto è stato sbloccato"})
            end
        end)
      end)
    end
end,"Sblocca il conto di un player."}

---- handcuff
local choice_handcuff = {function(player,choice)
  vRPclient.getNearestPlayer(player,{10},function(nplayer)
    local nuser_id = vRP.getUserId(nplayer)
    if nuser_id ~= nil then
      vRPclient.toggleHandcuff(nplayer,{})
    else
      vRPclient.notify(player,{lang.common.no_player_near()})
    end
  end)
end,lang.police.menu.handcuff.description()}

local revive_seq = {
  {"amb@medic@standing@kneel@enter","enter",1},
  {"amb@medic@standing@kneel@idle_a","idle_a",1},
  {"amb@medic@standing@kneel@exit","exit",1}
}

function vRP.controllamorte(weaponHash)
  if weaponHash ~= nil then
		if weaponHash == GetHashKey("WEAPON_RUN_OVER_BY_CAR") or weaponHash == GetHashKey("WEAPON_RAMMED_BY_CAR") then
			return "Travolgimento"
		end
		if weaponHash == GetHashKey("WEAPON_CROWBAR") or weaponHash == GetHashKey("WEAPON_BAT") or weaponHash == GetHashKey("WEAPON_HAMMER") or weaponHash == GetHashKey("WEAPON_GOLFCLUB") or weaponHash == GetHashKey("WEAPON_NIGHTSTICK") or weaponHash == GetHashKey("WEAPON_KNUCKLE") then
			return "Arma bianca"
		end
		if weaponHash == GetHashKey("WEAPON_DAGGER") or weaponHash == GetHashKey("WEAPON_KNIFE") then
			return "Accoltellamento"
		end
		if weaponHash == GetHashKey("WEAPON_SNSPISTOL") or weaponHash == GetHashKey("WEAPON_HEAVYPISTOL") or weaponHash == GetHashKey("WEAPON_VINTAGEPISTOL") or weaponHash == GetHashKey("WEAPON_PISTOL") or weaponHash == GetHashKey("WEAPON_APPISTOL") or weaponHash == GetHashKey("WEAPON_COMBATPISTOL") or weaponHash == GetHashKey("WEAPON_SNSPISTOL") then
			return "Trauma balistico"
		end
		if weaponHash == GetHashKey("WEAPON_GRENADELAUNCHER") or weaponHash == GetHashKey("WEAPON_HOMINGLAUNCHER") or weaponHash == GetHashKey("WEAPON_STICKYBOMB") or weaponHash == GetHashKey("WEAPON_PROXMINE") or weaponHash == GetHashKey("WEAPON_RPG") or weaponHash == GetHashKey("WEAPON_EXPLOSION") or weaponHash == GetHashKey("VEHICLE_WEAPON_TANK") then
			return "Esplosione"
		end
		if weaponHash == GetHashKey("WEAPON_MICROSMG") or weaponHash == GetHashKey("WEAPON_SMG") or weaponHash == GetHashKey("WEAPON_ASSAULTSMG") or weaponHash == GetHashKey("WEAPON_MG") or weaponHash == GetHashKey("WEAPON_COMBATMG") or weaponHash == GetHashKey("WEAPON_COMBATPDW") or weaponHash == GetHashKey("WEAPON_MINIGUN") then
			return "Trauma balistico"
		end
		if weaponHash == GetHashKey("WEAPON_ASSAULTRIFLE") or weaponHash == GetHashKey("WEAPON_CARBINERIFLE") or weaponHash == GetHashKey("WEAPON_ADVANCEDRIFLE") or weaponHash == GetHashKey("WEAPON_BULLPUPRIFLE") or weaponHash == GetHashKey("WEAPON_SPECIALCARBINE") or weaponHash == GetHashKey("WEAPON_GUSENBERG") then
			return "Trauma balistico"
		end
		if weaponHash == GetHashKey("WEAPON_MARKSMANRIFLE") or weaponHash == GetHashKey("WEAPON_SNIPERRIFLE") or weaponHash == GetHashKey("WEAPON_HEAVYSNIPER") or weaponHash == GetHashKey("WEAPON_ASSAULTSNIPER") or weaponHash == GetHashKey("WEAPON_REMOTESNIPER") then
			return "Trauma balistico"
		end
		if weaponHash == GetHashKey("WEAPON_BULLPUPSHOTGUN") or weaponHash == GetHashKey("WEAPON_ASSAULTSHOTGUN") or weaponHash == GetHashKey("WEAPON_PUMPSHOTGUN") or weaponHash == GetHashKey("WEAPON_HEAVYSHOTGUN") or weaponHash == GetHashKey("WEAPON_SAWNOFFSHOTGUN") then
			return "Trauma balistico"
		end
		if weaponHash == GetHashKey("WEAPON_HATCHET") or weaponHash == GetHashKey("WEAPON_MACHETE") then
			return "Arma affilata"
		end
		if weaponHash == GetHashKey("WEAPON_MOLOTOV") then
			return "Bruciato"
		end
		return "Scoonosciuta"
	end
  return "Sconosciuta"
end

local choice_mortecontr = {function(player,choice)
  vRPclient.getNearestPlayer(player,{10},function(nplayer)
    local nuser_id = vRP.getUserId(nplayer)
    if nuser_id ~= nil then
      vRPclient.isInComa(nplayer,{}, function(in_coma)
        if in_coma then
          local aspetta = math.random(10,20)
          vRPclient.playAnim(player,{false,revive_seq,false}) -- anim
          Citizen.Wait(aspetta*1000)
          local data = vRP.getUserDataTable(nuser_id)
          local morte = vRP.controllamorte(data.ultimamorte)
          vRPclient.notify(player,{"Morte per: "..morte})
        end
    end)
  end
end)
end} -- heal 50

local choice_putinveh = {function(player,choice)
  vRPclient.getNearestPlayer(player,{10},function(nplayer)
    local nuser_id = vRP.getUserId(nplayer)
    if nuser_id ~= nil then
      vRPclient.isHandcuffed(nplayer,{}, function(handcuffed)  -- check handcuffed
        if handcuffed then
          vRPclient.putInNearestVehicleAsPassenger(nplayer, {5})
        else
          vRPclient.notify(player,{lang.police.not_handcuffed()})
        end
      end)
    else
      vRPclient.notify(player,{lang.common.no_player_near()})
    end
  end)
end,lang.police.menu.putinveh.description()}

local choice_getoutveh = {function(player,choice)
  vRPclient.getNearestPlayer(player,{10},function(nplayer)
    local nuser_id = vRP.getUserId(nplayer)
    if nuser_id ~= nil then
      vRPclient.isHandcuffed(nplayer,{}, function(handcuffed)  -- check handcuffed
        if handcuffed then
          vRPclient.ejectVehicle(nplayer, {})
        else
          vRPclient.notify(player,{lang.police.not_handcuffed()})
        end
      end)
    else
      vRPclient.notify(player,{lang.common.no_player_near()})
    end
  end)
end,lang.police.menu.getoutveh.description()}

---- askid
local choice_askid = {function(player,choice)
  vRPclient.getNearestPlayer(player,{10},function(nplayer)
    local nuser_id = vRP.getUserId(nplayer)
    if nuser_id ~= nil then
      vRPclient.notify(player,{lang.police.menu.askid.asked()})
      vRP.request(nplayer,lang.police.menu.askid.request(),15,function(nplayer,ok)
        if ok then
          vRP.getUserIdentity(nuser_id, function(identity)
            if identity then
              -- display identity and business
              local name = identity.name
              local firstname = identity.firstname
              local age = identity.age
              local phone = identity.phone
              local registration = identity.registration
              local bname = ""
              local bcapital = 0
              local home = ""
              local number = ""
              local business2 = vRP.getUserGroupByType(nuser_id,"business")

              vRP.getUserBusiness(nuser_id, function(business)
                if business then
                  bname = business.name
                  bcapital = business.capital
                end

                vRP.getUserAddress(nuser_id, function(address)
                  if address then
                    home = address.home
                    number = address.number
                  end

                  local content = lang.police.identity.info({name,firstname,age,registration,phone,home,number,business2,nuser_id})
                  vRPclient.setDiv(player,{"police_identity",".div_police_identity{background-image: url(https://i.imgur.com/0eA5cnm.jpg); position: fixed; left: 30px; border: 2px solid #000000; border-top-left-radius: 30px; border-bottom-left-radius: 30px; border-top-right-radius: 30px; border-bottom-right-radius: 30px; top: 350px; left: 550px; font-size: 17px; font-family: verdana, arial, sans-serif; font-style: italic; text-transform: capitalize; box-shadow: -1px -1px 60px -1px rgba(0,0,0,1); font-weight: bold; text-shadow: 1px 1px #ff0000; width: 300px;height: 210px; padding: 25px; color: black; }",content})
                  -- request to hide div
                  vRP.request(player, lang.police.menu.askid.request_hide(), 1000, function(player,ok)
                    vRPclient.removeDiv(player,{"police_identity"})
                  end)
                end)
              end)
            end
          end)
        else
          vRPclient.notify(player,{lang.common.request_refused()})
        end
      end)
    else
      vRPclient.notify(player,{lang.common.no_player_near()})
    end
  end)
end, lang.police.menu.askid.description()}

---- police check
local choice_check = {function(player,choice)
  vRPclient.getNearestPlayer(player,{5},function(nplayer)
    local nuser_id = vRP.getUserId(nplayer)
    if nuser_id ~= nil then
      vRPclient.notify(nplayer,{lang.police.menu.check.checked()})
      vRPclient.getWeapons(nplayer,{},function(weapons)
        -- prepare display data (money, items, weapons)
        local money = vRP.getMoney(nuser_id)
        local items = ""
        local data = vRP.getUserDataTable(nuser_id)
        if data and data.inventory then
          for k,v in pairs(data.inventory) do
            local item = vRP.items[k]
            if item then
              items = items.."<br />"..item.name.." ("..v.amount..")"
            end
          end
        end

        for k,v in pairs(cfg.weapons_items) do -- transfer seizable items
          local amount = vRP.getInventoryItemAmount(nuser_id,v)
          if amount > 0 then
            if vRP.tryGetInventoryItem(nuser_id,v,amount,true) then
              items = items.."<br />"..v.." ("..amount..")"
            end
          end
        end

        local weapons_info = ""
        for k,v in pairs(weapons) do
          weapons_info = weapons_info.."<br />"..k.." ("..v.ammo..")"
        end

        vRPclient.setDiv(player,{"police_check",".div_police_check{ background-color: rgba(0,0,0,0.75); color: white; font-weight: bold; width: 500px; padding: 10px; margin: auto; margin-top: 150px; }",lang.police.menu.check.info({money,items,weapons_info})})
        -- request to hide div
        vRP.request(player, lang.police.menu.check.request_hide(), 1000, function(player,ok)
          vRPclient.removeDiv(player,{"police_check"})
        end)
      end)
    else
      vRPclient.notify(player,{lang.common.no_player_near()})
    end
  end)
end, lang.police.menu.check.description()}

local choice_check_house = {function(player,choice)
  vRPclient.getNearestPlayer(player,{5},function(nplayer)
    local nuser_id = vRP.getUserId(nplayer)
    if nuser_id ~= nil then
      vRPclient.notify(nplayer,{"Ti hanno controllato l'inventario di casa"})
        -- prepare display data (money, items, weapons)
        local money = vRP.getMoney(nuser_id)
        local items = ""
        vRP.getSData("chest:u"..nuser_id.."home", function(data)
          local items_chest = json.decode(data) or {}
          for k,v in pairs(items_chest) do
              items = items.."<br />"..k.." ("..v.amount..")"
          end

        vRPclient.setDiv(player,{"police_check_house",".div_police_check_house{ background-color: rgba(0,0,0,0.75); color: white; font-weight: bold; width: 500px; padding: 10px; margin: auto; margin-top: 150px; }",lang.police.menu.check.info2({items})})
        -- request to hide div
        vRP.request(player, lang.police.menu.check.request_hide(), 1000, function(player,ok)
          vRPclient.removeDiv(player,{"police_check_house"})
        end)
      end)
    else
      vRPclient.notify(player,{lang.common.no_player_near()})
    end
  end)
end, lang.police.menu.check.description()}

local choice_seize_weapons = {function(player, choice)
  local user_id = vRP.getUserId(player)
  if user_id ~= nil then
    vRPclient.getNearestPlayer(player, {5}, function(nplayer)
      local nuser_id = vRP.getUserId(nplayer)
      if nuser_id ~= nil and vRP.hasPermission(nuser_id, "police.seizable") then
        vRPclient.isHandcuffed(nplayer,{}, function(handcuffed)  -- check handcuffed
          if handcuffed then
            vRPclient.getWeapons(nplayer,{},function(weapons)
              for k,v in pairs(weapons) do -- display seized weapons
                -- vRPclient.notify(player,{lang.police.menu.seize.seized({k,v.ammo})})
                -- convert weapons to parametric weapon items
                vRP.giveInventoryItem(user_id, "wbody|"..k, 1, true)
                if v.ammo > 0 then
                  vRP.giveInventoryItem(user_id, "wammo|"..k, v.ammo, true)
                end
              end

              -- clear all weapons
              vRPclient.giveWeapons(nplayer,{{},true})
              vRPclient.notify(nplayer,{lang.police.menu.seize.weapons.seized()})
            end)
          else
            vRPclient.notify(player,{lang.police.not_handcuffed()})
          end
        end)
      else
        vRPclient.notify(player,{lang.common.no_player_near()})
      end
    end)
  end
end, lang.police.menu.seize.weapons.description()}

local choice_seize_items = {function(player, choice)
  local user_id = vRP.getUserId(player)
  if user_id ~= nil then
    vRPclient.getNearestPlayer(player, {5}, function(nplayer)
      local nuser_id = vRP.getUserId(nplayer)
      if nuser_id ~= nil and vRP.hasPermission(nuser_id, "police.seizable") then
        vRPclient.isHandcuffed(nplayer,{}, function(handcuffed)  -- check handcuffed
          if handcuffed then
            for k,v in pairs(cfg.weapons_items) do -- transfer seizable items
              local amount = vRP.getInventoryItemAmount(nuser_id,v)
              if amount > 0 then
                if vRP.tryGetInventoryItem(nuser_id,v,amount,true) then
                  vRP.giveInventoryItem(user_id,v,amount,false)
                  vRPclient.notify(player,{lang.police.menu.seize.seized({v,amount})})
                end
              end
            end
            for k,v in pairs(cfg.seizable_items) do -- transfer seizable items
              local amount = vRP.getInventoryItemAmount(nuser_id,v)
              if amount > 0 then
                local item = vRP.items[v]
                if item then -- do transfer
                  if vRP.tryGetInventoryItem(nuser_id,v,amount,true) then
                    vRP.giveInventoryItem(user_id,v,amount,false)
                    vRPclient.notify(player,{lang.police.menu.seize.seized({item.name,amount})})
                  end
                end
              end
            end

            vRPclient.notify(nplayer,{lang.police.menu.seize.items.seized()})
          else
            vRPclient.notify(player,{lang.police.not_handcuffed()})
          end
        end)
      else
        vRPclient.notify(player,{lang.common.no_player_near()})
      end
    end)
  end
end, lang.police.menu.seize.items.description()}

local choice_give_exp = {function(player,choice) 
  local user_id = vRP.getUserId(player)
    if user_id ~= nil then
      vRPclient.getNearestPlayers(player,{15},function(nplayers)
      local usrList = ""
      for k,v in pairs(nplayers) do
        usrList = usrList .. "[" .. vRP.getUserId(k) .. "]" .. GetPlayerName(k) .. " | "
      end
      vRP.prompt(player,"Inserisci [user_id] (Player vicini): " .. usrList .. "","",function(player,nuserid) 
        if nuserid ~= nil and nuserid ~= "" then 
          local n_user_id = tonumber(nuserid)
          local nplayer = vRP.getUserSource(n_user_id)
          vRP.prompt(player,"Exp","",function(player, amount)
            if amount ~= nil and amount ~= "" then 
              local namount = tonumber(amount)
              vRP.varyExp(n_user_id,"jobs","poli",namount)
              vRPclient.notify(nplayer,{"Ti sono stati dati "..namount.." exp."})
            end
          end)
        end
      end)
    end)
  end
end,"Dai EXP per l'inquadramento polizia ad un player"}

-- toggle jail nearest player
local choice_jail = {function(player, choice)
  local user_id = vRP.getUserId(player)
  if user_id ~= nil then
    vRPclient.getNearestPlayer(player, {5}, function(nplayer)
      local nuser_id = vRP.getUserId(nplayer)
      if nuser_id ~= nil then
        vRPclient.isJailed(nplayer, {}, function(jailed)
          if jailed then -- unjail
            vRPclient.unjail(nplayer, {})
            vRPclient.notify(nplayer,{lang.police.menu.jail.notify_unjailed()})
            vRPclient.notify(player,{lang.police.menu.jail.unjailed()})
          else -- find the nearest jail
            vRPclient.getPosition(nplayer,{},function(x,y,z)
              local d_min = 1000
              local v_min = nil
              for k,v in pairs(cfg.jails) do
                local dx,dy,dz = x-v[1],y-v[2],z-v[3]
                local dist = math.sqrt(dx*dx+dy*dy+dz*dz)

                if dist <= d_min and dist <= 15 then -- limit the research to 15 meters
                  d_min = dist
                  v_min = v
                end

                -- jail
                if v_min then
                  vRPclient.jail(nplayer,{v_min[1],v_min[2],v_min[3],v_min[4]})
                  vRPclient.notify(nplayer,{lang.police.menu.jail.notify_jailed()})
                  vRPclient.notify(player,{lang.police.menu.jail.jailed()})
                else
                  vRPclient.notify(player,{lang.police.menu.jail.not_found()})
                end
              end
            end)
          end
        end)
      else
        vRPclient.notify(player,{lang.common.no_player_near()})
      end
    end)
  end
end, lang.police.menu.jail.description()}

local choice_fine = {function(player, choice)
  local user_id = vRP.getUserId(player)
  if user_id ~= nil then
    vRPclient.getNearestPlayer(player, {5}, function(nplayer)
      local nuser_id = vRP.getUserId(nplayer)
      if nuser_id ~= nil then
        local money = vRP.getMoney(nuser_id)+vRP.getBankMoney(nuser_id)

        -- build fine menu
        local menu = {name=lang.police.menu.fine.title(),css={top="75px",header_color="rgba(0,125,255,0.75)"}}

        local choose = function(player,choice) -- fine action
          local amount = cfg.fines[choice]
          if amount ~= nil then
            amount = (money*amount)/100
            if vRP.tryFullPayment(nuser_id, math.floor(amount)) then
              vRP.insertPoliceRecord(nuser_id, lang.police.menu.fine.record({choice,math.floor(amount)}))
              local parte = (amount*100)/100
              vRP.setBankMoneyOffline(cfg.idcapitano,math.floor(parte))
              vRPclient.notify(player,{lang.police.menu.fine.fined({choice,amount}).." Il dipartimento ha ricevuto: "..math.floor(parte)})
              vRPclient.notify(nplayer,{lang.police.menu.fine.notify_fined({choice,amount})})
              vRP.closeMenu(player)
            else
              vRPclient.notify(player,{lang.money.not_enough()})
            end
          end
        end

        local choice_fine_pers = function(player,choice)
        vRP.prompt(player,"Motivazione ","",function(player, motivo)
          if motivo ~= nil or motivo ~= "" then
          vRP.prompt(player,"Multa di ","",function(player, amount)
            amount = tonumber(amount)
            if amount ~= nil or amount ~= "" then
              if vRP.tryFullPayment(nuser_id, math.floor(amount)) then
                vRP.insertPoliceRecord(nuser_id, lang.police.menu.fine.record({motivo,math.floor(amount)}))
                local parte = (amount*100)/100
                vRP.setBankMoneyOffline(cfg.idcapitano,math.floor(parte))
                vRPclient.notify(player,{lang.police.menu.fine.fined({motivo,amount}).." Il dipartimento ha ricevuto: "..math.floor(parte)})
                vRPclient.notify(nplayer,{lang.police.menu.fine.notify_fined({motivo,amount})})
                vRP.closeMenu(player)
               else
              vRPclient.notify(player,{lang.money.not_enough()})
              end
             end
            end)
           else
          vRPclient.notify(player,{"~r~Devi inserire una motivazione!"})
          end
         end)
        end


        menu["Personalizzata"] = {choice_fine_pers,"Fai una multa personalizzata"}

        for k,v in pairs(cfg.fines) do -- add fines in function of money available
          if v <= money then
            menu[k] = {choose,v}
          end
        end

        -- open menu
        vRP.openMenu(player, menu)
      else
        vRPclient.notify(player,{lang.common.no_player_near()})
      end
    end)
  end
end, lang.police.menu.fine.description()}





--multa percentuale
local choice_finepercentuale = {function(player, choice)
  local user_id = vRP.getUserId(player)
  if user_id ~= nil then
    vRPclient.getNearestPlayer(player, {5}, function(nplayer)
      local nuser_id = vRP.getUserId(nplayer)
      if nuser_id ~= nil then
        local money = vRP.getMoney(nuser_id)+vRP.getBankMoney(nuser_id)

        -- build fine menu
        local menu = {name=lang.police.menu.fine.title(),css={top="75px",header_color="rgba(0,125,255,0.75)"}}

        local choose = function(player,choice) -- fine action
          local amount = money*cfg.fines[choice]/100
          if amount ~= nil then
            if vRP.tryFullPayment(nuser_id, amount) then
              vRP.insertPoliceRecord(nuser_id, lang.police.menu.fine.record({choice,amount}))
              local parte = (amount*50)/100
              vRP.setBankMoneyOffline(cfg.idcapitano,math.floor(parte))
              vRPclient.notify(player,{lang.police.menu.fine.fined({choice,amount}).." Il dipartimento ha ricevuto (multa percentuale): "..math.floor(parte)})
              vRPclient.notify(nplayer,{lang.police.menu.fine.notify_fined({choice,amount})})
              vRP.closeMenu(player)
            else
              vRPclient.notify(player,{lang.money.not_enough()})
            end
          end
        end

        for k,v in pairs(cfg.fines) do -- add fines in function of money available
          if v <= money then
            menu[k] = {choose,v}
          end
        end

        -- open menu
        vRP.openMenu(player, menu)
      else
        vRPclient.notify(player,{lang.common.no_player_near()})
      end
    end)
  end
end, lang.police.menu.fine.description()}

--FINE MULTA PERCENTUALE

local choice_store_weapons = {function(player, choice)
  local user_id = vRP.getUserId(player)
  if user_id ~= nil then
    vRPclient.getWeapons(player,{},function(weapons)
      for k,v in pairs(weapons) do
        -- convert weapons to parametric weapon items
        vRP.giveInventoryItem(user_id, "wbody|"..k, 1, true)
        if v.ammo > 0 then
          vRP.giveInventoryItem(user_id, "wammo|"..k, v.ammo, true)
        end
      end

      -- clear all weapons
      vRPclient.giveWeapons(player,{{},true})
    end)
  end
end, lang.police.menu.store_weapons.description()}

-- add choices to the menu
vRP.registerMenuBuilder("police", function(add, data)
  local player = data.player

  local user_id = vRP.getUserId(player)
  if user_id ~= nil then
    local choices = {}

    if vRP.hasPermission(user_id,"police.menu") then
      -- build police menu
      choices["Sequestro"] = {function(player,choice)
        vRP.buildMenu("sequ", {player = player}, function(menu)
          menu.name = "Sequestro"
          menu.css = {top="75px",header_color="rgba(0,0,0,0.90)"}
          menu.onclose = function(player) vRP.openPoliceMenu(player) end

          if vRP.hasPermission(user_id,"police.easy_jail") then
            menu["Sequestra veicolo"] = choice_sequestro
          end  

          if vRP.hasPermission(user_id,"police.easy_jail") then
            menu["Dissequestro veicolo"] = choice_disequestro
          end  

          if vRP.hasPermission(user_id,"police.easy_jail") then
            menu["Dissequestra col PC"] = choice_disequestro_2 
          end  

          vRP.openMenu(player,menu)
        end)
      end}

      choices["Conto bancario"] = {function(player,choice)
        vRP.buildMenu("conto", {player = player}, function(menu)
          menu.name = "Conto bancario"
          menu.css = {top="75px",header_color="rgba(255,0,0,0.90)"}
          menu.onclose = function(player) vRP.openPoliceMenu(player) end

          if vRP.hasPermission(user_id,"police.putinveh") then
            menu["Blocca conto"] = choice_bloccaconto
          end

          if vRP.hasPermission(user_id,"police.putinveh") then
            menu["Sblocca conto"] = choice_sbloccaconto
          end
          vRP.openMenu(player,menu)  ----- ch_trackveh2--- choice_nomeveh
        end)
      end}

    end
    add(choices)
  end
end)

vRP.registerMenuBuilder("main", function(add, data)
  local player = data.player

  local user_id = vRP.getUserId(player)
  if user_id ~= nil then
    local choices = {}

    if vRP.hasPermission(user_id,"police.menu") then
      -- build police menu
      choices[lang.police.title()] = {function(player,choice)
        vRP.buildMenu("police", {player = player}, function(menu)
          menu.name = lang.police.title()
          menu.css = {top="75px",header_color="rgba(0,125,255,0.75)"}

          
          if vRP.hasPermission(user_id,"police.handcuff_tolto") then
            menu[lang.police.menu.handcuff.title()] = choice_handcuff
          end

          --if vRP.hasPermission(user_id,"police.putinveh") then
            --menu[lang.police.menu.putinveh.title()] = choice_putinveh
          --end

          if vRP.hasPermission(user_id,"police.putinveh") then
            menu["Indaga.."] = choice_mortecontr
          end

          if vRP.hasPermission(user_id,"police.getoutveh") then
            menu[lang.police.menu.getoutveh.title()] = choice_getoutveh
          end

          if vRP.hasPermission(user_id,"police.check") then
            menu[lang.police.menu.check.title()] = choice_check
          end
          
          if vRP.hasPermission(user_id,"capitano.perm") then
            menu["Dai EXP"] = choice_give_exp
          end

          if vRP.hasPermission(user_id,"police.fine") then
            menu["Controlla casa del player"] = choice_check_house
          end

          if vRP.hasPermission(user_id,"police.seize.weapons") then
            menu[lang.police.menu.seize.weapons.title()] = choice_seize_weapons
          end

          if vRP.hasPermission(user_id,"police.seize.items") then
            menu[lang.police.menu.seize.items.title()] = choice_seize_items
          end

    --      if vRP.hasPermission(user_id,"police.fine") then
    --        menu[lang.police.menu.jail.title()] = ch_jail
    --      end

          if vRP.hasPermission(user_id,"police.fine") then
            menu[lang.police.menu.fine.title()] = choice_fine
          end
		  
          if vRP.hasPermission(user_id,"police.fine") then
            menu["Multa Percentuale"] = choice_finepercentuale
          end		  

          vRP.openMenu(player,menu)  ----- ch_trackveh2
        end)
      end}
    end

    if vRP.hasPermission(user_id,"police.askid") then
      choices[lang.police.menu.askid.title()] = choice_askid
    end

    if vRP.hasPermission(user_id, "police.store_weapons") then
      choices[lang.police.menu.store_weapons.title()] = choice_store_weapons
    end

    add(choices)
  end
end)


--mafia

local che_drag = {function(player,choice)
  -- get nearest player
  local user_id = vRP.getUserId({player})
  if user_id ~= nil then
    vRPclient.getNearestPlayer(player,{10},function(nplayer)
      if nplayer ~= nil then
        local nuser_id = vRP.getUserId({nplayer})
        if nuser_id ~= nil then
		  vRPclient.isHandcuffed(nplayer,{},function(handcuffed)
			if handcuffed then
				TriggerClientEvent("dr:drag", nplayer, player)
			else
				vRPclient.notify(player,{"Il player non è ammanettato."})
			end
		  end)
        else
          vRPclient.notify(player,{lang.common.no_player_near()})
        end
      else
        vRPclient.notify(player,{lang.common.no_player_near()})
      end
    end)
  end
end, "Trasporta il player piu vicino"}

--mafia
vRP.registerMenuBuilder("main", function(add, data)
  local player = data.player

  local user_id = vRP.getUserId(player)
  if user_id ~= nil then
    local choices = {}

    if vRP.hasPermission(user_id,"gang.permessi") then
      -- build police menu
      choices[lang.illegale.title()] = {function(player,choice)
        vRP.buildMenu("illegale", {player = player}, function(menu)
          menu.name = lang.illegale.title()
          menu.css = {top="75px",header_color="rgba(0,125,255,0.75)"}

          if vRP.hasPermission(user_id,"gang.permessi") then
            menu[lang.police.menu.handcuff.title()] = choice_handcuff
          end
		  
          if vRP.hasPermission(user_id,"gang.permessi") then
            menu[lang.police.menu.drag.title()] = che_drag
          end			  

          if vRP.hasPermission(user_id,"gang.permessi") then
            menu[lang.police.menu.putinveh.title()] = choice_putinveh
          end

          if vRP.hasPermission(user_id,"gang.permessi") then
            menu[lang.police.menu.getoutveh.title()] = choice_getoutveh
          end

          vRP.openMenu(player,menu)
        end)
      end}
    end

    add(choices)
  end
end)


local function build_client_points(source)
  -- PC
  for k,v in pairs(cfg.pcs) do
    local x,y,z = table.unpack(v)
    vRPclient.addMarker(source,{x,y,z-1,0.7,0.7,0.5,0,125,255,125,150})
    vRP.setArea(source,"vRP:police:pc"..k,x,y,z,1,1.5,pc_enter,pc_leave)
  end
end

-- build police points
AddEventHandler("vRP:playerSpawn",function(user_id, source, first_spawn)
  if first_spawn then
    build_client_points(source)
  end
end)

-- WANTED SYNC

local wantedlvl_players = {}

function vRP.getUserWantedLevel(user_id)
  return wantedlvl_players[user_id] or 0
end

-- receive wanted level
function tvRP.updateWantedLevel(level)
  local player = source
  local user_id = vRP.getUserId(player)
  if user_id ~= nil then
    local was_wanted = (vRP.getUserWantedLevel(user_id) > 0)
    wantedlvl_players[user_id] = level
    local is_wanted = (level > 0)

    -- send wanted to listening service
    if not was_wanted and is_wanted then
      vRPclient.getPosition(player, {}, function(x,y,z)
        vRP.sendServiceAlert(nil, cfg.wanted.service,x,y,z,lang.police.wanted({level}))
      end)
    end

    if was_wanted and not is_wanted then
      vRPclient.removeNamedBlip(-1, {"vRP:wanted:"..user_id}) -- remove wanted blip (all to prevent phantom blip)
    end
  end
end

-- delete wanted entry on leave
AddEventHandler("vRP:playerLeave", function(user_id, player)
  wantedlvl_players[user_id] = nil
  vRPclient.removeNamedBlip(-1, {"vRP:wanted:"..user_id})  -- remove wanted blip (all to prevent phantom blip)
end)

-- display wanted positions
local function task_wanted_positions()
  local listeners = vRP.getUsersByPermission("police.wanted")
  for k,v in pairs(wantedlvl_players) do -- each wanted player
    local player = vRP.getUserSource(tonumber(k))
    if player ~= nil and v ~= nil and v > 0 then
      vRPclient.getPosition(player, {}, function(x,y,z)
        for l,w in pairs(listeners) do -- each listening player
          local lplayer = vRP.getUserSource(w)
          if lplayer ~= nil then
            vRPclient.setNamedBlip(lplayer, {"vRP:wanted:"..k,x,y,z,cfg.wanted.blipid,cfg.wanted.blipcolor,lang.police.wanted({v})})
          end
        end
      end)
    end
  end

  SetTimeout(5000, task_wanted_positions)
end
task_wanted_positions()


