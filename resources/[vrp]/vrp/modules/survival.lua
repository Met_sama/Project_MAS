local cfg = module("cfg/survival")
local lang = vRP.lang

-- api

function vRP.getHunger(user_id)
  local data = vRP.getUserDataTable(user_id)
  if data then
    return data.hunger
  end

  return 0
end

function vRP.getThirst(user_id)
  local data = vRP.getUserDataTable(user_id)
  if data then
    return data.thirst
  end

  return 0
end

function vRP.setHunger(user_id,value)
  local data = vRP.getUserDataTable(user_id)
  if data then
    data.hunger = value
    if data.hunger < 0 then data.hunger = 0
    elseif data.hunger > 100 then data.hunger = 100 
    end

    -- update bar
    local source = vRP.getUserSource(user_id)
    vRPclient.setProgressBarValue(source, {"vRP:hunger",data.hunger})
    if data.hunger >= 100 then
      vRPclient.setProgressBarText(source,{"vRP:hunger",lang.survival.starving()})
    else
      vRPclient.setProgressBarText(source,{"vRP:hunger",""})
    end
  end
end

function vRP.setThirst(user_id,value)
  local data = vRP.getUserDataTable(user_id)
  if data then
    data.thirst = value
    if data.thirst < 0 then data.thirst = 0
    elseif data.thirst > 100 then data.thirst = 100 
    end

    -- update bar
    local source = vRP.getUserSource(user_id)
    vRPclient.setProgressBarValue(source, {"vRP:thirst",data.thirst})
    if data.thirst >= 100 then
      vRPclient.setProgressBarText(source,{"vRP:thirst",lang.survival.thirsty()})
    else
      vRPclient.setProgressBarText(source,{"vRP:thirst",""})
    end
  end
end

function vRP.varyHunger(user_id, variation)
  local data = vRP.getUserDataTable(user_id)
  if data then
    local was_starving = data.hunger >= 100
    data.hunger = data.hunger + variation
    local is_starving = data.hunger >= 100

    -- apply overflow as damage
    local overflow = data.hunger-100
    if overflow > 0 then
      vRPclient.varyHealth(vRP.getUserSource(user_id),{-overflow*cfg.overflow_damage_factor})
    end

    if data.hunger < 0 then data.hunger = 0
    elseif data.hunger > 100 then data.hunger = 100 
    end

    -- set progress bar data
    local source = vRP.getUserSource(user_id)
    vRPclient.setProgressBarValue(source,{"vRP:hunger",data.hunger})
    if was_starving and not is_starving then
      vRPclient.setProgressBarText(source,{"vRP:hunger",""})
    elseif not was_starving and is_starving then
      vRPclient.setProgressBarText(source,{"vRP:hunger",lang.survival.starving()})
    end
  end
end

function vRP.varyThirst(user_id, variation)
  local data = vRP.getUserDataTable(user_id)
  if data then
    local was_thirsty = data.thirst >= 100
    data.thirst = data.thirst + variation
    local is_thirsty = data.thirst >= 100

    -- apply overflow as damage
    local overflow = data.thirst-100
    if overflow > 0 then
      vRPclient.varyHealth(vRP.getUserSource(user_id),{-overflow*cfg.overflow_damage_factor})
    end

    if data.thirst < 0 then data.thirst = 0
    elseif data.thirst > 100 then data.thirst = 100 
    end

    -- set progress bar data
    local source = vRP.getUserSource(user_id)
    vRPclient.setProgressBarValue(source,{"vRP:thirst",data.thirst})
    if was_thirsty and not is_thirsty then
      vRPclient.setProgressBarText(source,{"vRP:thirst",""})
    elseif not was_thirsty and is_thirsty then
      vRPclient.setProgressBarText(source,{"vRP:thirst",lang.survival.thirsty()})
    end
  end
end

-- tunnel api (expose some functions to clients)

function tvRP.varyHunger(variation)
  local user_id = vRP.getUserId(source)
  if user_id ~= nil then
    vRP.varyHunger(user_id,variation)
  end
end

function tvRP.varyThirst(variation)
  local user_id = vRP.getUserId(source)
  if user_id ~= nil then
    vRP.varyThirst(user_id,variation)
  end
end

-- tasks

-- hunger/thirst increase
function task_update()
  for k,v in pairs(vRP.users) do
    vRP.varyHunger(v,cfg.hunger_per_minute)
    vRP.varyThirst(v,cfg.thirst_per_minute)
  end

  SetTimeout(60000,task_update)
end
task_update()

-- handlers

-- init values
AddEventHandler("vRP:playerJoin",function(user_id,source,name,last_login)
  local data = vRP.getUserDataTable(user_id)
  if data.hunger == nil then
    data.hunger = 0
    data.thirst = 0
  end
end)

-- add survival progress bars on spawn
AddEventHandler("vRP:playerSpawn",function(user_id, source, first_spawn)
  local data = vRP.getUserDataTable(user_id)

  -- disable police
  vRPclient.setPolice(source,{cfg.police})
  -- set friendly fire
  vRPclient.setFriendlyFire(source,{cfg.pvp})

  vRPclient.setProgressBar(source,{"vRP:hunger","minimap",htxt,255,153,0,0})
  vRPclient.setProgressBar(source,{"vRP:thirst","minimap",ttxt,0,125,255,0})
  vRP.setHunger(user_id, data.hunger)
  vRP.setThirst(user_id, data.thirst)
end)

-- EMERGENCY

----Putinveh2

local choice_putinveh2 = {function(player,choice)
  vRPclient.getNearestPlayer(player,{10},function(nplayer)
    local nuser_id = vRP.getUserId(nplayer)
    if nuser_id ~= nil then
      vRPclient.isInComa(nplayer,{}, function(in_coma)
        if in_coma then
         vRPclient.putInNearestVehicleAsPassenger(nplayer, {5})
	      else
          vRPclient.notify(player,{"Player non in coma"})
        end
	     end)
	    else
	   vRPclient.notify(player,{lang.common.no_player_near()})
	  end
   end)
	end,lang.emergency.menu.putinveh2.description()}

----Getoutveh2
local choice_getoutveh2 = {function(player,choice)
  vRPclient.getNearestPlayer(player,{10},function(nplayer)
    local nuser_id = vRP.getUserId(nplayer)
    if nuser_id ~= nil then
       vRPclient.isInComa(nplayer,{}, function(in_coma)
        if in_coma then
         vRPclient.ejectVehicle(nplayer, {})
	      else
          vRPclient.notify(player,{"Player non in coma"})
        end
	  end)
	  else
	  vRPclient.notify(player,{lang.common.no_player_near()})
	 end
        end)
	end,lang.emergency.menu.getoutveh2.description()}

---- revive
local revive_seq = {
  {"mini@cpr@char_a@cpr_def","cpr_intro",1},
  {"mini@cpr@char_a@cpr_str","cpr_pumpchest",1},
  {"mini@cpr@char_a@cpr_str","cpr_pumpchest",1},
  {"mini@cpr@char_a@cpr_str","cpr_pumpchest",1},
  {"mini@cpr@char_a@cpr_str","cpr_pumpchest",1},
  {"mini@cpr@char_a@cpr_str","cpr_pumpchest",1},
  {"mini@cpr@char_a@cpr_str","cpr_pumpchest",1},
  {"mini@cpr@char_a@cpr_str","cpr_pumpchest",1},
  {"mini@cpr@char_a@cpr_str","cpr_pumpchest",1}
}

function tvRP.ControllaSangue(sangue, sangue2)
  sangue = string.upper(sangue)
  if sangue2 == "A" then
    if sangue == "A" then
      return true
    elseif sangue == "0" then
      return true
    else
      return false
    end
  end
  if sangue2 == "B" then
    if sangue == "B" then
      return true
    elseif sangue == "0" then
      return true
    else
      return false
    end
  end
  if sangue2 == "AB" then
      return true
  end
  if sangue2 == "0" then
    if sangue == "0" then
      return true
    else
      return false
    end
  end
end

local choice_revive = {function(player,choice)
  local chance = nil
  local user_id = vRP.getUserId(player)
  if user_id ~= nil then
    local exp = vRP.getExp(user_id,"jobs","medic")
    local lvl = math.floor(vRP.expToLevel(exp))
    vRPclient.getNearestPlayer(player,{10},function(nplayer)
      local nuser_id = vRP.getUserId(nplayer)
      if nuser_id ~= nil then
        TriggerClientEvent("dr:undrag", nplayer, player)
        vRPclient.isInComa(nplayer,{}, function(in_coma)
         local data = vRP.getUserDataTable(nuser_id)
          local sangue = nil
            if data.grupsangue == "A" then
              sangue = "sanguea"
            end
            if data.grupsangue == "B" then
              sangue = "sangueb"
            end
            if data.grupsangue == "AB" then
              sangue = "sangueab"
            end
            if data.grupsangue == "0" then
              sangue = "sangue0"
            end
          chance = math.random(1,lvl + 6)
          local chper = (1/(lvl+6))*100  
          if in_coma then
          vRP.prompt(player,"Sacca da utilizzare","",function(player, sangue2)
          if sangue2 ~= nil and sangue2 ~= "" then 
            if tvRP.ControllaSangue(sangue2, data.grupsangue) then
              if vRP.tryGetInventoryItem(user_id,"medkit",1,true) then
                local sanguenum = vRP.getInventoryItemAmount(user_id,sangue)
                if sanguenum >= 1 then
                  vRP.tryGetInventoryItem(user_id,sangue,1,true)
                  vRPclient.playAnim(player,{false,revive_seq,false})
                  vRPclient.notify(player,{"Possibilità di fallimento: "..math.floor(chper).."%"} )
                  --vRPclient.playAnim(player,{false,{{"mini@cpr@char_a@cpr_str","cpr_pumpchest",1}},true})
                  Citizen.Wait(20000)--20
                  if chance == 6 then
                    vRPclient.playAnim(player,{false,{{"mini@cpr@char_a@cpr_str","cpr_fail",1}},false})
                    --vRPclient.playAnim(nplayer,{false,{"mini@cpr@char_b@cpr_str","cpr_fail",1},false})
                    Citizen.Wait(5000)--5
                    vRPclient.killComa(nplayer) -- heal 50
                    vRPclient.notify(player,{"Hai fatto il possibile...."} )
                    vRPclient.notify(nplayer,{"Il medico ci ha provato, ma ha ~r~fallito."} )
                    vRP.varyExp(user_id,"jobs","medic",0.2)
                  else
                    vRPclient.playAnim(player,{false,{{"amb@medic@standing@kneel@exit","exit",1}},false})
                    SetTimeout(2000, function()
                      vRPclient.varyHealth(nplayer,{30})
                    end)
                    SetTimeout(7000, function()
                      vRPclient.varyHealth(nplayer,{30}) -- heal 50
                    end)
                    vRP.varyExp(user_id,"jobs","medic",0.5)
                  end
                else
                  vRPclient.notify(player,{"~r~Ti manca la sacca del sangue"})
                end
              end
            else
              if vRP.tryGetInventoryItem(user_id,"medkit",1,true) then
                local sanguenum = vRP.getInventoryItemAmount(user_id,sangue)
                if sanguenum >= 1 then
                  vRP.tryGetInventoryItem(user_id,sangue,1,true)
                  vRPclient.playAnim(player,{false,revive_seq,false})
                  vRPclient.notify(player,{"Possibilità di fallimento: "..math.floor(chper).."%"} )
                  --vRPclient.playAnim(player,{false,{{"mini@cpr@char_a@cpr_str","cpr_pumpchest",1}},true})
                  Citizen.Wait(20000) -- anim
                  vRPclient.playAnim(player,{false,{{"mini@cpr@char_a@cpr_str","cpr_fail",1}},false})
                  --vRPclient.playAnim(nplayer,{false,{"mini@cpr@char_b@cpr_str","cpr_fail",1},false})
                  Citizen.Wait(5000)
                  vRPclient.killComa(nplayer) -- heal 50
                  vRPclient.notify(player,{"~r~Sangue errato"} )
                  vRPclient.notify(nplayer,{"~r~Il medico ha provato a rianimarti con il SANGUE "..sangue2..", ma tu hai il GRUPPO "..data.grupsangue} )
                  vRP.varyExp(user_id,"jobs","medic",-0.8)
                else
                  vRPclient.notify(player,{"~r~Ti manca la sacca di sangue"})
                end
              end
            end
          else
            vRPclient.notify(player,{"~r~Non hai scritto niente"})
          end
          end)
          else
            vRPclient.notify(player,{lang.emergency.menu.revive.not_in_coma()})
          end
        end)
      else
        vRPclient.notify(player,{lang.common.no_player_near()})
      end
    end)
  end
end,lang.emergency.menu.revive.description()}

---Heal
local choice_contr = {function(player,choice)
  local user_id = vRP.getUserId(player)
  if user_id ~= nil then
    vRPclient.getNearestPlayer(player,{10},function(nplayer)
      local nuser_id = vRP.getUserId(nplayer)
      if nuser_id ~= nil then
        vRP.request(nplayer,"Il medico vuole prelevarti il sangue, accetti?",30,function(nplayer,ok)
          if ok then
        if vRP.tryGetInventoryItem(user_id,"provuota",1,true) then
          SetTimeout(2000, function()
          vRP.giveInventoryItem(user_id,"propiena",1,true)
          end)
        end
      else
        vRPclient.notify(player,{"~r~Il paziente ha rifiutato"})
      end
       end)
      else
        vRPclient.notify(player,{lang.common.no_player_near()})
      end
    end)
  end
end,"Controlla sangue"}

local choice_donazione = {function(player,choice)
  local user_id = vRP.getUserId(player)
   if user_id ~= nil then
    vRPclient.getNearestPlayer(player,{10},function(nplayer)
     local nuser_id = vRP.getUserId(nplayer)
      vRPclient.isInComa(nplayer,{}, function(in_coma)
       if not in_coma then
        if nuser_id ~= nil then
         vRP.request(nplayer,"Vuoi fare la donazione del sangue?~r~",30,function(nplayer,ok)
          if ok then
          local exp = vRP.getExp(nuser_id,"physical","strength")
            if exp > 0 then 
              local data = vRP.getUserDataTable(nuser_id)
              local tipo = string.lower(data.grupsangue)
              vRPclient.playAnim(nplayer,{false, {{"anim@mp_radio@garage@low","idle_a",1}}, false})
              SetTimeout(5000, function()
                vRP.giveInventoryItem(user_id,"sangue"..tipo,1,true)
                vRP.varyExp(user_id,"jobs","medic",0.2)
              end)
              vRPclient.stopAnim(nplayer,{true})
              vRPclient.stopAnim(nplayer,{false})
            else
              vRPclient.notify(player,{"~r~Questo player non può donare!"})
            end
            else
            vRPclient.notify(player,{"~r~Il player ha rifiutato"})
           end
          end)
         else
         vRPclient.notify(player,{lang.common.no_player_near()})
        end
       else
       vRPclient.notify(player,{"~r~Questo player non sta bene.."})
      end
     end)
    end)
  end
end,"Fai un prelievo di sangue al player più vicino."}


local choice_heal = {function(player,choice)
  local user_id = vRP.getUserId(player)
  if user_id ~= nil then
    vRPclient.getNearestPlayer(player,{10},function(nplayer)
      local nuser_id = vRP.getUserId(nplayer)
      if nuser_id ~= nil then
        vRPclient.isInComa(nplayer,{}, function(in_coma)
          if not in_coma then
             SetTimeout(1000, function()
              vRPclient.varyHealth(nplayer,{100})
			         vRPclient.notify(player,{lang.emergency.menu.revive.curato()})
              end)
            else
            vRPclient.notify(player,{lang.emergency.menu.revive.in_coma()})
          end
        end)
      else
        vRPclient.notify(player,{lang.common.no_player_near()})
      end
    end)
  end
end,lang.emergency.menu.revive.description()}
    
local revive_seq = {
  {"amb@medic@standing@kneel@enter","enter",1},
  {"amb@medic@standing@kneel@idle_a","idle_a",1},
  {"amb@medic@standing@kneel@exit","exit",1}
}

local ch_dragmed = {function(player,choice)
  -- get nearest player
  local user_id = vRP.getUserId(player)
  if user_id ~= nil then
    vRPclient.getNearestPlayer(player,{10},function(nplayer)
      if nplayer ~= nil then
      local nuser_id = vRP.getUserId(nplayer)
      if nuser_id ~= nil then
        vRPclient.isInComa(nplayer,{}, function(in_coma)
        if in_coma then
				TriggerClientEvent("dr:drag", nplayer, player)
			else
				vRPclient.notify(player,{"Il player non risulta in coma."})
			end
		  end)
        else
          vRPclient.notify(player,{lang.common.no_player_near()})
        end
      else
        vRPclient.notify(player,{lang.common.no_player_near()})
      end
    end)
  end
end, "Trasporta il player piu vicino"}

function vRP.controllamorteespe(weaponHash)
  if weaponHash ~= nil then
		if weaponHash == GetHashKey("WEAPON_RUN_OVER_BY_CAR") or weaponHash == GetHashKey("WEAPON_RAMMED_BY_CAR") then
			return "Probabilmente un incidente.."
		end
		if weaponHash == GetHashKey("WEAPON_CROWBAR") or weaponHash == GetHashKey("WEAPON_BAT") or weaponHash == GetHashKey("WEAPON_HAMMER") or weaponHash == GetHashKey("WEAPON_GOLFCLUB") or weaponHash == GetHashKey("WEAPON_NIGHTSTICK") or weaponHash == GetHashKey("WEAPON_KNUCKLE") then
			return "E' sicuramente un arma bianca!"
		end
		if weaponHash == GetHashKey("WEAPON_DAGGER") or weaponHash == GetHashKey("WEAPON_KNIFE") then
			return "E' stato sicuramento accoltellato"
		end
		if weaponHash == GetHashKey("WEAPON_SNSPISTOL") or weaponHash == GetHashKey("WEAPON_HEAVYPISTOL") or weaponHash == GetHashKey("WEAPON_VINTAGEPISTOL") or weaponHash == GetHashKey("WEAPON_PISTOL") or weaponHash == GetHashKey("WEAPON_APPISTOL") or weaponHash == GetHashKey("WEAPON_COMBATPISTOL") or weaponHash == GetHashKey("WEAPON_SNSPISTOL") then
			return "Probabilmente è morto a causa di una pistola"
		end
		if weaponHash == GetHashKey("WEAPON_GRENADELAUNCHER") or weaponHash == GetHashKey("WEAPON_HOMINGLAUNCHER") or weaponHash == GetHashKey("WEAPON_STICKYBOMB") or weaponHash == GetHashKey("WEAPON_PROXMINE") or weaponHash == GetHashKey("WEAPON_RPG") or weaponHash == GetHashKey("WEAPON_EXPLOSION") or weaponHash == GetHashKey("VEHICLE_WEAPON_TANK") then
			return "Probabilmente è morto per qualcosa di esplosivo"
		end
		if weaponHash == GetHashKey("WEAPON_MICROSMG") or weaponHash == GetHashKey("WEAPON_SMG") or weaponHash == GetHashKey("WEAPON_ASSAULTSMG") or weaponHash == GetHashKey("WEAPON_MG") or weaponHash == GetHashKey("WEAPON_COMBATMG") or weaponHash == GetHashKey("WEAPON_COMBATPDW") or weaponHash == GetHashKey("WEAPON_MINIGUN") then
			return "Probabilmente è morto a causa di una smg"
		end
		if weaponHash == GetHashKey("WEAPON_ASSAULTRIFLE") or weaponHash == GetHashKey("WEAPON_CARBINERIFLE") or weaponHash == GetHashKey("WEAPON_ADVANCEDRIFLE") or weaponHash == GetHashKey("WEAPON_BULLPUPRIFLE") or weaponHash == GetHashKey("WEAPON_SPECIALCARBINE") or weaponHash == GetHashKey("WEAPON_GUSENBERG") then
			return "Probabilmente è morto a causa di un fucile d'assalto"
		end
		if weaponHash == GetHashKey("WEAPON_MARKSMANRIFLE") or weaponHash == GetHashKey("WEAPON_SNIPERRIFLE") or weaponHash == GetHashKey("WEAPON_HEAVYSNIPER") or weaponHash == GetHashKey("WEAPON_ASSAULTSNIPER") or weaponHash == GetHashKey("WEAPON_REMOTESNIPER") then
			return "Probabilmente è morto a causa di una pistola, forse."
		end
		if weaponHash == GetHashKey("WEAPON_BULLPUPSHOTGUN") or weaponHash == GetHashKey("WEAPON_ASSAULTSHOTGUN") or weaponHash == GetHashKey("WEAPON_PUMPSHOTGUN") or weaponHash == GetHashKey("WEAPON_HEAVYSHOTGUN") or weaponHash == GetHashKey("WEAPON_SAWNOFFSHOTGUN") then
			return "Probabilmente è morto a causa di un fucile"
		end
		if weaponHash == GetHashKey("WEAPON_HATCHET") or weaponHash == GetHashKey("WEAPON_MACHETE") then
			return "Probabilmente è morto per un coltello"
		end
		if weaponHash == GetHashKey("WEAPON_MOLOTOV") then
			return "Probabilmente è morto per il fuoco"
		end
		return "Scoonosciuta"
	end
  return "Sconosciuta"
end

local choice_contrmedicmorte = {function(player,choice) 
  vRPclient.getNearestPlayer(player,{10},function(nplayer)
    local nuser_id = vRP.getUserId(nplayer)
    if nuser_id ~= nil then
      vRPclient.isInComa(nplayer,{}, function(in_coma)
        if in_coma then
          local aspetta = math.random(30,60)
          vRPclient.playAnim(player,{false,revive_seq,false}) -- anim
          Citizen.Wait(aspetta*1000)
          local data = vRP.getUserDataTable(nuser_id)
          local morte = vRP.controllamorteespe(data.ultimamorte)
          vRPclient.notify(player,{""..GetPlayerName(nplayer).." è morto per: "..morte})
        else
         vRPclient.notify(player,{"~r~Player non in coma.."})
        end
    end)
  end
 end)
end}

local function ch_contrmedicmorte(player,choice)
  vRPclient.getNearestPlayer(player,{10},function(nplayer)
    local nuser_id = vRP.getUserId(nplayer)
    if nuser_id ~= nil then
      vRPclient.isInComa(nplayer,{}, function(in_coma)
        if in_coma then
          local aspetta = math.random(30,60)
          vRPclient.playAnim(player,{false,revive_seq,false}) -- anim
          Citizen.Wait(aspetta*1000)
          local data = vRP.getUserDataTable(nuser_id)
          local morte = vRP.controllamorteespe(data.ultimamorte)
          vRPclient.notify(player,{""..GetPlayerName(nplayer).." è morto per: "..morte})
        else
         vRPclient.notify(player,{"~r~Player non in coma.."})
        end
    end)
  end
 end)
end -- heal 50

-- add choices to the main menu (emergency)
vRP.registerMenuBuilder("main", function(add, data)
  local player = data.player

  local user_id = vRP.getUserId(player)
  if user_id ~= nil then
    local choices = {}--choice_donazione

     choices[lang.emergency.title()] = {function(player,choice)
      local menu = {name=lang.emergency.title(),css={top="75px",header_color="rgba(0,125,255,0.75)"}}

      if vRP.hasPermission(user_id,"emergency.revive") then
        menu[lang.emergency.menu.putinveh2.title()] = choice_putinveh2
      end--choice_mortecontresp 
      
      if vRP.hasPermission(user_id,"emergency.revive") then
        menu["Donazione"] = choice_donazione
      end
      
      if vRP.hasPermission(user_id,"emergency.revive") then
        menu["Prelievo"] = choice_contr
      end

      if vRP.hasPermission(user_id,"emergency.revive") then
        menu["Controllo Morte"] = choice_contrmedicmorte
      end

      if vRP.hasPermission(user_id,"emergency.revive") then
        menu["Trasporta paziente"] = ch_dragmed
      end

      if vRP.hasPermission(user_id,"emergency.revive") then
        menu[lang.emergency.menu.getoutveh2.title()] = choice_getoutveh2
      end
	  
	  if vRP.hasPermission(user_id,"emergency.revive") then
        menu[lang.emergency.menu.heal.title()] = choice_heal
      end

      if vRP.hasPermission(user_id, "emergency.revive") then
        menu[lang.emergency.menu.revive.title()] = choice_revive
      end

      vRP.openMenu(player,menu)
    end}

    add(choices)
  end
end)

local menu_ems = {name="Controllo",css={top="75px",header_color="rgba(0,255,0,0.95)"}}
    
local function ch_analizza(player,choice)
  local user_id = vRP.getUserId(player)
  if user_id ~= nil then
  vRP.closeMenu(player)
  vRPclient.getNearestPlayer(player,{10},function(nplayer)
    local nuser_id = vRP.getUserId(nplayer)
    if nuser_id ~= nil then
      local data = vRP.getUserDataTable(nuser_id)
      tipo = string.lower(data.grupsangue)
      if vRP.tryGetInventoryItem(user_id,"propiena",1,true) then
        vRP.getUData(nuser_id,"vRP:malattia",function(value)
          if value ~= nil then
            local malattia = json.decode(value)
            if malattia ~= nil then
              vRP.giveInventoryItem(user_id,"analisi"..tipo.."_2",1,true)
              vRPclient.notify(player,{"~b~Sangue di ("..GetPlayerName(nplayer)..") tipo:"..data.grupsangue..", Malattie: "..malattia})
            else
              vRP.giveInventoryItem(user_id,"analisi"..tipo.."_2",1,true)
              vRPclient.notify(player,{"~b~Sangue di ("..GetPlayerName(nplayer)..") tipo:"..data.grupsangue})
            end
          else
            vRP.giveInventoryItem(user_id,"analisi"..tipo.."_2",1,true)
            vRPclient.notify(player,{"~b~Sangue di ("..GetPlayerName(nplayer)..") tipo:"..data.grupsangue})
          end
        end)
      end
     else
    vRPclient.notify(player,{"~r~Nessun player vicino"})
    end
  end)
 end
end

menu_ems["Controllo"] = {ch_contrmedicmorte}
menu_ems["Analisi"] = {ch_analizza}
menu_ems.onclose = function(player) -- close pc gui
  vRPclient.removeDiv(player,{"ems_as"})
end

local analisi = {
  {337.65161132812,-586.86059570312,43.32633972168}
  }

local function build_client_chest_ems(source)
  local user_id = vRP.getUserId(source)
  if user_id ~= nil then
    for k,v in pairs(analisi) do
      local x,y,z = table.unpack(v)

      local function emsanalisi_enter(source)
        local user_id = vRP.getUserId(source)
        if user_id ~= nil then
          if vRP.hasPermission(user_id,"emergency.revive") then
            vRP.openMenu(source,menu_ems)
          end
        end
      end

      local function emsanalisi_leave(source)
        vRP.closeMenu(source)
      end

      --vRPclient.addBlip(source,{x,y,z,153,49,"Analisi del sangue"})
      vRPclient.addMarker(source,{x,y,z-1,0.7,0.7,0.5,0,255,125,125,150})

      vRP.setArea(source,"vRP:ems_analisi"..k,x,y,z,3,1.5,emsanalisi_enter,emsanalisi_leave)
    end
  end
end

-- build police points
AddEventHandler("vRP:playerSpawn",function(user_id, source, first_spawn)
  if first_spawn then
    build_client_chest_ems(source)
  end
end)

