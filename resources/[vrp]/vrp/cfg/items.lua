-- define items, see the Inventory API on github

local cfg = {}
-- see the manual to understand how to create parametric items
-- idname = {name or genfunc, description or genfunc, genfunc choices or nil, weight or genfunc}
-- a good practice is to create your own item pack file instead of adding items here
cfg.items = {
  ["benzoilmetilecgonina"] = {"Benzoilmetilecgonina", "Usato per fare cocaina.", nil, 1.00}, -- no choices
  

  --  Creazioni armi e munizioni
  ["ferro"] = {"Ferro", "Utilizzato per vari crafting", nil, 1.20}, -- no choices
  ["nitrato_di_potassio"] = {"Nitrato", "nitrato di potassio", nil, 0.60}, -- no choices
  ["carbone"] = {"Carbone", "Carbone", nil, 1.20}, -- no choices
  ["zolfo"] = {"Zolfo", "Zollfo", nil, 0.70}, -- no choices
  ["oro"] = {"Oro", "", nil, 1.6}, -- no choices
  ["rame"] = {"Rame", "Rame", nil, 1.2},
  ["piombo"] = {"Piombo", "Piombo", nil, 1.1},
  ["tungsteno"] = {"Tungsteno", "Lega Metallica Utilizzata per creare principalmente armi", nil, 25.0},
  ["cobalto"] = {"Cobalto", "", nil, 1.3},
  ["nickel"] = {"Nickel", "", nil, 0.5},
  ["magnesio"] = {"Magnesio", "", nil, 0.6},
  ["polvere_nera"] = {"Polvere Da Sparo", "", nil, 0.2},

  ["canna_mk2"] = {"Canna MK2.0", "Componente necessario per armi di diverso calibro", nil, 5.0},
  ["canna_mk3"] = {"Canna MK3.0", "Componente necessario per armi di diverso calibro", nil, 7.5},
  ["canna_mk4"] = {"Canna MK4.0", "Componente necessario per armi di diverso calibro", nil, 10.0},


  ["sangue"] = {"Sangue", "Sangue buono", nil, 1},
  
  ["caffè"] = {"Caffè", "Ottimo caffè, chissà cosa si può fare!", nil, 0.5},
  ["caffeina"] = {"Caffeina", "Una sostanza un pò strana.. però buona!", nil, 1},
  
  --Negozio animali  
  ["croquettes"] = {"Cocchette", "Bocconcini per animali", nil, 0.25}, 
  
  ["proteine"] = {"Proteine", "Una sostanza che migliora i tuoi risultati in palestra!", nil, 1},
  
  ["pennablu"] = {"Penna Blu", "Usala per firmare in blu", nil, 0.25}, 
  ["pennanera"] = {"Penna Nera", "Usala per firmare in nero", nil, 0.25}, 

  --sangue
["sanguea"] = {"Sangue A", "Sangue TIPO:A",nil, 0.2},
["sangueb"] = {"Sangue B", "Sangue TIPO:B",nil, 0.2},
["sangueab"] = {"Sangue AB", "Sangue TIPO:AB",nil, 0.2},
["sangue0"] = {"Sangue 0", "Sangue TIPO:0",nil, 0.2},
["provuota"] = {"Provetta vuota", "Provetta vuota",nil, 0.1},
["propiena"] = {"Provetta con sangue", "Provetta con del sangue",nil, 0.15},
["analisia_2"] = {"Provetta | A", "Provetta:A",nil, 0.15},
["analisib_2"] = {"Provetta | B", "Provetta:B",nil, 0.15},
["analisiab_2"] = {"Provetta | AB", "Provetta:AB",nil, 0.15},
["analisi0_2"] = {"Provetta | 0", "Provetta:0",nil, 0.15}, 
  
  ---droghe
  ["krokodil"] = {"Krokodil", "Qualcosa di nuovo è arrivato in città!", nil, 1.00},
  
  ["papavero"] = {"Papavero", "Qualcosa di nuovo è arrivato in città!", nil, 1.00},
  
  ["oppio"] = {"Oppio", "Sembra oppio...", nil, 0.50},
  ["oppio_mix"] = {"Oppio Mix", "Sembra oppio...", nil, 1.00},
  ["oppio_super"] = {"Oppio Super", "Sembra oppio...", nil, 2.00},

  ["cocaina_super"] = {"Cocaina Super", "?????...", nil, 2.00},
  ["cocaina_mix"] = {"Cocaina Mix", "?????...", nil, 1.00},
  ["metadone"] = {"Metadone", "", nil, 0.50}, 
  ["metadone_easy"] = {"Metadone Semplice", "", nil, 0.50},  
  ["metadone_oppio"] = {"Metadone processato con Oppio", "", nil, 1.00},
  ["metadone_maria"] = {"Metadone processato con Maria", "", nil, 1.00},
  ["metadone_mix"] = {"Metadone Mix", "", nil, 2.00},
  ["metadone_super"] = {"Metadone Super", "", nil , 2.00},

  ["meth_processata"] = {"Meth processata", "", nil, 1.00},  
  ["meth_semplice"] = {"Meth semplice", "", nil, 0.50},  
  ["meth_mix"] = {"Meth Mix", "", nil, 2.00},
  ["meth_super"] = {"Meth Super", "", nil, 2.00},
  
  ["tesseramec"] = {"Tessera Meccanici", "Tessera Soccorso Stradale", nil, 0},
  ["tesseramed"] = {"Tessera Medici", "Tessera Medici", nil, 0},

  ["burro"] = {"Burro", "", nil, 0.10}, -- no choices
  ["smeraldo"] = {"Smeraldo", "", nil, 0.95}, -- no choices
  ["zaffiro"] = {"Zaffiro", "", nil, 0.95}, -- no choices
  ["aragosta"] = {"Aragosta", "", nil, 1.20}, -- no choices
  ["perla"] = {"Perle", "", nil, 0.80}, -- no choices
  ["ametista"] = {"Ametista", "", nil, 1.05}, -- no choices
  ["grappag"] = {"Grappa grezza", "Necessita di esser distilliato in distilleria", nil, 0.8}, -- no choices
  ["alcool"] = {"Alcool", "Usato per la fermentazione", nil, 0.8}, -- no choices
  ["whiskyg"] = {"Whisky da distillare", "Necessita di esser distilliato in distilleria", nil, 0.8}, -- no choices
  --["AK47"] = {"AK47", "A Russian masterpeice.", nil, 2.00}, -- no choices
  ["cargo"] = {"Pallet per aereo cargo", "Usato dal pilota cargo.", nil, 35.00}, -- no choices
  ["morfina"] = {"Morfina", "Usata dal medico per fare pillole insieme all'acido Acetilsalicilico.", nil, 0.01}, -- no choices
  ["acidoacetilsalicilico"] = {"Acido Acetilsalicilico", "Usato dal medico per fare pillole insieme alla morfina.", nil, 0.01}, -- no choices
  ["aci"] = {"Acido Idroiodico", "Usato per combinarlo con l'efedrina per fare metanfetamina.Illegale", nil, 0.50}, -- no choices
  ["acid"] = {"Efedrina", "Usata per combinarla con l'acido idroionico per fare metanfetamina.Illegale", nil, 0.50}, -- no choices
  --["M4A1"] = {"M4A1", "Helps give non-Americans freedom.", nil, 2.00}, -- no choices
  
  ["ptrasporto"] = {"Permesso Trasporto", "", nil, 0.10}, -- 
  ["boccetta"] = {"Boccetta Vitamine", "", nil, 0.10}, -- s
  ["maschera"] = {"Maschera", "Usalo per non farti riconoscere", nil, 0.10}, -- s
 
  
  --gioielli
  ["orecchini"] = {"Orecchini", "Ottimi accessori", nil, 0.15}, -- s
  ["gemelli"] = {"Gemelli", "Ottimi accessori", nil, 0.05}, -- s
  ["orologio"] = {"Orologio", "Ottimi accessori", nil, 0.60}, -- s
  ["collana_c"] = {"Collana con ciondolo", "Ottimi accessori", nil, 0.60}, -- s
  ["bracciale"] = {"Bracciale", "", nil, 0.80}, -- no choices 
  ["anello"] = {"Anello", "", nil, 0.15}, -- no choicesweapon_parts
  ["collana_perle"] = {"Collana di perle", "", nil, 0.90}, -- no choices
  ["anello_girlfriend"] = {"Anello Di fidanzamento", "", nil, 0.10}, -- no choices

  
  
  
  --pezziarmi
  ["pezzo_pistola"] = {"Pezzo Pistola", "Pezzo per fare le pistole", nil, 0.90}, -- no choices
  ["pezzo_carabina"] = {"Pezzo Carabina", "Pezzo per fare le carabine", nil, 0.90}, -- no choices
  ["pezzo_pistolac"] = {"Pezzo Pistola da Combattimento", "Pezzo per fare le pistole da combattimento", nil, 0.90}, -- no choices
  ["pezzo_ak"] = {"Pezzo AK-47", "Pezzo per fare gli ak-47", nil, 0.90}, -- no choices
  ["pezzo_pompa"] = {"Pezzo Fucile a Pompa", "Pezzo per fare i fucili a pompa", nil, 0.90}, -- no choices
  ["pezzo_smg"] = {"Pezzo Micro SMG", "Pezzo per fare i micro smg", nil, 0.90}, -- no choices
  ["pezzo_storditore"] = {"Pezzo Storditore", "Pezzo per fare gli storditori", nil, 0.90}, -- no choices

  
  
  ["pallet1"] = {"Pallet Alimentare", "Imballaggio per l'esportazione", nil, 0.80}, -- no choices
  ["casse"] = {"Cassa Pesce", "Venduta al mercato ittico", nil, 8.80}, -- no choices
  ["pallet2"] = {"Pallet Industriale", "Imballaggio per l'esportazione", nil, 0.80}, -- no choices
  ["pallet3"] = {"Pallet di bottiglie di whysky", "Imballaggio per l'esportazione di alcolici", nil, 100.80}, -- no choices
  ["pallet4"] = {"Pallet di bottiglie di grappa", "Imballaggio per l'esportazione di alcolici", nil, 100.80}, -- no choices
  ["schede"] = {"Schede Elettroniche", "Usate per vari strumenti elettronici", nil, 0.80}, -- no choices
  ["mandato"] = {"Mandato", "Rilasciato dal presidente del perquisizioni.", nil, 0.01}, -- no choices
  ["orzo"] = {"Orzo", "Usato per fare la birra al birrificio insieme con il luppolo e vari processi di fermentazione alcolica", nil, 0.10}, -- no choices
  ["grano"] = {"Grano", "Grano duro usato per fare la farina al mulino e vari processi di fermentazione alcolica.", nil, 0.10}, -- no choices
  --["orzo"] = {"Orzo", "Usato per fare marjuana.", nil, 0.50}, -- no choices
  ["olio"] = {"Olio", "Ottimo per la frittura.", nil, 0.50}, -- no choices
  ["olive"] = {"Olive", "Usate al frantoio per ricavare olio.", nil, 0.50}, -- no choices
  ["luppolo"] = {"Luppolo", "Usato per fare la birra al birrificio insieme con l'orzo.", nil, 0.20}, -- no choices
  --processo Marijuana
  ["cimetta"] = {"Cimetta", "Cimetta per marijuana", nil, 0.50}, -- no choices
  ["me"] = {"Metanfetamina grezza", "", nil, 0.80}, -- no choices
  ["farina"] = {"Farina", "Usato per fare il pane insieme all'acqua al forno.", nil, 0.40}, -- no choices
  ["tronchi"] = {"Tronchi", "Tronchi di abete usati in segheria.", nil, 20.50}, -- no choices
  ["pellet"] = {"Pellet", "Ricavati dai tronchi e venduti.", nil, 0.50}, -- no choices
  ["tavole"] = {"Tavole", "Ricavate dai tronchi e usate per fare mobili.", nil, 1.00}, -- no choices
  ["pedana"] = {"Pedana", "Ricavate dai tronchi e vendute.", nil, 1.00}, -- no choices
  ["mobili"] = {"Mobili", "Ricavati dalle tavole e venduti.", nil, 1.50}, -- no choices
  ["polvere_nera"] = {"Polvere da sparo", "Ottima per l'esplosivo.Ovviamente illegale", nil, 0.95}, -- no choices
  ["cristalli"] = {"Cristalli", "Usati per fare cristalli puri.", nil, 0.50}, -- no choices
  ["cristalli_p"] = {"Cristalli puri", "Usati per fare metanfetamina.", nil, 0.50}, -- no choices
  ["abilitazione"] = {"Licenza Avvocato", "Necessaria per esercitare la difesa in un processo", nil, 0.01}, -- no choices
  ["credit"] = {"Credit Card", "Credit card.", nil, 0.01}, -- no choices
  ["driver"] = {"Driver license Card", "license card.", nil, 0.01}, -- no choices
  ["licenza_taglie"] = {"Licenza cacciatori di taglia", "Licenza riconosciuta dallo stato per i cacciatori di taglia", nil, 0.01}, -- no choices
  ["tessera"] = {"Tessera sanitaria.", "Tessera sanitaria rilasciata dai medici su apposita sottroscrizione del paziente.", nil, 0.01}, -- no choices
  ["licenza_armi"] = {"Porto d'armi", "license armi.", nil, 0.01}, -- no choices
  ["bank_money"] = {"Denaro in banca", "$.", nil, 0}, -- no choices
  
  ["key_lspd"] = {"Mazzo di chiavi", "Mazzo di chiavi sconosciuto..", nil, 0},  
  ["key_armeria"] = {"Chiavi Armeria", "Mazzo di chiavi dell'armeria", nil, 0},  
  ["key_ems"] = {"Chiavi Medici", "Mazzo di chiavi dell'ospedale", nil, 0},  
  ["key_yakuza"] = {"Chiavi Sconosciute", "Mazzo di chiavi sconosciute", nil, 0},  
  
  ["key_lab"] = {"Sconosciuto", "Qualcosa simile a una chiave..", nil, 0} -- no choices
}

-- load more items function
local function load_item_pack(name)
  local items = module("cfg/item/"..name)
  if items then
    for k,v in pairs(items) do
      cfg.items[k] = v
    end
  else
    print("[vRP] item pack ["..name.."] not found")
  end
end

-- PACKS
load_item_pack("required")
load_item_pack("food")
load_item_pack("drugs")

return cfg
