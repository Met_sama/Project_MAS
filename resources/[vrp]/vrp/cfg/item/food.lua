-- define some basic inventory items

local items = {}

local function play_eat(player)
  local seq = {
    {"mp_player_inteat@burger", "mp_player_int_eat_burger_enter",1},
    {"mp_player_inteat@burger", "mp_player_int_eat_burger",1},
    {"mp_player_inteat@burger", "mp_player_int_eat_burger_fp",1},
    {"mp_player_inteat@burger", "mp_player_int_eat_exit_burger",1}
  }

  vRPclient.playAnim(player,{true,seq,false})
end

local function play_drink(player)
  local seq = {
    {"mp_player_intdrink","intro_bottle",1},
    {"mp_player_intdrink","loop_bottle",1},
    {"mp_player_intdrink","outro_bottle",1}
  }

  vRPclient.playAnim(player,{true,seq,false})
end

-- gen food choices as genfunc
-- idname
-- ftype: eat or drink
-- vary_hunger
-- vary_thirst
local function gen(ftype, vary_hunger, vary_thirst)
  local fgen = function(args)
    local idname = args[1]
    local choices = {}
    local act = "Unknown"
    if ftype == "eat" then act = "Mangia" elseif ftype == "drink" then act = "Bevi" end
    local name = vRP.getItemName(idname)

    choices[act] = {function(player,choice)
      local user_id = vRP.getUserId(player)
      if user_id ~= nil then
        if vRP.tryGetInventoryItem(user_id,idname,1,false) then
          if vary_hunger ~= 0 then vRP.varyHunger(user_id,vary_hunger) end
          if vary_thirst ~= 0 then vRP.varyThirst(user_id,vary_thirst) end

          if ftype == "drink" then
            vRPclient.notify(player,{"~b~ Bevendo "..name.."."})
            play_drink(player)
          elseif ftype == "eat" then
            vRPclient.notify(player,{"~o~ Mangiando "..name.."."})
            play_eat(player)
          end

          vRP.closeMenu(player)
        end
      end
    end}

    return choices
  end

  return fgen
end

-- DRINKS --

items["acqua"] = {"Bottiglia d'acqua","", gen("drink",0,-25),0.1}
items["latte"] = {"Latte","", gen("drink",0,-5),0.5}
items["coffe"] = {"Caffe","", gen("drink",0,-10),0.2}
items["tea"] = {"Tea","", gen("drink",0,-15),0.2}
items["limonata"] = {"ice-Tea","", gen("drink",0,-20), 0.5}
items["milkshake"] = {"Milkshake","", gen("drink",0,-20), 0.5}
items["succodifrutta"] = {"Orange Juice","", gen("drink",0,-25),0.5}
items["cocacola"] = {"Coca Cola","", gen("drink",0,-35),0.3}
items["redbull"] = {"Redbull","", gen("drink",0,-40),0.3}
items["vodka"] = {"Vodka","", gen("drink",15,-65),0.5}
items["vino"] = {"Nero D'avola","", gen("drink",-5,-65),0.5}
items["birra"] = {"Birra Los Santos","", gen("drink",-1,-65),0.5}
items["tisana"] = {"Tisana alla Marjuana","Ottima per rilassarsi", gen("drink",-500,-800),0.45}

--FOOD

-- create Breed item
items["pane"] = {"Pane","", gen("eat",-50,0),0.5}
items["hamburger"] = {"Hamburger","", gen("eat",-50,0),0.5}

items["uva"] = {"Uva","", gen("eat",-50,-50),0.5}
items["ciambella"] = {"Ciambella","", gen("eat",-15,0),0.2}
items["tacos"] = {"Tacos","", gen("eat",-20,0),0.2}
items["bisco"] = {"Biscotti al latte","", gen("eat",-40,0),0.2}
items["sandwich"] = {"Sandwich","A tasty snack.", gen("eat",-50,0),0.5}
items["kebab"] = {"Kebab","", gen("eat",-45,0),0.85}
items["vitello"] = {"Vitello di prima Qualità","", gen("eat",-65,0),0.5}
items["peperoni"] = {"Peperoni","", gen("eat",-30,-30),0.5}
items["pomodori"] = {"Pomodori","", gen("eat",-30,-30),0.5}
items["melenzane"] = {"Melenzane","", gen("eat",-30,-30),0.5}
items["fritte"] = {"Patatine fritte","", gen("eat",-10,-10),0.5}
items["mele"] = {"Mele","", gen("eat",-30,-30),0.5}
items["cefalo"] = {"Cefalo","", gen("eat",-10,15),0.50}
items["orata"] = {"Orata","", gen("eat",-10,15),0.90}
items["spada"] = {"Pesce Spada", "Ottimo per gli involtini", gen("eat",-10,15),10.3}
items["tonno"] = {"Tonno", "Ottimo per gli involtini", gen("eat",-10,15),10.3}
items["calamari"] = {"Calamari freschi", "Ottimo per gli involtini", gen("eat",-10,15),0.90}
items["seppie"] = {"Seppie", "Ottimo per gli involtini", gen("eat",-10,15),0.90}
items["scatola"] = {"Tonno in scatola","", gen("eat",-55,-10),0.85}
items["zuppa"] = {"Zuppa di pomodori","", gen("eat",-35,-15),0.85}
items["zuppa333"] = {"Zuppa di aragoste","", gen("eat",-65,-15),0.85}
items["involtini"] = {"Involtini di pesce spada","", gen("eat",-45,0),0.85}
items["insalata"] = {"Insalata di mare","", gen("eat",-45,-10),0.85}
items["ripieni"] = {"Calamari ripieni","", gen("eat",-85,-20),0.85}
items["gambero"] = {"Gambero Rosso","Ottimo per l'insalata", gen("eat",-15,-15),0.85}
items["polpo"] = {"Polipo","Ottimo per l'insalata", gen("eat",-15,-15),0.85}

return items
