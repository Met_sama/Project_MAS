
local items = {}

items["medkit"] = {"Medic Kit","",nil,0.5}
items["dirty_money"] = {"Soldi sporchi","",nil,0}
items["repairkit"] = {"Kit di riparazione","",nil,0.5}


--ARMI
items["wbody|WEAPON_SNIPERRIFLE"] = {"R700", "",nil,0.75}
items["wbody|WEAPON_FIREEXTINGUISHER"] = {"Fire Extinguisher", "",nil,0.75}
items["wbody|WEAPON_COMPACTGRENADELAUNCHER"] = {"M79 Thumper", "",nil,0.75}
items["wbody|WEAPON_SNOWBALL"] = {"Snowball", "",nil,0.75}
items["wbody|WEAPON_VINTAGEPISTOL"] = {"Pistola Vintage", "",nil,0.75}
items["wbody|WEAPON_COMBATPDW"] = {"MPX", "",nil,0.75}
items["wbody|WEAPON_HEAVYSNIPER_MK2"] = {"Barret 50.Cal", "",nil,0.75}
items["wbody|WEAPON_HEAVYSNIPER"] = {"Hai Cheattato Bravo", "",nil,0.75}
items["wbody|WEAPON_SWEEPERSHOTGUN"] = {"Striker", "",nil,0.75}
items["wbody|WEAPON_MICROSMG"] = {"UZI", "",nil,0.75}
items["wbody|WEAPON_WRENCH"] = {"Pipe Wrench", "",nil,0.75}
items["wbody|WEAPON_PISTOL"] = {"Pistola M9", "",nil,0.75}
items["wbody|WEAPON_PUMPSHOTGUN"] = {"Mossberg 500", "",nil,0.75}
items["wbody|WEAPON_PUMPSHOTGUN_MK2"] = {"R870", "",nil,0.75}
items["wbody|WEAPON_APPISTOL"] = {"Glock 21", "",nil,0.75}
items["wbody|WEAPON_BALL"] = {"Ball", "",nil,0.75}
items["wbody|WEAPON_MOLOTOV"] = {"Molotov", "",nil,0.75}
items["wbody|WEAPON_SMG"] = {"MP5", "",nil,0.75}
items["wbody|WEAPON_STICKYBOMB"] = {"Sticky Bomb", "",nil,0.75}
items["wbody|WEAPON_PETROLCAN"] = {"Jerry Can", "",nil,0.75}
items["wbody|WEAPON_STUNGUN"] = {"Tazer", "",nil,0.75}
items["wbody|WEAPON_ASSAULTRIFLE_MK2"] = {"AK-74", "",nil,0.75}
items["wbody|WEAPON_HEAVYSHOTGUN"] = {"Hai Cheattato Bravo", "",nil,0.75}
items["wbody|WEAPON_MINIGUN"] = {"Hai Cheattato Bravo", "",nil,0.75}
items["wbody|WEAPON_GOLFCLUB"] = {"Golf Club", "",nil,0.75}
items["wbody|WEAPON_FLAREGUN"] = {"Flare Gun", "",nil,0.75}
items["wbody|WEAPON_FLARE"] = {"Flare", "",nil,0.75}
items["wbody|WEAPON_GRENADELAUNCHERSMOKE"] = {"Invalid", "",nil,0.75}
items["wbody|WEAPON_SMOKEGRENADE"] = {"Granata Fumogena", "",nil,0.75}
items["wbody|WEAPON_HAMMER"] = {"Hammer", "",nil,0.75}
items["wbody|WEAPON_COMBATPISTOL"] = {"HK-P2000", "",nil,0.75}
items["wbody|WEAPON_GUSENBERG"] = {"Thompson", "",nil,0.75}
items["wbody|WEAPON_COMPACTRIFLE"] = {"AK-74u", "",nil,0.75}
items["wbody|WEAPON_HOMINGLAUNCHER"] = {"Homing Launcher", "",nil,0.75}
items["wbody|WEAPON_NIGHTSTICK"] = {"Nightstick", "",nil,0.75}
items["wbody|WEAPON_RAILGUN"] = {"Railgun", "",nil,0.75}
items["wbody|WEAPON_SAWNOFFSHOTGUN"] = {"Mossberg 500C", "",nil,0.75}
items["wbody|WEAPON_SMG_MK2"] = {"MP5 K", "",nil,0.75}
items["wbody|WEAPON_BULLPUPRIFLE"] = {"Bullpup Rifle", "",nil,0.75}
items["wbody|WEAPON_BULLPUPRIFLE_MK2"] = {"Famas", "",nil,0.75}
items["wbody|WEAPON_FIREWORK"] = {"Firework Launcher", "",nil,0.75}
items["wbody|WEAPON_COMBATMG"] = {"Combat MG", "",nil,0.75}
items["wbody|WEAPON_CARBINERIFLE"] = {"M4A1", "",nil,0.75}
items["wbody|WEAPON_CROWBAR"] = {"Crowbar", "",nil,0.75}
items["wbody|WEAPON_FLASHLIGHT"] = {"Torcia", "",nil,0.75}
items["wbody|WEAPON_DAGGER"] = {"Antique Cavalry Dagger", "",nil,0.75}
items["wbody|WEAPON_GRENADE"] = {"Granata", "",nil,0.75}
items["wbody|WEAPON_POOLCUE"] = {"Pool Cue", "",nil,0.75}
items["wbody|WEAPON_BAT"] = {"Baseball Bat", "",nil,0.75}
items["wbody|WEAPON_PISTOL50"] = {"Desert Eagle", "",nil,0.75}
items["wbody|WEAPON_KNIFE"] = {"Knife", "",nil,0.75}
items["wbody|WEAPON_MG"] = {"PKM", "",nil,0.75}
items["wbody|WEAPON_BULLPUPSHOTGUN"] = {"KSG", "",nil,0.75}
items["wbody|WEAPON_BZGAS"] = {"BZ Gas", "",nil,0.75}
items["wbody|WEAPON_GRENADELAUNCHER"] = {"Grenade Launcher", "",nil,0.75}
items["wbody|WEAPON_NIGHTVISION"] = {"Night Vision", "",nil,0.75}
items["wbody|WEAPON_MUSKET"] = {"Musket", "",nil,0.75}
items["wbody|WEAPON_ProximityMine"] = {"Proximity Mine", "",nil,0.75}
items["wbody|WEAPON_AdvancedRifle"] = {"TAR 21", "",nil,0.75}
items["wbody|WEAPON_RPG"] = {"RPG", "",nil,0.75}
items["wbody|WEAPON_PIPEBOMB"] = {"Pipe Bomb", "",nil,0.75}
items["wbody|WEAPON_MINISMG"] = {"Mini SMG", "",nil,0.75}
items["wbody|WEAPON_PISTOL_MK2"] = {"Pistola M9 MK2", "",nil,0.75}
items["wbody|WEAPON_ASSAULTRIFLE"] = {"AK-47", "",nil,0.75}
items["wbody|WEAPON_SPECIALCARBINE"] = {"G36", "",nil,0.75}
items["wbody|WEAPON_SPECIALCARBINE_MK2"] = {"G36 MK2", "",nil,0.75}
items["wbody|WEAPON_REVOLVER"] = {"Magnum", "",nil,0.75}
items["wbody|WEAPON_REVOLVER_MK2"] = {"Python", "",nil,0.75}
items["wbody|WEAPON_MARKSMANRIFLE"] = {"Marksman Rifle", "",nil,0.75}
items["wbody|WEAPON_MARKSMANRIFLE_MK2"] = {"MK14 EBR", "",nil,0.75}
items["wbody|WEAPON_BATTLEAXE"] = {"Battle Axe", "",nil,0.75}
items["wbody|WEAPON_HEAVYPISTOL"] = {"M1911", "",nil,0.75}
items["wbody|WEAPON_KNUCKLEDUSTER"] = {"Knuckle Duster", "",nil,0.75}
items["wbody|WEAPON_MACHINEPISTOL"] = {"TEC-9", "",nil,0.75}
items["wbody|WEAPON_COMBATMG_MK2"] = {"Combat MG Mk II", "",nil,0.75}
items["wbody|WEAPON_MARKSMANPISTOL"] = {"Marksman Pistol", "",nil,0.75}
items["wbody|WEAPON_MACHETE"] = {"Machete", "",nil,0.75}
items["wbody|WEAPON_SWITCHBLADE"] = {"Switchblade", "",nil,0.75}
items["wbody|WEAPON_ASSAULTSHOTGUN"] = {"Assault Shotgun", "",nil,0.75}
items["wbody|WEAPON_DBSHOTGUN"] = {"Canne Mozze", "",nil,0.75}
items["wbody|WEAPON_ASSAULTSMG"] = {"Assault SMG", "",nil,0.75}
items["wbody|WEAPON_HATCHET"] = {"Hatchet", "",nil,0.75}
items["wbody|WEAPON_BOTTLE"] = {"Bottle", "",nil,0.75}
items["wbody|WEAPON_CARBINERIFLE_MK2"] = {"HK416", "",nil,0.75}
items["wbody|WEAPON_KNUCKLE"] = {"Knucle", "",nil,0.75}
items["wbody|WEAPON_PARACHUTE"] = {"Parachute", "",nil,0.75}
items["wbody|WEAPON_SMOKEGRENADE"] = {"Tear Gas", "",nil,0.75}
items["wbody|WEAPON_SNSPISTOL"] = {"Makarov", "",nil,0.75}
items["wbody|WEAPON_SNSPISTOL_MK2"] = {"Makarov MK2", "",nil,0.75}
items["wbody|WEAPON_ASSAULTSMG"] = {"P90 MK", "",nil,0.75}
items["wbody|WEAPON_DOUBLEACTION"] = {"Colt 1892", "",nil,0.75}

--MUNIZIONI
items["wammo|WEAPON_SNIPERRIFLE"] = {"Sniper Rifle Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_FIREEXTINGUISHER"] = {"Fire Extinguisher Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_COMPACTGRENADELAUNCHER"] = {"Compact Grenade Launcher Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_SNOWBALL"] = {"Snowball", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_VINTAGEPISTOL"] = {"Vintage Pistol Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_COMBATPDW"] = {"Combat PDW Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_HEAVYSNIPER_MK2"] = {"Heavy Sniper Mk II Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_HEAVYSNIPER"] = {"Heavy Sniper Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_SWEEPERSHOTGUN"] = {"Sweeper Shotgun Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_MICROSMG"] = {"Micro SMG Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_WRENCH"] = {"Pipe Wrench Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_PISTOL"] = {"Pistol Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_PUMPSHOTGUN"] = {"Pump Shotgun Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_PUMPSHOTGUN_MK2"] = {"Pump Shotgun Munition", "",nil,0.01}
items["wammo|WEAPON_APPISTOL"] = {"AP Pistol Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_BALL"] = {"Ball Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_MOLOTOV"] = {"Molotov Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_SMG"] = {"SMG Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_STICKYBOMB"] = {"Sticky Bomb Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_PETROLCAN"] = {"Jerry Can Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_STUNGUN"] = {"Stun Gun Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_ASSAULTRIFLE_MK2"] = {"Assault Rifle Mk II Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_HEAVYSHOTGUN"] = {"Heavy Shotgun Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_MINIGUN"] = {"Minigun Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_GOLFCLUB"] = {"Golf Club Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_FLAREGUN"] = {"Flare Gun Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_FLARE"] = {"Flare Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_HAMMER"] = {"Hammer Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_COMBATPISTOL"] = {"Combat Pistol Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_GUSENBERG"] = {"Gusenberg Sweeper Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_COMPACTRIFLE"] = {"Compact Rifle Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_HOMINGLAUNCHER"] = {"Homing Launcher Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_NIGHTSTICK"] = {"Nightstick Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_RAILGUN"] = {"Railgun Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_SAWNOFFSHOTGUN"] = {"Sawed-Off Shotgun Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_SMG_MK2"] = {"SMG Mk II Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_BULLPUPRIFLE"] = {"Bullpup Rifle Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_BULLPUPRIFLE_MK2"] = {"Bullpup Rifle Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_FIREWORK"] = {"Firework Launcher Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_COMBATMG"] = {"Combat MG Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_CARBINERIFLE"] = {"Carbine Rifle Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_CROWBAR"] = {"Crowbar Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_FLASHLIGHT"] = {"Flashlight Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_DAGGER"] = {"Antique Cavalry Dagger Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_GRENADE"] = {"Grenade Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_POOLCUE"] = {"Pool Cue Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_BAT"] = {"Baseball Bat Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_PISTOL50"] = {"Pistol .50 Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_KNIFE"] = {"Knife Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_MG"] = {"MG Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_BULLPUPSHOTGUN"] = {"Bullpup Shotgun Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_BZGAS"] = {"BZ Gas Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_GRENADELAUNCHER"] = {"Grenade Launcher Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_NIGHTVISION"] = {"Night Vision Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_MUSKET"] = {"Musket Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_ProximityMine"] = {"Proximity Mine Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_AdvancedRifle"] = {"Advanced Rifle Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_RPG"] = {"RPG Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_PIPEBOMB"] = {"Pipe Bomb Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_MINISMG"] = {"Mini SMG Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_PISTOL_MK2"] = {"Pistol Mk II Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_ASSAULTRIFLE"] = {"Assault Rifle Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_SPECIALCARBINE"] = {"Special Carbine Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_SPECIALCARBINE_MK2"] = {"Special Carbine Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_REVOLVER"] = {"Heavy Revolver Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_REVOLVER_MK2"] = {"Heavy Revolver Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_MARKSMANRIFLE"] = {"Marksman Rifle Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_MARKSMANRIFLE_MK2"] = {"Marksman Rifle Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_BATTLEAXE"] = {"Battle Axe Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_HEAVYPISTOL"] = {"Heavy Pistol Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_KNUCKLEDUSTER"] = {"Knuckle Duster Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_MACHINEPISTOL"] = {"Machine Pistol Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_COMBATMG_MK2"] = {"Combat MG Mk II Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_MARKSMANPISTOL"] = {"Marksman Pistol Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_MACHETE"] = {"Machete Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_SWITCHBLADE"] = {"Switchblade Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_ASSAULTSHOTGUN"] = {"Assault Shotgun Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_DBSHOTGUN"] = {"Double Barrel Shotgun Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_ASSAULTSMG"] = {"Assault SMG Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_HATCHET"] = {"Hatchet Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_BOTTLE"] = {"Bottle Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_CARBINERIFLE_MK2"] = {"Carbine Rifle Mk II Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_KNUCKLE"] = {"Knucle - Pezzo", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_PARACHUTE"] = {"Parachute Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_SMOKEGRENADE"] = {"Tear Gas Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_BZGAS"] = {"Tear Gas Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_SNSPISTOL"] = {"SNS Pistol Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_SNSPISTOL_MK2"] = {"SNS Pistol Munition", "Arma da combattimento",nil,0.01}
items["wammo|WEAPON_ASSAULTSMG"] = {"Assault SMG Munition", "",nil,0.01}
items["wammo|WEAPON_DOUBLEACTION"] = {"Colt 1892", "",nil,0.01}

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- money
items["money"] = {"Soldi","Impacchetta soldi.",function(args)
  local choices = {}
  local idname = args[1]

  choices["Disfare"] = {function(player,choice,mod)
    local user_id = vRP.getUserId(player)
    if user_id ~= nil then
      local amount = vRP.getInventoryItemAmount(user_id, idname)
      vRP.prompt(player, "Quantità da disfare ? (max "..amount..")", "", function(player,ramount)
        ramount = parseInt(ramount)
        if vRP.tryGetInventoryItem(user_id, idname, ramount, true) then -- unpack the money
          vRP.giveMoney(user_id, ramount)
          vRP.closeMenu(player)
        end
      end)
    end
  end}

  return choices
end,0}

-- money binder
items["money_binder"] = {"Raccogli denaro","Usato per raccogliere $1000.",function(args)
  local choices = {}
  local idname = args[1]

  choices["Lega soldi"] = {function(player,choice,mod) -- bind the money
    local user_id = vRP.getUserId(player)
    if user_id ~= nil then
      local money = vRP.getMoney(user_id)
      if money >= 1000 then
        if vRP.tryGetInventoryItem(user_id, idname, 1, true) and vRP.tryPayment(user_id,1000) then
          vRP.giveInventoryItem(user_id, "money", 1000, true)
          vRP.closeMenu(player)
        end
      else
        vRPclient.notify(player,{vRP.lang.money.not_enough()})
      end
    end
  end}

  return choices
end,0}

local get_wname = function(weapon_id)
  local name = string.gsub(weapon_id,"WEAPON_","")
  name = string.upper(string.sub(name,1,1))..string.lower(string.sub(name,2))
  return name
end

--- weapon body
local wbody_name = function(args)
  return get_wname(args[2]).." body"
end

local wbody_desc = function(args)
  return ""
end

local wbody_choices = function(args)
  local choices = {}
  local fullidname = joinStrings(args,"|")

  choices["Equip"] = {function(player,choice)
    local user_id = vRP.getUserId(player)
    if user_id ~= nil then
      if vRP.tryGetInventoryItem(user_id, fullidname, 1, true) then -- give weapon body
        local weapons = {}
        weapons[args[2]] = {ammo = 0}
        vRPclient.giveWeapons(player, {weapons})

        vRP.closeMenu(player)
      end
    end
  end}

  return choices
end

local wbody_weight = function(args)
  return 0.75
end

items["wbody"] = {wbody_name,wbody_desc,wbody_choices,wbody_weight}

--- weapon ammo
local wammo_name = function(args)
  return get_wname(args[2]).." ammo"
end

local wammo_desc = function(args)
  return ""
end

local wammo_choices = function(args)
  local choices = {}
  local fullidname = joinStrings(args,"|")

  choices["Load"] = {function(player,choice)
    local user_id = vRP.getUserId(player)
    if user_id ~= nil then
      local amount = vRP.getInventoryItemAmount(user_id, fullidname)
      vRP.prompt(player, "Munizioni da caricare? (massimo "..amount..")", "", function(player,ramount)
        ramount = parseInt(ramount)

        vRPclient.getWeapons(player, {}, function(uweapons)
          if uweapons[args[2]] ~= nil then -- check if the weapon is equiped
            if vRP.tryGetInventoryItem(user_id, fullidname, ramount, true) then -- give weapon ammo
              local weapons = {}
              weapons[args[2]] = {ammo = ramount}
              vRPclient.giveWeapons(player, {weapons,false})
              vRP.closeMenu(player)
            end
          end
        end)
      end)
    end
  end}

  return choices
end

local wammo_weight = function(args)
  return 0.01
end

items["wammo"] = {wammo_name,wammo_desc,wammo_choices,wammo_weight}

return items
