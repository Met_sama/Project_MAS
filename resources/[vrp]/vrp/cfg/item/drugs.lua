
local items = {}

local function play_drink(player)
  local seq = {
    {"mp_player_intdrink","intro_bottle",1},
    {"mp_player_intdrink","loop_bottle",1},
    {"mp_player_intdrink","outro_bottle",1}
  }

  vRPclient.playAnim(player,{true,seq,false})
end

local pills_choices = {}
pills_choices["Prendi"] = {function(player,choice)
  local user_id = vRP.getUserId(player)
  if user_id ~= nil then
    if vRP.tryGetInventoryItem(user_id,"MedicinaX",1) then
      vRPclient.varyHealth(player,{10})
      vRPclient.notify(player,{"~g~ Stai usando le bende"})
      play_drink(player)
      vRP.closeMenu(player)
    end
  end
end}

local function play_smoke(player)
  local seq2 = {
    {"mp_player_int_uppersmoke","mp_player_int_smoke_enter",1},
    {"mp_player_int_uppersmoke","mp_player_int_smoke",1},
    {"mp_player_int_uppersmoke","mp_player_int_smoke_exit",1}
  }

  vRPclient.playAnim(player,{true,seq2,false})
end

local smoke_choices = {}
smoke_choices["Fuma"] = {function(player,choice)
  local user_id = vRP.getUserId(player)
  if user_id ~= nil then
    if vRP.tryGetInventoryItem(user_id,"marijuana",1) then
	  vRP.varyHunger(user_id,(20))
      vRPclient.notify(player,{"~g~ Stai fumando marijuana."})
      play_smoke(player)
      vRP.closeMenu(player)
    end
  end
end}

local function play_smell(player)
  local seq3 = {
    {"mp_player_intdrink","intro_bottle",1},
    {"mp_player_intdrink","loop_bottle",1},
    {"mp_player_intdrink","outro_bottle",1}
  }

  vRPclient.playAnim(player,{true,seq3,false})
end

local smell_choices = {}
smell_choices["Sniffa"] = {function(player,choice)
  local user_id = vRP.getUserId(player)
  if user_id ~= nil then
    if vRP.tryGetInventoryItem(user_id,"cocaine",1) then
	  vRP.varyThirst(user_id,(20))
      vRPclient.notify(player,{"~g~ sniffando cocaine."})
      play_smell(player)
      vRP.closeMenu(player)
    end
  end
end}

local function play_meth(player)
  local seq4 = {
    {"mp_player_intdrink","intro_bottle",1},
    {"mp_player_intdrink","loop_bottle",1},
    {"mp_player_intdrink","outro_bottle",1}
  }

  vRPclient.playAnim(player,{true,seq4,false})
end

local meth_choices = {}
meth_choices["Prendi"] = {function(player,choice)
  local user_id = vRP.getUserId(player)
  if user_id ~= nil then
    if vRP.tryGetInventoryItem(user_id,"meth",1) then
	  vRP.varyThirst(user_id,(20))
      vRPclient.notify(player,{"~g~ Prendendo meth."})
      play_meth(player)
      vRP.closeMenu(player)
    end
  end
end}

--MORFINA LUKE

local function play_morfina(player)
  local seq = {
    {"mp_player_intdrink","intro_bottle",1},
    {"mp_player_intdrink","loop_bottle",1},
    {"mp_player_intdrink","outro_bottle",1}
  }

  vRPclient.playAnim(player,{true,seq,false})
end

local morfina_choices = {}
morfina_choices["Prendi"] = {function(player,choice)
  local user_id = vRP.getUserId(player)
  if user_id ~= nil then
    if vRP.tryGetInventoryItem(user_id,"morfina",1) then
      vRPclient.varyHealth(player,{8})
      vRPclient.notify(player,{"~g~ Stai usando la morfina"})
      play_morfina(player)
      vRP.closeMenu(player)
    end
  end
end}

--MORFINA DILUITA

--MORFINA LUKE

local function play_morfinadil(player)
  local seq = {
    {"mp_player_intdrink","intro_bottle",1},
    {"mp_player_intdrink","loop_bottle",1},
    {"mp_player_intdrink","outro_bottle",1}
  }

  vRPclient.playAnim(player,{true,seq,false})
end

local morfinadil_choices = {}
morfinadil_choices["Prendi"] = {function(player,choice)
  local user_id = vRP.getUserId(player)
  if user_id ~= nil then
    if vRP.tryGetInventoryItem(user_id,"morfinadil",1) then
      vRPclient.varyHealth(player,{4})
      vRPclient.notify(player,{"~g~ Stai usando la morfina diluita"})
      play_morfinadil(player)
      vRP.closeMenu(player)
    end
  end
end}

--ANTIBIOTICI

local function play_antibiotici(player)
  local seq = {
    {"mp_player_intdrink","intro_bottle",1},
    {"mp_player_intdrink","loop_bottle",1},
    {"mp_player_intdrink","outro_bottle",1}
  }

  vRPclient.playAnim(player,{true,seq,false})
end

local antibiotici_choices = {}
antibiotici_choices["Prendi"] = {function(player,choice)
  local user_id = vRP.getUserId(player)
  if user_id ~= nil then
    if vRP.tryGetInventoryItem(user_id,"antibiotici",1) then
      vRPclient.varyHealth(player,{2})
      vRPclient.notify(player,{"~g~ Stai usando un antibiotico"})
      play_antibiotici(player)
      vRP.closeMenu(player)
    end
  end
end}


local function play_medicinax(player)
  local seq = {
    {"mp_player_intdrink","intro_bottle",1},
    {"mp_player_intdrink","loop_bottle",1},
    {"mp_player_intdrink","outro_bottle",1}
  }

  vRPclient.playAnim(player,{true,seq,false})
end

local medicinax_choices = {}
medicinax_choices["Utilizza"] = {function(player,choice)
  local user_id = vRP.getUserId(player)
  if user_id ~= nil then
    vRPclient.isInComa(player,{}, function(in_coma)    
      if not in_coma then
        if vRP.tryGetInventoryItem(user_id,"medicinax",1) then
          vRPclient.varyHealth(player,{25})
          vRPclient.notify(player,{"~g~ Stai usando la Medicina X."})
          play_drink(player)
          vRP.closeMenu(player)
        end
      end    
    end)
  end
end}

--luke
items["medicinax"] = {"Medicina X","Medicina X!",function(args) return medicinax_choices end,0.1}

items["morfina"] = {"Morfina","Ti farà sentire meglio!",function(args) return morfina_choices end,0.2}
items["morfinadil"] = {"Morfina diluita","Allevia il dolore!",function(args) return morfinadil_choices end,0.35}
items["antibiotici"] = {"Antibiotici","Allevia il dolore!",function(args) return antibiotici_choices end,0.5}

--items["MedicinaX"] = {"Medicina X versione 2","Usata per  curarsi.",function(args) return pills_choices end,0.4}
items["marijuana"] = {"Marjuana","Si fuma!",function(args) return smoke_choices end,0.25}


items["cocaina"] = {"Cocaina","Cocaina",function(args) return smell_choices end,1.00}
items["cocaina_tag"] = {"Cocaina tagliata","Cocaina tagliata",function(args) return smell_choices end,0.50}



items["meth"] = {"Metanfetamina","Metanfetamina.",function(args) return meth_choices end,0.50}



items["strumenti_medici"] = {"Magazzino medico","Usato dal trasportatore medico."}

return items