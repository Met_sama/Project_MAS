
local cfg = {}
-- list of weapons for sale
-- for the native name, see https://wiki.fivem.net/wiki/Weapons (not all of them will work, look at client/player_state.lua for the real weapon list)
-- create groups like for the garage config
-- [native_weapon_name] = {display_name,body_price,ammo_price,description}
-- ammo_price can be < 1, total price will be rounded

-- _config: blipid, blipcolor, permissions (optional, only users with the permission will have access to the shop)

cfg.gunshop_types = {
  ["policeloadout"] = {
    _config = {blipid=110,blipcolor=74, permissions = {"police.loadshop"}},
    ["WEAPON_PETROLCAN"] = {"Petrol",0,0,""},
    ["WEAPON_PUMPSHOTGUN"] = {"Pump Shotgun",0,0,""},
	  ["WEAPON_FLAREGUN"] = {"Flare Gun",0,0,""},
	  ["WEAPON_FLASHLIGHT"] = {"Flashlight",0,0,""},
    ["WEAPON_FLARE"] = {"Flare",0,0,""},
    ["WEAPON_NIGHTSTICK"] = {"Nighstick",0,0,""},
	  ["WEAPON_STUNGUN"] = {"Tazer",0,0,""},
    ["WEAPON_COMBATPISTOL"] = {"Combat Pistol",0,0,""}
    
  },
  ["emsloadout"] = {
    _config = {blipid=446,blipcolor=74, permissions = {"ems.loadshop"}},
    ["WEAPON_PETROLCAN"] = {"Petrol",0,0,""},
   	["WEAPON_FLAREGUN"] = {"Flare Gun",0,0,""},
	  ["WEAPON_FLASHLIGHT"] = {"Flashlight",0,0,""},
    ["WEAPON_FLARE"] = {"Flare",0,0,""},
    ["WEAPON_NIGHTSTICK"] = {"Nighstick",0,0,""},
	  ["WEAPON_STUNGUN"] = {"Tazer",0,0,""}
   },
     ["armaiolo"] = {
    _config = {blipid=567,blipcolor=45, permissions = {"armaiolo.armeria"}},
    ["WEAPON_KNIFE"] = {"Coltello",10,0,""},
    ["WEAPON_KNUCKLE"] = {"Tirapugni",10,0,""},
    ["WEAPON_DAGGER"] = {"Coltello Antico",10,0,""},
    ["WEAPON_SWITCHBLADE"] = {"Coltello serramanico",10,0,""},
    ["WEAPON_MACHETE"] = {"Machete",10,0,""},
    ["WEAPON_FLASHLIGHT"] = {"Torcia",10,0,""}
   },  
   
  ["bombsticky"] = {
    _config = {blipid=110,blipcolor=1},
   ["WEAPON_STICKYBOMB"] = {"Sticky bomb",50000,50000,""} 
 }
}
-- list of gunshops positions

cfg.gunshops = {
  --{"emsloadout", 232.89363098145,-1368.3338623047,39.534381866455}, -- spawn hospital
  --{"emsloadout", 1837.8341064453,3671.3837890625,34.276763916016}, -- sandy shores
  --{"emsloadout", -246.91954040527,6330.349609375,32.42618560791}, -- paleto
 -- {"policeloadout", 461.33551025391,-981.11071777344,30.689584732056},--- main pd
  --{"policeloadout", 1851.7342529297,3683.7416992188,34.267044067383}, -- sandy shores
  --{"policeloadout", -442.724609375,6012.6293945313,31.716390609741}, -- paleto
  {"armaiolo", 21.480379104614,-1105.0828857422,29.797039031982} -- armaiolo
  
  --{"Paleto", -331.50210571289,6082.5063476563,31.454769134521} -- Paleto
}

return cfg
