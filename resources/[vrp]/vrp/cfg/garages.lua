
local cfg = {}
-- define garage types with their associated vehicles
-- (vehicle list: https://wiki.fivem.net/wiki/Vehicles)

-- each garage type is an associated list of veh_name/veh_definition 
-- they need a _config property to define the blip and the vehicle type for the garage (each vtype allow one vehicle to be spawned at a time, the default vtype is "default")
-- this is used to let the player spawn a boat AND a car at the same time for example, and only despawn it in the correct garage
-- _config: vtype, blipid, blipcolor, permissions (optional, only users with the permission will have access to the shop)

cfg.rent_factor = 0.1 -- 10% of the original price if a rent
cfg.sell_factor = 0.40 -- sell for 75% of the original price

cfg.garage_types = {
  -- ["High End"]  = {  -- 150k price cap
    -- _config = {vtype="car",blipid=50,blipcolor=4},
  -- },
  
  ["police"] = {
    _config = {vtype="car",blipid=56,blipcolor=38,permissions={"police.vehicle"}},
	["police"] = {"1-Volante",0, "police"},
    ["police3"] = {"2-Volante-Sport",0, "police"},
    ["police4"] = {"3-Volante-Borghese",0, "police"},
    ["polgs350"] = {"4-Volante",0, "police"},
    ["polcoquette"] = {"5-Volante-Sport",0, "police"},
    ["riot"] = {"6-Blindato-Semplice",0, "police"},	
    ["riot2"] = {"7-Blindato-Avanzato",0, "police"},
	["flatbed"] = {"8-Flatbed",0, "police"}
  },
    ["sceriffo"] = {
    _config = {vtype="car",blipid=56,blipcolor=38,permissions={"police.vehicle"}},
    ["sheriff"] = {"1-Volante Sceriffo",0, "police"},
    ["sheriff2"] = {"2-Suv Sceriffo",0, "police"}
  },
     ["import"] = {
    _config = {vtype="car",blipid=545,blipcolor=16,permissions={"import.whitelisted"}},
    ["kalahari"] = {"kalahari",0, "Kalahari"}
  },  
  
  ["emergency"] = {
    _config = {vtype="car",blipid=50,blipcolor=3,permissions={"emergency.vehicle"}},
    ["ambulance3"] = {"1-Ambulanza",0, "emergency"},
    ["emssuv"] = {"2-SUV",0, "emergency"}
  },
  ["Police Helicopters"] = {
    _config = {vtype="car",blipid=602,blipcolor=38,radius=5.1,permissions={"police.vehicle"}},
    ["polmav"] = {"Elicottero",0, "emergency"}
	--["as350"] = {"AS-350 Ecureuil",0, "emergency"}
  },
  ["uber"] = {
    _config = {vtype="car",blipid=0,blipcolor=81,permissions={"uber.vehicle"}},
    ["taxi"] = {"Taxi",0, ""},
  },
  ["delivery"] = {
    _config = {vtype="bike",blipid=85,blipcolor=31,permissions={"delivery.vehicle"}},
    ["faggio3"] = {"faggio3",0, "faggio3"}
  },
  ["repair"] = {
    _config = {vtype="car",blipid=609,blipcolor=31,permissions={"dipendente.aci"}},
	  ["towtruck"] = {"Carro-Attrezzi",0, "towtruck"},
	  ["flatbed"] = {"Flatbed",0, "Flatbed"}
  },
  ["bus"] = {
    _config = {vtype="car",blipid=50,blipcolor=31},
    ["bus"] = {"bus",0, "bus"}
  },
  ["bici"] = {
    _config = {vtype="car",blipid=0,blipcolor=0},
    ["bmx"] = {"bmx",0, "bmx"},
    ["fixter"] = {"fixter",0, "fixter"},
    ["scorcher"] = {"scorcher",0, "scorcher"}
  },  
  ["Portavalori"] = {
    _config = {vtype="car",blipid=0,blipcolor=0,permissions={"bankdriver.vehicle"}},
    ["stockade"] = {"stockade",0, "stockade"}
  },
  ["taglialegna"] = {
    _config = {vtype="car",blipid=67,blipcolor=4,permissions={"taglia.vehicle"}},
    ["tiptruck"] = {"Camion",0, "camion"}
  },
  ["agricoltore"] = {
    _config = {vtype="car",blipid=67,blipcolor=4,permissions={"agri.vehicle"}},
    ["tractor2"] = {"Trattore",34000, "trattore"}
  },
  ["Barche da pesca"] = {
    _config = {vtype="boat",blipid=455,blipcolor=5,radius=9.1,permissions={"fisher.vehicle"}},
    ["dinghy2"] = {"Barca da pesca",0, "Una semplice barca da pesca!"}
  },
  ["Aeroporto"] = {
    _config = {vtype="car",blipid=307,blipcolor=6},
    ["mammatus"] = {"Cesna",70000, "stockade"},       
	  ["besra"] = {"Aereo",1200000, ""},
	  ["supervolito2"] = {"Volito",1500000, ""},
	  ["luxor"] = {"Jet privato",950000, ""},
	  ["maverick"] = {"Maverick",600000, ""},
	  ["velum"] = {"Aereo piccolo",300000, ""},
	  ["frogger"] = {"Elicottero",500000, ""},
	  ["shamal"] = {"Jet",1500000, ""},
	  ["blimp"] = {"Mongolfiera",3500000, ""}
  }
  
  
}

-- {garage_type,x,y,z}
cfg.garages = {
  {"bus",464.44540405273,-585.77453613281,28.499773025513},
  {"police",451.2121887207,-1018.2822875977,28.495378494263},	-- jobs garage
  {"sceriffo",1872.7989501954,3693.4311523438,33.53305053711},	-- jobs garage
  {"sceriffo", -478.56832885742,5997.1591796875,31.336700439454},	-- jobs garage  
  {"emergency",279.35409545898,-606.53863525391,43.057220458984},
  {"delivery",964.514770507813,-1020.13879394531,40.8475074768066},   -- jobs garage
--  {"repair",-183.27731323242,-1299.1448974609,31.295980453491},
  {"repair",528.46166992188,-181.07374572754,54.24946975708},
  {"repair",2345.1694335938,3097.5270996094,48.042190551758},
  {"Portavalori",222.68756103516,222.95631408691,105.41331481934},   -- jobs garage
  {"taglialegna",-576.33972167969,5236.2807617188,70.469436645508},
  {"Police Helicopters",449.30340576172,-981.24963378906,43.69165802002},  
  {"agricoltore",2553.8210449219,4673.298828125,33.916519165039},
  {"agricoltore",1955.8658447266,5164.736328125,46.852405548096},
  {"agricoltore",407.85290527344,6487.4868164063,28.63722038269},
  {"Barche da pesca",1536.9403076172,3872.2858886719,29.809471130371},
  {"Barche da pesca",-911.29315185547,-1365.5028076172,-0.47459509968758},
  {"Barche da pesca",-820.65008544922,-1516.3624267578,-0.47425931692123},
  {"bici",-256.3037109375,-985.88317871094,31.219892501832},
  {"import",1160.6081542968,-3073.6428222656,5.8710465431214},  
  {"EMS Helicopters",-475.24264526367,5988.7353515625,31.336685180664} -- Paleto Bay  
  
}

return cfg
