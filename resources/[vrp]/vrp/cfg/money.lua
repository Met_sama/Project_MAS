
local cfg = {}

-- start wallet/bank values
cfg.open_wallet = 500
cfg.open_bank = 1000


-- money display css
cfg.display_css = [[
@font-face {
    font-family: Pricedown;
    src: url(nui://vrp/gui/pdown.ttf);
}

.div_money{
  position: absolute;
  top: 42px;
  right: 10px;
  font-size: 48px;
  font-family: Pricedown;
  color: #ADFF2F;
  text-shadow: rgb(0, 0, 0) 1px 0px 0px, rgb(0, 0, 0) 0.533333px 0.833333px 0px, rgb(0, 0, 0) -0.416667px 0.916667px 0px, rgb(0, 0, 0) -0.983333px 0.133333px 0px, rgb(0, 0, 0) -0.65px -0.75px 0px, rgb(0, 0, 0) 0.283333px -0.966667px 0px, rgb(0, 0, 0) 0.966667px -0.283333px 0px;
}

.div_bmoney{
  position: absolute;
  top: 76px;
  right: 10px;
  font-size: 48px;
  font-family: Pricedown;
  color: #008000;
  text-shadow: rgb(0, 0, 0) 1px 0px 0px, rgb(0, 0, 0) 0.533333px 0.833333px 0px, rgb(0, 0, 0) -0.416667px 0.916667px 0px, rgb(0, 0, 0) -0.983333px 0.133333px 0px, rgb(0, 0, 0) -0.65px -0.75px 0px, rgb(0, 0, 0) 0.283333px -0.966667px 0px, rgb(0, 0, 0) 0.966667px -0.283333px 0px;
}



.div_money .symbol{
  content: url('https://i.imgur.com/is7xEN6.png'); 
 
}
.div_bmoney .symbol{
  content: url('https://i.imgur.com/cQ8uRn0.png');
  
}


]]

return cfg