
-- define emotes
-- use the custom emotes admin action to test emotes on-the-fly
-- animation list: http://docs.ragepluginhook.net/html/62951c37-a440-478c-b389-c471230ddfc5.htm

local cfg = {}

-- map of emote_name => {upper,seq,looping}
-- seq can also be a task definition, check the examples below
cfg.effetti = {
["asfa"] = "DeathFailOut"
}
cfg.emotes = {
["Popolari"] = {
  ["Siediti"] = {false, {task="WORLD_HUMAN_PICNIC"}, false},
  ["Siediti"] = {false, {task="PROP_HUMAN_SEAT_CHAIR_MP_PLAYER"}, false},
  ["Siediti 3"] = {
    false, {{"anim@heists@fleeca_bank@ig_7_jetski_owner","owner_idle",1}}, true
  },  
    
  ["Sdraiati"] = {false, {task="WORLD_HUMAN_SUNBATHE_BACK"}, false},
  ["Ballo"] = {
    false, {
    {"rcmnigel1bnmt_1b","dance_intro_tyler",1},
    {"rcmnigel1bnmt_1b","dance_loop_tyler",1}
  }, true
  },
  ["Applaudi"] = {false, {task="WORLD_HUMAN_CHEERING"}, false},
  ["No"] = {
    true, {{"gestures@f@standing@casual","gesture_head_no",1}}, false
  }
},
["RolePlay"] = {
  ["Statua"] = {false, {task="WORLD_HUMAN_HUMAN_STATUE"}, false},
  ["Addominali"] = {false, {task="WORLD_HUMAN_SIT_UPS"}, false},
  ["Registra"] = {false, {task="WORLD_HUMAN_MOBILE_FILM_SHOCKING"}, false},
  ["Guardia"] = {false, {task="WORLD_HUMAN_GUARD_STAND"}, false},
  ["Martello"] = {false, {task="WORLD_HUMAN_HAMMERING"}, false},
  ["Fai una foto"] = {false, {task="WORLD_HUMAN_PAPARAZZI"}, false},
  ["Impaziente"] = {false, {task="WORLD_HUMAN_STAND_IMPATIENT"}, false},
  ["Controllo folla"] = {false, {task="CODE_HUMAN_POLICE_CROWD_CONTROL"}, false},
  ["Indaga"] = {false, {task="CODE_HUMAN_POLICE_INVESTIGATE"}, false},
  ["Chiedi elemosina"] = {false, {task="WORLD_HUMAN_BUM_FREEWAY"}, false},
  ["Bere una birra"] = {false, {task="WORLD_HUMAN_PARTYING"}, false},
},
}
cfg.emotesp2 = {}
return cfg
