
local cfg = {}

-- define market types like garages and weapons
-- _config: blipid, blipcolor, permissions (optional, only users with the permission will have access to the market)

cfg.market_types = {
  ["food"] = {
    _config = {blipid=52, blipcolor=2},
    ["latte"] = 3,
    ["acqua"] = 2,
    ["coffee"] = 4,
    ["tea"] = 3,
    ["icetea"] = 3,
    ["succodifrutta"] = 3,
    ["cocacola"] = 5,
    ["redbull"] = 5,
    ["limonata"] = 3,
    ["vodka"] = 7,

    --Food
    ["bread"] = 3,
    ["donut"] = 3,
    ["tacos"] = 5,
    ["sandwich"] = 7,
    ["kebab"] = 7,
    ["pdonut"] = 5,
  },
  
  
    ["burgerking"] = {
    _config = {blipid=106, blipcolor=1},

    ["hamburger"] = 10,
    ["cocacola"] = 5,
    ["ciambella"] = 4,
    ["insalata"] = 6,
    ["milkshake"] = 3,
    ["fritte"] = 6,
	
  },
  
    ["boccetta"] = {
    _config = {blipid=51, blipcolor=68},
    ["acqua"] = 2
  },  
  
    ["Macchinetta"] = {
    _config = {blipid=0, blipcolor=0},
    ["acqua"] = 3,
    ["sandwich"] = 4
  },    

  
    ["Proteine"] = {
    _config = {blipid=0, blipcolor=0},
    ["proteine"] = 80
  },    
  
    ["negozioanimali"] = {
    _config = {blipid=0, blipcolor=0},
    ["croquettes"] = 10	
  },
   
  
--    ["pillboxhospital"] = {
--    _config = {blipid=51, blipcolor=68},
--    ["pills"] = 120
--  },
 
  ["emergencyloadout"] = {
    _config = {blipid=51, blipcolor=68, permissions={"emergency.market"}},
    ["medkit"] = 0,
    ["boccetta"] = 10,	
    ["provuota"] = 3,		
    --["pills"] = 115,
    ["sangue"] = 0	
  },
  
   ["polizia"] = {
    _config = {blipid=0, blipcolor=0, permissions={"police.handcuff"}},
    ["portoarmi1"] = 0,
    ["portoarmi2"] = 0,
    ["portoarmi3"] = 0,
    ["pennablu"] = 0
  }, 

    ["poliziacibo"] = {
    _config = {blipid=0, blipcolor=0, permissions={"police.handcuff"}},
    ["limonata"] = 7,
    ["cocacola"] = 7,	
    ["ciambella"] = 3,
    ["sandwich"] = 5
  },  
  
  
  
  ["armeria"] = {
    _config = {blipid=0, blipcolor=0, permissions={"armaiolo.armeria"}},
    ["maschera"] = 0	
  },

  ["tools"] = {
    _config = {blipid=402, blipcolor=47, permissions={"dipendente.aci"}},
    ["repairkit"] = 25
  },

  
  
  ["tequila"] = {
    _config =  {blipid=0, blipcolor=0, permissions={"garnet.droga"}},
    ["limonata"] = 7,
    ["cocacola"] = 7,	
    ["sandwich"] = 5,
    ["redbull"] = 5,
    ["coffee"] = 4,
    ["tea"] = 3,
    ["icetea"] = 3
  }  
}

-- list of markets {type,x,y,z}

cfg.markets = {
  {"burgerking",-1195.2235107422,-891.01208496094,13.995156288147},
--  {"burgerking",-1194.1273193359,-892.77264404297,13.995162963867},
--  {"burgerking",-1192.6802978516,-894.79107666016,13.995162963867},
  {"polizia",451.65048217774,-973.14538574218,30.689659118652},
  {"poliziacibo",436.29467773438,-986.71545410156,30.689678192138},
 
  {"pillboxhospital",316.79568481445,-588.12835693359,43.291831970215},
--  {"armeria",23.525035858154,-1109.0119628906,29.797019958496},
  {"Macchinetta",2069.0124511718,3341.6765136718,47.57823562622},
  {"Macchinetta",-38.14891052246,-1094.287109375,26.422369003296},
  {"Macchinetta",19.890424728394,-1114.2189941406,29.797039031982},
  {"Macchinetta",436.19476318359,-986.65313720703,30.689674377441},
  {"Macchinetta",312.61108398438,-587.783203125,43.291839599609},
 -- {"Macchinetta",313.65417480469,-588.06079101563,43.291839599609},
  {"Macchinetta",322.3258972168,-600.22906494141,43.291790008545},
  {"Macchinetta",321.13470458984,-599.73968505859,43.291790008545},
  {"negozioanimali",591.04364013672,2744.8718261718,42.043701171875},
  
--  {"food",128.1410369873, -1286.1120605469, 29.281036376953},
 -- {"boccetta",321.9562072754,-600.09423828125,43.291786193848},
  
  {"food",-47.522762298584,-1756.85717773438,29.4210109710693},
  {"food",25.7454013824463,-1345.26232910156,29.4970207214355},
--  {"Proteine",-1220.0640869141,-1577.2827148438,4.121922492981},  
  --{"drugstore",356.5361328125,-593.474304199219,28.7820014953613},
  {"emergencyloadout",326.40267944336,-583.00390625,43.317375183105}, -- spawn
  {"tools",548.4984741211,-191.36778259278,54.481342315674}
--  {"tequila",-562.08990478516,287.05740356446,82.176536560058}
}

return cfg
