
local cfg = {}

-- size of the sms history
cfg.sms_history = 15

-- maximum size of an sms
cfg.sms_size = 500

-- duration of a sms position marker (in seconds)
cfg.smspos_duration = 300

-- phone sounds (playAudioSource)
cfg.dialing_sound = "sounds/phone_dialing.ogg" -- loop
cfg.ringing_sound = "sounds/phone_ringing.ogg" -- loop
cfg.sms_sound = "sounds/phone_sms.ogg"

-- define phone services
-- blipid, blipcolor (customize alert blip)
-- alert_time (alert blip display duration in seconds)
-- alert_permission (permission required to receive the alert)
-- alert_notify (notification received when an alert is sent)
-- notify (notification when sending an alert)
cfg.services = {
  ["Polizia"] = {
    blipid = 304,
    eccezione = false,	
    blipcolor = 38,
    alert_time = 600, -- 5 minutes
    alert_permission = "police.service",
    alert_notify = "~r~Police alert:~n~~s~",
    notify = "~b~Hai chiamato la polizia.",
    answer_notify = "~b~Una o più volanti sono in arrivo.",
    chiama = true	
  },
  ["Medico"] = {
    blipid = 153,
    eccezione = false,	
    blipcolor = 1,
    alert_time = 600, -- 5 minutes
    alert_permission = "emergency.service",
    alert_notify = "~r~Chiamata medica ricevuta:~n~~s~",
    notify = "~b~Hai chiamato un medico.",
    answer_notify = "~b~Chiamata elaborata.",
    chiama = true	
  },
  ["Import % Export"] = {
    blipid = 479,
    eccezione = false,	
    blipcolor = 31,
    alert_time = 600, -- 5 minutes
    alert_permission = "import.whitelisted",
    alert_notify = "~r~Chiamata Import & Export ricevuta:~n~~s~",
    notify = "~b~Hai chiamato un dipendente.",
    answer_notify = "~b~Chiamata elaborata.",
    chiama = true	
  },  
  ["Meccanico"] = {
    blipid = 446,
    eccezione = false,	
    blipcolor = 5,
    alert_time = 600,
    alert_permission = "dipendente.aci",
    alert_notify = "~y~Chiamata aci ricevuta:~n~~s~",
    notify = "~y~Hai chiamato un aci.",
    answer_notify = "~y~Chiamata elaborata.",
    chiama = true	
  }
}

-- define phone announces
-- image: background image for the announce (800x150 px)
-- price: amount to pay to post the announce
-- description (optional)
-- permission (optional): permission required to post the announce
cfg.announces = {
  ["admin"] = {
    --image = "nui://vrp_mod/announce_admin.png",
    image = "http://i.imgur.com/kjDVoI6.png",
    price = 0,
    description = "Riservato agli amministratori.",
    permission = "admin.announce"
  },
  ["police"] = {
    --image = "nui://vrp_mod/announce_police.png",
    image = "http://i.imgur.com/DY6DEeV.png",
    price = 0,
    description = "Chiama la polizia.",
    permission = "police.announce"
  },
  ["meccanico"] = {
    --image = "nui://vrp_mod/announce_police.png",
    --image = "http://i.imgur.com/DY6DEeV.png",
	image = "https://i.imgur.com/Ph0lIni.png",
    price = 15,
    description = "Solo per i meccanici.",
    permission = "dipendente.aci",
  },
  ["agricoltore"] = {
    --image = "nui://vrp_mod/announce_police.png",
    --image = "http://i.imgur.com/DY6DEeV.png",
	image = "https://i.imgur.com/cgDLaDk.jpg",
    price = 10,
    description = "Solo per i prodotto agricoli.",
    permission = "agri.announce",
  },
  ["commerciale"] = {
    --image = "nui://vrp_mod/announce_commercial.png",Bounty.cloakroom
    image = "http://i.imgur.com/b2O9WMa.png",
    description = "Annuncio commerciale.",
    price = 10000
  },
   ["Assicuratore"] = {
    --image = "nui://vrp_mod/announce_commercial.png",Bounty.cloakroom
    image = "http://i.imgur.com/b2O9WMa.png",
    description = "Per uso assicurativo.",
	permission = "assicuratore.announce",
    price = 20
  },
  ["Imprenditore"] = {
    --image = "nui://vrp_mod/announce_commercial.png",Bounty.cloakroom
    image = "http://i.imgur.com/b2O9WMa.png",
	permission = "imprenditore.announce",
    description = "Annunci commerciali.",
    price = 10
  },
  ["party"] = {
    --image = "nui://vrp_mod/announce_party.png",
    image = "http://i.imgur.com/OaEnk64.png",
    description = "Stai organizzando un party? Fallo sapere in giro!.",
    price = 8000
  }
}

return cfg
