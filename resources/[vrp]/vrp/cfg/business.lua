
local cfg = {}

-- minimum capital to open a business
cfg.minimum_capital = 75000

-- capital transfer reset interval in minutes
-- default: reset every 24h
cfg.transfer_reset_interval = 600

-- commerce chamber {blipid,blipcolor}
cfg.blip = {500,2} 

-- positions of commerce chambers
cfg.commerce_chambers = {
	{123.49220275878,-762.638671875,45.751956939698}
}

return cfg
