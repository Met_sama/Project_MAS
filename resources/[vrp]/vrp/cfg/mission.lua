
local cfg = {}

-- mission display css
cfg.display_css = [[

@font-face {
  font-family: "GTA";
  src: url('GTA.ttf') format("truetype");
}

.div_mission{
  font-family: "GTA";
  font-size: 1.25em;
  position: absolute;
  top: 260px;
  right: 5px;
  color: white;
  background-color: rgba(0,0,0,0.75);
  padding: 8px;
  max-width: 300px;
}

.div_mission .name{
  font-family: "GTA";
  font-size: 1.25em;
  color: rgb(255,226,0);
  font-weight: bold;
}

.div_mission .step{
font-size: 1.25em;
  font-family: "GTA";
  color: rgb(0,255,0);
  font-weight: bold;
}
]]

return cfg
