
local cfg = {}

-- PCs positions
cfg.pcs = {
  {1853.21, 3689.51, 34.2671},
  {442.030609130859, -978.72705078125, 30.6896057128906},
  {-448.97076416016,6012.4208984375,31.71639251709},
  {1169.2861328125,-3198.498046875,-39.007965087891}
}

cfg.ritiro = {
  {1852.7481689453,3687.2841796875,34.267082214355},
  {437.37329101563,-979.73645019531,30.689601898193},
  {55.80545425415,-1770.7795410156,-162.25735473633}
}

cfg.punto_se = {532.146,-168.722,54.831,20.0}

-- vehicle tracking configuration
cfg.trackveh = {
  min_time = 5, -- min time in seconds
  max_time = 20,
  min_timelent = 60,
  max_timelent = 200, -- max time in seconds
  service = "police.service"  -- service to alert when the tracking is successful
}

-- wanted display
cfg.wanted = {
  blipid = 458,
  blipcolor = 38,
  service = "police.service"
}

-- illegal items (seize)
cfg.seizable_items = {
  "dirty_money",
  "marijuana",
  "cocaina",
  "cocaina_tag",
  "meth",
  "benzoilmetilecgonina",
  "krokodil",
  "cime_marijuana_alta",
  "papavero",
  "oppio",
  "wammo",
  "wbody"
}
cfg.weapons_items = {
  "wbody|WEAPON_FLASHLIGHT",
  "wammo|WEAPON_FLASHLIGHT",
--------------------------------
  "wammo|WEAPON_MICROSMG",
  "wbody|WEAPON_MICROSMG",
--------------------------------
  "wammo|WEAPON_BULLPUPSHOTGUN",
  "wbody|WEAPON_BULLPUPSHOTGUN",
--------------------------------
  "wbody|WEAPON_PETROLCAN",
  "wammo|WEAPON_PETROLCAN",
--------------------------------
  "wbody|WEAPON_COMBATPISTOL",
  "wammo|WEAPON_COMBATPISTOL",
--------------------------------
  "wbody|WEAPON_COMBATMG",
  "wammo|WEAPON_COMBATMG",
--------------------------------
  "wbody|WEAPON_SAWNOFFSHOTGUN",
  "wammo|WEAPON_SAWNOFFSHOTGUN",
--------------------------------
  "wammo|WEAPON_ASSAULTRIFLE",
  "wbody|WEAPON_ASSAULTRIFLE",
-------------------------------- 
  "wbody|WEAPON_CROWBAR",
  "wammo|WEAPON_CROWBAR",
--------------------------------
  "wammo|WEAPON_GRENADELAUNCHER_SMOKE",
  "wbody|WEAPON_GRENADELAUNCHER_SMOKE",
--------------------------------
  "wbody|WEAPON_HAMMER",
  "wammo|WEAPON_HAMMER",
--------------------------------
  "wammo|WEAPON_FIREEXTINGUISHER",
  "wbody|WEAPON_FIREEXTINGUISHER",
--------------------------------
  "wbody|WEAPON_STUNGUN",
  "wammo|WEAPON_STUNGUN",
--------------------------------
  "wammo|WEAPON_SMOKEGRENADE",
  "wbody|WEAPON_SMOKEGRENADE",
--------------------------------
  "wbody|WEAPON_MG",
  "wammo|WEAPON_MG",
--------------------------------
  "wammo|WEAPON_BAT",
  "wbody|WEAPON_BAT",
--------------------------------
  "wbody|WEAPON_ADVANCEDRIFLE",
  "wbody|WEAPON_ADVANCEDRIFLE",
--------------------------------
  "wbody|WEAPON_CARBINERIFLE",
  "wammo|WEAPON_CARBINERIFLE",
--------------------------------
  "wbody|WEAPON_GOLFCLUB",
  "wammo|WEAPON_GOLFCLUB",
--------------------------------
  "wammo|WEAPON_FLARE",
  "wbody|WEAPON_FLARE",
--------------------------------
  "wammo|WEAPON_PISTOL",
  "wbody|WEAPON_PISTOL",
--------------------------------
  "wammo|WEAPON_MINIGUN",
  "wbody|WEAPON_MINIGUN",
--------------------------------
  "wammo|WEAPON_KNIFE",
  "wbody|WEAPON_KNIFE",
--------------------------------
  "wammo|WEAPON_NIGHTSTICK",
  "wbody|WEAPON_NIGHTSTICK",
--------------------------------
  "wammo|WEAPON_SMG",
  "wbody|WEAPON_SMG",
--------------------------------
  "wammo|WEAPON_ASSAULTSHOTGUN",
  "wbody|WEAPON_ASSAULTSHOTGUN",
--------------------------------
  "wammo|WEAPON_GRENADE",
  "wbody|WEAPON_GRENADE",
--------------------------------
  "wammo|WEAPON_ASSAULTSMG",
  "wbody|WEAPON_ASSAULTSMG",
--------------------------------
  "wammo|WEAPON_PISTOL50",
  "wbody|WEAPON_PISTOL50",
}
-- jails {x,y,z,radius}
cfg.jails = {
  {459.485870361328,-1001.61560058594,24.914867401123,2.1},
  {459.305603027344,-997.873718261719,24.914867401123,2.1},
  {459.999938964844,-994.331298828125,24.9148578643799,1.6},
  {1794.0457763672,2483.4919433594,-122.69621276855,100}
}

cfg.idcapitano = 4

cfg.fines = {
  ["10 percento"] = 10,
  ["20 percento"] = 20,
  ["30 percento"] = 30,
  ["40 percento"] = 40,
  ["50 percento"] = 50,
  ["60 percento"] = 60,
  ["70 percento"] = 70,
  ["80 percento"] = 80,
  ["90 percento"] = 90,
   
}

return cfg
