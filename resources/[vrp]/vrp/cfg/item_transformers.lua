local cfg = {}

-- define static item transformers
-- see https://github.com/ImagicTheCat/vRP to understand the item transformer concept/definition

cfg.item_transformers = {
  -- example of harvest item transformer
  {
    name="Campo Peperoni", -- menu name
    permissions = {"agri.weed"}, -- you can add permissions
    r=0,g=125,b=255, -- color
    max_units=2500,
    units_per_minute=60,
    x=2224.587890625,y=5089.4208984375,z=49.264629364014,
    radius=20, height=4.5, -- area
    recipes = {
      ["Raccogli"] = { -- action name
        description="Raccogli peperoni", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={}, -- items taken per unit
        products={ -- items given per unit
          ["peperoni"] = 1
        }
      }
    }
  },
    {
    name="Vendita Marijuana", -- menu name
    r=0,g=125,b=255, -- color
    max_units=50000,
    units_per_minute=60,
    x=276.16986083984,y=47.524185180664,z=88.474479675292,		
    radius=2, height=4.5, -- area
    recipes = {
      ["Vendi Marijuana"] = { -- action name
        description="Vendi marijuana", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
        ["marijuana"] = 1
		},
        products={
		["dirty_money"] = 50
       }
      }
    }
  },
 
 --DROGA CAFFE'  -598.376953125,-1606.8559570312,27.010799407958
    {
    name="Raccolta meth", -- menu name
	permissions = {"gang.droga"}, -- you can add permissions
    r=0,g=125,b=255, -- color
    max_units=50000,
    units_per_minute=30,
    x=52.425228118896,y=7030.0234375,z=10.148733139038,	
    radius=2, height=4.5, -- area
    recipes = {
      ["Raccogli meth"] = { -- action name
        description="Raccogli meth..",
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={},
        products={
		    ["meth"] = 1
       }
      }
    }
  }, 
  {
    name="Processo meth", -- menu name
	  --permissions = {"gang.droga"}, -- you can add permissions
    r=0,g=125,b=255, -- color
    max_units=50000,
    units_per_minute=30,
    x=-533.65576171875,y=5290.298828125,z=74.20945739746,	
    radius=2, height=4.5, -- area
    recipes = {
      ["Processo meth semplice"] = { -- action name
      description="Processo meth..",
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
        reagents={
          ["marijuana"] = 1,
          ["meth"] = 1
        },
        products={
		    ["meth_semplice"] = 1
        }
      },
      ["Processo meth Super"] = { -- action name
      description="Processo meth..",
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
        reagents={
          ["meth_mix"] = 1,
          ["nitrato_di_potassio"] = 1,
          ["carbone"] = 1
        },
        products={
		    ["meth_super"] = 1
        }
      }
    }
  },
  {
    name="Vendita meth", -- menu name
	  --permissions = {"gang.droga"}, -- you can add permissions
    r=0,g=125,b=255, -- color
    max_units=50000,
    units_per_minute=30,
    x=710.33728027344,y=4185.373046875,z=40.882667541504,	
    radius=2, height=4.5, -- area
    recipes = {
      ["Vendita meth semplice"] = { -- action name
        description="Vendita meth semplice",
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
          ["meth_semplice"] = 1
        },
        products={
		  ["dirty_money"] = 80
       }
      },	  
      ["Vendi meth mix"] = { -- action name
      description="Vendi meth mix",
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={
        ["meth_mix"] = 1
      },
      products={
        ["dirty_money"] = 300
        }
      },
      ["Vendita meth Super"] = { -- action name
      description="Vendita meth Super",
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={
        ["meth_super"] = 1
      },
      products={
    ["dirty_money"] = 900
     }
    }
    }
  },
  --- new drug
  {
    name="Raccolta Metadone", -- menu name
	 permissions = {"garnet.droga"}, -- you can add permissions	
    r=0,g=125,b=255, -- color
    max_units=50000,
    units_per_minute=30,
    x=2461.5068359375,y=1589.7760009766,z=33.001750946044,		
    radius=2, height=4.5, -- area
    recipes = {
      ["Raccolta Metadone"] = { -- action name
        description="Stai raccogliendo metadone",
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={},
        products={
		  ["metadone"] = 1
       }
      }
    }
  },
  
  {
    name="Vendita Metadone", -- menu name
	 --permissions = {"garnet.droga"}, -- you can add permissions	
    r=0,g=125,b=255, -- color
    max_units=50000,
    units_per_minute=30,
    x=-2175.3701171875,y=4294.6103515625,z=49.058277130126,		
    radius=2, height=4.5, -- area
    recipes = {
      ["Vendita Metadone"] = { -- action name
        description="Stai Vendendo metadone",
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
          ["metadone"] = 1
        },
        products={
		      ["money"] = 30
       }
      }
    }
  },

  {   
  name="Miscelazione Metadone", -- menu name
  --permissions = {"gang.droga"}, -- you can add permissions	
  r=0,g=125,b=255, -- color
  max_units=50000,
  units_per_minute=60,
  x=1389.817993164,y=3608.7265625,z=38.941898345948,		
  radius=2, height=4.5, -- area
  recipes = {
    ["Metadone Semplice"] = { -- action name   
    description="Qualcosa di strano..",
    in_money=0, -- money taken per unit
    out_money=0, -- money earned per unit
    reagents={
      ["metadone"] = 1,
      ["marijuana"] = 1
        },
    products={
      ["metadone_easy"] = 1
        }
      },
      ["Metadone Super"] = { -- action name   
      description="Qualcosa di strano..",
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={
        ["metadone_mix"] = 1,
        ["nitrato_di_potassio"] = 1,
        ["carbone"] = 1
          },
      products={
        ["metadone_super"] = 1
          }
        }
    }
  },
  {
    name="Vendita Metadone Misc", -- menu name
--permissions = {"gang.droga"}, -- you can add permissions	
    r=0,g=125,b=255, -- color
    max_units=50000,
    units_per_minute=30,
    x=-3179.1801757812,y=1093.4978027344,z=20.840738296508,		
    radius=2, height=4.5, -- area
    recipes = {  
        ["Metadone Semplice"] = { -- action name
      description="Qualcosa di strano..",
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={
            ["metadone_easy"] = 1
        },
          products={
            ["dirty_money"] = 80
              }
            },            
        ["Metadone Mix"] = { -- action name
          description="Qualcosa di strano..",
          in_money=0, -- money taken per unit
          out_money=0, -- money earned per unit
          reagents={
            ["metadone_mix"] = 1
        },
          products={
            ["dirty_money"] = 300
              }
            },
        ["Metadone Super"] = { -- action name
            description="Qualcosa di strano..",
            in_money=0, -- money taken per unit
            out_money=0, -- money earned per unit
            reagents={
              ["metadone_super"] = 1
          },
            products={
              ["dirty_money"] = 900
                }
              }
          }
        },
  {
    name="Laboratorio Miscelazione", -- menu name
	--permissions = {"gang.droga"}, -- you can add permissions
    r=0,g=125,b=255, -- color
    max_units=50000,
    units_per_minute=500,
    x=3609.9350585938,y=3715.6667480468,z=29.689399719238,		
    radius=2, height=4.5, -- area
    recipes = {
      ["Processa Oppio Mix"] = { -- action name
        description="????????..",
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
          ["oppio"] = 1,
          ["meth_semplice"] = 1
        },
        products={
		      ["oppio_mix"] = 1
       }
      },
      ["Processa Super Coca"] = { -- action name
      description="????????..",
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={
          ["cocaina_mix"] = 1,
          ["nitrato_di_potassio"] = 1,
          ["carbone"] = 1
        },
      products={
          ["cocaina_super"] = 1
        }
      },
      ["Processa Meth Mix"] = { -- action name
      description="????????..",
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={
          ["meth_semplice"] = 1,
          ["cocaina"] = 1
        },
      products={
          ["meth_mix"] = 1
        }
      },
      ["Processa metadone Mix"] = { -- action name
      description="????????..",
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={
          ["metadone_easy"] = 1,
          ["oppio"] = 1
        },
      products={
          ["metadone_mix"] = 1
        }
      }
    }
  },
    {
    name="Processo krokodil", -- menu name
	  --permissions = {"gang.droga"}, -- you can add permissions	
    r=0,g=125,b=255, -- color
    max_units=50000,
    units_per_minute=30,
    x=1613.069946289,y=-1716.8717041016,z=89.719291687012,		
    radius=2, height=4.5, -- area
    recipes = {
      ["Crea krokodil"] = { -- action name
        description="Qualcosa di strano..",
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
        ["marijuana"] = 1,
        ["meth"] = 1,
        ["cocaina"] = 1,
		},
        products={
		  ["krokodil"] = 1
       }
      }
    }
  },  
    {
    name="Vendita krokodil", -- menu name
    r=0,g=125,b=255, -- color
    max_units=50000,
    units_per_minute=30,
    x=1223.2869873046,y=338.60150146484,z=81.99095916748,		
    radius=2, height=4.5, -- area
    recipes = {
      ["Vendi krokodil"] = { -- action name
        description="A questa gente interessa il krokodil!",
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
        ["krokodil"] = 1,
		},
        products={
		["dirty_money"] = 250
       }
      }
    }
  },   
  
--PAPAVERO
     {
    name="Raccolta papavero", -- menu name
	  permissions = {"mano.droga"}, -- you can add permissions
    r=0,g=125,b=255, -- color
    max_units=50000,
    units_per_minute=30,
    x=1884.172241211,y=4992.2309570312,z=51.356296539306,		
    radius=3, height=4.5, -- area
    recipes = {
      ["Raccogli papavero"] = { -- action name
        description="Raccogli papavero..",
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={},
        products={
		    ["papavero"] = 1
       }
      }
    }
  },  
    {
    name="Laboratorio Chimico", -- menu name
    r=0,g=125,b=255, -- color
    max_units=50000,
    units_per_minute=30,
    x=93.127479553222,y=3755.3168945312,z=40.773025512696,
    radius=2, height=4.5, -- area
    recipes = {
      ["Miscela il Papavero"] = { -- action name
        description="Qualcosa di strano..",
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
        ["marijuana"] = 1,
        ["papavero"] = 1
		},
        products={
		  ["oppio"] = 2
       }
      },
      ["Oppio + Nitrado + Carbone"] = { -- action name
      description="Qualcosa di strano..",
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={
        ["oppio_mix"] = 1,
        ["nitrato_di_potassio"] = 1,
        ["carbone"] = 1
    },
      products={
        ["oppio_super"] = 1
        }
      }
    }
  },  
    {
    name="Vendita oppio", -- menu name
    r=0,g=125,b=255, -- color
    max_units=50000,
    units_per_minute=30,
    x=1131.7634277344,y=-446.87915039062,z=66.48844909668,	
    radius=2, height=4.5, -- area
    recipes = {
      ["Vendi oppio"] = { -- action name
        description="A questa gente interessa l'oppio!",
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
        ["oppio"] = 1,
		},
        products={
		["dirty_money"] = 125
       }
      },
      ["Vendi oppio Mix"] = { -- action name
      description="A questa gente interessa l'oppio!",
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={
        ["oppio_mix"] = 1
    },
      products={
        ["dirty_money"] = 400
        }
      },
      ["Vendi Super oppio"] = { -- action name
      description="A questa gente interessa l'oppio!",
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={
        ["oppio_super"] = 1
    },
      products={
        ["dirty_money"] = 1000
        }
      }
    }
  },  
    {
    name="Processo Medicine", -- menu name
    permissions = {"ems.whitelisted"},
    r=0,g=125,b=255, -- color
    max_units=50000,
    units_per_minute=30,
    x=333.51834106446,y=-570.74389648438,z=43.317386627198,		
    radius=2, height=4.5, -- area
    recipes = {
      ["Crea MedicinaX"] = { -- action name
        description="Zucchero,cannella ed ogni cosa è bella..",
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
        ["boccetta"] = 1,
        ["acqua"] = 5,
        ["cimetta"] = 1,		
		},
        products={
		    ["medicinax"] = 1
       }
      }
    }
  }, 
   {
    name="Palestra Casa", -- menu name
    r=255,g=125,b=0, -- color
    max_units=1000,
    units_per_minute=1000,
    x=-2585.0209960938,y=1881.0502929688,z=162.34994506836,
    radius=3.5, height=1.0, -- area
    recipes = {
      ["Forza"] = { -- action name
        description="Accresci la tua forza.", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={}, -- items taken per unit
        products={}, -- items given per unit
        aptitudes={ -- optional
          ["physical.strength"] = 10.0 -- "group.aptitude", give 1 exp per unit
        }
      }
    } 
  },
  -- NUOVO E PIU CAZZUTO

  {
    name="Estrai Materiali", -- menu name
    --permissions = {"armaiolo.armeria"}, -- you can add permissions
    r=0,g=2,b=200, -- color
    max_units=10000,
    units_per_minute=10,
    x=2952.1040039062,y=2769.4145507812,z=39.05899810791, -- pos
    radius=2.0, height=1.5, -- area
    recipes = 
    {
      ["Estrazione Risorse"] = { -- action name
        description="Magnesio-Cobalto", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
        },
      products={ -- items given per unit
            ["cobalto"] = 1,
            ["magnesio"] = 2
        }
      }
    }
    },
    ---1822.6965332032,2970.2683105468,33.008361816406 Raccolta a fort zancudo
    {
        name="Preleva Materiali", -- menu name
        --permissions = {"armaiolo.armeria"}, -- you can add permissions
        r=0,g=2,b=200, -- color
        max_units=10000,
        units_per_minute=10,
        x=-1822.6965332032,y=2970.2683105468,z=33.008361816406, -- pos
        radius=2.0, height=1.5, -- area
        recipes = 
        {
          ["Prendi Polvere Da Sparo"] = { -- action name
            description="/", -- action description
            in_money=0, -- money taken per unit
            out_money=0, -- money earned per unit
            reagents={
            },
          products={ -- items given per unit
                ["polvere_nera"] = 2
          }
         },
            ["Preleva Materiali"] = { -- action name
              description="/", -- action description
              in_money=0, -- money taken per unit
              out_money=0, -- money earned per unit
              reagents={
              },
            products={ -- items given per unit
                  ["nickel"] = 2,
                  ["piombo"] = 1
        }
      }
    }
    },

  --Assembla componenti armi 1069.95703125,-2005.7279052734,32.083209991456
  {
    name="Fondi Materiali", -- menu name
    permissions = {"armaiolo.armeria"}, -- you can add permissions
    r=0,g=2,b=200, -- color
    max_units=10000,
    units_per_minute=10,
    x=1069.95703125,y=-2005.7279052734,z=32.083209991456, -- pos
    radius=2.0, height=1.5, -- area
    recipes = 
    {
      ["Canna MK2.0"] = { -- action name
        description="Componente necessario per armi di diverso calibro", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
            ["cobalto"] = 3,
            ["nickel"] = 4,
            ["magnesio"] = 3,
            ["carbone"] = 5
        },
      products={ -- items given per unit
            ["canna_mk2"] = 1
        }
      },
        ["Canna MK3.0"] = { -- action name
          description="Componente necessario per armi di diverso calibro", -- action description
          in_money=0, -- money taken per unit
          out_money=0, -- money earned per unit
          reagents={
              ["cobalto"] = 4,
              ["nickel"] = 6,
              ["magnesio"] = 4,
              ["carbone"] = 7
          },
        products={ -- items given per unit
              ["canna_mk3"] = 1
          }
        },
          ["Canna MK4.0"] = { -- action name
            description="Componente necessario per armi di diverso calibro", -- action description
            in_money=0, -- money taken per unit
            out_money=0, -- money earned per unit
            reagents={
                ["cobalto"] = 6,
                ["nickel"] = 8,
                ["magnesio"] = 6,
                ["carbone"] = 10
            },
          products={ -- items given per unit
                ["canna_mk4"] = 1
            }
          }
         }
    },

  --BUILD ARMI ARMAIOLO
  {
    name="Assenbla Armamenti", -- menu name
    permissions = {"armaiolo.armeria"}, -- you can add permissions
    r=0,g=2,b=200, -- color
    max_units=10000,
    units_per_minute=1,
    x=-1119.426147461,y=2697.5341796875,z=18.554153442382, -- pos	
    radius=2.0, height=1.5, -- area
    recipes = 
    {
	["Assembla Pistola"] = { -- action name
        description="Assembla Pistola", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["ferro"] = 3,
      ["rame"] = 1,
      ["piombo"] = 3
		},
      products={ -- items given per unit
        ["wbody|WEAPON_PISTOL"] = 1
      }
    },
    ["Assembla Pistola MK2"] = { -- action name
        description="Assembla Pistola MK2", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["ferro"] = 4,
      ["rame"] = 1,
      ["piombo"] = 3, 	
      ["canna_mk2"] = 1	  
		},
      products={ -- items given per unit
        ["wbody|WEAPON_PISTOL_MK2"] = 1
      }
    },
    ["Assembla Makarov"] = { -- action name
        description="Assembla Makarov", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["ferro"] = 3,
      ["rame"] = 1,
      ["piombo"] = 3	
		},
      products={ -- items given per unit
        ["wbody|WEAPON_SNSPISTOL"] = 1
      }
    },	
    ["Assembla M1911"] = { -- action name
        description="Assembla M1911", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["ferro"] = 5,
      ["rame"] = 2,
      ["piombo"] = 4 	  
		},
      products={ -- items given per unit
        ["wbody|WEAPON_HEAVYPISTOL"] = 1
      }
    },		
    ["Assembla Vintage"] = { -- action name
        description="Assembla Vintage", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["ferro"] = 2,
      ["rame"] = 1,
      ["piombo"] = 2  	  
		},
      products={ -- items given per unit
        ["wbody|WEAPON_VINTAGEPISTOL"] = 1
      }
    },		
    ["Assembla P90 MK"] = { -- action name
        description="Assembla P90 MK", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["ferro"] = 8,
      ["rame"] = 3,
      ["piombo"] = 5  	  
		},
      products={ -- items given per unit
        ["wbody|WEAPON_ASSAULTSMG"] = 1
      }
    },			
    ["Assembla TEC-9"] = { -- action name
        description="Assembla TEC-9", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["ferro"] = 5,
      ["rame"] = 2,
      ["piombo"] = 2  	  
		},
      products={ -- items given per unit
        ["wbody|WEAPON_MACHINEPISTOL"] = 1
      }
    },			
    ["Assembla TAR"] = { -- action name
        description="Assembla TAR", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["ferro"] = 8,
      ["rame"] = 3,
      ["piombo"] = 5  	  
		},
      products={ -- items given per unit
        ["wbody|WEAPON_ADVANCEDRIFLE"] = 1
      }
    },			
    ["Assembla Canne Mozze"] = { -- action name
        description="Assembla Canne Mozze", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["ferro"] = 7,
      ["rame"] = 1,
      ["piombo"] = 3 	  
		},
      products={ -- items given per unit
        ["wbody|WEAPON_DBSHOTGUN"] = 1
      }
    },		
    ["Assembla Colt 1892"] = { -- action name
        description="Assembla Colt 1892", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["ferro"] = 3,
      ["rame"] = 1,
      ["piombo"] = 1  	  
		},
      products={ -- items given per unit
        ["wbody|WEAPON_DOUBLEACTION"] = 1
      }
    },			
    ["Assembla Makarov MK2"] = { -- action name
        description="Assembla Makarov MK2", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["ferro"] = 4,
      ["rame"] = 2,
      ["piombo"] = 2 ,
      ["canna_mk2"] = 1	  
		},
      products={ -- items given per unit
        ["wbody|WEAPON_SNSPISTOL_MK2"] = 1
      }
    },		
    ["Assembla Micro SMG"] = { -- action name
        description="Assembla Micro SMG", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["ferro"] = 6,
      ["rame"] = 3,
      ["piombo"] = 3 	  
		},
      products={ -- items given per unit
        ["wbody|WEAPON_MICROSMG"] = 1
      }
    },	
    ["Assembla Revolver"] = { -- action name
        description="Assembla Revolver", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["ferro"] = 3,
      ["rame"] = 2,
      ["piombo"] = 3
		},
      products={ -- items given per unit
        ["wbody|WEAPON_REVOLVER"] = 1
      }
    },		
    ["Assembla APPistol"] = { -- action name
        description="Assembla APPistol", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["ferro"] = 2,
      ["rame"] = 2,
      ["piombo"] = 2  	  
		},
      products={ -- items given per unit
        ["wbody|WEAPON_APPISTOL"] = 1
      }
    },		
    ["Assembla Desert Eagle"] = { -- action name
        description="Assembla Desert Eagle", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["ferro"] = 4,
      ["rame"] = 3,
      ["piombo"] = 4  	  
		},
      products={ -- items given per unit
        ["wbody|WEAPON_PISTOL50"] = 1
      }
    },		
    ["Assembla Flare Gun"] = { -- action name
        description="Assembla Flare Gun", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["ferro"] = 2,
      ["rame"] = 1,
      ["piombo"] = 1  	  
		},
      products={ -- items given per unit
        ["wbody|WEAPON_FLAREGUN"] = 1
      }
    },	
    ["Assembla MP5 K"] = { -- action name
        description="Assembla MP5 K", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["ferro"] = 8,
      ["rame"] = 4,
      ["piombo"] = 5  	  
		},
      products={ -- items given per unit
        ["wbody|WEAPON_SMG_MK2"] = 1
      }
    },		
    ["Assembla PKM"] = { -- action name
        description="Assembla PKM", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["ferro"] = 16,
      ["rame"] = 8,
      ["piombo"] = 8,  	  
      ["tungsteno"] = 1,
      ["canna_mk3"] = 1 
		},
      products={ -- items given per unit
        ["wbody|WEAPON_MG"] = 1
      }
    },		
    ["Assembla Thompson"] = { -- action name
        description="Assembla Thompson", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["ferro"] = 8,
      ["rame"] = 3,
      ["piombo"] = 5  	  
		},
      products={ -- items given per unit
        ["wbody|WEAPON_GUSENBERG"] = 1
      }
    },		
    ["Assembla AK-47"] = { -- action name
        description="Assembla AK-47", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["ferro"] = 12,
      ["rame"] = 5,
      ["piombo"] = 6 	  
		},
      products={ -- items given per unit
        ["wbody|WEAPON_ASSAULTRIFLE"] = 1
      }
    },		
    ["Assembla AK-74"] = { -- action name
        description="Assembla AK-74", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["ferro"] = 14,
      ["rame"] = 6,
      ["piombo"] = 6,
	  ["canna_mk2"] = 1
		},
      products={ -- items given per unit
        ["wbody|WEAPON_ASSAULTRIFLE_MK2"] = 1
      }
    },		
    ["Assembla G36"] = { -- action name
        description="Assembla G36", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["ferro"] = 12,
      ["rame"] = 5,
      ["piombo"] = 6 	  
		},
      products={ -- items given per unit
        ["wbody|WEAPON_SPECIALCARBINE"] = 1
      }
    },		
    ["Assembla AK-74U"] = { -- action name
        description="Assembla AK-74U", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["ferro"] = 9,
      ["rame"] = 4,
      ["piombo"] = 4,
	  ["canna_mk2"] = 1
		},
      products={ -- items given per unit
        ["wbody|WEAPON_COMPACTRIFLE"] = 1
      }
    },		
    ["Assembla Sawn-Off"] = { -- action name
        description="Assembla Sawn-Off", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["ferro"] = 7,
      ["rame"] = 1,
      ["piombo"] = 3 	  
		},
      products={ -- items given per unit
        ["wbody|WEAPON_SAWNOFFSHOTGUN"] = 1
      }
    },		
    ["Assembla Striker"] = { -- action name
        description="Assembla Striker", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["ferro"] = 8,
      ["rame"] = 2,
      ["piombo"] = 3  	  
		},
      products={ -- items given per unit
        ["wbody|WEAPON_SWEEPERSHOTGUN"] = 1
      }
    },			
    ["Assembla KSG"] = { -- action name
        description="Assembla KSG", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["ferro"] = 8,
      ["rame"] = 2,
      ["piombo"] = 3  
		},
      products={ -- items given per unit
        ["wbody|WEAPON_BULLUPSHOTGUN"] = 1
      }
    },			
    ["Assembla R700"] = { -- action name
        description="Assembla KSG", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["ferro"] = 14,
      ["rame"] = 4,
      ["oro"] = 4,
      ["piombo"] = 5,  	  
      ["tungsteno"] = 2,
	  ["canna_mk4"] = 1
		},
      products={ -- items given per unit
        ["wbody|WEAPON_SNIPERRIFLE"] = 1
      }
    },	
    ["Assembla Barret 50.Cal"] = { -- action name
        description="Assembla Barret 50.Cal", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["ferro"] = 14,
      ["rame"] = 5,
      ["oro"] = 5,
      ["piombo"] = 6,  	 
      ["tungsteno"] = 2,
	  ["canna_mk4"] = 1
		},
      products={ -- items given per unit
        ["wbody|WEAPON_HEAVYSNIPER_MK2"] = 1
      }
    },	
    ["Assembla M79 Thumper"] = { -- action name
        description="Assembla M79 Thumper", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["ferro"] = 12,
      ["rame"] = 3,
      ["oro"] = 3,
      ["piombo"] = 7,
      ["tungsteno"] = 2,
	  ["canna_mk3"] = 1
		},
      products={ -- items given per unit
        ["wbody|WEAPON_COMPACTLAUNCHER"] = 1
      }
    },		
    ["Assembla R870"] = { -- action name
        description="Assembla R870", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["ferro"] = 8,
      ["rame"] = 4,
      ["piombo"] = 4,  	  
	  ["canna_mk2"] = 1
		},
      products={ -- items given per unit
        ["wbody|WEAPON_PUMPSHOTGUN_MK2"] = 1
      }
    },		
    ["Assembla FAMAS"] = { -- action name
        description="Assembla FAMAS", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["ferro"] = 9,
      ["rame"] = 5,
      ["piombo"] = 6,  
	  ["canna_mk2"] = 1
		},
      products={ -- items given per unit
        ["wbody|WEAPON_BULLPUPRIFLE_MK2"] = 1
      }
    },	
    ["Assembla G36 MK"] = { -- action name
        description="Assembla G36 MK", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["ferro"] = 9,
      ["rame"] = 6,
      ["piombo"] = 6, 
      ["canna_mk2"] = 1 	  
		},
      products={ -- items given per unit
        ["wbody|WEAPON_SPECIALCARBINE_MK2"] = 1
      }
    },	
    ["Assembla Python"] = { -- action name
        description="Assembla Python", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["ferro"] = 6,
      ["rame"] = 3,
      ["piombo"] = 4,  	  
	  ["canna_mk2"] = 1
		},
      products={ -- items given per unit
        ["wbody|WEAPON_REVOLVER_MK2"] = 1
      }
    },	
    ["Assembla Granata"] = { -- action name
        description="Assembla Granata", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["ferro"] = 8,
      ["rame"] = 2,
      ["oro"] = 1,
      ["piombo"] = 3,  
      ["tungsteno"] = 1		  
		},
      products={ -- items given per unit
        ["wbody|WEAPON_GRENADE"] = 1
      }
    },	
    ["Assembla Pistola Comb."] = { -- action name
        description="Assembla Pistola Comb.", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["ferro"] = 4,
      ["rame"] = 2,
      ["piombo"] = 3
	  },
      products={ -- items given per unit
        ["wbody|WEAPON_COMBATPISTOL"] = 1
      }
    },	
    ["Assembla MP5"] = { -- action name
        description="Assembla MP5.", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["ferro"] = 5,
      ["rame"] = 4,
      ["piombo"] = 3
	  },
      products={ -- items given per unit
        ["wbody|WEAPON_SMG"] = 1
      }
    },	
    ["Assembla MPX"] = { -- action name
        description="Assembla MPX.", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["ferro"] = 6,
      ["rame"] = 5,
      ["piombo"] = 4
	  },
      products={ -- items given per unit
        ["wbody|WEAPON_COMBATPDW"] = 1
      }
    },	
    ["Assembla M4A1"] = { -- action name
        description="Assembla M4A1.", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["ferro"] = 12,
      ["rame"] = 7,
      ["piombo"] = 6
	  },
      products={ -- items given per unit
        ["wbody|WEAPON_CARBINERIFLE"] = 1
      }
    },		
    ["Assembla MOSSBERG 500"] = { -- action name
        description="Assembla MOSSBERG 500", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["ferro"] = 8,
      ["rame"] = 3,
      ["piombo"] = 4
	  },
      products={ -- items given per unit
        ["wbody|WEAPON_PUMPSHOTGUN"] = 1
      }
    },	
    ["Assembla HK416"] = { -- action name
        description="Assembla HK416", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["ferro"] = 14,
      ["rame"] = 7,
      ["oro"] = 1,
      ["piombo"] = 8,
	  ["canna_mk2"] = 1
	  },
      products={ -- items given per unit
        ["wbody|WEAPON_CARBINERIFLE_MK2"] = 1
      }
    },
    ["Assembla MK14"] = { -- action name
        description="Assembla MK14", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["ferro"] = 14,
      ["rame"] = 4,
      ["oro"] = 4,
      ["piombo"] = 5,
      ["tungsteno"] = 5,
	  ["canna_mk4"] = 1
	  },
      products={ -- items given per unit
        ["wbody|WEAPON_MARKSMANRIFLE_MK2"] = 1
      }
    },	
    ["Assembla Storditore"] = { -- action name
        description="Assembla Storditore", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["ferro"] = 2,
      ["rame"] = 1,
      ["piombo"] = 1  
		},
      products={ -- items given per unit
        ["wbody|WEAPON_STUNGUN"] = 1
      }
    }
    }
   },  

      --Costruzione Munizioni
  {
    name="Assenbla Munizioni", -- menu name
    permissions = {"armaiolo.armeria"}, -- you can add permissions
    r=0,g=2,b=200, -- color
    max_units=10000,
    units_per_minute=1,
    x=107.6876296997,y=6629.4868164062,z=31.787229537964, -- pos	
    radius=2.0, height=1.5, -- area
    recipes = 
    {
	["Assembla Pistola"] = { -- action name
        description="Assembla Pistola", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["nitrato_di_potassio"] = 15,
      ["zolfo"] = 15,
      ["polvere_nera"] = 60
		},
      products={ -- items given per unit
        ["wammo|WEAPON_PISTOL"] = 30
      }
    },
    ["Assembla Pistola MK2"] = { -- action name
        description="Assembla Pistola MK2", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["nitrato_di_potassio"] = 15,
      ["zolfo"] = 15,
      ["polvere_nera"] = 60	  
		},
      products={ -- items given per unit
        ["wammo|WEAPON_PISTOL_MK2"] = 1
      }
    },
    ["Assembla Makarov"] = { -- action name
        description="Assembla Makarov", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["nitrato_di_potassio"] = 15,
      ["zolfo"] = 15,
      ["polvere_nera"] = 60  
		},
      products={ -- items given per unit
        ["wammo|WEAPON_SNSPISTOL"] = 30
      }
    },	
    ["Assembla M1911"] = { -- action name
        description="Assembla M1911", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["nitrato_di_potassio"] = 15,
      ["zolfo"] = 15,
      ["polvere_nera"] = 60	  
		},
      products={ -- items given per unit
        ["wammo|WEAPON_HEAVYPISTOL"] = 30
      }
    },		
    ["Assembla Vintage"] = { -- action name
        description="Assembla Vintage", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["nitrato_di_potassio"] = 15,
      ["zolfo"] = 15,
      ["polvere_nera"] = 60	  
		},
      products={ -- items given per unit
        ["wammo|WEAPON_VINTAGEPISTOL"] = 30
      }
    },		
    ["Assembla P90 MK"] = { -- action name
        description="Assembla P90 MK", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["nitrato_di_potassio"] = 20,
      ["zolfo"] = 20,
      ["polvere_nera"] = 50	  
		},
      products={ -- items given per unit
        ["wammo|WEAPON_ASSAULTSMG"] = 25
      }
    },			
    ["Assembla TEC-9"] = { -- action name
        description="Assembla TEC-9", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["nitrato_di_potassio"] = 15,
      ["zolfo"] = 15,
      ["polvere_nera"] = 60 	  
		},
      products={ -- items given per unit
        ["wammo|WEAPON_MACHINEPISTOL"] = 30
      }
    },			
    ["Assembla TAR"] = { -- action name
        description="Assembla TAR", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["nitrato_di_potassio"] = 30,
      ["zolfo"] = 30,
      ["polvere_nera"] = 30 	  
		},
      products={ -- items given per unit
        ["wammo|WEAPON_ADVANCEDRIFLE"] = 15
      }
    },			
    ["Assembla Canne Mozze"] = { -- action name
        description="Assembla Canne Mozze", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["nitrato_di_potassio"] = 20,
      ["zolfo"] = 20,
      ["polvere_nera"] = 20  
		},
      products={ -- items given per unit
        ["wammo|WEAPON_DBSHOTGUN"] = 10
      }
    },		
    ["Assembla Colt 1892"] = { -- action name
        description="Assembla Colt 1892", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["nitrato_di_potassio"] = 15,
      ["zolfo"] = 15,
      ["polvere_nera"] = 60  
		},
      products={ -- items given per unit
        ["wammo|WEAPON_DOUBLEACTION"] = 30
      }
    },			
    ["Assembla Makarov MK"] = { -- action name
        description="Assembla Makarov MK", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["nitrato_di_potassio"] = 15,
      ["zolfo"] = 15,
      ["polvere_nera"] = 60  
		},
      products={ -- items given per unit
        ["wammo|WEAPON_SNSPISTOL_MK2"] = 30
      }
    },		
    ["Assembla Micro SMG"] = { -- action name
        description="Assembla Micro SMG", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["nitrato_di_potassio"] = 20,
      ["zolfo"] = 20,
      ["polvere_nera"] = 50
		},
      products={ -- items given per unit
        ["wammo|WEAPON_MICROSMG"] = 25
      }
    },	
    ["Assembla Revolver"] = { -- action name
        description="Assembla Revolver", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["nitrato_di_potassio"] = 15,
      ["zolfo"] = 15,
      ["polvere_nera"] = 60 	  
		},
      products={ -- items given per unit
        ["wammo|WEAPON_REVOLVER"] = 30
      }
    },		
    ["Assembla APPistol"] = { -- action name
        description="Assembla APPistol", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["nitrato_di_potassio"] = 15,
      ["zolfo"] = 15,
      ["polvere_nera"] = 60 	  
		},
      products={ -- items given per unit
        ["wammo|WEAPON_APPISTOL"] = 30
      }
    },		
    ["Assembla Desert Eagle"] = { -- action name
        description="Assembla Desert Eagle", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["nitrato_di_potassio"] = 15,
      ["zolfo"] = 15,
      ["polvere_nera"] = 60	  
		},
      products={ -- items given per unit
        ["wammo|WEAPON_PISTOL50"] = 30
      }
    },		
    ["Assembla Flare Gun"] = { -- action name
        description="Assembla Flare Gun", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["nitrato_di_potassio"] = 5,
      ["zolfo"] = 5,
      ["polvere_nera"] = 25	  
		},
      products={ -- items given per unit
        ["wammo|WEAPON_FLAREGUN"] = 5
      }
    },	
    ["Assembla MP5 K"] = { -- action name
        description="Assembla MP5 K", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["nitrato_di_potassio"] = 20,
      ["zolfo"] = 20,
      ["polvere_nera"] = 50  
		},
      products={ -- items given per unit
        ["wammo|WEAPON_SMG_MK2"] = 25
      }
    },		
    ["Assembla PKM"] = { -- action name
        description="Assembla PKM", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["nitrato_di_potassio"] = 30,
      ["zolfo"] = 30,
      ["polvere_nera"] = 30
		},
      products={ -- items given per unit
        ["wammo|WEAPON_MG"] = 1
      }
    },		
    ["Assembla Thompson"] = { -- action name
        description="Assembla Thompson", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["nitrato_di_potassio"] = 20,
      ["zolfo"] = 20,
      ["polvere_nera"] = 50	  
		},
      products={ -- items given per unit
        ["wammo|WEAPON_GUSENBERG"] = 25
      }
    },		
    ["Assembla AK-47"] = { -- action name
        description="Assembla AK-47", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["nitrato_di_potassio"] = 30,
      ["zolfo"] = 30,
      ["polvere_nera"] = 30
		},
      products={ -- items given per unit
        ["wammo|WEAPON_ASSAULTRIFLE"] = 15
      }
    },		
    ["Assembla AK-74"] = { -- action name
        description="Assembla AK-74", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["nitrato_di_potassio"] = 30,
      ["zolfo"] = 30,
      ["polvere_nera"] = 30
		},
      products={ -- items given per unit
        ["wammo|WEAPON_ASSAULTRIFLE_MK2"] = 15
      }
    },		
    ["Assembla G36"] = { -- action name
        description="Assembla G36", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["nitrato_di_potassio"] = 30,
      ["zolfo"] = 30,
      ["polvere_nera"] = 30	  
		},
      products={ -- items given per unit
        ["wammo|WEAPON_SPECIALCARBINE"] = 15
      }
    },		
    ["Assembla AK-74U"] = { -- action name
        description="Assembla AK-74U", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["nitrato_di_potassio"] = 30,
      ["zolfo"] = 30,
      ["polvere_nera"] = 30 
		},
      products={ -- items given per unit
        ["wammo|WEAPON_COMPACTRIFLE"] = 15
      }
    },		
    ["Assembla Sawn-Off"] = { -- action name
        description="Assembla Sawn-Off", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["nitrato_di_potassio"] = 20,
      ["zolfo"] = 20,
      ["polvere_nera"] = 20	  
		},
      products={ -- items given per unit
        ["wammo|WEAPON_SAWNOFFSHOTGUN"] = 10
      }
    },		
    ["Assembla Striker"] = { -- action name
        description="Assembla Striker", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["nitrato_di_potassio"] = 20,
      ["zolfo"] = 20,
      ["polvere_nera"] = 20	  
		},
      products={ -- items given per unit
        ["wammo|WEAPON_SWEEPERSHOTGUN"] = 10
      }
    },			
    ["Assembla KSG"] = { -- action name
        description="Assembla KSG", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["nitrato_di_potassio"] = 20,
      ["zolfo"] = 20,
      ["polvere_nera"] = 20
		},
      products={ -- items given per unit
        ["wammo|WEAPON_BULLUPSHOTGUN"] = 10
      }
    },			
    ["Assembla R700"] = { -- action name
        description="Assembla KSG", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["nitrato_di_potassio"] = 15,
      ["zolfo"] = 15,
      ["polvere_nera"] = 30
		},
      products={ -- items given per unit
        ["wammo|WEAPON_SNIPERRIFLE"] = 5
      }
    },	
    ["Assembla Barret 50.Cal"] = { -- action name
        description="Assembla Barret 50.Cal", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["nitrato_di_potassio"] = 15,
      ["zolfo"] = 15,
      ["polvere_nera"] = 30
		},
      products={ -- items given per unit
        ["wammo|WEAPON_HEAVYSNIPER_MK2"] = 5
      }
    },	
    ["Assembla M79 Thumper"] = { -- action name
        description="Assembla M79 Thumper", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["nitrato_di_potassio"] = 30,
      ["zolfo"] = 30,
      ["polvere_nera"] = 30
		},
      products={ -- items given per unit
        ["wammo|WEAPON_COMPACTLAUNCHER"] = 1
      }
    },		
    ["Assembla R870"] = { -- action name
        description="Assembla R870", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["nitrato_di_potassio"] = 20,
      ["zolfo"] = 20,
      ["polvere_nera"] = 20 
		},
      products={ -- items given per unit
        ["wammo|WEAPON_PUMPSHOTGUN_MK2"] = 10
      }
    },		
    ["Assembla FAMAS"] = { -- action name
        description="Assembla FAMAS", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["nitrato_di_potassio"] = 30,
      ["zolfo"] = 30,
      ["polvere_nera"] = 30	  
		},
      products={ -- items given per unit
        ["wammo|WEAPON_BULLPUPRIFLE_MK2"] = 5
      }
    },	
    ["Assembla G36 MK"] = { -- action name
        description="Assembla G36 MK", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["nitrato_di_potassio"] = 30,
      ["zolfo"] = 30,
      ["polvere_nera"] = 30 	  
		},
      products={ -- items given per unit
        ["wammo|WEAPON_SPECIALCARBINE_MK2"] = 5
      }
    },	
    ["Assembla Python"] = { -- action name
        description="Assembla Python", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["nitrato_di_potassio"] = 15,
      ["zolfo"] = 15,
      ["polvere_nera"] = 60  
		},
      products={ -- items given per unit
        ["wammo|WEAPON_REVOLVER_MK2"] = 30
      }
    },	
    ["Assembla Granata"] = { -- action name
        description="Assembla Granata", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["nitrato_di_potassio"] = 30,
      ["zolfo"] = 30,
      ["polvere_nera"] = 30	  
		},
      products={ -- items given per unit
		["wammo|WEAPON_GRENADE"] = 1
      }
    },	
    ["Assembla Pistola Comb."] = { -- action name
        description="Assembla Pistola Comb.", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["nitrato_di_potassio"] = 15,
      ["zolfo"] = 15,
      ["polvere_nera"] = 60
	  },
      products={ -- items given per unit
        ["wammo|WEAPON_COMBATPISTOL"] = 30
      }
    },	
    ["Assembla MP5"] = { -- action name
        description="Assembla MP5.", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["nitrato_di_potassio"] = 20,
      ["zolfo"] = 20,
      ["polvere_nera"] = 50
	  },
      products={ -- items given per unit
        ["wammo|WEAPON_SMG"] = 25
      }
    },	
    ["Assembla MPX"] = { -- action name
        description="Assembla MPX.", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["nitrato_di_potassio"] = 20,
      ["zolfo"] = 20,
      ["polvere_nera"] = 50
	  },
      products={ -- items given per unit
        ["wammo|WEAPON_COMBATPDW"] = 25
      }
    },	
    ["Assembla M4A1"] = { -- action name
        description="Assembla M4A1.", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["nitrato_di_potassio"] = 30,
      ["zolfo"] = 30,
      ["polvere_nera"] = 30
	  },
      products={ -- items given per unit
        ["wammo|WEAPON_CARBINERIFLE"] = 15
      }
    },		
    ["Assembla MOSSBERG 500"] = { -- action name
        description="Assembla MOSSBERG 500", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["nitrato_di_potassio"] = 20,
      ["zolfo"] = 20,
      ["polvere_nera"] = 20
	  },
      products={ -- items given per unit
        ["wammo|WEAPON_PUMPSHOTGUN"] = 10
      }
    },	
    ["Assembla HK416"] = { -- action name
        description="Assembla HK416", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["nitrato_di_potassio"] = 30,
      ["zolfo"] = 30,
      ["polvere_nera"] = 30
	  },
      products={ -- items given per unit
        ["wammo|WEAPON_CARBINERIFLE_MK2"] = 15
      }
    },
    ["Assembla MK14"] = { -- action name
        description="Assembla MK14", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
      ["nitrato_di_potassio"] = 15,
      ["zolfo"] = 15,
      ["polvere_nera"] = 30
	  },
      products={ -- items given per unit
        ["wammo|WEAPON_MARKSMANRIFLE_MK2"] = 5
      }
    }
    }
   },  	
   
   
   
   
   --AGRICOLTORE
   
  {
    name="Campo Pomodori", -- menu name
    permissions = {"agri.weed"}, -- you can add permissions
    r=0,g=125,b=255, -- color
    max_units=50000,
    units_per_minute=1000,
    x=1983.0229492188,y=4840.9750976563,z=43.930229187012,
    radius=20, height=4.5, -- area
    recipes = {
      ["Raccogli"] = { -- action name
        description="Raccogli pomodori", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={}, -- items taken per unit
        products={ -- items given per unit
          ["pomodori"] = 1
        }
      }
    }
  },
  {
    name="Ritiro kit kings of engine", -- menu name
    permissions = {"repair.market1"}, -- you can add permissions474.56155395508,-1308.59765625,29.206600189209
    r=0,g=125,b=255, -- color
    max_units=50000,
    units_per_minute=1000,
    x=474.56155395508,y=-1308.59765625,z=29.206600189209,
    radius=2, height=4.5, -- area
    recipes = {
      ["prendi"] = { -- action name
        description="Prendi kit", -- action description
        in_money=100, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={}, -- items taken per unit
        products={ -- items given per unit
          ["repairkit"] = 1
        }
      }
    }
  },
  {
    name="Campo Melenzane", -- menu name
    permissions = {"agri.weed"}, -- you can add permissions
    r=0,g=125,b=255, -- color
    max_units=50000,
    units_per_minute=1000,
    x=1264.9382324219,y=3544.5810546875,z=35.14733505249,
    radius=20, height=4.5, -- area
    recipes = {
      ["Raccogli"] = { -- action name
        description="Raccogli melenzane", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={}, -- items taken per unit
        products={ -- items given per unit
          ["melenzane"] = 1
        }
      }
    }
  },
  {
    name="Aragoste", -- menu name
    permissions = {"mission.delivery.fish"}, -- you can add permissions
    r=0,g=125,b=255, -- color
    max_units=5000,
    units_per_minute=1,
    x=3827.5107421875,y=5340.345703125,z=-87.813774108887,
    radius=20, height=4.5, -- area
    recipes = {
      ["Raccogli"] = { -- action name
        description="Pesca aragoste", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={}, -- items taken per unit
        products={ -- items given per unit
          ["aragosta"] = 1
        }
      }
    }
  },
 
 --marijuana libera
   {
    name="Processo 1", -- menu name
    r=0,g=125,b=255, -- color
    max_units=10000,
    units_per_minute=30,
    x=1050.7476806641,y=-3205.4145507813,z=-39.109344482422,		
    radius=2, height=2, -- area
    recipes = {
      ["Processo"] = { -- action name
        description="Pianta dei cimetta, e prendi la marijuana", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
        ["cimetta"] = 1
		},
        products={ -- items given per unit
          ["marijuana"] = 2
        }
      }
    }
  },
  
     {
    name="Processo.", -- menu name
    r=0,g=125,b=255, -- color
    max_units=10000,
    units_per_minute=30,
    x=-126.89821624756,y=2793.2639160156,z=53.107749938964,	
    radius=2, height=2, -- area
    recipes = {
      ["Processo"] = { -- action name
        description="Pianta dei cimetta, e prendi la marijuana", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
        ["cimetta"] = 1
		},
        products={ -- items given per unit
          ["marijuana"] = 2
        }
      }
    }
  },
  
     {
    name="Processo 2", -- menu name
    r=0,g=125,b=255, -- color
    max_units=10000,
    units_per_minute=30,
    x=1057.6866455078,y=-3200.8388671875,z=-39.076774597168,	
    radius=1, height=2, -- area
    recipes = {
      ["Processo"] = { -- action name
        description="Pianta dei cimetta, e prendi la marijuana", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
        ["cimetta"] = 1
		},
        products={ -- items given per unit
          ["marijuana"] = 2
        }
      }
    }
  },
     {
    name="Processo 3", -- menu name
    r=0,g=125,b=255, -- color
    max_units=10000,
    units_per_minute=30,
    x=1051.9780273438,y=-3199.142578125,z=-39.108573913574,		
    radius=1, height=2, -- area
    recipes = {
      ["Processo"] = { -- action name
        description="Pianta dei cimetta, e prendi la marijuana", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
        ["cimetta"] = 1
		},
        products={ -- items given per unit
          ["marijuana"] = 2
        }
      }
    }
  },  
     {
    name="Processo 4", -- menu name
    r=0,g=125,b=255, -- color
    max_units=10000,
    units_per_minute=30,
    x=1052.1651611328,y=-3194.181640625,z=-39.148983001709,		
    radius=1, height=2, -- area
    recipes = {
      ["Processo"] = { -- action name
        description="Pianta dei cimetta, e prendi la marijuana", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
        ["cimetta"] = 1
		},
        products={ -- items given per unit
          ["marijuana"] = 2
        }
      }
    }
  },  
     {
    name="Processo 5", -- menu name
    r=0,g=125,b=255, -- color
    max_units=10000,
    units_per_minute=30,
    x=1056.1345214844,y=-3189.7409667969,z=-39.113162994385,		
    radius=1, height=2, -- area
    recipes = {
      ["Processo"] = { -- action name
        description="Pianta dei cimetta, e prendi la marijuana", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
        ["cimetta"] = 1
		},
        products={ -- items given per unit
          ["marijuana"] = 2
        }
      }
    }
  },      
     {
    name="Processo 6", -- menu name
    r=0,g=125,b=255, -- color
    max_units=10000,
    units_per_minute=30,
    x=1062.1129150391,y=-3193.0170898438,z=-39.101783752441,		
    radius=1, height=2, -- area
    recipes = {
      ["Processo"] = { -- action name
        description="Pianta dei cimetta, e prendi la marijuana", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
        ["cimetta"] = 1
		},
        products={ -- items given per unit
          ["marijuana"] = 2
        }
      }
    }
  },    
     {
    name="Processo 7", -- menu name
    r=0,g=125,b=255, -- color
    max_units=10000,
    units_per_minute=30,
    x=1062.1441650391,y=-3199.6457519531,z=-39.14672088623,		
    radius=1, height=2, -- area
    recipes = {
      ["Processo"] = { -- action name
        description="Pianta dei cimetta, e prendi la marijuana", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
        ["cimetta"] = 1
		},
        products={ -- items given per unit
          ["marijuana"] = 2
        }
      }
    }
  },      
     {
    name="Processo 8", -- menu name
    r=0,g=125,b=255, -- color
    max_units=10000,
    units_per_minute=30,
    x=1058.5832519531,y=-3188.7092285156,z=-39.151229858398,			
    radius=1, height=2, -- area
    recipes = {
      ["Processo"] = { -- action name
        description="Pianta dei cimetta, e prendi la marijuana", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
        ["cimetta"] = 1
		},
        products={ -- items given per unit
          ["marijuana"] = 2
        }
      }
    }
  },       
  {
    name="Campo mungitura", -- menu name
    permissions = {"agri.weed"}, -- you can add permissions 2404.3623046875,5034.4013671875,45.993152618408
    r=0,g=125,b=255, -- color
    max_units=8000,
    units_per_minute=2,
    x=2404.3623046875,y=5034.4013671875,z=45.993152618408,
    radius=10, height=4.5, -- area
    recipes = {
      ["Mungi"] = { -- action name
        description="Mungi mucca", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={}, -- items taken per unit
        products={ -- items given per unit
          ["latte"] = 1
        }
      }
    }
  },
  {
    name="Raccolta Alcool etilico", -- menu name
    permissions = {"clande.permi"}, -- you can add permissions
    r=0,g=125,b=255, -- color
    max_units=100000,
    units_per_minute=2,
    x=496.34432983398,y=-1969.7376708984,z=24.904523849487,
    radius=6, height=4.5, -- area
    recipes = {
      ["Prendi"] = { -- action name
        description="Prendi Alcool etilico", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={}, -- items taken per unit
        products={ -- items given per unit
          ["alcool"] = 1
        }
      }
    }
  },
  {
    name="Campo Mele", -- menu name
    permissions = {"agri.weed"}, -- you can add permissions
    r=0,g=125,b=255, -- color
    max_units=50000,
    units_per_minute=1000,
    x=352.02847290039,y=6512.798828125,z=28.521469116211,
    radius=20, height=4.5, -- area
    recipes = {
      ["Raccogli"] = { -- action name
        description="Raccogli mele", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={}, -- items taken per unit
        products={ -- items given per unit
          ["mele"] = 1
        }
      }
    }
  },
  {
    name="Campo Luppolo", -- menu name
    permissions = {"agri.weed"}, -- you can add permissions
    r=0,g=125,b=255, -- color
    max_units=50000,
    units_per_minute=1000,
    x=2593.5219726563,y=4399.1254882813,z=40.887733459473,
    radius=20, height=4.5, -- area
    recipes = {
      ["Raccogli"] = { -- action name
        description="Raccogli luppolo", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={}, -- items taken per unit
        products={ -- items given per unit
          ["luppolo"] = 1
        }
      }
    }
  },
  {
    name="Campo Olive", -- menu name
    permissions = {"agri.weed"}, -- you can add permissions
    r=0,g=125,b=255, -- color
    max_units=50000,
    units_per_minute=1000,
    x=1560.8188476563,y=1547.8620605469,z=107.07224273682,
    radius=20, height=4.5, -- area
    recipes = {
      ["Raccogli"] = { -- action name
        description="Raccogli olive", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={}, -- items taken per unit
        products={ -- items given per unit
          ["olive"] = 1
        }
      }
    }
  },
  {
    name="Campo Grano", -- menu name
    permissions = {"agri.weed"}, -- you can add permissions
    r=0,g=125,b=255, -- color
    max_units=50000,
    units_per_minute=1000,
    x=2615.4116210938,y=4490.537109375,z=37.20458984375,
    radius=20, height=4.5, -- area
    recipes = {
      ["Raccogli"] = { -- action name
        description="Raccogli grano", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={}, -- items taken per unit
        products={ -- items given per unit
          ["grano"] = 1
        }
      }
    }
  },
  {
    name="Campo orzo", -- menu name
    permissions = {"agri.weed"}, -- you can add permissions
    r=0,g=125,b=255, -- color
    max_units=50000,
    units_per_minute=1000,
    x=2657.6906738281,y=4550.0390625,z=39.014011383057,
    radius=20, height=4.5, -- area
    recipes = {
      ["Raccogli"] = { -- action name
        description="raccogli orzo", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={}, -- items taken per unit
        products={ -- items given per unit
          ["orzo"] = 1
        }
      }
    }
  },
  {
    name="Vigna", -- menu name
    permissions = {"agri.weed"}, -- you can add permissions
    r=0,g=125,b=255, -- color
    max_units=5000,
    units_per_minute=100,
    x=-1711.0678710938,y=1956.4924316406,z=129.07405090332,
    radius=20, height=4.5, -- area
    recipes = {
      ["Raccogli"] = { -- action name
        description="Raccogli uva", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={}, -- items taken per unit
        products={ -- items given per unit
          ["uva"] = 1
        }
      }
    }
  },
  {
    name="Vigna1", -- menu name
    permissions = {"agri.weed"}, -- you can add permissions
    r=0,g=125,b=255, -- color
    max_units=5000,
    units_per_minute=100,
    x=-1808.1247558594,y=2096.4169921875,z=133.67063903809,
    radius=20, height=4.5, -- area
    recipes = {
      ["Raccogli"] = { -- action name
        description="Raccogli uva", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={}, -- items taken per unit
        products={ -- items given per unit
          ["uva"] = 1
        }
      }
    }
  },
  {
    name="Raccolta Acido idroionico", -- menu name
    permissions = {"harvest.weed"}, -- you can add permissions
    r=0,g=125,b=255, -- color
    max_units=500,
    units_per_minute=1,
    x=3537.0803222656,y=3663.1616210938,z=28.121871948242,
    radius=6, height=4.5, -- area
    recipes = {
      ["Raccogli"] = { -- action name
        description="Raccogli acido", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={}, -- items taken per unit
        products={ -- items given per unit
          ["aci"] = 1
        }
      }
    }
  },
  {
    name="Raccolta Efedrina", -- menu name
    permissions = {"harvest.weed"}, -- you can add permissions
    r=0,g=125,b=255, -- color
    max_units=500,
    units_per_minute=1,
    x=3559.173828125,y=3668.9921875,z=28.121887207031,
    radius=6, height=4.5, -- area
    recipes = {
      ["Raccogli"] = { -- action name
        description="Raccogli efedrina", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={}, -- items taken per unit
        products={ -- items given per unit
          ["acid"] = 1
        }
      }
    }
  },
  {
    name="Bosco", -- menu name
    permissions = {"taglia.weed"}, -- you can add permissions
    r=0,g=125,b=255, -- color
    max_units=50000,
    units_per_minute=1000,
    x=-879.55358886719,y=5266.4404296875,z=85.827674865723,
    radius=20, height=4.5, -- area
    recipes = {
      ["Taglia"] = { -- action name
        description="Tagli gli alberi el bosco", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={}, -- items taken per unit
        products={ -- items given per unit
          ["tronchi"] = 1
        }
      }
    }
  },
    {
    name="Pesca", -- menu name
    permissions = {"mission.delivery.fish"}, -- you can add permissions
    r=0,g=125,b=255, -- color
    max_units=500000,
    units_per_minute=1000,
    x=743.19586181641,y=3895.3967285156,z=30.5,
    radius=20, height=4.5, -- area
    recipes = {
      ["Cattura pesci"] = { -- action name
        description="Prova a pescare cefali", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={}, -- items taken per unit
        products={ -- items given per unit
          ["cefalo"] = 4
        }
      },
      ["Pesca"] = { -- action name
        description="Prova a pescare orate", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={}, -- items taken per unit
        products={ -- items given per unit
          ["orata"] = 4
        }
      }
    }
  },
  {
    name="Pesca1", -- menu name
    permissions = {"mission.delivery.fish"}, -- you can add permissions
    r=0,g=125,b=255, -- color
    max_units=500000,
    units_per_minute=1000,
    x=-2407.5844726563,y=-1822.8997802734,z=-0.28521373867989,
    radius=20, height=4.5, -- area
    recipes = {
      ["Cattura pesci"] = { -- action name
        description="Prova a pescare seppie", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={}, -- items taken per unit
        products={ -- items given per unit
          ["seppie"] = 4
        }
      },
      ["Pesca"] = { -- action name
        description="Prova a pescare calamari", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={}, -- items taken per unit
        products={ -- items given per unit
          ["calamari"] = 4
        }
      }
    }
  },
  {
    name="Pesca2", -- menu name
    permissions = {"mission.delivery.fish"}, -- you can add permissions
    r=0,g=125,b=255, -- color
    max_units=500000,
    units_per_minute=1000,
    x=-5021.7026367188,y=3237.5471191406,z=1.8756295442581,
    radius=20, height=4.5, -- area
    recipes = {
      ["Cattura pesci"] = { -- action name
        description="Prova a pescare tonni", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={}, -- items taken per unit
        products={ -- items given per unit
          ["tonno"] = 2
        }
      },
      ["Pesca"] = { -- action name
        description="Prova a pescare pesci spada", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={}, -- items taken per unit
        products={ -- items given per unit
          ["spada"] = 2
        }
      }
    }
  },
  {
    name="Pesca3", -- menu name
    permissions = {"mission.delivery.fish"}, -- you can add permissions
    r=0,g=125,b=255, -- color
    max_units=500000,
    units_per_minute=1000,
    x=2651.0673828125,y=-1774.3349609375,z=0.23611964285374,
    radius=20, height=4.5, -- area
    recipes = {
      ["Pesca gamberi"] = { -- action name
        description="Prova a pescare i gamberi", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={}, -- items taken per unit
        products={ -- items given per unit
          ["gambero"] = 4
        }
      },
      ["Pesca Polipi"] = { -- action name
        description="Prova a pescare polipi", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={}, -- items taken per unit
        products={ -- items given per unit
          ["polpo"] = 4
        }
      }
    }
  },
 
  {
    name="Industria", -- menu name
    permissions = {"imprenditore.weed"}, -- you can add permissions
    r=0,g=125,b=255, -- color
    max_units=10000000,
    units_per_minute=100,
    x=1735.2452392578,y=-1669.7935791016,z=112.58013153076,
    radius=3, height=1.5, -- area
    recipes = {
      ["Fai scatolette di tonno"] = { -- action name
        description="Ottieni delle scatole di tonno da rivendere", -- action description
        in_money=50, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={ -- items taken per unit
          ["tonno"] = 2,
		  ["ferro"] = 1
        },
        products={ -- items given per unit
          ["scatola"] = 10
        },
        aptitudes={ -- optional
          ["science.chemicals"] = 6
        }
      },
	  ["Fai Tisana alla marijuana"] = { -- action name
        description="Ottieni del a tisana rilassante alla marijuana", -- action description
        in_money=50, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={ -- items taken per unit
          ["marijuana"] = 20,
		      ["acqua"] = 3
        },
        products={ -- items given per unit
          ["tisana"] = 8
        },
        aptitudes={ -- optional
          ["science.chemicals"] = 6
        }
      },
	  ["Fai zuppa di pomodori"] = { -- action name
        description="Ottieni della zuppa", -- action description
        in_money=50, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={ -- items taken per unit
          ["pomodori"] = 50,
		  ["olive"] = 15,
		  ["acqua"] = 20
        },
        products={ -- items given per unit
          ["zuppa"] = 35
        },
        aptitudes={ -- optional
          ["science.chemicals"] = 6
        }
      },
	  ["Fai cassa di pesce"] = { -- action name
        description="Ottieni cassa", -- action description
        in_money=50, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={ -- items taken per unit
          ["cefalo"] = 20,
		  ["orata"] = 20,
		  ["seppie"] = 15,
		  ["tonno"] = 3,
		  ["tavole"] = 5,
		  ["pedana"] = 1,
		  ["spada"] = 3,
		  ["polpo"] = 15
        },
        products={ -- items given per unit
          ["casse"] = 1
        },
        aptitudes={ -- optional
          ["science.chemicals"] = 6
        }
      },
	  ["Fai involtini di pesce spada"] = { -- action name
        description="Ottieni involtini", -- action description
        in_money=50, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={ -- items taken per unit
          ["pane"] = 4,
		  ["spada"] = 2
        },
        products={ -- items given per unit
          ["involtini"] = 20
        },
        aptitudes={ -- optional
          ["science.chemicals"] = 6
        }
      },
	  ["Fai calamari ripieni"] = { -- action name
        description="Ottieni calamari ripieni", -- action description
        in_money=50, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={ -- items taken per unit
          ["calamari"] = 10,
		  ["pomodori"] = 4,
		  ["melenzane"] = 4
        },
        products={ -- items given per unit
          ["ripieni"] = 25
        },
        aptitudes={ -- optional
          ["science.chemicals"] = 6
        }
      },
	  ["Fai insalata di mare"] = { -- action name
        description="Ottieni dell'ottima insalata di mare", -- action description
        in_money=50, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={ -- items taken per unit
          ["polpo"] = 10,
		  ["calamari"] = 10,
		  ["pomodori"] = 4,
		  ["olive"] = 15,
		  ["gambero"] = 10,
		  ["seppie"] = 10
        },
        products={ -- items given per unit
          ["insalata"] = 50
        },
        aptitudes={ -- optional
          ["science.chemicals"] = 6
        }
      },
	  ["Fai paller alimentare"] = { -- action name
        description="Ottieni pallet per il trasporto merci", -- action description
        in_money=50, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={ -- items taken per unit
          ["grano"] = 20,
		  ["orzo"] = 20,
		  ["olio"] = 10,
		  ["farina"] = 10,
		  ["vino"] = 10,
		  ["uva"] = 10,
		  ["pedana"] = 2,
		  ["pane"] = 20
        },
        products={ -- items given per unit
          ["pallet1"] = 1
        },
        aptitudes={ -- optional
          ["science.chemicals"] = 6
        }
      },
	  ["Fai pallet industriale"] = { -- action name
        description="Ottieni pallet per il trasporto merci", -- action description
        in_money=50, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={ -- items taken per unit
          ["ferro"] = 10,
		  ["nitrato_di_potassio"] = 10,
		  ["zolfo"] = 10,
		  ["oro"] = 10,
		  ["carbone"] = 10,
		  ["pedana"] = 2
        },
        products={ -- items given per unit
          ["pallet2"] = 1
        },
        aptitudes={ -- optional
          ["science.chemicals"] = 6
        }
      },
	  ["Fai zuppa di aragoste"] = { -- action name
        description="Ottieni la zuppa", -- action description
        in_money=50, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={ -- items taken per unit
          ["aragosta"] = 10,
		  ["pomodori"] = 20,
		  ["acqua"] = 10
        },
        products={ -- items given per unit
          ["zuppa333"] = 1
        },
        aptitudes={ -- optional
          ["science.chemicals"] = 6
        }
      },
	  ["Fai biscotti al latte"] = { -- action name
        description="Ottieni biscotti", -- action description
        in_money=50, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={ -- items taken per unit
          ["latte"] = 20,
		  ["farina"] = 30,
		  ["olio"] = 10,
		  ["burro"] = 10,
		  ["acqua"] = 10
        },
        products={ -- items given per unit
          ["bisco"] = 1
        },
        aptitudes={ -- optional
          ["science.chemicals"] = 6
        }
      },
	  ["Fai burro"] = { -- action name
        description="Ottieni burro", -- action description
        in_money=50, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={ -- items taken per unit
          ["latte"] = 30,
        },
        products={ -- items given per unit
          ["burro"] = 1
        },
        aptitudes={ -- optional
          ["science.chemicals"] = 6
        }
      },
      ["Costruisci schede elettroniche"] = { -- action name
          description="Costrusci mobili con le tavole", -- action description
          in_money=50, -- money taken per unit
          out_money=0, -- money earned per unit
          reagents={ -- items taken per unit
          ["ferro"] = 10,
		  ["acidoacetilsalicilico"] = 10,
		  ["oro"] = 10
          },
          products={ -- items given per unit
            ["schede"] = 10
          },
          aptitudes={ -- optional
            ["science.chemicals"] = 6
          }
        }
    }
  },
      {
    name="Medical Transport", -- menu name
    permissions = {"mission.delivery.medical"}, -- you can add permissions
    r=0,g=125,b=255, -- color
    max_units=200000,
    units_per_minute=1000,
    x=237.81951904297,y=-1359.5804443359,z=39.534374237061,
    radius=3, height=1.5, -- area
    recipes = {
      ["Prendi"] = { -- action name
        description="Ottieni gli stumenti", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={}, -- items taken per unit
        products={ -- items given per unit
          ["strumenti_medici"] = 1
        }
      }
	 }
  },
   {
    name="Fermentazione prodotti alcolici", -- menu name
	permissions = {"clande.permi"}, -- job drug dealer required to use
    r=0,g=255,b=0, -- color
    max_units=100000,
    units_per_minute=1,
    x=440.79925537109,y=6458.0639648438,z=35.864948272705, -- pos (needed for public use lab tools
    radius=3.1, height=1.5, -- area
    recipes = {
      ["Fai whisky grezzo"] = { -- action name
        description="Inizia la fermentazione", -- action description
        in_money=150, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={ -- items taken per unit
          ["orzo"] = 5,
		  ["alcool"] = 10,
		  ["grano"] = 5
        },
        products={ -- items given per unit
          ["whiskyg"] = 1
        },
        aptitudes={ -- optional
         ["science.chemicals"] = 6
        }
       },
	   ["Fai Grappa grezza"] = { -- action name
        description="Inizia la fermentazione", -- action description
        in_money=150, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={ -- items taken per unit
          ["mele"] = 5,
		  ["uva"] = 5,
		  ["alcool"] = 10
        },
        products={ -- items given per unit
          ["grappag"] = 1
        },
        aptitudes={ -- optional
         ["science.chemicals"] = 6
        }
       }
      }
     },
	 {
    name="Distillazione prodotti alcolici", -- menu name
	permissions = {"clande.permi"}, -- job drug dealer required to use
    r=0,g=255,b=0, -- color
    max_units=100000,
    units_per_minute=1,
    x=1442.7677001953,y=6332.2783203125,z=23.981897354126, -- pos (needed for public use lab tools
    radius=2.1, height=1.5, -- area
    recipes = {
      ["Fai whisky"] = { -- action name
        description="inizia a distillare", -- action description
        in_money=250, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={ -- items taken per unit
          ["whiskyg"] = 2
        },
        products={ -- items given per unit
          ["whisky"] = 1
        },
        aptitudes={ -- optional
         ["science.chemicals"] = 6
        }
       },
	   ["Fai Grappa"] = { -- action name
        description="Inizia la distillazione", -- action description
        in_money=250, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={ -- items taken per unit
          ["grappag"] = 2
        },
        products={ -- items given per unit
          ["grappa"] = 1
        },
        aptitudes={ -- optional
         ["science.chemicals"] = 6
        }
       },
	   ["Fai pallet whisky"] = { -- action name
        description="Imbaballa", -- action description
        in_money=550, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={ -- items taken per unit
		  ["pedana"] = 2,
          ["whisky"] = 40
        },
        products={ -- items given per unit
          ["pallet3"] = 1
        },
        aptitudes={ -- optional
         ["science.chemicals"] = 6
        }
       },
	   ["Fai pallet Grappa"] = { -- action name
        description="Imballa", -- action description
        in_money=550, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={ -- items taken per unit
		  ["pedana"] = 2,
          ["grappa"] = 40
        },
        products={ -- items given per unit
          ["pallet4"] = 1
        },
        aptitudes={ -- optional
         ["science.chemicals"] = 6
        }
       }
      }
     },
	  {
    name="Laboratorio Metanfetamina", -- menu name
	permissions = {"harvest.weeeeeed"}, -- job drug dealer required to use
    r=0,g=255,b=0, -- color1005.8500366211,-3200.3237304688,-38.519317626953
    max_units=500,
    units_per_minute=1,
    x=1005.8500366211,y=-3200.3237304688,z=-38.519317626953, -- pos (needed for public use lab tools
    radius=1.1, height=1.5, -- area
    recipes = {
	 ["Fai met grezza"] = { -- action name
        description="Fai met grezza", -- action description
        in_money=350, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={ -- items taken per unit
          ["aci"] = 6,
		  ["acid"] = 6
        },
        products={ -- items given per unit
          ["me"] = 1
        },
        aptitudes={ -- optional
         ["science.chemicals"] = 6
        }
       },
      ["Fai Met"] = { -- action name
        description="Fai Met Pura", -- action description
        in_money=200, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={ -- items taken per unit
          ["me"] = 2
        },
        products={ -- items given per unit
          ["met"] = 1
        },
        aptitudes={ -- optional
         ["science.chemicals"] = 6
        }
       }
      }
     },
  {
    name="Oleificio", -- menu name
	permissions = {"agri.weed"}, -- job drug dealer required to use
    r=0,g=255,b=0, -- color
    max_units=100000,
    units_per_minute=1,
    x=-82.199295043945,y=6496.7924804688,z=31.490894317627, -- pos (needed for public use lab tools
    radius=3.1, height=1.5, -- area
    recipes = {
      ["Fai Olio"] = { -- action name
        description="Macina le olive per ottenere olio", -- action description
        in_money=50, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={ -- items taken per unit
          ["olive"] = 4
        },
        products={ -- items given per unit
          ["olio"] = 2
        },
        aptitudes={ -- optional
         ["science.chemicals"] = 6
        }
       }
      }
     },
  {
    name="Birrificio", -- menu name
	permissions = {"agri.weed"}, -- job drug dealer required to use
    r=0,g=255,b=0, -- color
    max_units=100000,
    units_per_minute=1,
    x=854.39099121094,y=-1830.2412109375,z=29.11234664917, -- pos (needed for public use lab tools
    radius=3.1, height=1.5, -- area
    recipes = {
      ["Fai Birra"] = { -- action name
        description="usa il luppolo l'acqua e l'orzo per ottenere birra", -- action description
        in_money=50, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={ -- items taken per unit
          ["luppolo"] = 4,
		  ["orzo"] = 6,
		  ["acqua"] = 2
        },
        products={ -- items given per unit
          ["birra"] = 2
        },
        aptitudes={ -- optional
         ["science.chemicals"] = 6
        }
       }
      }
     },
  {
    name="Palmento", -- menu name
	permissions = {"agri.weed"}, -- job drug dealer required to use
    r=0,g=255,b=0, -- color
    max_units=100000,
    units_per_minute=1,
    x=163.9909362793,y=2284.8041992188,z=93.821411132813, -- pos (needed for public use lab tools
    radius=3.1, height=1.5, -- area
    recipes = {
      ["Fai il vino"] = { -- action name
        description="Usa l'uva per ottebere del buonissimo nero d'avola, occhio alla fermentazione", -- action description
        in_money=50, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={ -- items taken per unit
          ["uva"] = 4
        },
        products={ -- items given per unit
          ["vino"] = 2
        },
        aptitudes={ -- optional
         ["science.chemicals"] = 6
        }
       }
      }
     },
  {
    name="Mulino", -- menu name
	permissions = {"agri.weed"}, -- job drug dealer required to use
    r=0,g=255,b=0, -- color
    max_units=100000,
    units_per_minute=1,
    x=2892.8220214844,y=4381.6010742188,z=50.353462219238, -- pos (needed for public use lab tools
    radius=3.1, height=1.5, -- area
    recipes = {
      ["Fai farina"] = { -- action name
        description="Usa il grano per ottenere farina", -- action description
        in_money=50, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={ -- items taken per unit
          ["grano"] = 4
        },
        products={ -- items given per unit
          ["farina"] = 2
        },
        aptitudes={ -- optional
         ["science.chemicals"] = 6
        }
       },
	   ["Fai Pane"] = { -- action name
          description="Ottieni il pane dall'acqua e dalla farina", -- action description
          in_money=50, -- money taken per unit
          out_money=0, -- money earned per unit
          reagents={ -- items taken per unit
          ["farina"] = 4,
		  ["acqua"] = 2
          },
          products={ -- items given per unit
            ["pane"] = 3
          },
          aptitudes={ -- optional
            ["science.chemicals"] = 6
          }
        }
      }
     },
{
    name="Falegnameria", -- menu name
    permissions = {"taglia.weed"}, -- you can add permissions
    r=0,g=125,b=255, -- color
    max_units=10000000,
    units_per_minute=100,
    x=-560.873046875,y=5318.7509765625,z=73.599685668945,
    radius=3, height=1.5, -- area
    recipes = {
      ["Fai tavole"] = { -- action name
        description="Ottieni delle tavole per fare mobili", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={ -- items taken per unit
          ["tronchi"] = 1
        },
        products={ -- items given per unit
          ["tavole"] = 40
        },
        aptitudes={ -- optional
          ["science.chemicals"] = 6
        }
      },
	  ["Fai pellet"] = { -- action name
        description="Ottieni del pellet per il riscaldamento", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={ -- items taken per unit
          ["tronchi"] = 1
        },
        products={ -- items given per unit
          ["pellet"] = 80
        },
        aptitudes={ -- optional
          ["science.chemicals"] = 6
        }
      },
	  ["Fai pedane"] = { -- action name
        description="Ottieni pedane per il trasporto merci", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={ -- items taken per unit
          ["tavole"] = 40
        },
        products={ -- items given per unit
          ["pedana"] = 20
        },
        aptitudes={ -- optional
          ["science.chemicals"] = 6
        }
      },
      ["Costruisci mobili"] = { -- action name
          description="Costrusci mobili con le tavole", -- action description
          in_money=0, -- money taken per unit
          out_money=0, -- money earned per unit
          reagents={ -- items taken per unit
          ["tavole"] = 40
          },
          products={ -- items given per unit
            ["mobili"] = 6
          },
          aptitudes={ -- optional
            ["science.chemicals"] = 6
          }
        }
    }
  },
  {
    name="Farmacia", -- menu name
	permissions = {"emergencypresi.pills"}, -- job drug dealer required to use
    r=0,g=255,b=0, -- color
    max_units=100000,
    units_per_minute=1,
    x=583.17932128906,y=-3112.0764160156,z=6.0692577362061, -- pos (needed for public use lab tools 583.17932128906,-3112.0764160156,6.0692577362061
    radius=3.1, height=1.5, -- area
    recipes = {
      ["Fai pillole"] = { -- action name
        description="Mischia le due sostanze per fare le pillole", -- action description
        in_money=50, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={ -- items taken per unit
          ["morfina"] = 5,
		  ["acidoacetilsalicilico"] = 12
        },
        products={ -- items given per unit
          ["pills"] = 1
        },
        aptitudes={ -- optional
         ["science.chemicals"] = 6
        }
       }
      }
     },
	 {
    name="Raccolta morfina", -- menu name
    permissions = {"emergency.pills"}, -- you can add permissions   2660.4301757813,1469.4583740234,24.500772476196
    r=0,g=125,b=255, -- color
    max_units=200000,
    units_per_minute=1000,
    x=2660.4301757813,y=1469.4583740234,z=24.500772476196,
    radius=3, height=1.5, -- area
    recipes = {
      ["Prendi"] = { -- action name
        description="Ottieni morfina", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={}, -- items taken per unit
        products={ -- items given per unit
          ["morfina"] = 1
        }
      }
	 }
  },
  {
    name="Fabrica acido acetilsalicilico", -- menu name
    permissions = {"emergency.pills"}, -- you can add permissions    1690.5385742188,-1671.6882324219,111.52456665039
    r=0,g=125,b=255, -- color
    max_units=200000,
    units_per_minute=1000,
    x=1690.5385742188,y=-1671.6882324219,z=111.52456665039,
    radius=3, height=1.5, -- area
    recipes = {
      ["Prendi"] = { -- action name
        description="Ottieni Acido", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={}, -- items taken per unit
        products={ -- items given per unit
          ["acidoacetilsalicilico"] = 1
        }
      }
	 }
  },
  {
    name="Hacker", -- menu name
	permissions = {"hacker.credit_cards"}, -- you can add permissions
    r=255,g=125,b=0, -- color
    max_units=5,
    units_per_minute=1,
    x=1275.5639648438,y=-1710.5926513672,z=54.771453857422,
    radius=2, height=1.0, -- area
    recipes = {
      ["hacking"] = { -- action name
        description="Hacking credit cards.", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={}, -- items taken per unit
        products={
		["credit"] = 1,
		["dirty_money"] = 555
		}, -- items given per unit
        aptitudes={ -- optional
          ["hacker.hacking"] = 0.8 -- "group.aptitude", give 1 exp per unit
        }
      }
    }
  },
  {
    name="Patente", -- menu name
	permissions = {"police.jail"}, -- you can add permissions
    r=255,g=125,b=0, -- color
    max_units=100000,
    units_per_minute=300,
    x=-447.53091430664,y=6007.7348632813,z=31.716375350952,
    radius=0.5, height=1.0, -- area
    recipes = {
      ["Patente"] = { -- action name
        description="Rilascio patente.", -- action description`
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={}, -- items taken per unit
        products={
		["driver"] = 1
		}, -- items given per unit
        aptitudes={} -- optional
      }
    }
  },
  {
    name="Patente", -- menu name
	permissions = {"police.jail"}, -- you can add permissions
    r=255,g=125,b=0, -- color
    max_units=100000,
    units_per_minute=300,
    x=437.59295654297,y=-994.62463378906,z=30.689603805542,   
    radius=0.5, height=1.0, -- area
    recipes = {
      ["Patente"] = { -- action name
        description="Rilascio patente.", -- action description`
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={}, -- items taken per unit
        products={
		["driver"] = 1
		}, -- items given per unit
        aptitudes={} -- optional
      }
    }
  },
  {
    name="Rilascio porto d'armi", -- menu name
	permissions = {"police.porto"}, -- you can add permissions
    r=255,g=125,b=0, -- color
    max_units=1000000,
    units_per_minute=300,
    x=451.5690612793,y=-973.17163085938,z=30.689598083496,
    radius=0.5, height=1.0, -- area
    recipes = {
      ["Ritira il porto d'armi"] = { -- action name
       description="Prendi il porto d'armi.", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={}, -- items taken per unit
        products={
		["licenza_armi"] = 1
		}, -- items given per unit
        aptitudes={} -- optional
      }
    }
  },
  {
    name="Rilascio porto d'armi", -- menu name
	permissions = {"police.porto"}, -- you can add permissions
    r=255,g=125,b=0, -- color
    max_units=1000000,
    units_per_minute=300,
    x=-449.98257446289,y=6010.498046875,z=31.716375350952,
    radius=0.5, height=1.0, -- area
    recipes = {
      ["Ritira il porto d'armi"] = { -- action name
       description="Prendi il porto d'armi.", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={}, -- items taken per unit
        products={
		["licenza_armi"] = 1
		}, -- items given per unit
        aptitudes={} -- optional
      }
    }
  },
  {
    name="Missione portavalori", -- menu name
	permissions = {"bankdriver.money"}, -- you can add permissions
    r=255,g=125,b=0, -- color
    max_units=300,
    units_per_minute=1,
    x=236.87298583984,y=217.09535217285,z=106.28678894043,
    radius=2, height=1.0, -- area
    recipes = {
      ["Ritira il denaro"] = { -- action name
       description="Prendi i soldi per la missione.", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={}, -- items taken per unit
        products={
		["bank_money"] = 500000
		}, -- items given per unit
        aptitudes={} -- optional
      }
    }
  },
 {
    name="Fabbrica polvere da sparo", -- menu name
	permissions = {"harvestzolfo.weed"}, -- job drug dealer required to use
    r=0,g=255,b=0, -- color
    max_units=100000,
    units_per_minute=300,
    x=-1594.7272949219,y=5208.349609375,z=4.3101463317871, -- pos (needed for public use lab tools-1594.7272949219,5208.349609375,4.3101463317871)
    radius=8.1, height=1.5, -- area
    recipes = {
      ["polvere_nera"] = { -- action name
        description="fai polvere da sparo", -- action description
        in_money=50, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={ -- items taken per unit
          ["nitrato_di_potassio"] = 6,
		  ["carbone"] = 2,
		  ["zolfo"] = 2
        },
        products={ -- items given per unit
          ["polvere_nera"] = 1
        },
        aptitudes={ -- optional
         ["science.chemicals"] = 6
        }
       }
      }
     },
{
    name="Laboratorio Gioielli [IMPRENDITORE]", -- menu name
	permissions = {"imprenditore.weed"}, -- job drug dealer required to use
    r=0,g=255,b=0, -- color
    max_units=20000,
    units_per_minute=3000,
    x=-630.74664306641,y=-229.65124511719,z=38.057060241699, -- pos (needed for public use lab tool-630.74664306641,-229.65124511719,38.057060241699)
    radius=1.1, height=1.5, -- area
    recipes = {
      ["Collana_di_perle"] = { -- action name
        description="fai collana di perle", -- action description
        in_money=100, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={ -- items taken per unit
          ["perla"] = 5,
		  ["oro"] = 2
        },
        products={ -- items given per unit
          ["collana_perle"] = 1
        },
        aptitudes={ -- optional
          ["science.chemicals"] = 6
        }
      },
      ["Anello"] = { -- action name
        description="fai anelli", -- action description
        in_money=150, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={ -- items taken per unit
          ["smeraldo"] = 2,
		  ["ametista"] = 3,
		  ["oro"] = 5,
		  ["zaffiro"] = 3
        },
        products={ -- items given per unit
          ["anello"] = 1
        },
        aptitudes={ -- optional
          ["science.chemicals"] = 6
        }
      },
      ["Gemelli"] = { -- action name
        description="Fai orecchini", -- action description
        in_money=150, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={ -- items taken per unit
		   ["oro"] = 5,
		   ["ametista"] = 3,			 
		   ["zaffiro"] = 3
        },
        products={ -- items given per unit
          ["gemelli"] = 1
        },
        aptitudes={ -- optional
          ["science.chemicals"] = 6
        }
      },		  
      ["Orologio"] = { -- action name
        description="Fai orecchini", -- action description
        in_money=150, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={ -- items taken per unit
		   ["oro"] = 5,
		   ["ferro"] = 5,
		   ["smeraldo"] = 3,			 
		   ["zaffiro"] = 3
        },
        products={ -- items given per unit
          ["orologio"] = 1
        },
        aptitudes={ -- optional
          ["science.chemicals"] = 6
        }
      },		  
      ["Collana con Ciondolo"] = { -- action name
        description="Fai collana con ciondolo", -- action description
        in_money=150, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={ -- items taken per unit
		   ["oro"] = 7,
		   ["smeraldo"] = 3,			 
		   ["zaffiro"] = 3
        },
        products={ -- items given per unit
          ["collana_c"] = 1
        },
        aptitudes={ -- optional
          ["science.chemicals"] = 6
        }
      },			  
      ["Bracciale"] = { -- action name
          description="Fai bracciale", -- action description
          in_money=180, -- money taken per unit
          out_money=0, -- money earned per unit
          reagents={ -- items taken per unit
		   ["ametista"] = 3,
		   ["oro"] = 7,
		   ["zaffiro"] = 3
          },
          products={ -- items given per unit
            ["bracciale"] = 1
          },
          aptitudes={ -- optional
            ["science.chemicals"] = 6
          }
        }
		}
      },
-----------------------------------------------------------COCAINA----------------------------------------------------------	  
 {
    name="Raccolta benzoilmetilecgonina", -- menu name
    permissions = {"mafia.punti"}, -- you can add permissions   2660.4301757813,1469.4583740234,24.500772476196
    r=0,g=125,b=255, -- color
    max_units=5000,
    units_per_minute=1000,
    x=488.01318359375,y=-2207.4711914062,z=5.918273448944,	
    radius=3, height=1.5, -- area
    recipes = {
      ["Raccogli"] = { -- action name
        description="Raccolta benzoilmetilecgonina", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={}, -- items taken per unit
        products={ -- items given per unit
          ["benzoilmetilecgonina"] = 1
        }
      }
	 }
  },	    
 {
    name="Processo cocaina", -- menu name
    permissions = {"mafia.punti"}, -- you can add permissions   2660.4301757813,1469.4583740234,24.500772476196
    r=0,g=125,b=255, -- color
    max_units=5000,
    units_per_minute=1000,
    x=142.513961792,y=-3213.1723632812,z=5.8575916290284,	
    radius=3, height=1.5, -- area
    recipes = {
      ["Processa"] = { -- action name
        description="Processo cocaina", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={
          ["marijuana"] = 1,    
		      ["benzoilmetilecgonina"] = 1  }, -- items taken per unit
        products={ -- items given per unit
          ["cocaina"] = 1
        }
      },
      ["Processa Coca Mix"] = { -- action name
      description="Processo Coca Mix",
      in_money=0, -- money taken per unit
      out_money=0, -- money earned per unit
      reagents={
          ["cocaina_tag"] = 1,
          ["metadone_easy"] = 1
        },
      products={
          ["cocaina_mix"] = 1
        }
      }
	 }
  },	  
 {
    name="Taglio cocaina", -- menu name
    --permissions = {"mafia.punti"},
    r=0,g=125,b=255, -- color
    max_units=5000,
    units_per_minute=60,
    x=2729.5131835938,y=1598.7766113282,z=24.50094795227,		
    radius=3, height=1.5, -- area
    recipes = {
      ["Taglio"] = { -- action name
        description="Taglia cocaina", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={          
		      ["cocaina"] = 1  }, -- items taken per unit
        products={ -- items given per unit
          ["cocaina_tag"] = 2
        }
      }
	 }
  },	  
 {
    name="Vendita cocaina", -- menu name
    r=0,g=125,b=255, -- color
    max_units=5000,
    units_per_minute=60,
    x=-2313.2319335938,y=260.81997680664,z=169.60206604004,		
    radius=3, height=1.5, -- area
    recipes = {
      ["Vendi Cocaina"] = { -- action name
        description="Vendita cocaina", -- action description
        in_money=0, -- money taken per unit
        out_money=0, -- money earned per unit
	--	sporchi_money=250,
        reagents={
		  ["cocaina_tag"] = 1  }, -- items taken per unit
        products={ 
		  ["dirty_money"] = 175
		  }
    },
    ["Vendi Cocaina Mix"] = { -- action name
    description="Vendita cocaina", -- action description
    in_money=0, -- money taken per unit
    out_money=0, -- money earned per unit
--	sporchi_money=250,
    reagents={
      ["cocaina_mix"] = 1  }, -- items taken per unit
    products={ 
      ["dirty_money"] = 400
      }
    },
    ["Vendi Cocaina Super"] = { -- action name
    description="Vendita cocaina", -- action description
    in_money=0, -- money taken per unit
    out_money=0, -- money earned per unit
--	sporchi_money=250,
    reagents={
      ["cocaina_super"] = 1  }, -- items taken per unit
    products={ 
      ["dirty_money"] = 1000
     }
     }
	 }
  },		  
  
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
	  
  {
    name="Laboratorio Droghe", -- menu name
    r=0,g=255,b=0, -- color
    max_units=800,
    units_per_minute=3,
    x=-78.729629516602,y=-1395.4854736328,z=29.359991073608, -- pos (needed for public use lab tools)
    radius=1.1, height=1.5, -- area
    recipes = {
      ["cocaina"] = { -- action name
        description="fai cocaina", -- action description
        in_money=250, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={ -- items taken per unit
          ["benzoilmetilecgonina"] = 4
        },
        products={ -- items given per unit
          ["cocaina"] = 1
        },
        aptitudes={ -- optional
          ["laboratory.cocaina"] = 3, -- "group.aptitude", give 1 exp per unit
          ["science.chemicals"] = 6
        }
      },
      ["marijuana"] = { -- action name
        description="fai marijuana", -- action description
        in_money=100, -- money taken per unit
        out_money=0, -- money earned per unit
        reagents={ -- items taken per unit
          ["cimetta"] = 4
        },
        products={ -- items given per unit
          ["marijuana"] = 2
        },
        aptitudes={ -- optional
          ["laboratory.marijuana"] = 3, -- "group.aptitude", give 1 exp per unit
          ["science.chemicals"] = 6
          }
        }
      }
    }
}
cfg.hidden_transformers = {
  ["Campo Marijuana"] = {
    def = {
      name="Campo di marijuana", -- menu name
      r=0,g=200,b=0, -- color
      max_units=100000,
      units_per_minute=20000,
      x=0,y=0,z=0, -- pos
      radius=3, height=1.5, -- area
      recipes = {
        ["Raccogli"] = { -- action name
          description="Raccolta marijuana.", -- action description
          in_money=0, -- money taken per unit
          out_money=0, -- money earned per unit
          reagents={}, -- items taken per unit
          products={ -- items given per unit
            ["cimetta"] = 1
          }
        }
      }
    },
    positions = {
      {2223.4643554688,5576.6030273438,53.812492370605}	  
    }
  },  

  ["Miniera di Ferro"] = {
    def = {
      name="Miniera", -- menu name
      permissions = {"harvestzolfo.weed"}, -- you can add permissions
      r=0,g=200,b=0, -- color
      max_units=10000,
      units_per_minute=40,
      x=0,y=0,z=0, -- pos
      radius=5, height=1.5, -- area
      recipes = {
        ["Raccogli"] = { -- action name
          description="Raccolto ferro.", -- action description
          in_money=0, -- money taken per unit
          out_money=0, -- money earned per unit
          reagents={}, -- items taken per unit
          products={ -- items given per unit
            ["ferro"] = 1
          }
        }
      }
    },
    positions = {
      {-423.95166015625,2064.6076660156,120.04020690918}
    }
  },
  ["Miniera di Rame"] = {
    def = {
      name="Miniera", -- menu name
      permissions = {"harvestzolfo.weed"}, -- you can add permissions
      r=0,g=200,b=0, -- color
      max_units=10000,
      units_per_minute=40,
      x=0,y=0,z=0, -- pos
      radius=5, height=1.5, -- area
      recipes = {
        ["Raccogli"] = { -- action name
          description="Raccolto rame.", -- action description
          in_money=0, -- money taken per unit
          out_money=0, -- money earned per unit
          reagents={}, -- items taken per unit
          products={ -- items given per unit
            ["rame"] = 1
          }
        }
      }
    },
    positions = {
      {-442.9252319336,2014.2115478516,123.54566192626}
    }
  },  
  ["Miniera di Zolfo"] = {
    def = {
      name="Miniera", -- menu name
      permissions = {"harvestzolfo.weed"}, -- you can add permissions	  
      r=0,g=200,b=0, -- color
      max_units=10000,
      units_per_minute=60,
      x=0,y=0,z=0, -- pos
      radius=5, height=1.5, -- area
      recipes = {
        ["Raccogli"] = { -- action name
          description="Raccolto zolfo.", -- action description
          in_money=0, -- money taken per unit
          out_money=0, -- money earned per unit
          reagents={}, -- items taken per unit
          products={ -- items given per unit
            ["zolfo"] = 1
          }
        }
      }
    },
    positions = {
      {-486.02743530273,1894.6463623047,119.99822998047}
    }
  },
  ["Miniera di Potassio"] = {
    def = {
      name="Miniera", -- menu name
      permissions = {"harvestzolfo.weed"}, -- you can add permissions	  	  
      r=0,g=200,b=0, -- color
      max_units=10000,
      units_per_minute=30,
      x=0,y=0,z=0, -- pos
      radius=5, height=1.5, -- area
      recipes = {
        ["Raccogli"] = { -- action name
          description="Raccolto nitrato di potassio.", -- action description
          in_money=0, -- money taken per unit
          out_money=0, -- money earned per unit
          reagents={}, -- items taken per unit
          products={ -- items given per unit
            ["nitrato_di_potassio"] = 1
          }
        }
      }
    },
    positions = {
      {-472.64715576172,2089.2072753906,120.06805419922}
    }
  },
  ["Miniera di Carbone"] = {
    def = {
      name="Miniera", -- menu name
      permissions = {"harvestzolfo.weed"}, -- you can add permissions	  	  
      r=0,g=200,b=0, -- color
      max_units=10000,
      units_per_minute=300,
      x=0,y=0,z=0, -- pos
      radius=5, height=1.5, -- area
      recipes = {
        ["Raccogli"] = { -- action name
          description="Raccolto carbone.", -- action description
          in_money=0, -- money taken per unit
          out_money=0, -- money earned per unit
          reagents={}, -- items taken per unit
          products={ -- items given per unit
            ["carbone"] = 1
          }
        }
      }
    },
	positions = {
      {-559.85430908203,1889.0947265625,123.06143951416}
    }
  },
	["Giacimenti di oro"] = {
    def = {
      name="Miniera", -- menu name
      r=0,g=200,b=0, -- color
      max_units=10000,
      units_per_minute=20,
      x=0,y=0,z=0, -- pos
      radius=5, height=1.5, -- area
      recipes = {
        ["Raccogli"] = { -- action name
          description="Raccolto Oro.", -- action description
          in_money=0, -- money taken per unit
          out_money=0, -- money earned per unit
          reagents={}, -- items taken per unit
          products={ -- items given per unit
            ["oro"] = 1
          }
        }
      }
    },
	positions = {
      {-740.833984375,2880.4487304688,14.212715148926}
    }
  },
	["Giacimenti di oro"] = {
    def = {
      name="Miniera", -- menu name
      r=0,g=200,b=0, -- color
      max_units=10000,
      units_per_minute=20,
      x=0,y=0,z=0, -- pos
      radius=5, height=1.5, -- area
      recipes = {
        ["Raccogli"] = { -- action name
          description="Raccolto Oro.", -- action description
          in_money=0, -- money taken per unit
          out_money=0, -- money earned per unit
          reagents={}, -- items taken per unit
          products={ -- items given per unit
            ["oro"] = 1
          }
        }
      }
    },
	positions = {
      {-1387.8432617188,2604.5061035156,1.6506723165512}
    }
  },
	["Giacimenti di oro"] = {
    def = {
      name="Miniera", -- menu name
      r=0,g=200,b=0, -- color
      max_units=10000,
      units_per_minute=20,
      x=0,y=0,z=0, -- pos
      radius=5, height=1.5, -- area
      recipes = {
        ["Raccogli"] = { -- action name
          description="Raccolto oro.", -- action description
          in_money=0, -- money taken per unit
          out_money=0, -- money earned per unit
          reagents={}, -- items taken per unit
          products={ -- items given per unit
            ["oro"] = 1
          }
        }
      }
    },
	positions = {
      {-585.53228759766,4402.7944335938,15.897897720337}
    }
  },
	["Giacimento Ametiste"] = {
    def = {
      name="Giacimento", -- menu name
      r=0,g=200,b=0, -- color
      max_units=10000,
      units_per_minute=20,
      x=0,y=0,z=0, -- pos
      radius=5, height=1.5, -- area
      recipes = {
        ["Raccogli"] = { -- action name
          description="Raccolte ametiste.", -- action description
          in_money=0, -- money taken per unit
          out_money=0, -- money earned per unit
          reagents={}, -- items taken per unit
          products={ -- items given per unit
            ["ametista"] = 1
          }
        }
      }
    },
	positions = {
      {-767.10687255859,4313.3833007813,148.4880065918}
    }
  },
	["Giacimento Smeraldi"] = {
    def = {
      name="Giacimento", -- menu name
      r=0,g=200,b=0, -- color
      max_units=10000,
      units_per_minute=20,
      x=0,y=0,z=0, -- pos
      radius=5, height=1.5, -- area
      recipes = {
        ["Raccogli"] = { -- action name
          description="Raccolto smeraldo.", -- action description
          in_money=0, -- money taken per unit
          out_money=0, -- money earned per unit
          reagents={}, -- items taken per unit
          products={ -- items given per unit
            ["smeraldo"] = 1
          }
        }
      }
    },
	positions = {
      {3337.6901855469,-52.814640045166,2.063051700592}
    }
  },
	["Giacimento Zaffiri"] = {
    def = {
      name="Giacimento", -- menu name
      r=0,g=200,b=0, -- color
      max_units=10000,
      units_per_minute=20,
      x=0,y=0,z=0, -- pos
      radius=5, height=1.5, -- area
      recipes = {
        ["Raccogli"] = { -- action name
          description="Raccolto zaffiro.", -- action description
          in_money=0, -- money taken per unit
          out_money=0, -- money earned per unit
          reagents={}, -- items taken per unit
          products={ -- items given per unit
            ["zaffiro"] = 1
          }
        }
      }
    },
    positions = {
      {2878.9299316406,6296.1665039063,63.438846588135}
    }
  },
  ["Giacimento Perle"] = {
    def = {
      name="Giacimento", -- menu name
      r=0,g=200,b=0, -- color
      max_units=10000,
      units_per_minute=20,
      x=0,y=0,z=0, -- pos
      radius=5, height=1.5, -- area
      recipes = {
        ["Raccogli"] = { -- action name
          description="Raccolte perle.", -- action description
          in_money=0, -- money taken per unit
          out_money=0, -- money earned per unit
          reagents={}, -- items taken per unit
          products={ -- items given per unit
            ["perla"] = 1
          }
        }
      }
    },
    positions = {
      {2776.7790527344,-533.70086669922,0.47092932462692}
    }
  },
  --MINATORE
  
     ["Vendita Materiali Minatore"] = {
    def = {
      name="Vendita Materiali", -- menu name
      r=0,g=200,b=0, -- color
      max_units=10000,
      units_per_minute=90,
      x=0,y=0,z=0, -- pos
      radius=5, height=1.5, -- area
      recipes = {
        ["Vendi Ferro"] = { -- action name
          description="Vendita.", -- action description
          in_money=0, -- money taken per unit
          out_money=15, -- money earned per unit
          reagents={
		      ["ferro"] = 1     }, 
          products={ }
        },
        ["Vendi Zolfo"] = { -- action name
          description="Vendita.", -- action description
          in_money=0, -- money taken per unit
          out_money=15, -- money earned per unit
          reagents={
		      ["zolfo"] = 1     }, 
          products={ }
        },
        ["Vendi Nitrato"] = { -- action name
          description="Vendita.", -- action description
          in_money=0, -- money taken per unit
          out_money=15, -- money earned per unit
          reagents={
		      ["nitrato_di_potassio"] = 1     }, 
          products={ }
        },
        ["Vendi Carbone"] = { -- action name
          description="Vendita.", -- action description
          in_money=0, -- money taken per unit
          out_money=15, -- money earned per unit
          reagents={
		      ["carbone"] = 1     }, 
          products={ }
        },
      }
    },
    positions = {
      {1239.9182128906,-3172.9714355468,7.1048564910888}
    }
  },    
  
  
  
  ["Raccolta cristalli"] = {
    def = {
      name="Raccolta cristalli", -- menu name
      permissions = {"harvest.weed"}, -- you can add permissions
      r=0,g=200,b=0, -- color
      max_units=600,
      units_per_minute=60,
      x=0,y=0,z=0, -- pos
      radius=5, height=1.5, -- area
      recipes = {
        ["Raccogli"] = { -- action name
          description="Raccogli cristalli.", -- action description
          in_money=0, -- money taken per unit
          out_money=0, -- money earned per unit
          reagents={}, -- items taken per unit
          products={ -- items given per unit
      	  ["cristalli"] = 1
          }
        }
      }
    },
    positions = {
      {1992.5993652344,3044.1806640625,47.215068817139}
    }
  },
  
  
  
  
}

-- time in minutes before hidden transformers are relocated (min is 5 minutes)
cfg.hidden_transformer_duration = 5*24*60 -- 5 days

-- configure the information reseller (can sell hidden transformers positions)
cfg.informer = {
  infos = {
    ["weed field"] = 20000
  },
  positions = {
  },
  interval = 60, -- interval in minutes for the reseller respawn
  duration = 10, -- duration in minutes of the spawned reseller
  blipid = 133,
  blipcolor = 2
}

return cfg