
local cfg = {}

-- define each group with a set of permissions
-- _config property:
--- gtype (optional): used to have only one group with the same gtype per player (example: a job gtype to only have one job)
--- onspawn (optional): function(player) (called when the player spawn with the group)
--- onjoin (optional): function(player) (called when the player join the group)
--- onleave (optional): function(player) (called when the player leave the group)
--- (you have direct access to vRP and vRPclient, the tunnel to client, in the config callbacks)

cfg.groups = {
  ["superadmin"] = {
    _config = {onspawn = function(player) vRPclient.notify(player,{"Sei superadmin."}) end},
    "player.group.add",
    "player.group.remove",
    "player.giveitem",
    "admin.overlay",
    "player.group.add",
    "player.group.remove",
	  "spawn.veh",
    "player.list",
    "player.whitelist",
    "player.unwhitelist",
    "player.kick",
    "player.ban",
    "player.unban",
    "player.noclip",
    "player.custom_emote",
    "player.custom_sound",
    "player.display_custom",
    "player.coords",
    "player.tptome",
    "player.tpto",
    "admin.spawnveh",
    "player.givemoney",
   -- "emergency.revive",
    "emergency.shop",
    "admin.godmode",
  },
  ["admin"] = {
  "admin.tickets",
  "admin.announce",
	"player.group.add",
  "player.group.remove",
  "player.list",
  "player.whitelist",
  "player.unwhitelist",
	--"player.givemoney",
  "player.kick",
	"spawn.veh",
  "player.ban",
  "player.unban",
  "player.noclip",
    --"player.custom_emote",
    --"player.custom_sound",
    --"player.display_custom",
  "player.coords",
  "player.tptome",
	--"emergency.revive",
	"emergency.shop",
  "player.tpto"
  },
  ["recruiter"] = {
    "player.list",
	"admin.tickets",
	"admin.tickets",
	"player.noclip",
	"player.ban",
	"player.unban",
	"player.kick",
	"player.tptome",
	"player.tpto",
	"player.group.add",
    "player.group.remove",
	"player.givemoney"
    },
  ["helper"] = {
  "player.list",
	"admin.tickets",
	"player.whitelist",	
	"player.kick",
	"player.noclip",
	"player.tptome",
	"player.tpto",
	"player.group.add",
  "player.group.remove"
    },
  ["lukeeh"] = {
	"player.whitelist",	
	"player.kick",
	"player.noclip",
	"player.tptome",
	"player.tpto",
	"player.group.add",
  "player.group.remove"
    },	
  ["mod"] = {
	"admin.tickets",
  "player.list",
	"player.tptome",
	"player.tpto",
  "player.group.remove",
	"player.kick",
    "player.coords"
  },
  -- the group user is auto added to all logged players
  ["user"] = {
    "player.phone",
    "player.calladmin",
		"player.cazzuto",
		"player.fix_haircut",
		"player.check",
	--"mugger.mug",
    "police.askid",
    "player.store_weapons",
		"player.store_money",
	--"player.check",
		"player.loot",
		"player.player_menu",
		"player.userlist",
		"coma.skipper",
		"coma.caller",
    "police.seizable",	-- can be seized
		"user.paycheck"
  },
  ["pulizias"] = {
	"pulizia.soldi"
  },  
  
  ["Pescatore"] = {
    _config = { gtype = "job",
	onspawn = function(player) vRPclient.notify(player,{"Sei un pescatore."}) end
	},
	"mission.delivery.fish",
	"player.impresa",
    "fisher.service",
	"pesca.service",
	"delivery.paycheck",
	"fisher.vehicle"
    },
	["Mafia"] = {
    _config = { gtype = "gang",
	onspawn = function(player) vRPclient.notify(player,{"Fai parte di un organizzazione illegale."}) end
	},
	"mafia.build",
	"mafia.service",
	"mafia.punti",	
	"gang.permessi",
	"mafia.key"
    },
	
	["Messicani"] = {
    _config = { gtype = "gang",
	onspawn = function(player) vRPclient.notify(player,{"Fai parte di un organizzazione illegale."}) end
	},
	"mexico.build",
	"mexico.service",
	"mexico.key"
    },
	
	
	["kings of engine"] = {
    _config = { gtype = "job",
	onspawn = function(player) vRPclient.notify(player,{""}) end
	},
	"vehicle.repair",
    "vehicle.replace",
    "repair.service",
	"mec.announce",
	"repair.vehicle",
	"repair.market1",
	"harvest.weeeeeed",
	"mission.drugseller.weed",
    "drugseller.market"
    },
	["Agricoltore"] = {
    _config = { gtype = "job",
	onspawn = function(player) vRPclient.notify(player,{"Sei un Agricoltore."}) end
	},
	"mission.delivery.agri",
    "agri.service",
	"player.impresa",
	"agri.weed",
	"agri.announce",
	"delivery.paycheck",
	"agri.vehicle"
    },
	["Imprenditore"] = {
    _config = { gtype = "job",
	onspawn = function(player) vRPclient.notify(player,{"Sei un Imprenditore."}) end
	},
	"mission.delivery.imprenditore",
    "imprenditore.service",
	"imprenditore.weed",
	"imprenditore.announce",
	"delivery.paycheck"
    },
	["Pilota cargo"] = {
    _config = { gtype = "job",
	onspawn = function(player) vRPclient.notify(player,{"Sei un pilota da cargo."}) end
	},
	"mission.pilot.cargo",
	"pilot.vehicle",
	"pilot.paycheck"
    },
	["Artigiano"] = {
    _config = { gtype = "job",
	onspawn = function(player) vRPclient.notify(player,{"Sei un Artigiano."}) end
	},
	"mission.delivery.taglia",
	"delivery.paycheck",
	"arti.service",
	"player.impresa",
	"taglia.weed",
	"taglia.vehicle"
    },
  ["Medical Transport"] = {
    _config = { gtype = "job",
	onspawn = function(player) vRPclient.notify(player,{"Sei un trasportatore medico."}) end
	},
	"mission.delivery.medical",
    "medical.service",
	"delivery.paycheck",
	"medical.vehicle"
    },
  ["Armaiolo"] = {
    _config = { gtype = "gang",
	onspawn = function(player) vRPclient.notify(player,{"Sei un armaiolo."}) end
	},
	"armaiolo.service",
    "armaiolo.armeria",
	"gang.droga",
    "armeria.chest1",
    "armeria.chest2"
    },
	["Negozio_Animali"] = {
		"luke.negozio"
	},
  ["Contrabbandiere d'armi"] = {
    _config = { gtype = "job",
	onspawn = function(player) vRPclient.notify(player,{"Sei un trafficante d'armi."}) end
	},
	"mission.weapons.smuggler",
	"delivery.paycheck",
    "smuggler.service"
    },
  ["LSPD-Recluta"] = {
    _config = { 
      gtype = "gang",
      onjoin = function(player) vRPclient.setCop(player,{true}) end,
      onspawn = function(player) vRPclient.setCop(player,{true}) end,
      onleave = function(player) vRPclient.setCop(player,{true}) end
    },
    "Officer.cloakroom",
		"highway.cloakroom",
    "police.pc",
	--"toggle.service",
    "police.handcuff",
    "police.stipendiorecluta",
    "police.putinveh",
		"police.menu_interaction",
		"player.cazzuto",
		"no.autovelox",
    "police.getoutveh",
    "police.check",
    "police.service",
		"police.drag",
		"police.easy_cuff",
		"police.easy_fine",
		"police.license",
		"police.easy_jail",
		"police.easy_unjail",
		"police.spikes",
		"police.menu",
    "police.wanted",
    "police.seize.weapons",
    "police.seize.items",
    "police.jail",
    "police.fine",
		"police.freeze",
    "police.announce",
   -- "-police.store_weapons",
    "-police.seizable",	-- negative permission, police can't seize itself, even if another group add the permission
		"police.vehicle",
		"police.loadshop",
		"cop.whitelisted",
		"emergency.market",
--	"emergency.revive",
		"emergency.shop",
	--"player.list",
		"police.paycheck"
  },
  ["LSPD-Agente"] = {
    _config = { 
      gtype = "gang",
      onjoin = function(player) vRPclient.setCop(player,{true}) end,
      onspawn = function(player) vRPclient.setCop(player,{true}) end,
      onleave = function(player) vRPclient.setCop(player,{true}) end
    },
    "Officer.cloakroom",
		"highway.cloakroom",
    "police.pc",
	--"toggle.service",
    "police.handcuff",
    "police.stipendioagente",
    "police.putinveh",
		"police.menu_interaction",
		"player.cazzuto",
		"no.autovelox",
    "police.getoutveh",
    "police.check",
    "police.service",
		"police.drag",
		"police.easy_cuff",
		"police.easy_fine",
		"police.license",
		"police.easy_jail",
		"police.easy_unjail",
		"police.spikes",
		"police.menu",
    "police.wanted",
    "police.seize.weapons",
    "police.seize.items",
    "police.jail",
    "police.fine",
		"police.freeze",
    "police.announce",
   -- "-police.store_weapons",
    "-police.seizable",	-- negative permission, police can't seize itself, even if another group add the permission
		"police.vehicle",
		"police.loadshop",
		"cop.whitelisted",
		"emergency.market",
--	"emergency.revive",
		"emergency.shop",
	--"player.list",
		"police.paycheck"
  },  
  ["Sceriffo"] = {
    _config = { 
      gtype = "job",
      onjoin = function(player) vRPclient.setCop(player,{true}) end,
      onspawn = function(player) vRPclient.setCop(player,{true}) end,
      onleave = function(player) vRPclient.setCop(player,{true}) end
    },
    "Officer.cloakroom",
	"highway.cloakroom",
	"police.stipendiosceriffo",	
    "police.pc",
	--"toggle.service",
    "police.handcuff",
	"sheriff.cloakroom",
    "police.putinveh",
	"police.menu_interaction",
	"player.cazzuto",
	"no.autovelox",
    "police.getoutveh",
    "police.check",
    "police.service",
	"police.drag",
	"police.easy_cuff",
	"police.easy_fine",
	"police.license",
	"police.easy_jail",
	"police.easy_unjail",
	"police.spikes",
	"police.menu",
    "police.wanted",
    "police.seize.weapons",
    "police.seize.items",
    "police.jail",
    "police.fine",
	"police.freeze",
    "police.announce",
   -- "-police.store_weapons",
    "-police.seizable",	-- negative permission, police can't seize itself, even if another group add the permission
	"police.vehicle",
	"police.loadshop",
	"cop.whitelisted",
	"emergency.market",
--	"emergency.revive",
	"emergency.shop",
	--"player.list",
    "police.paycheck",
    "police.cassa1"
  },  
   ["LSPD-Ufficiale"] = {
    _config = { 
      gtype = "gang",
      onjoin = function(player) vRPclient.setCop(player,{true}) end,
      onspawn = function(player) vRPclient.setCop(player,{true}) end,
      onleave = function(player) vRPclient.setCop(player,{true}) end
    },
    "Officer.cloakroom",
	"highway.cloakroom",
    "police.pc",
	"police.stipendioufficiale",
	--"toggle.service",
    "police.handcuff",
    "police.putinveh",
	"police.menu_interaction",
	"player.cazzuto",
	"no.autovelox",
    "police.getoutveh",
    "police.check",
    "police.service",
	"police.drag",
	"police.easy_cuff",
	"police.easy_fine",
	"police.license",
	"police.easy_jail",
	"police.easy_unjail",
	"police.spikes",
	"police.menu",
    "police.wanted",
    "police.seize.weapons",
    "police.seize.items",
    "police.jail",
    "police.fine",
	"police.freeze",
    "police.announce",
   -- "-police.store_weapons",
    "-police.seizable",	-- negative permission, police can't seize itself, even if another group add the permission
	"police.vehicle",
	"police.loadshop",
	"cop.whitelisted",
	"emergency.market",
--	"emergency.revive",
	"emergency.shop",
	--"player.list",
    "police.paycheck",
    "police.cassa"
  },
    ["LSPD-Detective"] = {
    _config = { 
      gtype = "gang",
      onjoin = function(player) vRPclient.setCop(player,{true}) end,
      onspawn = function(player) vRPclient.setCop(player,{true}) end,
      onleave = function(player) vRPclient.setCop(player,{true}) end
    },
    "Officer.cloakroom",
	"highway.cloakroom",
    "police.pc",
	--"toggle.service",
    "police.handcuff",
    "police.putinveh",
	"police.stipendiodetective",
	"police.menu_interaction",
	"player.cazzuto",
	"no.autovelox",
    "police.getoutveh",
    "police.check",
    "police.service",
	"police.drag",
	"police.easy_cuff",
	"police.easy_fine",
	"police.license",
	"police.easy_jail",
	"police.easy_unjail",
	"police.spikes",
	"police.menu",
    "police.wanted",
    "police.seize.weapons",
    "police.seize.items",
    "police.jail",
    "police.fine",
	"police.freeze",
    "police.announce",
   -- "-police.store_weapons",
    "-police.seizable",	-- negative permission, police can't seize itself, even if another group add the permission
	"police.vehicle",
	"police.loadshop",
	"cop.whitelisted",
	"emergency.market",
--	"emergency.revive",
	"emergency.shop",
	--"player.list",
    "police.paycheck",
    "police.cassa"
  },
   ["LSPD-Tenente"] = {
    _config = { 
      gtype = "gang",
      onjoin = function(player) vRPclient.setCop(player,{true}) end,
      onspawn = function(player) vRPclient.setCop(player,{true}) end,
      onleave = function(player) vRPclient.setCop(player,{true}) end
    },
    "Officer.cloakroom",
	"highway.cloakroom",
    "police.pc",
	--"toggle.service",
    "police.handcuff",
	"police.stipendiotenente",
    "police.putinveh",
	"police.menu_interaction",
	"player.cazzuto",
	"no.autovelox",
    "police.getoutveh",
    "police.check",
    "police.service",
	"police.drag",
	"police.easy_cuff",
	"police.easy_fine",
	"police.license",
	"police.easy_jail",
	"police.easy_unjail",
	"police.spikes",
	"police.menu",
    "police.wanted",
    "police.seize.weapons",
    "police.seize.items",
    "police.jail",
    "police.fine",
	"police.freeze",
    "police.announce",
   -- "-police.store_weapons",
    "-police.seizable",	-- negative permission, police can't seize itself, even if another group add the permission
	"police.vehicle",
	"police.loadshop",
	"cop.whitelisted",
	"emergency.market",
--	"emergency.revive",
	"emergency.shop",
	--"player.list",
    "police.paycheck",
    "police.cassa"
  },
    ["LSPD-Capitano"] = {
    _config = { 
      gtype = "gang",
      onjoin = function(player) vRPclient.setCop(player,{true}) end,
      onspawn = function(player) vRPclient.setCop(player,{true}) end,
      onleave = function(player) vRPclient.setCop(player,{true}) end
    },
    "Officer.cloakroom",
	"highway.cloakroom",
    "police.pc",
	--"toggle.service",
    "police.handcuff",
	"police.stipendiocapitano",
    "police.putinveh",
	"police.menu_interaction",
	"player.cazzuto",
	"no.autovelox",
    "police.getoutveh",
    "police.check",
    "police.service",
	"police.drag",
	"police.easy_cuff",
	"police.easy_fine",
	"police.license",
	"police.easy_jail",
	"police.easy_unjail",
	"police.spikes",
	"police.menu",
    "police.wanted",
    "police.seize.weapons",
    "police.seize.items",
    "police.jail",
    "police.fine",
	"police.freeze",
    "police.announce",
   -- "-police.store_weapons",
    "-police.seizable",	-- negative permission, police can't seize itself, even if another group add the permission
	"police.vehicle",
	"police.loadshop",
	"cop.whitelisted",
	"emergency.market",
--	"emergency.revive",
	"emergency.shop",
	--"player.list",
    "police.paycheck",
    "police.cassa",
    "police.cassacap"
  },
    ["LSPD-ViceCapitano"] = {
    _config = { 
      gtype = "gang",
      onjoin = function(player) vRPclient.setCop(player,{true}) end,
      onspawn = function(player) vRPclient.setCop(player,{true}) end,
      onleave = function(player) vRPclient.setCop(player,{true}) end
    },
    "Officer.cloakroom",
	"highway.cloakroom",
    "police.pc",
	--"toggle.service",
    "police.handcuff",
	"police.stipendiovicecapitano",
    "police.putinveh",
	"police.menu_interaction",
	"player.cazzuto",
	"no.autovelox",
    "police.getoutveh",
    "police.check",
    "police.service",
	"police.drag",
	"police.easy_cuff",
	"police.easy_fine",
	"police.license",
	"police.easy_jail",
	"police.easy_unjail",
	"police.spikes",
	"police.menu",
    "police.wanted",
    "police.seize.weapons",
    "police.seize.items",
    "police.jail",
    "police.fine",
	"police.freeze",
    "police.announce",
   -- "-police.store_weapons",
    "-police.seizable",	-- negative permission, police can't seize itself, even if another group add the permission
	"police.vehicle",
	"police.loadshop",
	"cop.whitelisted",
	"emergency.market",
--	"emergency.revive",
	"emergency.shop",
	--"player.list",
    "police.paycheck",
    "police.cassa",
    "police.cassacap"
  },  
  ["EMS-Infermiere"] = {
    _config = { gtype = "gang",
	onspawn = function(player) vRPclient.notify(player,{"Sei un medico"}) end
	},
    "emergency.revive",
    "stipendio.infermiere",
	"police.pc",
	--"police.wanted",
    "emergency.shop",
    "emergency.service",
	"emergency.cloakroom",
	"no.autovelox",
	"police.menu_interaction",
	"emergency.pills",
	"emergency.vehicle",
	"emergency.market",
	"ems.whitelisted",
	"mission.delivery.medical",
    "medical.service",
	"medical.vehicle",
	"ems.loadshop",
	--"player.list",
    "emergency.paycheck",
    "ems.chest"
  },
  ["EMS-Specializzando"] = {
    _config = { gtype = "gang",
	onspawn = function(player) vRPclient.notify(player,{"Sei un medico"}) end
	},
    "emergency.revive",
    "stipendio.specializzando",	
	"police.pc",
	--"police.wanted",
    "emergency.shop",
    "emergency.service",
	"emergency.cloakroom",
	"no.autovelox",
	"police.menu_interaction",
	"emergency.pills",
	"emergency.vehicle",
	"emergency.market",
	"ems.whitelisted",
	"mission.delivery.medical",
    "medical.service",
	"medical.vehicle",
	"ems.loadshop",
	--"player.list",
    "emergency.paycheck",
    "ems.chest"
  },  
  ["EMS-Dottore"] = {
    _config = { gtype = "gang",
	onspawn = function(player) vRPclient.notify(player,{"Sei un medico"}) end
	},
    "emergency.revive",
    "stipendio.dottore",	
	"police.pc",
	--"police.wanted",
    "emergency.shop",
    "emergency.service",
	"emergency.cloakroom",
	"no.autovelox",
	"police.menu_interaction",
	"emergency.pills",
	"emergency.vehicle",
	"emergency.market",
	"ems.whitelisted",
	"mission.delivery.medical",
    "medical.service",
	"medical.vehicle",
	"ems.loadshop",
	--"player.list",
    "emergency.paycheck",
    "ems.chest"
  },  
  ["EMS-Direttore Sanitario"] = {
    _config = { gtype = "gang",
	onspawn = function(player) vRPclient.notify(player,{"Sei un medico"}) end
	},
    "emergency.revive",
    "stipendio.direttoresanitario",	
	"police.pc",
	--"police.wanted",
    "emergency.shop",
    "emergency.service",
	"emergency.cloakroom",
	"no.autovelox",
	"police.menu_interaction",
	"emergency.pills",
	"emergency.vehicle",
	"emergency.market",
	"ems.whitelisted",
	"mission.delivery.medical",
    "medical.service",
	"medical.vehicle",
	"ems.loadshop",
	--"player.list",
    "emergency.paycheck",
    "ems.chest"
  }, 
  ["EMS-Direttore Generale"] = {
    _config = { gtype = "gang",
	onspawn = function(player) vRPclient.notify(player,{"Sei un medico"}) end
	},
    "emergency.revive",
    "stipendio.direttoregenerale",	
	"police.pc",
	--"police.wanted",
    "emergency.shop",
    "emergency.service",
	"emergency.cloakroom",
	"no.autovelox",
	"police.menu_interaction",
	"emergency.pills",
	"emergency.vehicle",
	"emergency.market",
	"ems.whitelisted",
	"mission.delivery.medical",
    "medical.service",
	"medical.vehicle",
	"ems.loadshop",
	--"player.list",
	"emergency.paycheck",
	"ems.chest"
  },   
 -- ["Gunrunning"] = {
   -- _config = { gtype = "job",
	--onspawn = function(player) vRPclient.notify(player,{"Coming soon, Please wait!"}) end
  --}
--},
  ["Disoccupato"] = {
    _config = { gtype = "job",
	onspawn = function(player) vRPclient.notify(player,{"Sei disoccupato, vai all'agenzia di lavoro."}) end
	},
	"citizen.paycheck"
  },
  ["Fattorino"] = {
    _config = { gtype = "job",
	onspawn = function(player) vRPclient.notify(player,{"Sei un Fattorino, Stipendio  : $1000."}) end
	},
	"mission.delivery.food",
	"delivery.vehicle",
	"delivery.paycheck"
  },
  ["Minatore"] = {
    _config = { gtype = "job",
	    onspawn = function(player) vRPclient.notify(player,{"sei uno minatore."}) end
	  },
    "mission.drugsellerminatore.weed",
	"minatore.service",
	"player.impresa",
	"delivery.paycheck",
	"harvestzolfo.weed"
  },
  ["Trasportavalori"] = {
    _config = { gtype = "job",
	onspawn = function(player) vRPclient.notify(player,{"Sei un trasportavalori. Salario : $1000 "}) end
	},
	"mission.bankdriver.moneybank",
	"mission.bankdriver.moneybank2",
	"bankdriver.vehicle",
	"bankdriver.paycheck",
	"bankdriver.money"
  },
      ["Dispatcher"] = {
    _config = { gtype = "job",
	  onjoin = function(player) vRPclient.setCop(player,{true}) end,
      onspawn = function(player) vRPclient.setCop(player,{true}) end,
      onleave = function(player) vRPclient.setCop(player,{false}) end
	},
    "police.pc",
    "police.check",
    "police.service",
    "police.wanted",
    "police.announce",
   -- "-police.store_weapons",
    "-police.seizable",	-- negative permission, police can't seize itself, even if another group add the permission
	"emergency.market",
	"emergency.revive",
	"emergency.shop",
	"cop.whitelisted",
	"Dispatch.paycheck"
  },
  
  
  
  
  
  
  
  --business
   ["Burger Shot"] = {
    _config = { gtype = "business",
	    onspawn = function(player) vRPclient.notify(player,{"Burger ."}) end
	  },
    "burgershot.business",
	"propr.aut"
  },  
   ["Negozio Di Animali"] = {
    _config = { gtype = "business",
	    onspawn = function(player) vRPclient.notify(player,{"Burger ."}) end
	  },
    "animali.business",
	"propr.aut"
  },     
  -- whitelist group for police, emergency and president jobs / add player to this group and user can view the job selection / search in the map
  -- moderator=president / president is guy from the server give a player group cop ems moderator when admin is offline / sallary : $10.000
  ["telefono"] = {
    "telefono.whitelisted"
	},	
	--per aggiungere lavoratori al motorsport (leaderMenu)
  ["capomotorsport"] = {
    "capo.whitelisted"
	},	
    ["Import & Export"] = {
    _config = { gtype = "gang",
	onspawn = function(player) vRPclient.notify(player,{"Fai parte dell'Import Export"}) end
	},
	"import.whitelisted",
	"import.cassa1",
	"import.cassa2",
	"import.cassa3",
	"import.cassa4"
	},
    ["Benny's"] = {
    _config = { gtype = "gang",
	onspawn = function(player) vRPclient.notify(player,{"Fai parte del Bennys"}) end
	},
	"bennys.whitelist"
	},	
    ["Concessionario"] = {
    "concessionario.whitelisted"
  },  
     ["importcapo"] = {
    "import.whitelisted"
  },        
     ["Capo Aci"] = {
   	_config = { gtype = "gang",
	  onspawn = function(player) vRPclient.notify(player,{"Fai parte dell'ACI"}) end
	},
    "aci.chest",
	"dipendente.aci",
    "vehicle.tow",	
	"police.announce"
  },  
     ["MotorSport"] = {
   	_config = { gtype = "job",
	  onspawn = function(player) vRPclient.notify(player,{"Fai parte dell'ACI"}) end
	},
    "motorsport.chest",
    "concessionario.whitelisted",
	"modifica.auto",
    "motorsport.chest1",
    "luke.concessionaria"
	},    
     ["Dipendente Aci"] = {
   	_config = { gtype = "gang",
	  onspawn = function(player) vRPclient.notify(player,{"Fai parte dell'ACI"}) end
	},
    "dipendente.aci",
    "vehicle.tow",
	"police.announce"
  },    
     ["aciboss"] = {
    "aci.chest"
  },  
     ["import5"] = {
    "import.whitelisted"
  },    
  ["La Mano"] = {
   	_config = { gtype = "gang",
	  onspawn = function(player) vRPclient.notify(player,{"Fai parte della fazione < La Mano >"}) end
	},
	  "exotic.whitelisted",
	  "mano.droga",
    "exotic.vehicle",
	  "gang.permessi",	
    "player.group.add",
    "player.group.remove",
    "player.list"
  },
  ["Weazel News"] = {
   	_config = { gtype = "gang",
	  onspawn = function(player) vRPclient.notify(player,{"Sei un Giornalista"}) end
	},
    "weazel",
    "weazel.camera",
    "weazel.mic"
  },    
   ["Capo Concessionaria"] = {
   	_config = { gtype = "job",
	  onspawn = function(player) vRPclient.notify(player,{"Sei il capo della concessionaria"}) end
	},
    "luke.concessionaria",
    "luke.capo",	
	  "spawn.veh"	,
	  "modifica.auto",
    "luke.whitelisted",
    "conc.patenti"
  },  
 
   ["Concessionaria Dal Milanese"] = {
   	_config = { gtype = "job",
	  onspawn = function(player) vRPclient.notify(player,{"Fai parte del concessionario dal milanese"}) end
	},
    "luke.concessionaria",
		"spawn.veh",
	  "modifica.auto",		
    "luke.whitelisted",
    "conc.patenti"
  },   
        
  ["Medico"] =  {
    _config = {
	onspawn = function(player) vRPclient.notify(player,{"Sei il medico di Los Santos"}) end
	},
    "fai.tessera",
	  "mission.delivery.agri",
    "agri.service",
	  "player.impresa",
	  "agri.weed",
	  "agri.announce",
	  "agri.vehicle",
	  "citizen.paycheck"
  },
  ["ragazzee"] = {  
    "ragazze.vestitii"
  },
  ["crx"] = {
   "player.list",
   "emergencypresi.pills",
   "president.whitelisted",
   "presidente.service",
   "police.pc",
   "police.wanted",
   "mission.trafficante.crx",
   "police.easy_fine",
   "police.check",
   "police.menu",
   "police.fine",
   "player.store_money",
   "player.osserva",
   "uber.service",
   "uber.vehicle",
   "agri.weed",
   "build.gun",
   "arma.service",
   "hacker.hack",
   "hacker.credit_cards",
   "drugseller.market",
   "harvest.weeeeeed",
   "harvest.weed",
   "clande.permi"
   
  },
  ["Primario"] = {
   _config ={
	onspawn = function(player) vRPclient.notify(player,{"Sei il primario di Los santos"}) end
	},
    "emergencypresi.pills",
	  "player.impresa",
	  "player.group.add",
	  "mission.delivery.agri",
    "agri.service",
	  "agri.vehicle",
	  "agri.weed",
	  "agri.announce",
	  "police.pc",
	  "pilot.paycheck",
	  "agri.vehicle"
   },
  ["Assicuratore"] = {
   _config ={
	onspawn = function(player) vRPclient.notify(player,{"Sei un assicuratore"}) end
	},
    "assi.permission",
	  "assicuratore.announce"
   },
   ["Hermanos"] = {
    _config = { gtype = "job",
	onspawn = function(player) vRPclient.notify(player,{"Sei un Hermanos."}) end
	},
  "buildd.gun",
	"delivery.paycheck",
	"harvest.weeeeeed",
	"mission.drugseller.weed",
  "drugseller.market",
	"armaa.service"
  },
  ["Lost"] = {
    _config = { gtype = "job",
	onspawn = function(player) vRPclient.notify(player,{"Sei un Lost Bikers."}) end
	},
  "build.gun",
	"vehicle.repair",
  "vehicle.replace",
	"delivery.paycheck",
	"repair.vehicle",
	"repair.market",
	"harvest.weeeeeed",
	"mission.drugseller.weed",
  "drugseller.market",
  "repair.service",
	"mec.announce",
	"arma.service"
  },
  
  ["West Side Bloods"] = {
    _config = { gtype = "gang",
	onspawn = function(player) vRPclient.notify(player,{"Fai parte dei Bloods."}) end
	},
    "gang.permessi"
	},    
  ["Yakuza"] = {
    _config = { gtype = "gang",
	onspawn = function(player) vRPclient.notify(player,{"Fai parte dei Bloods."}) end
	},
    "yakuza.permessi"
	},  	
  ["Samcro"] = {
    _config = { gtype = "gang",
	onspawn = function(player) vRPclient.notify(player,{"Fai parte della famiglia Vagos."}) end
	},
    "garnet.droga",
    "gang.permessi"
  },
  
  ["Cosa Nostra"] = {
    _config = { gtype = "gang",
	onspawn = function(player) vRPclient.notify(player,{"Fai parte della famiglia Cosa Nostra."}) end
	},
    
    "gang.permessi"
	},


  ["Presidente"] = {
    _config = { gtype = "job",
	onspawn = function(player) vRPclient.notify(player,{"Sei il presidente, Salario : $40000."}) end
	},
	"player.list",
	"president.whitelisted",
	"player.group.add",
	--"emergencypresi.pills",
	"president.cloakroom",
	--"player.store_money",
	"police.wanted",
	--"uber.service",
	--"uber.vehicle",
	"player.kick",
	--"police.pc",
	"player.impresa",
	"presidente.service",
	--"police.check",
	--"player.osserva",
	--"agri.announce",
	"pre.announce",
	--"vehicle.repair",
    --"vehicle.replace",
	--"police.menu",
	--"police.fine",
    --"repair.service",
	--"police.easy_fine",
	--"mec.announce",
	--"repair.vehicle",
	--"repair.market",
	"president.paycheck",
	--"mission.delivery.agri",
	"mission.delivery.imprenditore",
    "imprenditore.service",
	"imprenditore.weed",
	"imprenditore.announce"
    --"agri.service",
	--"emergency.revive",
	--"emergency.shop",
	--"cop.whitelisted",
	--"agri.weed",
	--"agri.vehicle"
},
  ["buyer"] = {  
    "exotic.vehicle"
  },
  ["ems"] = {  
    "ems.whitelisted"
  },
  ["aci"] = {  
    "aci.whitelisted"
  },  
   ["luke"] = {  
    "luke.whitelisted"	
  }, 
  
  
  ["moderator"] = {
    "president.whitelisted"
  }
}

-- groups are added dynamically using the API or the menu, but you can add group when an user join here
cfg.users = {
  [1] = { -- give superadmin and admin group to the first created user on the database
    "superadmin",
    "admin",
	  "recruiter"
  }
}

-- group selectors
-- _config
--- x,y,z, blipid, blipcolor, permissions (optional)

cfg.selectors = {
  ["Ufficio di collocamento"] = {
    _config = {x = -268.363739013672, y = -957.255126953125, z = 31.22313880920410, blipid = 351, blipcolor = 47},
--  "Tassista",
--  "Meccanico",
--  "Gioielliere",
--	"Imprenditore",
--	"Minatore",
--	"Artigiano",
--	"Agricoltore",
	"Fattorino",
	"Trasportavalori",
--	"Medical Transport",
	"Pescatore",
    "Disoccupato"
  },  
}

return cfg

