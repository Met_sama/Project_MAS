local tvRP.MountMod = {}

local Mount = 0
local InMount = false
local MountInRange = false
local Carotte = 0
local RopeMount = 0
local AttachValue = {20, -0.4, 0, 0, 0, 90, 0}
local Target2 = 0
local CanMount = false
local CowHash = GetHashKey("A_C_Cow")	
local BoarHash = GetHashKey("A_C_Boar")
local DeerHash = GetHashKey("A_C_Deer")
local PigHash = GetHashKey("A_C_Pig")
local SharkHash = GetHashKey("A_C_SharkTiger")	
local ChickenhawkHash = GetHashKey("A_C_Chickenhawk")	
local ChimpHash = GetHashKey("A_C_Chimp")	
local ChopHash = GetHashKey("A_C_Chop")	
local CormorantHash = GetHashKey("A_C_Cormorant")	
local CoyoteHash = GetHashKey("A_C_Coyote")	
local CrowHash = GetHashKey("A_C_Crow")	
local FishHash = GetHashKey("A_C_Fish")	
local HenHash = GetHashKey("A_C_Hen")	
local HuskyHash = GetHashKey("A_C_Husky")	
local MtLionHash = GetHashKey("A_C_MtLion")
local PigeonHash = GetHashKey("A_C_Pigeon")	
local RatHash = GetHashKey("A_C_Rat")	
local RetrieverHash = GetHashKey("A_C_Retriever")	
local RhesusHash = GetHashKey("A_C_Rhesus")	
local RottweilerHash = GetHashKey("A_C_Rottweiler")	
local SeagullHash = GetHashKey("A_C_Seagull")	
local shepherdHash = GetHashKey("A_C_shepherd")	
local dolphinHash = GetHashKey("a_c_dolphin")	
local sharkhammerHash = GetHashKey("a_c_sharkhammer")
local killerwhaleHash = GetHashKey("a_c_killerwhale")
local humpbackHash = GetHashKey("a_c_humpback")


local MountTable = {}
MountTable[1] = {CowHash, 20, -0.4, 0, 0, 0, 90, 0}
MountTable[2] = {DeerHash, 20, -0.4, 0, 0.2, 0, 0, -90}
MountTable[3] = {MtLionHash, 20, -0.4, 0, 0.2, 0, 0, -90}
MountTable[4] = {SharkHash, 20, -0.4, 0, 0.2, 0, 0, -90}
MountTable[5] = {dolphinHash, 20, -0.4, 0, 0.2, 0, 0, -90}
MountTable[6] = {killerwhaleHash, 20, -0.4, 0, 0.2, 0, 0, -90}
MountTable[7] = {humpbackHash, 20, -0.4, 0, 0.2, 0, 0, -90}
local i = 1
local MountCheckpoint = 0
local CarotteHash = GetHashKey("prop_bowling_ball")
local Forward = false
local IsStopped = true

local AnimalSelected = 1

local MountInAnimCore = "veh@jeep@bodhi@rds@enter_exit"
local MountInAnim = "get_in"

local MountOutAnimCore = "veh@jeep@bodhi@rds@enter_exit"
local MountOutAnim = "get_out"

local MountIDLEAnimCore = "amb@code_human_in_car_idles@generic@ds@idle_a"
local MountIDLEAnim = "idle_a"
local MountIDLE2AnimCore = "veh@helicopter@ds@idle_panic"
local MountIDLE2Anim = "sit"


function tvRP.MountMod.init()
	local pid = GetPlayerPed(-1)	
	DetachEntity(pid,true, true)
	ClearPedTasks(pid)	
end



function tvRP.MountMod.init()
		if(InMount) then
			
			InMount = false
				
			local pid = GetPlayerPed(-1)	
		
			tvRP.MountModFix_Animation(MountOutAnimCore, MountOutAnim)
			Citizen.Wait(1000)	
			DetachEntity(pid,true, true)	
			ClearPedTasks(pid)
			--ROPE.DELETE_ROPE(RopeMount)
		elseif(MountInRange) then	
	
			InMount = true	
			local MountModel = GetEntityModel(Mount)
			ClearPedTasks(Mount)	
			local pid = GetPlayerPed(-1)	
			local heading = GetEntityHeading(Mount)
				
			ClearPedTasks(pid)
			tvRP.MountModFix_Animation(MountInAnimCore, MountInAnim)
			Citizen.Wait(1000)
			if(MountModel == MtLionHash) then
				tvRP.MountModFix_Animation(MountIDLE2AnimCore, MountIDLE2Anim)
			else
				tvRP.MountModFix_Animation(MountIDLEAnimCore, MountIDLEAnim)
			end			
			
			AttachEntityToEntity(pid, Mount, AttachValue[1], AttachValue[2], AttachValue[3], AttachValue[4], AttachValue[5], AttachValue[6], AttachValue[7], false, false, false, false, 2, true)
		
			Citizen.Wait(1000)
				
		end

	end

	

	
	if (InMount) then
		tvRP.MountModDisableMelee()
		tvRP.MountModCheckDeath()
		
			--tvRP.MountMod.Fix_Animation(MountIDLEAnimCore, MountIDLEAnim)
		local pid = GetPlayerPed(-1)
		local heading = GetEntityHeading(Mount)
		local CamRot = GetGameplayCamRot(2)
		local Height = GetEntityHeightAboveGround(Mount)
			
		local MountModel = GetEntityModel(Mount)
		if(MountModel ~= MtLionHash) then
			TaskAchieveHeading(Mount, CamRot.z, 1000)
		end
			
		SetEntityHeading(pid,heading)


		if(IsControlJustPressed(Keys.Z))then
		
			local pid = GetPlayerPed(-1)
			local location = GetEntityCoords(pid, nil)
			local CamRot = GetGameplayCamRot(2)
			local Distance = 5
			local SpawnPosition = location
			
			SpawnPosition.x = location.x - ( math.sin(math.rad(CamRot.z)) * Distance )
			SpawnPosition.y = location.y + ( math.cos(math.rad(CamRot.z)) * Distance )
			SpawnPosition.z = location.z
			--SpawnPosition.z = location.z + CamRot.x - 10
			TaskGoStraightToCoord(Mount, SpawnPosition.x, SpawnPosition.y, SpawnPosition.z, 120,10, CamRot.z, 0)
		end
		
		if(IsControlJustPressed(Keys.Space) and 	Height < 1.1)then

			local Type = GetPedType(Mount)	
			
			
			if(MountModel == MtLionHash) then
				local pid = GetPlayerPed(-1)
				local PedList,CountPed = GetPedNearbyPeds(pid, 1, -1)	
				ClearPedTasks(Mount)
				ClearPedTasks(PedList[2])
				TaskCombatPed(Mount, PedList[2], 0, 1) 
				--Citizen.Wait(1000)				
			elseif(Type == 4 or Type == 5) then

				local pid = GetPlayerPed(-1)
				local heading = GetEntityHeading(Mount)		
				local Fx = -math.sin(math.rad(heading))
				local Fy = math.cos(math.rad(heading))
				local power = 10
				TaskJump(Mount, true)	
				Citizen.Wait(1000)

			end
			

		end
	end
  end
end)






-- Functions


function tvRP.MountModFix_Animation(Wanted_CoreAnim, Wanted_anim)

	local pid = GetPlayerPed(-1)
	
	RequestAnimDict(Wanted_CoreAnim)
	while (not HasAnimDictLoaded(Wanted_CoreAnim)) do Citizen.Wait(50) end
	TaskPlayAnim(pid,Wanted_CoreAnim, Wanted_anim, 2.0, -2.0, -1, 33, 0, false, false, false)

end

function tvRP.MountModFix_Animation2(Wanted_CoreAnim, Wanted_anim)

	local pid = GetPlayerPed(-1)
	
	RequestAnimDict(Wanted_CoreAnim)
	while (not HasAnimDictLoaded(Wanted_CoreAnim)) do Citizen.Wait(50) end
	TaskPlayAnim(pid,Wanted_CoreAnim, Wanted_anim, 2.0, -2.0, -1, 1, 0, false, false, false)

end


function tvRP.MountModSpawnCarotte(Hash)

	local pid = GetPlayerPed(-1)
	local location = GetEntityCoords(pid, nil)
	
	RequestModel(Hash)
	while (not HasModelLoaded(Hash)) do Citizen.Wait (50) end
	
	Carotte = CreateObject(Hash, location.x, location.y, location.z, true, false, true)
	
	SetModelAsNoLongerNeeded(Hash)
		
end		
	
	
function tvRP.MountModSpawnPed(Hash)

	local pid = GetPlayerPed(-1)
	local location = GetEntityCoords(pid, nil)
	
	RequestModel(Hash)	
	while (not HasModelLoaded(Hash)) do Citizen.Wait (50) end
			
	Mount = CreatePed( 26, Hash, location.x + 1, location.y + 1, location.z,-25,true,false)
	--ENTITY.SET_ENTITY_INVINCIBLE(Mount, true)
	SetModelAsNoLongerNeeded(Hash)	

end


function tvRP.MountModDisableMelee()

		local pid = GetPlayerPed(-1)
		local weaponOK, WeaponType = GetCurrentPedWeapon(pid, 0, true)
		local GroupeWeapon = GetWeaponitypeGroup(WeaponType)
		
		if(GroupeWeapon == 2685387236 or GroupeWeapon == 3566412244) then
			DisablePlayerFiring(pid, true)		
		end

end

function tvRP.MountModCheckDeath()

	local pid = GetPlayerPed(-1)	
	if (IsEntityDead(pid) or IsEntityDead(Mount)or IsPlayerBeingArrested(pid, true)) then
		DetachEntity(pid,true, true)
		ClearPedTasks(pid)	
		InMount = false
		Mount = 0
	end

end




function tvRP.MountModMountDetect()

	local pid = GetPlayerPed(-1)
	local PVec = GetEntityForwardVector(pid)	
	local PLoc = GetEntityCoords(pid, nil)			
	local PedList,CountPed = GetPedNearbyPeds(pid, 1, -1)
	if (PedList[1] ~= nil) then
		local TargetHash = GetEntityModel(PedList[1])	
	
		local Type = GetPedType(PedList[1])
		
		local vehiclePlayer = GetVehiclePedIsIn(pid, false)
		local vehiclePed = GetVehiclePedIsIn(PedList[1], false)		
		
		
		
		if(vehiclePlayer == 0 and vehiclePed == 0) then
		
			if(Type == 28) then
				for k,v in ipairs(MountTable) do
					if(TargetHash == MountTable[k][1]) then
						AttachValue = {MountTable[k][2], MountTable[k][3], MountTable[k][4], MountTable[k][5], MountTable[k][6], MountTable[k][7], MountTable[k][8]}	
						Mount = PedList[1]
					end
				end	
				
			elseif(Type == 4 or Type == 5) then
			--31086
				AttachValue = {0, 0, -0.2, 0.1, 0, 90, 0}
				Mount = PedList[1]
			else
				Mount = 0
			end
			
			
			if (IsEntityInAngledArea(Mount, PLoc.x, PLoc.y, PLoc.z, PLoc.x + (PVec.x * 1), PLoc.y + (PVec.y * 1), PLoc.z, 45, false, false, 10) ) then
				MountInRange = true
			else
				MountInRange = false	
			end
		else
			Mount = 0
			MountInRange = false
		end	
	else
		Mount = 0
		MountInRange = false
	end
end



return MountMod