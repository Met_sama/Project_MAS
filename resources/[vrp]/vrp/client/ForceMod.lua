local ForceMod = {}
--■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
--    The Force Mod by Ideo
--■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■

--■■■■■■■■■■■■■■■■■■■■■■■■ KEYBINDING ■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■

	local ForceKey = 69 -- E
	local ForceKeyPad = 76 --RB

------------------------------------------------------------------------------------------
--	KEYBOARD KEYS
------------------------------------------------------------------------------------------
--	Space = 32            D4 = 52       O = 79             NumPad4 = 100         F9 = 120
--  PageUp = 33           D5 = 53       P = 80             NumPad5 = 101         F10 = 121
--	Next = 34             D6 = 54       Q = 81             NumPad6 = 102         F11 = 122
--  End = 35              D7 = 55       R = 82             NumPad7 = 103         F12 = 123
--	Home = 36             D8 = 56       S = 83             NumPad8 = 104         F13 = 124
--  Left = 37             D9 = 57       T = 84             NumPad9 = 105         F14 = 125
--	Up = 38               A = 65        U = 85             Multiply = 106        F15 = 126
--  Right = 39            B = 66        V = 86             Add = 107             F16 = 127
--	Down = 40             C = 67        W = 87             Separator = 108       F17 = 128
--  Select = 41           D = 68        X = 88             Subtract = 109        F18 = 129
--  print = 42            E = 69        Y = 89             Decimal = 110         F19 = 130
--	Execute = 43          F = 70        Z = 90             Divide = 111          F20 = 131
--  printScreen = 44      G = 71        LWin = 91          F1 = 112              F21 = 132
--	Insert = 45           H = 72        RWin = 92          F2 = 113              F22 = 133
--  Delete = 46           I = 73        Apps = 93          F3 = 114              F23 = 134
--	Help = 47             J = 74        Sleep = 95         F4 = 115              F24 = 135
--  D0 = 48               K = 75        NumPad0 = 96       F5 = 116
--	D1 = 49               L = 76        NumPad1 = 97       F6 = 117
--  D2 = 50               M = 77        NumPad2 = 98       F7 = 118
--	D3 = 51               N = 78        NumPad3 = 99       F8 = 119

------------------------------------------------------------------------------------------
--	CONTROLLER KEYS
------------------------------------------------------------------------------------------
-- A    - 73 - 70 - 21 - 18
-- X    - 99 - 22
-- Y    - 75 - 56 - 53 - 49 - 23
-- B    - 80 - 45
--
-- L3   - 86 - 28
-- R3   - 79 - 93 - 50 - 29 - 26

-- LB   - 68 - 89 - 38 - 37
-- RB   - 69 - 76 - 90 - 55 - 44
-- LT   - 77 - 72 - 88 - 91 - 25 - 15 - 10
-- RT   - 78 - 71 - 87 - 92 - 46 - 24 - 14 - 11

-- DPUP - 42 - 27
-- DPDN - 48 - 43 - 20 - 19
-- DPLT - 85 - 58 - 52 - 47
-- DPRT - 74 - 54 - 51

-- RSRT - 66 - 13 - 6
-- RSLT - 5
-- RSUP - 3
-- RSDN - 67 - 12 - 4 - 2

-- LSRT - 64 - 59 - 35 - 30 - 9 - 1
-- LSLT - 63 - 34
-- LSUP - 61 - 40 - 32
-- LSDN - 60 - 41 - 39 - 33 - 31 - 8
--
-- CTRL - 62 - 36

--■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■


local Force = 0.5	
local KeyPressed = false
local KeyTimer = 0
local KeyDelay = 15
local ForceEnabled = false
local P_Notif = {}
local P_Notif_i = 1

local StartedAnimWait = false
local StartPush = false
local PushDelay = 0

local RagCount = 0
local ForceAnimCore = "amb@world_human_aa_coffee@base"
local ForceAnim = "base"
--local ForceAnimCore = "anim@am_hold_up@male"
--local ForceAnim = "shoplift_high"
local PushAnimCore = "anim@mp_player_intcelebrationmale@thumbs_up"
local PushAnim = "thumbs_up"

function ForceMod.tick()
	
	if (KeyPressed) then
		KeyTimer = KeyTimer + 1
		if(KeyTimer >= KeyDelay) then
			KeyTimer = 0
			KeyPressed = false
		end
	end
	
	
	
	if((get_key_pressed(ForceKey) or  CONTROLS.IS_CONTROL_PRESSED(2,ForceKeyPad))and not KeyPressed and not ForceEnabled)then

		KeyPressed = true

		ForceEnabled = true
		
		if (ForceEnabled) then
			Ragdolled = false
			AUDIO.PLAY_SOUND_FRONTEND(-1, "FocusOut", "HintCamSounds", true)
			--ForceMod.DrawText("ON")
			ForceMod.Walkable_Animation(ForceAnimCore, ForceAnim)
		else
			Ragdolled = false
			local pid = PLAYER.PLAYER_PED_ID()	
			AI.CLEAR_PED_TASKS(pid)	
			--ForceMod.DrawText("OFF")
		end
	end
	
	if (StartedAnimWait) then
		CONTROLS.SET_PAD_SHAKE(2, 1000, PushDelay*2)
		PushDelay = PushDelay + 1
		if (PushDelay >= 60) then
			PushDelay = 0
			StartedAnimWait = false	
			StartPush = true
			ForceEnabled = false
		end
	end

	if (StartPush) then
		
		StartPush = false
		local pid = PLAYER.PLAYER_PED_ID()

		GRAPHICS._START_SCREEN_EFFECT("FocusIn", 0, false)
		AUDIO.PLAY_SOUND_FRONTEND(-1, "FocusOut", "HintCamSounds", true)
		GAMEPLAY.SET_TIME_SCALE(0.2)

		
		wait(1000)
		GRAPHICS._STOP_SCREEN_EFFECT("FocusIn")
		--GRAPHICS._START_SCREEN_EFFECT("MinigameTransitionOut", 0, false)

		GAMEPLAY.SET_TIME_SCALE(1)
		AI.CLEAR_PED_TASKS(pid)	
		AUDIO.PLAY_SOUND_FRONTEND(-1, "FocusOut", "HintCamSounds", true)
		local vec = ENTITY.GET_ENTITY_FORWARD_VECTOR(pid)	
		local loc = ENTITY.GET_ENTITY_COORDS(pid, nil)		
		local CamRot = CAM.GET_GAMEPLAY_CAM_ROT(2)

		local force = 5

		local Fx = -( math.sin(math.rad(CamRot.z)) * force*10 )
		local Fy = ( math.cos(math.rad(CamRot.z)) * force*10 )
		local Fz = force * (CamRot.x*0.2)
		
		--local Fx =  vec.x * force*10 
		--local Fy =  vec.y * force*10 
		local PlayerVeh = PED.GET_VEHICLE_PED_IS_IN(pid, false)		
		local PedList,CountPed = PED.GET_PED_NEARBY_PEDS(pid, 1, -1)	
		local VehList,CountVeh = PED.GET_PED_NEARBY_VEHICLES(pid, 1)
		
		for k in ipairs(VehList) do
		
			ENTITY.SET_ENTITY_INVINCIBLE(VehList[k], false)	
			if (ENTITY.IS_ENTITY_ON_SCREEN(VehList[k])) then
				if(VehList[k] ~= PlayerVeh) then
					ENTITY.APPLY_FORCE_TO_ENTITY(VehList[k], 1, Fx, Fy,Fz, 0,0,0, true, false, true, true, true, true)			
				end
			end
			
		end
		
		for k in ipairs(PedList) do
		
			if (ENTITY.IS_ENTITY_ON_SCREEN(PedList[k])) then
				ENTITY.APPLY_FORCE_TO_ENTITY(PedList[k], 1, Fx, Fy,Fz, 0,0,0, true, false, true, true, true, true)
			end
			
		end
		
	end	
	
	
	if((get_key_pressed(ForceKey) or  CONTROLS.IS_CONTROL_PRESSED(2,ForceKeyPad)) and not KeyPressed and ForceEnabled)then

		KeyPressed = true		
		StartedAnimWait = true
		
		local pid = PLAYER.PLAYER_PED_ID()		
		local PlayerVeh = PED.GET_VEHICLE_PED_IS_IN(pid, false)
		if(PlayerVeh == 0) then

			local CamRot = CAM.GET_GAMEPLAY_CAM_ROT(2)
			ENTITY.SET_ENTITY_HEADING(pid,CamRot.z)
			AI.CLEAR_PED_TASKS(pid)	
			ForceMod.Fix_Animation(PushAnimCore, PushAnim)
			
		end
		
	end	
	
	

	
	
	if (ForceEnabled) then
		if not StartedAnimWait then
			CONTROLS.SET_PAD_SHAKE(0, 10, 10)
		end
		local pid = PLAYER.PLAYER_PED_ID()
		local vec = ENTITY.GET_ENTITY_FORWARD_VECTOR(pid)	
		local loc = ENTITY.GET_ENTITY_COORDS(pid, nil)	
		local CamRot = CAM.GET_GAMEPLAY_CAM_ROT(2)
		local PlayerVeh = PED.GET_VEHICLE_PED_IS_IN(pid, false)
		CamRot.x = ((CamRot.x + 90) * 0.08) - 2
		CamRot.x = CamRot.x * CamRot.x
	
	
		local Markerloc = loc
		local High = (CamRot.x-20)*0.1
		if (High < -0.5) then High = -0.5 end
		
		
		Markerloc.x = loc.x - ( math.sin(math.rad(CamRot.z)) * 20 )
		Markerloc.y = loc.y + ( math.cos(math.rad(CamRot.z)) * 20 )
		Markerloc.z = loc.z
		
		local PedList,CountPed = PED.GET_PED_NEARBY_PEDS(pid, 1, -1)	
		local VehList,CountVeh = PED.GET_PED_NEARBY_VEHICLES(pid, 1)	

		for k in ipairs(VehList) do
			ENTITY.SET_ENTITY_INVINCIBLE(VehList[k], true)			
			if(VehList[k] ~= PlayerVeh) then
			if (ENTITY.IS_ENTITY_ON_SCREEN(VehList[k])) then
				local Vehloc = ENTITY.GET_ENTITY_COORDS(VehList[k], nil)	
				local distance = GAMEPLAY.GET_DISTANCE_BETWEEN_COORDS(Markerloc.x, Markerloc.y, Markerloc.z, Vehloc.x, Vehloc.y, Vehloc.z, true)
	
				local distX = Markerloc.x - Vehloc.x
				local distY = Markerloc.y - Vehloc.y
				local distZ = Markerloc.z - Vehloc.z
				
				local Velocity = ENTITY.GET_ENTITY_VELOCITY(VehList[k])	
				local Pspeed = ENTITY.GET_ENTITY_SPEED(pid)				
				local ForceX = -Velocity.x*0.01 + distX/(distance*5)*((0.1*Pspeed)+1)
				local ForceY = -Velocity.y*0.01 + distY/(distance*5)*((0.1*Pspeed)+1)
				local ForceZ = High + Force - Velocity.z + distZ/(distance*5)*((0.1*Pspeed)+1)
				

				ENTITY.APPLY_FORCE_TO_ENTITY(VehList[k], 1, ForceX, ForceY, ForceZ, 0,0,0, true, false, true, true, true, true)
			end
			end
		end

		for k in ipairs(PedList) do
			if (ENTITY.IS_ENTITY_ON_SCREEN(PedList[k])) then
				if (PED.IS_PED_RAGDOLL(PedList[k])) then
					local Pedloc = ENTITY.GET_ENTITY_COORDS(PedList[k], nil)	
					local distance = GAMEPLAY.GET_DISTANCE_BETWEEN_COORDS(Markerloc.x, Markerloc.y, Markerloc.z, Pedloc.x, Pedloc.y, Pedloc.z, true)
		
					local distX = Markerloc.x - Pedloc.x
					local distY = Markerloc.y - Pedloc.y
					local distZ = Markerloc.z - Pedloc.z
					
					local Velocity = ENTITY.GET_ENTITY_VELOCITY(PedList[k])	
					local Pspeed = ENTITY.GET_ENTITY_SPEED(pid)	
					
					local ForceX = -Velocity.x*0.01 + distX/(distance*5)
					local ForceY = -Velocity.y*0.01 + distY/(distance*5)
					local ForceZ = High + Force - Velocity.z + distZ/(distance*5)*((0.1*Pspeed)+1)
					

					ENTITY.APPLY_FORCE_TO_ENTITY(PedList[k], 1, ForceX, ForceY, ForceZ, 0,0,0, true, false, true, true, true, true)
				end
			end
		end		
		
		if (not Ragdolled) then
		
			for k in ipairs(PedList) do

				PED.SET_PED_TO_RAGDOLL(PedList[k], 4000, 5000, 1, true, true, true)
				
			end		
		
			Ragdolled = true
		
		end
		
		RagCount = RagCount + 1
		
		if (RagCount > 200) then
			RagCount = 0
			Ragdolled = false	
		end

	end
	
end


function ForceMod.Walkable_Animation(Wanted_CoreAnim, Wanted_anim)

	local pid = PLAYER.PLAYER_PED_ID()
	
	STREAMING.REQUEST_ANIM_DICT(Wanted_CoreAnim)
	while (not STREAMING.HAS_ANIM_DICT_LOADED(Wanted_CoreAnim)) do wait(50) end
	AI.TASK_PLAY_ANIM(pid,Wanted_CoreAnim, Wanted_anim, 2.0, -2.0, -1, 49, 0, true, true, false)

end

function ForceMod.Fix_Animation(Wanted_CoreAnim, Wanted_anim)

	local pid = PLAYER.PLAYER_PED_ID()
	
	STREAMING.REQUEST_ANIM_DICT(Wanted_CoreAnim)
	while (not STREAMING.HAS_ANIM_DICT_LOADED(Wanted_CoreAnim)) do wait(50) end
	AI.TASK_PLAY_ANIM(pid,Wanted_CoreAnim, Wanted_anim, 10.0, -2.0, -1, 52, 0, true, true, false)

end
function ForceMod.DrawText(Text)
	for k in pairs(P_Notif) do 
		UI._REMOVE_NOTIFICATION(P_Notif[k])
	end
	
	UI._SET_NOTIFICATION_TEXT_ENTRY("STRING")
	UI._ADD_TEXT_COMPONENT_STRING(Text)
	P_Notif[P_Notif_i] = UI._DRAW_NOTIFICATION(false, true)
	P_Notif_i = P_Notif_i + 1
end

return ForceMod