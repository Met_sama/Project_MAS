
-- periodic player state update

local state_ready = false

AddEventHandler("playerSpawned",function() -- delay state recording
  state_ready = false
  
  Citizen.CreateThread(function()
    Citizen.Wait(30000)
    state_ready = true
  end)
end)

Citizen.CreateThread(function()
  while true do
    Citizen.Wait(30000)

    if IsPlayerPlaying(PlayerId()) and state_ready then
      local x,y,z = table.unpack(GetEntityCoords(GetPlayerPed(-1),true))
      vRPserver.updatePos({x,y,z})
      vRPserver.updateHealth({tvRP.getHealth()})
      vRPserver.updateWeapons({tvRP.getWeapons()})
      vRPserver.updateCustomization({tvRP.getCustomization()})
    end
  end
end)

-- WEAPONS

-- def
local weapon_types = {
  "WEAPON_SNIPERRIFLE",
  "WEAPON_FIREEXTINGUISHER",
  "WEAPON_COMPACTGRENADELAUNCHER",
  "WEAPON_SNOWBALL",
  "WEAPON_VINTAGEPISTOL",
  "WEAPON_COMBATPDW",
  "WEAPON_HEAVYSNIPER_MK2",
  "WEAPON_HEAVYSNIPER",
  "WEAPON_SWEEPERSHOTGUN",
  "WEAPON_MICROSMG",
  "WEAPON_WRENCH",
  "WEAPON_PISTOL",
  "WEAPON_PUMPSHOTGUN",
  "WEAPON_PUMPSHOTGUN_MK2",
  "WEAPON_APPISTOL",
  "WEAPON_BALL",
  "WEAPON_MOLOTOV",
  "WEAPON_SMG",
  "WEAPON_STICKYBOMB",
  "WEAPON_PETROLCAN",
  "WEAPON_STUNGUN",
  "WEAPON_ASSAULTRIFLE_MK2",
  "WEAPON_HEAVYSHOTGUN",
  "WEAPON_MINIGUN",
  "WEAPON_GOLFCLUB",
  "WEAPON_FLAREGUN",
  "WEAPON_FLARE",
  "WEAPON_GRENADELAUNCHER_SMOKE",
  "WEAPON_HAMMER",
  "WEAPON_COMBATPISTOL",
  "WEAPON_GUSENBERG",
  "WEAPON_COMPACTRIFLE",
  "WEAPON_HOMINGLAUNCHER",
  "WEAPON_NIGHTSTICK",
  "WEAPON_RAILGUN",
  "WEAPON_SAWNOFFSHOTGUN",
  "WEAPON_SMG_MK2",
  "WEAPON_BULLPUPRIFLE",
  "WEAPON_FIREWORK",
  "WEAPON_COMBATMG",
  "WEAPON_CARBINERIFLE",
  "WEAPON_CROWBAR",
  "WEAPON_FLASHLIGHT",
  "WEAPON_DAGGER",
  "WEAPON_GRENADE",
  "WEAPON_POOLCUE",
  "WEAPON_BAT",
  "WEAPON_PISTOL50",
  "WEAPON_KNIFE",
  "WEAPON_KNUCLE",
  "WEAPON_MG",
  "WEAPON_BULLPUPSHOTGUN",
  "WEAPON_BZGAS",
  "WEAPON_GRENADELAUNCHER",
  "WEAPON_NIGHTVISION",
  "WEAPON_MUSKET",
  "WEAPON_ProximityMine",
  "WEAPON_AdvancedRifle",
  "WEAPON_RPG",
  "WEAPON_PIPEBOMB",
  "WEAPON_MINISMG",
  "WEAPON_SNSPISTOL",
  "WEAPON_PISTOL_MK2",
  "WEAPON_SNSPISTOL_MK2",
  "WEAPON_ASSAULTRIFLE",
  "WEAPON_SPECIALCARBINE",
  "WEAPON_SPECIALCARBINE_MK2",
  "WEAPON_REVOLVER",
  "WEAPON_REVOLVER_MK2",
  "WEAPON_MARKSMANRIFLE",
  "WEAPON_MARKSMANRIFLE_MK2",
  "WEAPON_BATTLEAXE",
  "WEAPON_HEAVYPISTOL",
  "WEAPON_KNUCKLEDUSTER",
  "WEAPON_KNUCKLE",
  "WEAPON_MACHINEPISTOL",
  "WEAPON_COMBATMG_MK2",
  "WEAPON_MARKSMANPISTOL",
  "WEAPON_MACHETE",
  "WEAPON_SWITCHBLADE",
  "WEAPON_ASSAULTSHOTGUN",
  "WEAPON_DOUBLEBARRELSHOTGUN",
  "WEAPON_ASSAULTSMG",
  "WEAPON_HATCHET",
  "WEAPON_BOTTLE",
  "WEAPON_CARBINERIFLE_MK2",
  "WEAPON_DOUBLEACTION",   
  "WEAPON_PARACHUTE",
  "WEAPON_SMOKEGRENADE",
}

function tvRP.getWeaponTypes()
  return weapon_types
end

function tvRP.getWeapons()
  local player = GetPlayerPed(-1)

  local ammo_types = {} -- remember ammo type to not duplicate ammo amount

  local weapons = {}
  for k,v in pairs(weapon_types) do
    local hash = GetHashKey(v)
    if HasPedGotWeapon(player,hash) then
      local weapon = {}
      weapons[v] = weapon

      local atype = Citizen.InvokeNative(0x7FEAD38B326B9F74, player, hash)
      if ammo_types[atype] == nil then
        ammo_types[atype] = true
        weapon.ammo = GetAmmoInPedWeapon(player,hash)
      else
        weapon.ammo = 0
      end
    end
  end

  return weapons
end

function tvRP.giveWeapons(weapons,clear_before)
  local player = GetPlayerPed(-1)

  -- give weapons to player

  if clear_before then
    RemoveAllPedWeapons(player,true)
  end

  for k,weapon in pairs(weapons) do
    local hash = GetHashKey(k)
    local ammo = weapon.ammo or 0

    GiveWeaponToPed(player, hash, ammo, false)
  end
end

--[[
function tvRP.dropWeapon()
  SetPedDropsWeapon(GetPlayerPed(-1))
end
--]]

-- PLAYER CUSTOMIZATION

-- parse part key (a ped part or a prop part)
-- return is_proppart, index
local function parse_part(key)
  if type(key) == "string" and string.sub(key,1,1) == "p" then
    return true,tonumber(string.sub(key,2))
  else
    return false,tonumber(key)
  end
end

function tvRP.maschera(tipo)
  local player = GetPlayerPed(-1)
  local hash = GetEntityModel(player)
  local maskp = table.unpack(tipo)
  --print("maskp = "..maskp)
  if mask == false then
   if maskp ~= 0 then
    if hash == GetHashKey("mp_m_freemode_01") or hash == GetHashKey("mp_f_freemode_01")  then
      SetPedComponentVariation(player, 1, maskp, 0, 0)
      mask = true
    else
      tvRP.notify("~r~Non indossi il player model standard.")
    end
  else
    tvRP.mascheradef()
  end

  else 

    if hash == GetHashKey("mp_m_freemode_01") or hash == GetHashKey("mp_f_freemode_01")  then
      SetPedComponentVariation(player, 1, 0, 0, 0)
      mask = false
    else
      tvRP.notify("~r~Non indossi il player model standard.")
    end

  end
end


function tvRP.mascheradef()
  local player = GetPlayerPed(-1)
  local hash = GetEntityModel(player)
  
  if mask == false then

  tvRP.notify("~b~Maschera.")

    if hash == GetHashKey("mp_m_freemode_01") or hash == GetHashKey("mp_f_freemode_01")  then
      SetPedComponentVariation(player, 1, 46, 7, 0)
      mask = true
    else
      tvRP.notify("~r~Qualcosa è andata storta.")
    end

  else 

	  if hash == GetHashKey("mp_m_freemode_01") or hash == GetHashKey("mp_f_freemode_01") then
      SetPedComponentVariation(player, 1, 0, 0, 0)
      mask = false
    else
      tvRP.notify("~r~Qualcosa è andata storta.")
    end

  end  
end

function tvRP.getDrawables(part)
  local isprop, index = parse_part(part)
  if isprop then
    return GetNumberOfPedPropDrawableVariations(GetPlayerPed(-1),index)
  else
    return GetNumberOfPedDrawableVariations(GetPlayerPed(-1),index)
  end
end

function tvRP.getDrawableTextures(part,drawable)
  local isprop, index = parse_part(part)
  if isprop then
    return GetNumberOfPedPropTextureVariations(GetPlayerPed(-1),index,drawable)
  else
    return GetNumberOfPedTextureVariations(GetPlayerPed(-1),index,drawable)
  end
end

function tvRP.getCustomization()
  local ped = GetPlayerPed(-1)

  local custom = {}

  custom.modelhash = GetEntityModel(ped)

  -- ped parts
  for i=0,20 do -- index limit to 20
    custom[i] = {GetPedDrawableVariation(ped,i), GetPedTextureVariation(ped,i), GetPedPaletteVariation(ped,i)}
  end

  -- props
  for i=0,10 do -- index limit to 10
    custom["p"..i] = {GetPedPropIndex(ped,i), math.max(GetPedPropTextureIndex(ped,i),0)}
  end

  return custom
end

-- partial customization (only what is set is changed)
function tvRP.setCustomization(custom) -- indexed [drawable,texture,palette] components or props (p0...) plus .modelhash or .model
  local exit = TUNNEL_DELAYED() -- delay the return values

  Citizen.CreateThread(function() -- new thread
    if custom then
      local ped = GetPlayerPed(-1)
      local mhash = nil

      -- model
      if custom.modelhash ~= nil then
        mhash = custom.modelhash
      elseif custom.model ~= nil then
        mhash = GetHashKey(custom.model)
      end

      if mhash ~= nil then
        local i = 0
        while not HasModelLoaded(mhash) and i < 10000 do
          RequestModel(mhash)
          Citizen.Wait(10)
        end

        if HasModelLoaded(mhash) then
          -- changing player model remove weapons, so save it
          local weapons = tvRP.getWeapons()
          SetPlayerModel(PlayerId(), mhash)
          tvRP.giveWeapons(weapons,true)
          SetModelAsNoLongerNeeded(mhash)
        end
      end

      ped = GetPlayerPed(-1)

      -- parts
      for k,v in pairs(custom) do
        if k ~= "model" and k ~= "modelhash" then
          local isprop, index = parse_part(k)
          if isprop then
            if v[1] < 0 then
              ClearPedProp(ped,index)
            else
              SetPedPropIndex(ped,index,v[1],v[2],v[3] or 2)
            end
          else
            SetPedComponentVariation(ped,index,v[1],v[2],v[3] or 2)
          end
        end
      end
    end

    exit({})
  end)
end

-- fix invisible players by resetting customization every minutes
--[[
Citizen.CreateThread(function()
  while true do
    Citizen.Wait(60000)
    if state_ready then
      local custom = tvRP.getCustomization()
      custom.model = nil
      custom.modelhash = nil
      tvRP.setCustomization(custom)
    end
  end
end)
--]]
