local chiamata = false
local chiamata_2 = false
local chiamataincorso = false
local CellphoneObject = nil
local chiamante = nil
local channel = nil
local nome1 = nil
local phone = nil
local scon = false
local sec2 = 0

function tvRP.ChiamataInArrivo(nplayer,can,nome_1,ukn)
  if not chiamata and not chiamataincorso then 
    nome1 = nome_1
    scon = ukn
    channel = tonumber(can)
    chiamante = nplayer
    chiamata = true
  else
    vRPserver.ChiamataOccupatoSync({nplayer})
    vRPserver.ChiamataPersa({nplayer,nome_1})
  end 
end

function tvRP.ChiamandoSync(nplayer,nchannel,phone_n)
  if not chiamata and not chiamataincorso then 
    channel = tonumber(nchannel)
    chiamante = nplayer
    chiamata_2 = true
    phone = phone_n
  else
    tvRP.notify("~r~Sei già in chiamata..")
  end 
end

function tvRP.ChiamataOccupata()
  chiamata_2 = false
  tvRP.notify("~r~Occupato..")
end

Citizen.CreateThread(function()
  while true do
   Citizen.Wait(0)
    if chiamata then

      if not scon then 
        tvRP.DrawvRPText2D(0.54,0.88,"~h~~r~CHIAMATA: ~b~~u~"..nome1,0.6)		
      else 
        tvRP.DrawvRPText2D(0.54,0.88,"~h~~r~CHIAMATA: ~b~~u~SCONOSCIUTO",0.6)
      end

        tvRP.DrawvRPText2D(0.57,0.92,"~b~~g~[F5]  ~b~~r~[F6]",0.6)

      DisableControlAction(0, 166,true)
      DisableControlAction(0, 167,true)

      if IsDisabledControlJustReleased(0, 166) then
        chiamata = false
        --tvRP.removeDiv("chiamata_telefono")
        tvRP.ChiamataAccettata()
        vRPserver.ChiamataAccettataSync({chiamante})
      elseif IsDisabledControlJustReleased(0, 167) then
        chiamata = false
        channel = nil
        vRPserver.ChiamataRifiutataSync({chiamante})
      end

    end
  end
end)

Citizen.CreateThread(function()
  while true do
   Citizen.Wait(0)
    if chiamata_2 then
     tvRP.DrawvRPText2D(0.55,0.9,"~h~~r~CHIAMANDO: ~b~~u~"..phone,0.6)
     DisableControlAction(0, 167,true)
      if IsDisabledControlJustReleased(0, 167) then
        tvRP.ChiamataFinita()
        vRPserver.ChiamataFinitaSync({chiamante})
        chiamata_2 = false
      end
    end
  end
end)

Citizen.CreateThread(function()
  while true do
    Citizen.Wait(4800)
    if chiamata then
      TriggerEvent('InteractSound_CL:PlayOnOne','chiamata_cellulare',0.07)
      --PlaySound(-1, "Menu_Accept", "Phone_SoundSet_Default", 0, 0, 1)
      --Citizen.Wait(150)
      --PlaySound(-1, "Menu_Accept", "Phone_SoundSet_Default", 0, 0, 1)
      --Citizen.Wait(150)
      --PlaySound(-1, "Menu_Accept", "Phone_SoundSet_Default", 0, 0, 1)
    end
  end
end)

function tvRP.ChiamataAccettata()
  chiamataincorso = true
  tvRP.PhoneObj()
  NetworkSetVoiceChannel(tonumber(channel))
  NetworkSetTalkerProximity(0.0)
  chiamata_2 = false
end

--ClearPedTasks(playerPed)
--DeleteObject(CellphoneObject)
--CellphoneObject = nil
--channel = nil
function tvRP.PhoneObj()
  local playerPed = GetPlayerPed(-1)
  local coords     = GetEntityCoords(playerPed)
  local bone       = GetPedBoneIndex(playerPed, 28422)
  local phoneModel = GetHashKey('prop_npc_phone_02')

  RequestAnimDict('cellphone@')
    
  while not HasAnimDictLoaded('cellphone@') do
    Citizen.Wait(0)
  end

  TaskPlayAnim(playerPed, 'cellphone@', 'cellphone_call_listen_base', 1.0, -1, -1, 50, 0, false, false, false)

  RequestModel(phoneModel)
  
  while not HasModelLoaded(phoneModel) do
    Citizen.Wait(0)
  end

  CellphoneObject = CreateObject(phoneModel, coords.x, coords.y, coords.z, 1, 1, 0)

  AttachEntityToEntity(CellphoneObject, playerPed, bone, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1, 1, 0, 0, 2, 1)
end

function tvRP.ChiamataRifiutata()
  PlaySound(-1, "Hang_Up", "Phone_SoundSet_Michael", 0, 0, 1)
  chiamata_2 = false
  tvRP.notify("~r~Chiamata rifiutata..")
end

function tvRP.ChiamataFinita()
  PlaySound(-1, "Hang_Up", "Phone_SoundSet_Michael", 0, 0, 1)
  chiamata = false
  chiamataincorso = false
  ClearPedTasks(GetPlayerPed(-1))
  DeleteObject(CellphoneObject)
  CellphoneObject = nil
  Citizen.InvokeNative(0xE036A705F989E049)
  ResetTime()
end


Citizen.CreateThread(function()
  while true do
   Citizen.Wait(1000)
   if chiamataincorso then
    sec2 = sec2 + 1
   end
  end
end)

function ResetTime()
  sec2 = 0
  channel = nil
end

Citizen.CreateThread(function()
  while true do
   Citizen.Wait(0)
    if chiamataincorso then

      if not IsEntityPlayingAnim(PlayerPedId(), 'cellphone@', 'cellphone_call_listen_base', 3) then
				TaskPlayAnim(GetPlayerPed(PlayerId()), 1.0, -1, -1, 50, 0, 0, 0, 0) -- 50 = 32 + 16 + 2
        TaskPlayAnim(GetPlayerPed(PlayerId()), 'cellphone@', 'cellphone_call_listen_base', 1.0, -1, -1, 50, 0, false, false, false)
      end

      SetCurrentPedWeapon(GetPlayerPed(-1), GetHashKey("WEAPON_UNARMED"), true)
      
      tvRP.DrawvRPText2D(0.54,0.9,"~h~~r~IN CHIAMATA",0.5)
      tvRP.DrawvRPText2D(0.55,0.92,"~h~~r~Tempo: ~b~~u~"..tvRP.SecondsToClock(sec2),0.5)
      DisableControlAction(0, 167,true)
      if IsDisabledControlJustReleased(0, 167) then
        if sec2 > 1 then
        vRPserver.ChiamataFinitaSync({chiamante})
        tvRP.ChiamataFinita()
        end
      end

    end
  end
end)

function tvRP.DrawvRPText2D(x,y,text,scale)
  SetTextColour(r, g, b, 255)
  SetTextFont(0)
  SetTextScale(scale, scale)
  SetTextWrap(0.0, 1.0)
  SetTextCentre(false)
  SetTextDropshadow(0, 0, 0, 0, 255)
  SetTextEdge(1, 0, 0, 0, 255)
  SetTextEntry("STRING")
  AddTextComponentString(text)
  DrawText(x, y)
end