-- Animation Variables
local loadedAnims = false
local noclip_ANIM_A = "amb@world_human_stand_impatient@male@no_sign@base"
local noclip_ANIM_B = "base"

-- Noclip Variables
local in_noclip_mode = false
local showtrainer = false
local travelSpeed = 4
local curLocation
local curRotation
local curHeading
local target
local menuOpts = ""

function tvRP.toggleNoclip()
  if(in_noclip_mode)then
      turnNoClipOff()
  else
      turnNoClipOn()
  end
end

function turnNoClipOn()
  blockinput = true -- Prevent Trainer access while in noclip mode.
  local playerPed = PlayerPedId()
  local inVehicle = IsPedInAnyVehicle( playerPed, false )   

  if ( not inVehicle ) then
      _LoadAnimDict( noclip_ANIM_A )
      loadedAnims = true
  end

  -- Update starting position for noclip
  local x, y, z = table.unpack( GetEntityCoords( playerPed, false ) )
  curLocation = { x = x, y = y, z = z }
  curRotation = GetEntityRotation( playerPed, false )
  curHeading = GetEntityHeading( playerPed )

  in_noclip_mode = true
  showtrainer = true
end

function turnNoClipOff()
  local playerPed = PlayerPedId()
  local inVehicle = IsPedInAnyVehicle( playerPed, false )

  if ( inVehicle ) then
      local veh = GetVehiclePedIsUsing( playerPed )
      SetEntityInvincible( veh, false )
  else
      ClearPedTasksImmediately( playerPed )
  end

  SetUserRadioControlEnabled( true )
  SetPlayerInvincible( PlayerId(), false )
  SetEntityInvincible( target, false )

  showtrainer = false
  in_noclip_mode = false  
  blockinput = false -- Allow Trainer Access Again
end

Citizen.CreateThread( function()
  local rotationSpeed = 2.5
  local forwardPush = 1.8

  local speeds = { 0.05, 0.2, 0.8, 1.8, 3.6, 5.4, 15.0 }

  local moveUpKey = 44      -- Q
  local moveDownKey = 20    -- Z
  local moveForwardKey = 32 -- W
  local moveBackKey = 33    -- S
  local rotateLeftKey = 34  -- A
  local rotateRightKey = 35 -- D
  local changeSpeedKey = 21 -- LSHIFT

  -- Sync Forward Push with Travel Speed. Only called when travelSpeed is updated.
  function updateForwardPush()
      forwardPush = speeds[ travelSpeed ]
  end

  -- Updates the players position
  function handleMovement(xVect,yVect)
      if ( IsControlJustPressed( 1, changeSpeedKey ) or IsDisabledControlJustPressed( 1, changeSpeedKey ) ) then
          travelSpeed = travelSpeed + 1 

          if ( travelSpeed > getTableLength(speeds) ) then 
              travelSpeed = 1
          end

          updateForwardPush();
      end 

      if ( IsControlPressed( 1, moveUpKey ) or IsDisabledControlPressed( 1, moveUpKey ) ) then
          curLocation.z = curLocation.z + forwardPush / 2
      elseif ( IsControlPressed( 1, moveDownKey ) or IsDisabledControlPressed( 1, moveDownKey ) ) then
          curLocation.z = curLocation.z - forwardPush / 2
      end

      if ( IsControlPressed( 1, moveForwardKey ) or IsDisabledControlPressed( 1, moveForwardKey ) ) then
          curLocation.x = curLocation.x + xVect
          curLocation.y = curLocation.y + yVect
      elseif ( IsControlPressed( 1, moveBackKey ) or IsDisabledControlPressed( 1, moveBackKey ) ) then
          curLocation.x = curLocation.x - xVect
          curLocation.y = curLocation.y - yVect
      end

      if ( IsControlPressed( 1, rotateLeftKey ) or IsDisabledControlPressed( 1, rotateLeftKey ) ) then
          curHeading = curHeading + rotationSpeed
      elseif ( IsControlPressed( 1, rotateRightKey ) or IsDisabledControlPressed( 1, rotateRightKey ) ) then
          curHeading = curHeading - rotationSpeed
      end 
  end

   while true do
      Citizen.Wait( 0 )

      if ( in_noclip_mode ) then
          local playerPed = PlayerPedId()

          if ( IsEntityDead( playerPed ) ) then
              turnNoClipOff()

              -- Ensure we get out of noclip mode
              Citizen.Wait( 100 )
          else 
              target = playerPed 
              
              -- Handle Noclip Movement.
              local inVehicle = IsPedInAnyVehicle( playerPed, true )

              if ( inVehicle ) then
                  target = GetVehiclePedIsUsing( playerPed )
              end

              SetEntityVelocity( playerPed, 0.0, 0.0, 0.0 )
              SetEntityRotation( playerPed, 0, 0, 0, 0, false )

              -- Prevent Conflicts/Damage
              SetUserRadioControlEnabled( false )
              SetPlayerInvincible( PlayerId(), true )
              SetEntityInvincible( target, true )

              -- Play animation on foot.
              if ( not inVehicle ) then
                  TaskPlayAnim( playerPed, noclip_ANIM_A, noclip_ANIM_B, 8.0, 0.0, -1, 9, 0, 0, 0, 0 ) 
              end

              local xVect = forwardPush * math.sin( degToRad( curHeading ) ) * -1.0
              local yVect = forwardPush * math.cos( degToRad( curHeading ) )

              handleMovement( xVect, yVect )

              -- Update player postion.
              SetEntityCoordsNoOffset( target, curLocation.x, curLocation.y, curLocation.z, true, true, true )
              SetEntityHeading( target, curHeading - rotationSpeed )
          end
      end
   end
end )

local speedDraw = ""
Citizen.CreateThread( function()
  while true do 
    Citizen.Wait(0)
    if showtrainer then
      if travelSpeed == 1 then
        speedDraw = "Slowest"
      elseif travelSpeed == 2 then
        speedDraw = "Slower"
      elseif travelSpeed == 3 then
        speedDraw = "Slow"
      elseif travelSpeed == 4 then
        speedDraw = "Normal"
      elseif travelSpeed == 5 then
        speedDraw = "Fast"
      elseif travelSpeed == 6 then
        speedDraw = "Faster"
      elseif travelSpeed == 7 then
        speedDraw = "Fastest"
      end
      SetTextColour(204, 204, 0, 255)
      SetTextFont(0)
      SetTextScale(0.6, 0.6)
      SetTextWrap(0.0, 1.0)
      SetTextCentre(false)
      SetTextDropshadow(2, 2, 0, 0, 0)
      SetTextEdge(1, 255, 255, 255, 205)
      SetTextEntry("STRING")
      AddTextComponentString("~b~Velocità = "..speedDraw)
      DrawText(0.8,0.68)
    end
  end
end)

function _LoadAnimDict( dict )
  while ( not HasAnimDictLoaded( dict ) ) do 
      RequestAnimDict( dict )
      Citizen.Wait( 5 )
  end 
end 

function degToRad( degs )
  return degs * 3.141592653589793 / 180 
end

function getTableLength(T)
  local count = 0
  for _ in pairs(T) do 
      count = count + 1
  end
  return count
end
