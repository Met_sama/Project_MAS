
function IsInFrontOfWater()
local location = GetOffsetPosition();
            OutputArgument z = new OutputArgument();
            local groundFound = GET_GROUND_Z_FOR_3D_COORD, ProbeLocation.X, ProbeLocation.Y, ProbeLocation.Z, z, false);
            float groundZ = z.GetResult<float>();
            ProbeLocation.Z = (float)groundZ - 0.1f;
            Log.ToChat(ProbeLocation.ToString());
            Ped = CreatePed(new Model("topo"), ProbeLocation);
            ProbePed.PositionNoOffset = ProbeLocation;
            Opacity = 0
            Citizen.Wait(50)
            bool isProbeInWater = Function.Call<bool>(Hash.IS_ENTITY_IN_WATER, ProbePed.Handle);
            ProbePed.Delete();
            bool isPlayerInWater = Function.Call<bool>(Hash.IS_ENTITY_IN_WATER, PlayerPed.Handle);
            return isPlayerInWater || isProbeInWater;
        }
