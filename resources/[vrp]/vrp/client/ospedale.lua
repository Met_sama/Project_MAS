Morte = false
AnimDict = "amb@world_human_sunbathe@female@back@base"
AnimName = "base"
time22 = 420
letto = ""

letti = {
    {-451.80526733398,-362.77960205078,-185.60404968262,180.0,"1"},
    {-448.53184570313,-362.91775512695,-185.58776855469,180.0,"2"},
    {-448.71304321289,-356.27661132813,-185.57777404785,360.0,"3"},
    {-451.41076660156,-356.23715209961,-185.57777404785,360.0,"4"},
    {-451.88710000000,-353.17110000000,-185.57770000000,180.0,"5"},
    {-448.47660000000,-353.20540000000,-185.57770000000,180.0,"6"},
    {-455.46110000000,-354.02920000000,-185.80540000000,360.0,"7"}
}

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(0)
		if Morte then
			while not HasAnimDictLoaded(AnimDict) do
				RequestAnimDict(AnimDict)
				Citizen.Wait(100)
            end
      
			if not IsEntityPlayingAnim(PlayerPedId(), AnimDict, AnimName, 1) then
				TaskPlayAnim(GetPlayerPed(PlayerId()), 1.0, -1, -1, 1, 0, 0, 0, 0) -- 50 = 32 + 16 + 2
				TaskPlayAnim(GetPlayerPed(PlayerId()), AnimDict, AnimName, 1.0, -1, -1, 1, 0, 0, 0, 0)
            end
		end
	end
end)

function tvRP.SecondsToClock(seconds)
  local seconds = tonumber(seconds)

  if seconds <= 0 then
    return "00:00:00";
  else
    hours = string.format("%02.f", math.floor(seconds/3600));
    mins = string.format("%02.f", math.floor(seconds/60 - (hours*60)));
    secs = string.format("%02.f", math.floor(seconds - hours*3600 - mins *60));
    return hours..":"..mins..":"..secs
  end
end

Citizen.CreateThread(function()
	while true do
	  Citizen.Wait(0)
        if Morte then

          tvRP.OspDrawText2D(0.28,0.92,"~h~~b~Letto ~b~~r~"..letto.." ~h~~b~Rimanente = ~b~~r~"..tvRP.SecondsToClock(time22),0.5)

          DisablePlayerFiring(PlayerId(), true)
          DisableControlAction(0,25,true) -- disable aim
          DisableControlAction(0,224,true) -- disable M
          DisableControlAction(0,44,true) -- INPUT_COVER
          DisableControlAction(0,37,true) -- INPUT_SELECT_WEAPON
          DisableControlAction(0,32,true)
          DisableControlAction(0,8,true)
          DisableControlAction(0,9,true)
          DisableControlAction(0,34,true)
          SetCurrentPedWeapon(GetPlayerPed(-1), GetHashKey("WEAPON_UNARMED"), true)

      end
    end
end)

Citizen.CreateThread(function()
	while true do
	  Citizen.Wait(1000)
        if Morte then
            time22 = time22 - 1
            SetEntityVisible(GetPlayerPed(-1), true, 1)
            if time22 <= 0 then
               tvRP.OspedaleDisattiva()
            end
        end
    end
end)

function tvRP.OspedaleAttiva()
    vRPserver.ToogleOspedale({1})
    local x,y,z,g,l = table.unpack(letti[math.random(1,#(letti))])
    time22 = 420
    letto = l
    SetEntityCoords(GetPlayerPed(-1), x, y, z, 1,0,0,1)

    SetEntityHeading(GetPlayerPed(-1),g)

        local custom = tvRP.getCustomization()
        custom.model = nil
        custom.modelhash = nil
        tvRP.setCustomization(custom)
    Morte = true
    Citizen.Wait(2000)
    FreezeEntityPosition(GetPlayerPed(-1),true)
end

function tvRP.OspedaleDisattiva()
    local ply = GetPlayerPed(-1)
    vRPserver.ToogleOspedale({0})
    time22 = 420
    Morte = false
    FreezeEntityPosition(GetPlayerPed(-1),false)
    ClearPedSecondaryTask(ply)
    ClearPedTasks(ply)
end

function tvRP.OspDrawText2D(x,y,text,scale)
    SetTextColour(204, 204, 0, 255)
    SetTextFont(0)
    SetTextScale(scale, scale)
    SetTextWrap(0.0, 1.0)
    SetTextCentre(false)
    SetTextDropshadow(2, 2, 0, 0, 0)
    SetTextEdge(1, 255, 255, 255, 205)
    SetTextEntry("STRING")
    AddTextComponentString(text)
    DrawText(x, y)
end