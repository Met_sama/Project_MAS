local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")
vRPvc = {}
vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP","vrp_voice_display")
VCclient = Tunnel.getInterface("vrp_voice_display","vrp_voice_display")
Tunnel.bindInterface("vrp_voice_display",vRPvc)

function vRPvc.setVoiceDisplay(css,text)
	vRPclient.setDiv(source,{"voice_text",css,text})
    vRPclient.setDiv(source,{"voice_icon",css,text})
end

function vRPvc.ControllaSePermesso()
    local user_id = vRP.getUserId({source})
    if user_id ~= nil then
        if vRP.hasPermission({user_id,"police.pc"}) then
            return true
        end
    end
    return false
end