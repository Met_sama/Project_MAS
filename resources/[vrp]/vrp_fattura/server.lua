MySQL = module("vrp_mysql", "MySQL")
local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")
local htmlEntities = module("vrp", "lib/htmlEntities")

vRPft = {}
vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP","vrp_fattura")
FTclient = Tunnel.getInterface("vrp_fattura","vrp_fattura")
Tunnel.bindInterface("vrp_fattura",vRPft)

local css = ".div_fatture{ background-image: url(https://i.imgur.com/V0HxA6x.jpg); background-color: rgba(0,0,0,0.75); color: black; font-size: 17px; font-family: 'Courier New', Courier, monospace; font-style: italic; text-transform: capitalize; font-weight: bold; width: 500px; padding: 10px; margin: auto; margin-top: 150px; }"

local set_fattura = {function(player,choice)
    local user_id = vRP.getUserId({player})
    if user_id ~= nil then
        vRPclient.getNearestPlayer(player,{5},function(nplayer)
        local nuser_id = vRP.getUserId({nplayer})
            if nuser_id ~= nil then
                vRP.prompt({player,"("..GetPlayerName(nplayer)..") Motivo fattura","",function(player, fatt)
                    if fatt ~= nil and fatt ~= "" then 
                        vRP.prompt({player,"Prezzo da pagare","",function(player, prezzo)
                            if tonumber(prezzo) > 0 then
                                vRP.prompt({player,"Da pagare entro..","GG/MM/AAAA",function(player, giorno)
                                    if giorno ~= nil and giorno ~= "" then 
                                        vRP.getUserIdentity({user_id, function(identity)
                                            if identity then
                                                local line = "Prezzo da pagare "..prezzo.."$<br/>Motivo: "..fatt.."<br/>Da: "..identity.name.." "..identity.firstname.."<br/>Scadenza: "..giorno.."<br/>"
                                                vRP.request({nplayer, "Accettare? - "..line, 200, function(nplayer,ok)
                                                    if ok then
                                                      vRP.getUData({nuser_id, "vRP:fatture", function(data)
                                                        if data == nil or data == "" or data == {} then
                                                            vRP.setUData({nuser_id, "vRP:fatture", line})
                                                            vRPclient.notify(player,{"~g~Fattura registrata"})
                                                            vRPclient.notify(nplayer,{"~b~La fattura è stata registrata ricorda che devi pagarla entro il ~b~"..giorno.."."})
                                                        else
                                                            vRPclient.notify(player,{"~r~Il player ha già una fattura in sospeso..."})
                                                            vRPclient.notify(nplayer,{"~r~Hai già una fattura in sospeso..."})
                                                        end
                                                      end})
                                                    else
                                                        vRPclient.notify(player,{"~r~Il player non ha accettato..."})
                                                    end
                                                end})
                                            end
                                        end})
                                    else
                                        vRPclient.notify(player,{"~r~Data non valida"})
                                    end
                                end})
                            else
                                vRPclient.notify(player,{"~r~Prezzo non valido"})
                            end
                        end})
                    else
                        vRPclient.notify(player,{"~r~Motivazione non valida"})
                    end
                end})
            else
                vRPclient.notify(player,{"~r~Nessun Player vicino.."})
            end
        end)
    end
end}

local remove_fattura = {function(player,choice)
    local user_id = vRP.getUserId({player})
    if user_id ~= nil then
        vRPclient.getNearestPlayer(player,{5},function(nplayer)
            local nuser_id = vRP.getUserId({nplayer})
            nuser_id = nuser_id
            if nuser_id ~= nil then
                vRP.request({nplayer, GetPlayerName(player).." vuole cancellare le tue fatture.. ", 200, function(nplayer,ok)
                    if ok then
                        print("[FATTURE] - "..GetPlayerName(player).." ha cancellato le fatture di "..GetPlayerName(nplayer))
                        vRP.setUData({nuser_id, "vRP:fatture", ""})
                        vRPclient.notify(nplayer,{"~g~Le tue fatture sono state eliminate.."})
                        vRPclient.notify(player,{"~g~Il player ha accettato.."})
                    else
                        vRPclient.notify(player,{"~r~Il player non ha accettato.."})
                    end
                end})
            else
                vRPclient.notify(player,{"~r~Nessun Player vicino.."})
            end
        end)
    end
end}

local look_fattura = {function(player,choice)
    local user_id = vRP.getUserId({player})
    if user_id ~= nil then
        vRP.getUData({user_id, "vRP:fatture", function(content)
            vRPclient.setDiv(player,{"fatture",css,content})
        end})
        vRP.request({player, "Nascondi resoconto", 1000, function(player,ok)
            vRPclient.removeDiv(player,{"fatture"})
        end})
    end
end}

local get_fattura = {function(player,choice)
    local user_id = vRP.getUserId({player})
    if user_id ~= nil then
        vRPclient.getNearestPlayer(player,{5},function(nplayer)
            local nuser_id = vRP.getUserId({nplayer})
            if nuser_id ~= nil then
                vRP.getUData({nuser_id, "vRP:fatture", function(content)
                    vRPclient.setDiv(player,{"fatture",css,content})
                end})
                vRP.request({player, "Nascondi resoconto", 1000, function(player,ok)
                    vRPclient.removeDiv(player,{"fatture"})
                end})
            else
                vRPclient.notify(player,{"~r~Nessun Player vicino.."})
            end
        end)
    end
end}

local fatture_menu = {function(player,choice)
	local user_id = vRP.getUserId({player})
	local menu = {}
		menu.name = "Fatture"
		menu.css = {top="75px",header_color="rgba(99,34,0,0.90)"}
		menu.onclose = function(player) vRP.openMainMenu({player}) end

		if vRP.hasPermission({user_id,"player.loot"}) then
			menu["Scrivi fattura"] = set_fattura
		end  

		if vRP.hasPermission({user_id,"player.loot"}) then
			menu["Leggi fatture"] = look_fattura
		end  

		if vRP.hasPermission({user_id,"police.drag"}) then
			menu["Controlla fatture"] = get_fattura
		end

        if vRP.hasPermission({user_id,"police.drag"}) then
			menu["Cancella fattura"] = remove_fattura
        end
        
		vRP.openMenu({player,menu})  
end}

vRP.registerMenuBuilder({"main", function(add, data)
    local player = data.player

    local user_id = vRP.getUserId({player})
    if user_id ~= nil then
        local choices = {}

        if vRP.hasPermission({user_id,"player.loot"}) then
            choices["Fatture"] = fatture_menu
	    end
		
		add(choices)
    end
end})
