lang = "en"
-- lang = "fr"

settings = {}
settings["en"] = {
	openMenu = "Premi ~g~E~w per aprire il menu.",
	electricError = "~r~Stai guidando un'auto elettrica.",
	fuelError = "~r~Non è un'auto elettrica.",
	buyFuel = "Compra Benzina",
	liters = "Litri",
	percent = "Percentuale",
	confirm = "Conferma",
	fuelStation = "Pompa di Benzina",
	boatFuelStation = "Pompa di Benzina | Barche",
	avionFuelStation = "Pompa di Benzina | Aerei ",
	heliFuelStation = "Pompa di Benzina | Elicotteri",
	price = "Prezzo"
}

settings["fr"] = {
	openMenu = "Appuyez sur ~g~E~w~ pour ouvrir le menu.",
	electricError = "~r~Vous avez une voiture électrique.",
	fuelError = "~r~Vous n'êtes pas au bon endroit.",
	buyFuel = "acheter de l'essence",
	liters = "litres",
	percent = "pourcent",
	confirm = "Valider",
	fuelStation = "Station essence",
	boatFuelStation = "Station d'essence | Bateau",
	avionFuelStation = "Station d'essence | Avions",
	heliFuelStation = "Station d'essence | Hélicoptères",
	price = "prix"
}



hud_form = 1 -- 1 : Vertical | 2 = Horizontal
hud_x = 0.175 
hud_y = 0.885

text_x = 0.2575
text_y = 0.975


electricityPrice = 1 -- NOT RANDOMED !!!