local zaino_shops = {
	{2135.0764160156,4770.9301757813,40.992557525635,"player.phone"}
}

local function ch_compra_zaino_10kg(player,choice)
  local user_id = vRP.getUserId({player})
	if user_id ~= nil then
		local kg = 10
		local weight = math.ceil(kg/2)
		local new_weight = vRP.getInventoryWeight({user_id})+weight
		if new_weight <= vRP.getInventoryMaxWeight({user_id}) then
			if vRP.tryPayment({user_id,0}) then
				vRP.closeMenu({player})
				GetValueByNumber("zaino",999999, function(nkey) 
					local lol = json.decode(nkey)
					local number = tonumber(lol.value)
					local key = number + 1
					SetTimeout(2000,function()
						local tipo = "zaino"
						local nomesp = "zaino_"..key
						local contenuto = "Una sacca in tessuto caricata sulle spalle di una persona e assicurata con due fasce che vanno sopra le spalle e sotto le ascelle."
						local nomevi = "Zaino("..key..")"
						local menu = {}
						local scelta = function(args)
							local menu = {}
								menu["Apri"] = {function(player,choice)
									vRP.openChest({player, "zaino_"..key, math.ceil(kg),false,nil,nil})
								end}
							return menu
						end
						local scelta_data = key
						vRP.defInventoryItem({nomesp,nomevi,contenuto,scelta,weight})
						vRP.giveInventoryItem({user_id, "zaino_"..key, 1, true})
						local dati = {name = nomevi, desc = contenuto, choice = scelta_data, weight = weight}
						DefInventoryItemSQL("zaino",nomesp, key, dati)
						ProssimaKey("zaino",key)
					end)
				end)
			else
				vRPclient.notify(player,{lang.money.not_enough()})
			end
		else
			vRPclient.notify(player,{lang.inventory.full()})
		end
	end
end

local function ch_compra_zaino_20kg(player,choice)
  local user_id = vRP.getUserId({player})
	if user_id ~= nil then
		local kg = 20
		local weight = math.ceil(kg/2)
		local new_weight = vRP.getInventoryWeight({user_id})+weight
		if new_weight <= vRP.getInventoryMaxWeight({user_id}) then
			if vRP.tryPayment({user_id,0}) then
				vRP.closeMenu({player})
				GetValueByNumber("zaino",999999, function(nkey) 
					local lol = json.decode(nkey)
					local number = tonumber(lol.value)
					local key = number + 1
					SetTimeout(2000,function()
						local tipo = "zaino"
						local nomesp = "zaino_"..key
						local contenuto = "Una sacca in tessuto caricata sulle spalle di una persona e assicurata con due fasce che vanno sopra le spalle e sotto le ascelle."
						local nomevi = "Zaino("..key..")"
						local scelta = function(args)
							local menu = {}
								menu["Apri"] = {function(player,choice)
									vRP.openChest({player, "zaino_"..key, math.ceil(kg),false,nil,nil})
								end}
							return menu
						end
						local scelta_data = key
						vRP.defInventoryItem({nomesp,nomevi,contenuto,scelta,weight})
						vRP.giveInventoryItem({user_id, "zaino_"..key, 1, true})
						local dati = {name = nomevi, desc = contenuto, choice = scelta_data, weight = weight}
						DefInventoryItemSQL("zaino",nomesp, key, dati)
						ProssimaKey("zaino",key)
					end)
				end)
			else
				vRPclient.notify(player,{lang.money.not_enough()})
			end
		else
			vRPclient.notify(player,{lang.inventory.full()})
		end
	end
end

local function ch_compra_zaino_30kg(player,choice)
  local user_id = vRP.getUserId({player})
	if user_id ~= nil then
		local kg = 30
		local weight = math.ceil(kg/2)
		local new_weight = vRP.getInventoryWeight({user_id})+weight
		if new_weight <= vRP.getInventoryMaxWeight({user_id}) then
			if vRP.tryPayment({user_id,0}) then
				vRP.closeMenu({player})
				GetValueByNumber("zaino",999999, function(nkey) 
					local lol = json.decode(nkey)
					local number = tonumber(lol.value)
					local key = number + 1
					SetTimeout(2000,function()
						local tipo = "zaino"
						local nomesp = "zaino_"..key
						local contenuto = "Una sacca in tessuto caricata sulle spalle di una persona e assicurata con due fasce che vanno sopra le spalle e sotto le ascelle."
						local nomevi = "Zaino("..key..")"
						local scelta = function(args)
							local menu = {}
								menu["Apri"] = {function(player,choice)
									vRP.openChest({player, "zaino_"..key, math.ceil(kg),false,nil,nil})
								end}
							return menu
						end
						local scelta_data = key
						vRP.defInventoryItem({nomesp,nomevi,contenuto,scelta,weight})
						vRP.giveInventoryItem({user_id, "zaino_"..key, 1, true})
						local dati = {name = nomevi, desc = contenuto, choice = scelta_data, weight = weight}
						DefInventoryItemSQL("zaino",nomesp, key, dati)
						ProssimaKey("zaino",key)
					end)
				end)
			else
				vRPclient.notify(player,{lang.money.not_enough()})
			end
		else
			vRPclient.notify(player,{lang.inventory.full()})
		end
	end
end

local function ch_compra_zaino_40kg(player,choice)
  local user_id = vRP.getUserId({player})
	if user_id ~= nil then
		local kg = 40
		local weight = math.ceil(kg/2)
		local new_weight = vRP.getInventoryWeight({user_id})+weight
		if new_weight <= vRP.getInventoryMaxWeight({user_id}) then
			if vRP.tryPayment({user_id,0}) then
				vRP.closeMenu({player})
				GetValueByNumber("zaino",999999, function(nkey) 
					local lol = json.decode(nkey)
					local number = tonumber(lol.value)
					local key = number + 1
					SetTimeout(2000,function()
						local tipo = "zaino"
						local nomesp = "zaino_"..key
						local contenuto = "Una sacca in tessuto caricata sulle spalle di una persona e assicurata con due fasce che vanno sopra le spalle e sotto le ascelle."
						local nomevi = "Zaino("..key..")"
						local scelta = function(args)
							local menu = {}
								menu["Apri"] = {function(player,choice)
									vRP.openChest({player, "zaino_"..key, math.ceil(kg),false,nil,nil})
								end}
							return menu
						end
						local scelta_data = key
						vRP.defInventoryItem({nomesp,nomevi,contenuto,scelta,weight})
						vRP.giveInventoryItem({user_id, "zaino_"..key, 1, true})
						local dati = {name = nomevi, desc = contenuto, choice = scelta_data, weight = weight}
						DefInventoryItemSQL("zaino",nomesp, key, dati)
						ProssimaKey("zaino",key)
					end)
				end)
			else
				vRPclient.notify(player,{lang.money.not_enough()})
			end
		else
			vRPclient.notify(player,{lang.inventory.full()})
		end
	end
end

local function ch_compra_zaino_50kg(player,choice)
  local user_id = vRP.getUserId({player})
	if user_id ~= nil then
		local kg = 50
		local weight = math.ceil(kg/2)
		local new_weight = vRP.getInventoryWeight({user_id})+weight
		if new_weight <= vRP.getInventoryMaxWeight({user_id}) then
			if vRP.tryPayment({user_id,0}) then
				vRP.closeMenu({player})
				GetValueByNumber("zaino",999999, function(nkey) 
					local lol = json.decode(nkey)
					local number = tonumber(lol.value)
					local key = number + 1
					SetTimeout(2000,function()
						local tipo = "zaino"
						local nomesp = "zaino_"..key
						local contenuto = "Una sacca in tessuto caricata sulle spalle di una persona e assicurata con due fasce che vanno sopra le spalle e sotto le ascelle."
						local nomevi = "Zaino("..key..")"
						local scelta = function(args)
							local menu = {}
								menu["Apri"] = {function(player,choice)
									vRP.openChest({player, "zaino_"..key, math.ceil(kg),false,nil,nil})
								end}
							return menu
						end
						local scelta_data = key
						vRP.defInventoryItem({nomesp,nomevi,contenuto,scelta,weight})
						vRP.giveInventoryItem({user_id, "zaino_"..key, 1, true})
						local dati = {name = nomevi, desc = contenuto, choice = scelta_data, weight = weight}
						DefInventoryItemSQL("zaino",nomesp, key, dati)
						ProssimaKey("zaino",key)
					end)
				end)
			else
				vRPclient.notify(player,{lang.money.not_enough()})
			end
		else
			vRPclient.notify(player,{lang.inventory.full()})
		end
	end
end

local zshop_menu = {name="Zaino Shop",css={top="75px",header_color="rgba(0,255,255,0.75)"}}
zshop_menu["Zaino da 10kg"] = {ch_compra_zaino_10kg,"100.000$"}
zshop_menu["Zaino da 20kg"] = {ch_compra_zaino_20kg,"200.000$"}
zshop_menu["Zaino da 30kg"] = {ch_compra_zaino_30kg,"300.000$"}
zshop_menu["Zaino da 40kg"] = {ch_compra_zaino_40kg,"400.000$"}
zshop_menu["Zaino da 50kg"] = {ch_compra_zaino_50kg,"500.000$"}
zshop_menu.onclose = function(source) vRP.closeMenu({source}) end


local function build_client_zaino_shop(source)
  local user_id = vRP.getUserId({source})
  if user_id ~= nil then
    for k,v in pairs(zaino_shops) do
      local x,y,z,perm = table.unpack(v)

      local zaino_shop_enter = function(source)
        local user_id = vRP.getUserId({source})
				if user_id ~= nil and vRP.hasPermission({user_id,perm or {}}) then
					vRP.openMenu({source,zshop_menu})
        end
      end

      local function zaino_shop_leave(source)
        vRP.closeMenu({source})
      end

      vRPclient.addBlip(source,{x,y,z,0,0,"Niente"})
      vRPclient.addMarker(source,{x,y,z-1,0.7,0.7,0.5,0,255,125,125,150})

      vRP.setArea({source,"vRP:zaino_shop"..k,x,y,z,1,1.5,zaino_shop_enter,zaino_shop_leave})
    end
  end
end

AddEventHandler("vRP:playerSpawn",function(user_id, source, first_spawn)
	if first_spawn then
	  build_client_zaino_shop(source)
	end
end)