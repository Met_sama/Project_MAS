MySQL = module("vrp_mysql", "MySQL")
local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")

vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP","vrp_items")
vRPfg = Tunnel.getInterface("vrp_items","vrp_items")
local cfg = module("vrp_items", "cfg/items_creatori")
--SQL--
MySQL.createCommand("vRP/items_table", [[
	CREATE TABLE IF NOT EXISTS vrp_porto_armi(
		iid INTEGER,
		ikey VARCHAR(255),
		ivalue TEXT,
		CONSTRAINT pk_porto_armi PRIMARY KEY(iid)
	);
	CREATE TABLE IF NOT EXISTS vrp_zaino(
		iid INTEGER,
		ikey VARCHAR(255),
		ivalue TEXT,
		CONSTRAINT pk_zaino PRIMARY KEY(iid)
	);
	CREATE TABLE IF NOT EXISTS vrp_patenti(
		iid INTEGER,
		ikey VARCHAR(255),
		ivalue TEXT,
		CONSTRAINT pk_patente PRIMARY KEY(iid)
	);
	CREATE TABLE IF NOT EXISTS vrp_fogli(
		iid INTEGER,
		ikey VARCHAR(255),
		ivalue TEXT,
		CONSTRAINT pk_foglio PRIMARY KEY(iid)
	);
]])

MySQL.createCommand("vRP/crea_porto_armi","INSERT IGNORE INTO vrp_porto_armi(iid,ikey,ivalue) VALUES(@iid,@ikey,@ivalue)")
MySQL.createCommand("vRP/get_porto_armi_number","SELECT ikey FROM vrp_porto_armi WHERE iid = @iid")
MySQL.createCommand("vRP/get_porto_armi_value","SELECT ivalue FROM vrp_porto_armi WHERE iid = @iid")
MySQL.createCommand("vRP/set_new_number_porto_armi","UPDATE vrp_porto_armi SET ivalue = @ivalue WHERE iid = @iid")

MySQL.createCommand("vRP/crea_zaino","INSERT IGNORE INTO vrp_zaino(iid,ikey,ivalue) VALUES(@iid,@ikey,@ivalue)")
MySQL.createCommand("vRP/get_zaino_number","SELECT ikey FROM vrp_zaino WHERE iid = @iid")
MySQL.createCommand("vRP/get_zaino_value","SELECT ivalue FROM vrp_zaino WHERE iid = @iid")
MySQL.createCommand("vRP/set_new_number_zaino","UPDATE vrp_zaino SET ivalue = @ivalue WHERE iid = @iid")

MySQL.createCommand("vRP/crea_patente","INSERT IGNORE INTO vrp_patenti(iid,ikey,ivalue) VALUES(@iid,@ikey,@ivalue)")
MySQL.createCommand("vRP/get_patente_number","SELECT ikey FROM vrp_patenti WHERE iid = @iid")
MySQL.createCommand("vRP/get_patente_value","SELECT ivalue FROM vrp_patenti WHERE iid = @iid")
MySQL.createCommand("vRP/set_new_number_patente","UPDATE vrp_patenti SET ivalue = @ivalue WHERE iid = @iid")

MySQL.createCommand("vRP/crea_foglio","INSERT IGNORE INTO vrp_fogli(iid,ikey,ivalue) VALUES(@iid,@ikey,@ivalue)")
MySQL.createCommand("vRP/get_foglio_number","SELECT ikey FROM vrp_fogli WHERE iid = @iid")
MySQL.createCommand("vRP/get_foglio_value","SELECT ivalue FROM vrp_fogli WHERE iid = @iid")
MySQL.createCommand("vRP/set_new_number_foglio","UPDATE vrp_fogli SET ivalue = @ivalue WHERE iid = @iid")


MySQL.execute("vRP/items_table")

MySQL.createCommand("vRP/IBK_porto_armi","INSERT IGNORE INTO vrp_porto_armi(iid,ikey,ivalue) VALUES(@iid,@ikey,@ivalue)")
MySQL.createCommand("vRP/IBK_zaino","INSERT IGNORE INTO vrp_zaino(iid,ikey,ivalue) VALUES(@iid,@ikey,@ivalue)")
MySQL.createCommand("vRP/IBK_patente","INSERT IGNORE INTO vrp_patenti(iid,ikey,ivalue) VALUES(@iid,@ikey,@ivalue)")
MySQL.createCommand("vRP/IBK_foglio","INSERT IGNORE INTO vrp_fogli(iid,ikey,ivalue) VALUES(@iid,@ikey,@ivalue)")

MySQL.execute("vRP/IBK_porto_armi", {iid = 999999, ikey = "number", ivalue = json.encode({value = 0})})
MySQL.execute("vRP/IBK_zaino", {iid = 999999, ikey = "number", ivalue = json.encode({value = 0})})
MySQL.execute("vRP/IBK_patente", {iid = 999999, ikey = "number", ivalue = json.encode({value = 0})})
MySQL.execute("vRP/IBK_foglio", {iid = 999999, ikey = "number", ivalue = json.encode({value = 0})})

local itemslist = {
	"porto_armi",
	"zaino",
	"foglio",
	"patente"
}

for k,v in pairs(cfg.items_creatori) do
	vRP.defInventoryItem({k,v.name,v.desc,v.choices,v.weight})
end

function ProssimaKey(tipo,last,cbr)
	local dati = {value = last}
	MySQL.execute("vRP/set_new_number_"..tipo, {iid = 999999, ivalue = json.encode(dati)})
end

function DefInventoryItemSQL(tipo,spawnname,key,data)
	MySQL.execute("vRP/crea_"..tipo, {iid = key, ikey = spawnname, ivalue = json.encode(data)})
end

function GetItemByNumber(tipo,number,cbr)
	local task = Task(cbr)
  
	MySQL.query("vRP/get_"..tipo.."_number", {iid = number}, function(rows, affected)
	  if #rows > 0 then
			task({rows[1].ikey})
	  else
			task()
	  end
	end)
end


function GetValueByNumber(tipo,number,cbr)
	local task = Task(cbr)
  
	MySQL.query("vRP/get_"..tipo.."_value", {iid = number}, function(rows, affected)
		if #rows > 0 then
			task({rows[1].ivalue})
	  else
			task()
	  end
	end)
end

local function build_items()
	for _,k in pairs(itemslist) do
		GetValueByNumber(k,999999, function(key) 
		local dec = json.decode(key)
		local number = tonumber(dec.value)
			if number > 0 then
				for i=1, number do
					GetItemByNumber(k,i,function(ikey)
						GetValueByNumber(k,i,function(ivalue)
							if k ~= "zaino" then
								local val = json.decode(ivalue)
								vRP.defInventoryItem({ikey,val.name,val.desc,nil,val.weight})
							else
								local val = json.decode(ivalue)
								local scelta = function(args)
									local menu = {}
										menu["Apri"] = {function(player,choice)
											vRP.openChest({player, "zaino_"..val.choice, math.ceil(val.weight*2),false,nil,nil})
										end}
									return menu
								end
								vRP.defInventoryItem({ikey,val.name,val.desc,scelta,val.weight})
							end
						end)
					end)
				end
			end
		end)
	end
end

SetTimeout(12000,function()
  build_items()
end)
