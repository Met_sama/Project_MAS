local cfg = {}

cfg.items_creatori = {
  ["portoarmi1"] = {
    name = "Contratto porto d'Armi LV.1",
    desc = "Firma questo contratto per ottenere il tuo porto d'armi",
    choices = function(args)
    local menu = {}
      menu["Firma"] = {function(player,choice)
        local user_id = vRP.getUserId({player})
         if user_id ~= nil then
           if vRP.tryGetInventoryItem({user_id,"portoarmi1",1,false}) and ( vRP.tryGetInventoryItem({user_id,"pennablu",1,false}) or vRP.tryGetInventoryItem({user_id,"pennanera",1,false}) ) then
             vRPclient.notify(player,{"~b~Scrivendo...."})
             vRPclient.playAnim(player,{false,{task="CODE_HUMAN_MEDIC_TIME_OF_DEATH"},true})
             
             vRP.getUserIdentity({user_id, function(identity)
               GetValueByNumber("porto_armi",999999, function(nkey) 
                 local lol = json.decode(nkey)
                 local number = tonumber(lol.value)
                 local key = number + 1
                 local nomesp = "portoarmi_"..key
                 local nomevi = "Porto d'armi LV1 (CF:"..user_id..")"
                 local contenuto = "Porto d'armi LV1 (CF:"..user_id..")"
                   if identity then
                     contenuto = "Porto d'armi di "..identity.firstname.." "..identity.name
                   end
                 local scelta = nil
                 local weight = 0.02
                 vRP.defInventoryItem({nomesp,nomevi,contenuto,scelta,iweight})
                 SetTimeout(2000,function()
                 vRP.giveInventoryItem({user_id, nomesp, 1, true})
                 local dati = {name = nomevi, desc = contenuto, choice = scelta, weight = weight}
                 DefInventoryItemSQL("porto_armi",nomesp, key, dati)
                 ProssimaKey("porto_armi",key)
                 vRPclient.stopAnim(player,{true})
                 vRPclient.stopAnim(player,{false})
                 end)
               end)
             end})
           vRP.closeMenu({player})
          else
            vRPclient.notify(player,{"~r~Ti manca la penna...."})
           end
        end
      end}
    return menu
    end,
   weight = 0.1
  },
   ["portoarmi2"] = {
    name = "Contratto porto d'Armi LV.2",
    desc = "Firma questo contratto per ottenere il tuo porto d'armi",
    choices = function(args)
    local menu = {}
      menu["Firma"] = {function(player,choice)
        local user_id = vRP.getUserId({player})
         if user_id ~= nil then
           if vRP.tryGetInventoryItem({user_id,"portoarmi2",1,false}) and ( vRP.tryGetInventoryItem({user_id,"pennablu",1,false}) or vRP.tryGetInventoryItem({user_id,"pennanera",1,false}) ) then
             vRPclient.notify(player,{"~b~Scrivendo...."})
             vRPclient.playAnim(player,{false,{task="CODE_HUMAN_MEDIC_TIME_OF_DEATH"},true})
             
             vRP.getUserIdentity({user_id, function(identity)
               GetValueByNumber("porto_armi",999999, function(nkey) 
                 local lol = json.decode(nkey)
                 local number = tonumber(lol.value)
                 local key = number + 1
                 local nomesp = "portoarmi_"..key
                 local nomevi = "Porto d'armi LV2 (CF:"..user_id..")"
                 local contenuto = "Porto d'armi LV2 (CF:"..user_id..")"
                   if identity then
                     contenuto = "Porto d'armi di "..identity.firstname.." "..identity.name
                   end
                 local scelta = nil
                 local weight = 0.02
                 vRP.defInventoryItem({nomesp,nomevi,contenuto,scelta,iweight})
                 SetTimeout(2000,function()
                 vRP.giveInventoryItem({user_id, nomesp, 1, true})
                 local dati = {name = nomevi, desc = contenuto, choice = scelta, weight = weight}
                 DefInventoryItemSQL("porto_armi",nomesp, key, dati)
                 ProssimaKey("porto_armi",key)
                 vRPclient.stopAnim(player,{true})
                 vRPclient.stopAnim(player,{false})
                 end)
               end)
             end})
           vRP.closeMenu({player})
          else
            vRPclient.notify(player,{"~r~Ti manca la penna...."})
           end
        end
      end}
    return menu
    end,
   weight = 0.1
  }, 
   ["portoarmi3"] = {
    name = "Contratto porto d'Armi LV.3",
    desc = "Firma questo contratto per ottenere il tuo porto d'armi",
    choices = function(args)
    local menu = {}
      menu["Firma"] = {function(player,choice)
        local user_id = vRP.getUserId({player})
         if user_id ~= nil then
           if vRP.tryGetInventoryItem({user_id,"portoarmi3",1,false}) and ( vRP.tryGetInventoryItem({user_id,"pennablu",1,false}) or vRP.tryGetInventoryItem({user_id,"pennanera",1,false}) ) then
             vRPclient.notify(player,{"~b~Scrivendo...."})
             vRPclient.playAnim(player,{false,{task="CODE_HUMAN_MEDIC_TIME_OF_DEATH"},true})
             
             vRP.getUserIdentity({user_id, function(identity)
               GetValueByNumber("porto_armi",999999, function(nkey) 
                 local lol = json.decode(nkey)
                 local number = tonumber(lol.value)
                 local key = number + 1
                 local nomesp = "portoarmi_"..key
                 local nomevi = "Porto d'armi LV3 (CF:"..user_id..")"
                 local contenuto = "Porto d'armi LV3 (CF:"..user_id..")"
                   if identity then
                     contenuto = "Porto d'armi di "..identity.firstname.." "..identity.name
                   end
                 local scelta = nil
                 local weight = 0.02
                 vRP.defInventoryItem({nomesp,nomevi,contenuto,scelta,iweight})
                 SetTimeout(2000,function()
                 vRP.giveInventoryItem({user_id, nomesp, 1, true})
                 local dati = {name = nomevi, desc = contenuto, choice = scelta, weight = weight}
                 DefInventoryItemSQL("porto_armi",nomesp, key, dati)
                 ProssimaKey("porto_armi",key)
                 vRPclient.stopAnim(player,{true})
                 vRPclient.stopAnim(player,{false})
                 end)
               end)
             end})
           vRP.closeMenu({player})
          else
            vRPclient.notify(player,{"~r~Ti manca la penna...."})
           end
        end
      end}
    return menu
    end,
   weight = 0.1
  },   
  ["contratto_a"] = {
    name = "Contratto Patente A",
    desc = "Firma questo contratto per ottenere la patente intestata alla tua persona.",
    choices = function(args)
    local menu = {}
      menu["Firma"] = {function(player,choice)
        local user_id = vRP.getUserId({player})
         if user_id ~= nil then
           if vRP.tryGetInventoryItem({user_id,"contratto_a",1,false}) and ( vRP.tryGetInventoryItem({user_id,"pennablu",1,false}) or vRP.tryGetInventoryItem({user_id,"pennanera",1,false}) ) then
             vRPclient.notify(player,{"~g~scrivendo...."})
             vRPclient.playAnim(player,{false,{task="CODE_HUMAN_MEDIC_TIME_OF_DEATH"},true})
             
             vRP.getUserIdentity({user_id, function(identity)
               GetValueByNumber("patente",999999, function(nkey) 
                 local lol = json.decode(nkey)
                 local number = tonumber(lol.value)
                 local key = number + 1
                 local patente = "A"
                 local nomesp = "patente_"..key
                 local nomevi = "Patente "..patente.." (CF:"..user_id..")"
                 local contenuto = "Patente "..patente.." (CF:"..user_id..")"
                   if identity then
                     contenuto = "Patente "..patente.." di "..identity.firstname.." "..identity.name
                   end
                 local scelta = nil
                 local weight = 0.02
                 vRP.defInventoryItem({nomesp,nomevi,contenuto,scelta,iweight})
                 SetTimeout(2000,function()
                 vRP.giveInventoryItem({user_id, nomesp, 1, true})
                 local dati = {name = nomevi, desc = contenuto, choice = scelta, weight = weight}
                 DefInventoryItemSQL("patente",nomesp, key, dati)
                 ProssimaKey("patente",key)
                 vRPclient.stopAnim(player,{true})
                 vRPclient.stopAnim(player,{false})
                 end)
               end)
             end})
           vRP.closeMenu({player})
           else
           vRPclient.notify(player,{"~g~Ti manca una penna o un foglio...."})
          end
        end
      end}
    return menu
    end,
   weight = 0.1
   },
   ["contratto_b"] = {
    name = "Contratto Patente B",
    desc = "Firma questo contratto per ottenere la patente intestata alla tua persona.",
    choices = function(args)
    local menu = {}
      menu["Firma"] = {function(player,choice)
        local user_id = vRP.getUserId({player})
         if user_id ~= nil then
           if vRP.tryGetInventoryItem({user_id,"contratto_b",1,false}) and ( vRP.tryGetInventoryItem({user_id,"pennablu",1,false}) or vRP.tryGetInventoryItem({user_id,"pennanera",1,false}) ) then
             vRPclient.notify(player,{"~g~scrivendo...."})
             vRPclient.playAnim(player,{false,{task="CODE_HUMAN_MEDIC_TIME_OF_DEATH"},true})
             
             vRP.getUserIdentity({user_id, function(identity)
               GetValueByNumber("patente",999999, function(nkey) 
                 local lol = json.decode(nkey)
                 local number = tonumber(lol.value)
                 local key = number + 1
                 local patente = "B"
                 local nomesp = "patente_"..key
                 local nomevi = "Patente "..patente.." (CF:"..user_id..")"
                 local contenuto = "Patente "..patente.." (CF:"..user_id..")"
                   if identity then
                     contenuto = "Patente "..patente.." di "..identity.firstname.." "..identity.name
                   end
                 local scelta = nil
                 local weight = 0.02
                 vRP.defInventoryItem({nomesp,nomevi,contenuto,scelta,iweight})
                 SetTimeout(2000,function()
                 vRP.giveInventoryItem({user_id, nomesp, 1, true})
                 local dati = {name = nomevi, desc = contenuto, choice = scelta, weight = weight}
                 DefInventoryItemSQL("patente",nomesp, key, dati)
                 ProssimaKey("patente",key)
                 vRPclient.stopAnim(player,{true})
                 vRPclient.stopAnim(player,{false})
                 end)
               end)
             end})
           vRP.closeMenu({player})
         else
           vRPclient.notify(player,{"~g~Ti manca una penna o un foglio...."})
          end
        end
      end}
    return menu
    end,
   weight = 0.1
   },
   ["contratto_c"] = {
    name = "Contratto Patente C",
    desc = "Firma questo contratto per ottenere la patente intestata alla tua persona.",
    choices = function(args)
    local menu = {}
      menu["Firma"] = {function(player,choice)
        local user_id = vRP.getUserId({player})
         if user_id ~= nil then
           if vRP.tryGetInventoryItem({user_id,"contratto_c",1,false}) and ( vRP.tryGetInventoryItem({user_id,"pennablu",1,false}) or vRP.tryGetInventoryItem({user_id,"pennanera",1,false}) ) then
             vRPclient.notify(player,{"~g~scrivendo...."})
             vRPclient.playAnim(player,{false,{task="CODE_HUMAN_MEDIC_TIME_OF_DEATH"},true})
             
             vRP.getUserIdentity({user_id, function(identity)
               GetValueByNumber("patente",999999, function(nkey) 
                 local lol = json.decode(nkey)
                 local number = tonumber(lol.value)
                 local key = number + 1
                 local patente = "C"
                 local nomesp = "patente_"..key
                 local nomevi = "Patente "..patente.." (CF:"..user_id..")"
                 local contenuto = "Patente "..patente.." (CF:"..user_id..")"
                   if identity then
                     contenuto = "Patente "..patente.." di "..identity.firstname.." "..identity.name
                   end
                 local scelta = nil
                 local weight = 0.02
                 vRP.defInventoryItem({nomesp,nomevi,contenuto,scelta,iweight})
                 SetTimeout(2000,function()
                 vRP.giveInventoryItem({user_id, nomesp, 1, true})
                 local dati = {name = nomevi, desc = contenuto, choice = scelta, weight = weight}
                 DefInventoryItemSQL("patente",nomesp, key, dati)
                 ProssimaKey("patente",key)
                 vRPclient.stopAnim(player,{true})
                 vRPclient.stopAnim(player,{false})
                 end)
               end)
             end})
           vRP.closeMenu({player})
         else
           vRPclient.notify(player,{"~g~Ti manca una penna o un foglio...."})
          end
        end
      end}
    return menu
    end,
   weight = 0.1
   },
   ["contratto_d"] = {
    name = "Contratto Patente D",
    desc = "Firma questo contratto per ottenere la patente intestata alla tua persona.",
    choices = function(args)
    local menu = {}
      menu["Firma"] = {function(player,choice)
        local user_id = vRP.getUserId({player})
         if user_id ~= nil then
           if vRP.tryGetInventoryItem({user_id,"contratto_d",1,false}) and ( vRP.tryGetInventoryItem({user_id,"pennablu",1,false}) or vRP.tryGetInventoryItem({user_id,"pennanera",1,false}) ) then
             vRPclient.notify(player,{"~g~scrivendo...."})
             vRPclient.playAnim(player,{false,{task="CODE_HUMAN_MEDIC_TIME_OF_DEATH"},true})
             
             vRP.getUserIdentity({user_id, function(identity)
               GetValueByNumber("patente",999999, function(nkey) 
                 local lol = json.decode(nkey)
                 local number = tonumber(lol.value)
                 local key = number + 1
                 local patente = "D"
                 local nomesp = "patente_"..key
                 local nomevi = "Patente "..patente.." (CF:"..user_id..")"
                 local contenuto = "Patente "..patente.." (CF:"..user_id..")"
                   if identity then
                     contenuto = "Patente "..patente.." di "..identity.firstname.." "..identity.name
                   end
                 local scelta = nil
                 local weight = 0.02
                 vRP.defInventoryItem({nomesp,nomevi,contenuto,scelta,iweight})
                 SetTimeout(2000,function()
                 vRP.giveInventoryItem({user_id, nomesp, 1, true})
                 local dati = {name = nomevi, desc = contenuto, choice = scelta, weight = weight}
                 DefInventoryItemSQL("patente",nomesp, key, dati)
                 ProssimaKey("patente",key)
                 vRPclient.stopAnim(player,{true})
                 vRPclient.stopAnim(player,{false})
                 end)
               end)
             end})
           vRP.closeMenu({player})
         else
           vRPclient.notify(player,{"~g~Ti manca una penna o un foglio...."})
          end
        end
      end}
    return menu
    end,
   weight = 0.1
   },
   ["contratto_be"] = {
    name = "Contratto Patente BE",
    desc = "Firma questo contratto per ottenere la patente intestata alla tua persona.",
    choices = function(args)
    local menu = {}
      menu["Firma"] = {function(player,choice)
        local user_id = vRP.getUserId({player})
         if user_id ~= nil then
           if vRP.tryGetInventoryItem({user_id,"contratto_be",1,false}) and ( vRP.tryGetInventoryItem({user_id,"pennablu",1,false}) or vRP.tryGetInventoryItem({user_id,"pennanera",1,false}) ) then
             vRPclient.notify(player,{"~b~Scrivendo...."})
             vRPclient.playAnim(player,{false,{task="CODE_HUMAN_MEDIC_TIME_OF_DEATH"},true})
             
             vRP.getUserIdentity({user_id, function(identity)
               GetValueByNumber("patente",999999, function(nkey) 
                 local lol = json.decode(nkey)
                 local number = tonumber(lol.value)
                 local key = number + 1
                 local patente = "BE"
                 local nomesp = "patente_"..key
                 local nomevi = "Patente "..patente.." (CF:"..user_id..")"
                 local contenuto = "Patente "..patente.." (CF:"..user_id..")"
                   if identity then
                     contenuto = "Patente "..patente.." di "..identity.firstname.." "..identity.name
                   end
                 local scelta = nil
                 local weight = 0.02
                 vRP.defInventoryItem({nomesp,nomevi,contenuto,scelta,iweight})
                 SetTimeout(2000,function()
                 vRP.giveInventoryItem({user_id, nomesp, 1, true})
                 local dati = {name = nomevi, desc = contenuto, choice = scelta, weight = weight}
                 DefInventoryItemSQL("patente",nomesp, key, dati)
                 ProssimaKey("patente",key)
                 vRPclient.stopAnim(player,{true})
                 vRPclient.stopAnim(player,{false})
                 end)
               end)
             end})
           vRP.closeMenu({player})
         else
           vRPclient.notify(player,{"~r~Ti manca la penna...."})
          end
        end
      end}
    return menu
    end,
   weight = 0.1
   },
   ["fogliovuoto"] = {
    name = "Foglio di carta",
    desc = "Scrivi nel foglio",
    choices = function(args)
    local menu = {}
      menu["Scrivi"] = {function(player,choice)
        local user_id = vRP.getUserId({player})
         if user_id ~= nil then
           if vRP.tryGetInventoryItem({user_id,"fogliovuoto",1,false}) and ( vRP.tryGetInventoryItem({user_id,"pennablu",1,false}) or vRP.tryGetInventoryItem({user_id,"pennanera",1,false}) ) then
             vRPclient.notify(player,{"~b~Scrivendo...."})
             vRPclient.playAnim(player,{false,{task="CODE_HUMAN_MEDIC_TIME_OF_DEATH"},true})
             vRP.prompt({player,"Contenuto:","",function(player,contenuto) 
               contenuto = contenuto or ""
             
             GetValueByNumber("foglio",999999, function(nkey) 
               local lol = json.decode(nkey)
               local number = tonumber(lol.value)
               local key = number + 1
               SetTimeout(5000,function()
                 local nomesp = "foglio_"..key
                 local nomevi = "Foglio n°"..key
                 local scelta = nil
                 local weight = 0.01
               vRP.defInventoryItem({nomesp,nomevi,contenuto,scelta,weight})
               vRP.giveInventoryItem({user_id, "foglio_"..key, 1, true})
               local dati = {name = nomevi, desc = contenuto, choice = scelta, weight = weight}
               DefInventoryItemSQL("foglio",nomesp, key, dati)
               ProssimaKey("foglio",key)
               vRPclient.stopAnim(player,{true})
               vRPclient.stopAnim(player,{false})
               end)
              end)
             end})
              vRP.closeMenu({player})
            else
             vRPclient.notify(player,{"~r~Ti manca la penna...."})
          end
        end
      end}
    return menu
    end,
   weight = 0.1
   }
}

return cfg
