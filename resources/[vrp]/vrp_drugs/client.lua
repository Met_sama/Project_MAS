vRPd = {}
Tunnel.bindInterface("vrp_drugs",vRPd)
Proxy.addInterface("vrp_drugs",vRPd)
Dserver = Tunnel.getInterface("vrp_drugs","vrp_drugs")
vRPserver = Tunnel.getInterface("vRP","vrp_drugs")
vRP = Proxy.getInterface("vRP")

-- SCREEN

-- play a screen effect
-- name, see https://wiki.fivem.net/wiki/Screen_Effects
-- duration: in seconds, if -1, will play until stopScreenEffect is called
function vRPd.playScreenEffect(name, duration)
  if duration < 0 then -- loop
    StartScreenEffect(name, 0, true)
  else
    StartScreenEffect(name, 0, true)

    Citizen.CreateThread(function() -- force stop the screen effect after duration+1 seconds
      Citizen.Wait(math.floor((duration+1)*1000))
      StopScreenEffect(name)
    end)
  end
end

--propcibo
--- net lo metti true o false dipende se vuoi sincornizzarlo con gli altri player o lasciarlo solo clien
--- objModel è tipo "prop_prova"
--- tempo è la durata prima di cancellare l'oggetto 

function vRPd.AttaccaOggetto(obj,net)
    if net == nil then net = false end

    local playerPed = GetPlayerPed(PlayerId())
    local coords     = GetEntityCoords(playerPed)
    local bone       = GetPedBoneIndex(playerPed, 18905)---https://wiki.gtanet.work/index.php?title=Bones
    local objModel = GetHashKey(obj)

    RequestModel(objModel)
  
    while not HasModelLoaded(objModel) do
        Citizen.Wait(0)
    end
    
    local objEntity = CreateObject(objModel, coords.x, coords.y, coords.z, 1, 1, 0)
    
    AttachEntityToEntity(objEntity, playerPed, bone, 0.13, 0.09, 0.01, -169.0, -274.0, 62.0, 1, 1, 0, 0, 2, 1)

    return objEntity
end

function vRPd.AttaccaOggettoTemporaneamente(obj,tempo,net)
    if net == nil then net = false end

    local playerPed = GetPlayerPed(PlayerId())
    local coords     = GetEntityCoords(playerPed)
    local bone       = GetPedBoneIndex(playerPed, 18905)---https://wiki.gtanet.work/index.php?title=Bones
    local objModel = GetHashKey(obj)

    RequestModel(objModel)
  
    while not HasModelLoaded(objModel) do
        Citizen.Wait(0)
    end
    
    local objEntity = CreateObject(objModel, coords.x, coords.y, coords.z, net, 1, 0)
    
    AttachEntityToEntity(objEntity, playerPed, bone, 0.13, 0.09, 0.01, -169.0, -274.0, 62.0, 1, 1, 0, 0, 2, 1)

    Citizen.Wait(tempo)

    DetachEntity(objEntity)
    DeleteEntity(objEntity)
end














-- stop a screen effect
-- name, see https://wiki.fivem.net/wiki/Screen_Effects
function vRPd.stopScreenEffect(name)
  StopScreenEffect(name)
end

function vRPd.setArmour(armour)
  local player = GetPlayerPed(-1)
  local n = math.floor(armour)
  SetPedArmour(player,n)
end

--PROP CIBO

-- MOVEMENT CLIPSET
function vRPd.playMovement(clipset,blur,drunk,fade,clear)
  --request anim
  RequestAnimSet(clipset)
  while not HasAnimSetLoaded(clipset) do
    Citizen.Wait(0)
  end
  -- fade out
  if fade then
    DoScreenFadeOut(1000)
    Citizen.Wait(1000)
  end
  -- clear tasks
  if clear then
    ClearPedTasksImmediately(GetPlayerPed(-1))
  end
  -- set timecycle
 -- SetTimecycleModifier("spectator5")
  -- set blur
  if blur then 
    SetPedMotionBlur(GetPlayerPed(-1), true) 
  end
  -- set movement
  SetPedMovementClipset(GetPlayerPed(-1), clipset, true)
  -- set drunk
  if drunk then
    SetPedIsDrunk(GetPlayerPed(-1), true)
  end
  -- fade in
  if fade then
    DoScreenFadeIn(1000)
  end
  
end

function vRPd.resetMovement(fade)
  -- fade
  if fade then
    DoScreenFadeOut(1000)
    Citizen.Wait(1000)
    DoScreenFadeIn(1000)
  end
  -- reset all
  ClearTimecycleModifier()
  ResetScenarioTypesEnabled()
  ResetPedMovementClipset(GetPlayerPed(-1), 0)
  SetPedIsDrunk(GetPlayerPed(-1), false)
  SetPedMotionBlur(GetPlayerPed(-1), false)
end
