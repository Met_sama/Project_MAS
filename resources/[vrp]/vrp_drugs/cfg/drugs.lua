local cfg = {}

cfg.drugs= {
	["fascette"] = {
    name = "Fascetta",
    desc = "Fascette in plastica che possono essere tagliate",
    choices = function(args)
	  local menu = {}
      menu["Usa"] = {function(player,choice)
  	    local user_id = vRP.getUserId({player})
				if user_id ~= nil then
					vRPclient.getNearestPlayer(player,{10},function(nplayer)
						local nuser_id = vRP.getUserId({nplayer})
						if nuser_id ~= nil then
							vRPclient.isInComa(nplayer,{}, function(in_coma)
								if not in_coma then
									vRPclient.isHandcuffed(nplayer,{}, function(handcuffed)  -- check handcuffed
										if not handcuffed then 
											if vRP.tryGetInventoryItem({user_id,"fascette",1,false}) then
												TriggerClientEvent("dr:drag", nplayer, player)
												vRPclient.playAnim(player,{false,{{"mp_arresting","a_uncuff",1}},false})
												vRPclient.playAnim(nplayer,{false,{{"mp_arresting","arrested_spin_r_0",1}},false})
												SetTimeout(5200, function()
													vRPclient.notify(player,{"~r~Ricorda che il player ha la capacità di togliersi le manette...."})
													TriggerClientEvent("dr:undrag", nplayer, player)
													vRPclient.toggleHandcuff(nplayer,{})
													vRP.closeMenu({nplayer})
													Wait(1000)
													vRP.closeMenu({nplayer})
												end)
											end
										else
											vRPclient.notify(player,{"~r~Il player è già ammanettato...."})
										end		
									end)
								else
									vRPclient.notify(player,{"~r~Il player è in coma...."})
								end
							end)
						else
							vRPclient.notify(player,{"~r~Nessun player vicino...."})
						end
					end)
  		   vRP.closeMenu({player})
  		  end
  	  end}
	  return menu
    end,
	 weight = 0.1
	},
	["grattaevinci"] = {
    name = "Gratta e Vinci!",
    desc = "Gratta e vinci 1M di dollari.",
    choices = function(args)
	  local menu = {}
      menu["Gratta"] = {function(player,choice)
  	    local user_id = vRP.getUserId({player})
				if user_id ~= nil then
					local vincita = 0 
  	      if vRP.tryGetInventoryItem({user_id,"grattaevinci",1,false}) then
  		    	vRPclient.notify(player,{"~g~grattando...."})
  		    	vRPclient.playAnim(player,{true,{{"amb@world_human_tourist_mobile@female@enter","enter",1}},false})
  		    	SetTimeout(1000,function()
							vRPclient.playAnim(player,{true,{{"amb@world_human_tourist_mobile@female@idle_a","idle_a",1}},true})
						end)
						SetTimeout(6000,function()
							vincita = math.random(0,100000)
							if vincita == 123 then
								vRPclient.notify(player,{"~b~Hai vinto 1M$!!!"})
								vRP.giveMoney({user_id,1000000})
							else
								vRPclient.notify(player,{"~r~Sarai più fortunato la prossima volta...."})
							end
							vRPclient.playAnim(player,{true,{{"amb@world_human_tourist_mobile@female@exit","exit",1}},false})
  		    	end)
  		   		vRP.closeMenu({player})
  		  	end
  	    end
  	  end}
	  return menu
    end,
	 weight = 0.1
	},--science.chemicals  "science.mathematics
	["giornale"] = {
    name = "Giornale LSE",
    desc = "Tutto il sistema economico in tempo reale. L'economia cambia ogni tre ore",
    choices = function(args)
	  local menu = {}
      menu["Leggi"] = {function(player,choice)
  	    local user_id = vRP.getUserId({player})
  	    if user_id ~= nil then
  	      if vRP.tryGetInventoryItem({user_id,"giornale",1,false}) then
  		    vRPclient.notify(player,{"~g~leggendo...."})
  		    vRPclient.playAnim(player,{true,{{"amb@world_human_tourist_mobile@female@enter","enter",1}},false})
  		    SetTimeout(1000,function()
					vRPclient.playAnim(player,{true,{{"amb@world_human_tourist_mobile@female@idle_a","idle_a",1}},true})
					vRP.getSData({"vRP:economy_trs", function(data)
						local economy_trs = json.decode(data) or {}
						local css = [[
							.div_giornale{ 
								position: center;
								margin: auto; 
								padding: 10px; 
								width: 220px; 
								margin-top: 155px; 
								margin-left: 2px;
								background-color: rgba(0,0,0,0.85);
								border-radius:20px;
								color: white; 
								font-family: "Monospace",monospace,monospace;
								font-weight: bold; 
								font-size: 1.5em;
							} 
						]]
					local content = ""
					local count = 0
					for k,v in pairs(economy_trs) do
						local htr = economy_trs[k]
							for g,i in pairs(htr) do 
								count = count+1
								if i ~= nil or g ~= nil then
									if g ~= "timestamp" then
										--local up = vRP.PrendiEconomiaCambiata({tostring(g)})
										css = css..[[.div_giornale .name]]..tostring(g)..[[{color: rgb(255,120,2);}]]
										css = css..[[.div_giornale .prezzo]]..tostring(g)..[[{color: rgb(255,0,0);}]]
										content = content.."<br />".."    ".." <span class=\"name"..tostring(g).."\">"..tostring(g).."</span> => <span class=\"prezzo"..tostring(g).."\">"..tostring(i.out_money).."</span>"

								  end
								end
								count = count-1
						  end
					end

          if count == 0 then
						vRPclient.setDiv(player,{"giornale", css, content})
					end
				end})
  		    end)
  		    SetTimeout(20000,function()
					vRPclient.playAnim(player,{true,{{"amb@world_human_tourist_mobile@female@exit","exit",1}},false})
					--vRP.varyExp({user_id,"science","medicina",2})
					vRPclient.removeDiv(player,{"giornale"})
  		    end)
  		   vRP.closeMenu({player})
  		  end
  	    end
  	  end}
	  return menu
    end,
	 weight = 0.1
	},
	["giornale2"] = {
    name = "Giornale Illegale LSE",
    desc = "Tutto il sistema economico illegale in tempo reale. L'economia cambia ogni tre ore",
    choices = function(args)
	  local menu = {}
      menu["Leggi"] = {function(player,choice)
  	    local user_id = vRP.getUserId({player})
  	    if user_id ~= nil then
  	      if vRP.tryGetInventoryItem({user_id,"giornale2",1,false}) then
  		    vRPclient.notify(player,{"~g~leggendo...."})
  		    vRPclient.playAnim(player,{true,{{"amb@world_human_tourist_mobile@female@enter","enter",1}},false})
  		    SetTimeout(1000,function()
					vRPclient.playAnim(player,{true,{{"amb@world_human_tourist_mobile@female@idle_a","idle_a",1}},true})
					vRP.getSData({"vRP:economy_ill_trs", function(data)
						local economy_trs = json.decode(data) or {}
						local css = [[
							.div_giornale_ill{ 
								position: center;
								margin: auto; 
								padding: 10px; 
								width: 220px; 
								margin-top: 155px; 
								margin-left: 2px;
								background-color: rgba(0,0,0,0.85);
								border-radius:20px;
								color: white; 
								font-family: "Monospace",monospace,monospace;
								font-weight: bold; 
								font-size: 1.5em;
							} 
						]]
					local content = ""
					local count = 0
					for k,v in pairs(economy_trs) do
						local htr = economy_trs[k]
							for g,i in pairs(htr) do 
								count = count+1
								if i ~= nil or g ~= nil then
									if g ~= "timestamp" then
										--local up = vRP.PrendiEconomiaCambiata({tostring(g)})
										css = css..[[.div_giornale_ill .name]]..tostring(g)..[[{color: rgb(12,255,2);}]]
										css = css..[[.div_giornale_ill .prezzo]]..tostring(g)..[[{color: rgb(255,0,0);}]]
										content = content.."<br />".."    ".." <span class=\"name"..tostring(g).."\">"..tostring(g).."</span> => <span class=\"prezzo"..tostring(g).."\">"..tostring(i.out_money).."</span>"

								  end
								end
								count = count-1
						  end
					end

          if count == 0 then
						vRPclient.setDiv(player,{"giornale_ill", css, content})
					end
				end})
  		    end)
  		    SetTimeout(20000,function()
					vRPclient.playAnim(player,{true,{{"amb@world_human_tourist_mobile@female@exit","exit",1}},false})
					--vRP.varyExp({user_id,"science","medicina",2})
					vRPclient.removeDiv(player,{"giornale_ill"})
  		    end)
  		   vRP.closeMenu({player})
  		  end
  	    end
  	  end}
	  return menu
    end,
	 weight = 0.1
	},
	["methblu"] = { 
    name = "Cristalli di meth blu",
    desc = "Cristalli di meth blu.",
    choices = function(args)
	  local menu = {}
      menu["Fuma"] = {function(player,choice)
  	    local user_id = vRP.getUserId({player})
  	    if user_id ~= nil then
  	      if vRP.tryGetInventoryItem({user_id,"methblu",1,false}) then
							vRPclient.notify(player,{"~g~Fumando cristalli di meth."})
							TriggerClientEvent("LSLI:ObjTemp", player, 18905,"prop_bong_01", 12, 0.1, -0.3, 0.07, -100.0, 0.0, 0.0)
							local seq = {
								{"anim@safehouse@bong","bong_stage4",1}
							}
							vRPclient.playAnim(player,{false,seq,false})
							SetTimeout(7500,function()
								TriggerClientEvent("LSLI:PartTemp", player,"cut_trevor1","cs_meth_pipe_smoke",12844,2.0,5, -0.03, 0.05, 0.0, 0.0, 0.0, 0.0)
							end)
							SetTimeout(3500,function()
								--Dclient.setArmour(player,{100})
								Dclient.playMovement(player,{"move_p_m_zero_janitor",false,false,true,false})
								Dclient.playScreenEffect(player, {"DrugsMichaelAliensFightIn", 600})
							end)
							SetTimeout(600000,function()
							Dclient.resetMovement(player,{false})
							end)
							vRP.closeMenu({player})
					 end
  	    end
  	  end}
	  return menu
    end,
	weight = 0.2
	},
	["soia"] = { 
    name = "Nudols di Soia",
    desc = "I noodles nascono in Cina oltre 4mila anni fa e da lì si diffondono in Oriente. Si producono con un impasto di farina di qualunque tipo a cui si aggiunge acqua ed eventualmente uova e con cui si creano dei fili di pasta di spessore variabile a seconda delle tradizioni locali. '",
    choices = function(args)
	  local menu = {}
      menu["Mangia"] = {function(player,choice)
  	    local user_id = vRP.getUserId({player})
  	    if user_id ~= nil then
  	      if vRP.tryGetInventoryItem({user_id,"soia",1,false}) then
							vRPclient.notify(player,{"~g~Mangiando Nudols di Soia..."})
							local seq = {
								{"mp_player_inteat@burger", "mp_player_int_eat_burger_enter",1},
								{"mp_player_inteat@burger", "mp_player_int_eat_burger",1},
								{"mp_player_inteat@burger", "mp_player_int_eat_burger_fp",1},
								{"mp_player_inteat@burger", "mp_player_int_eat_exit_burger",1}
							}
						
							vRPclient.playAnim(player,{true,seq,false})
							SetTimeout(3000,function()
								vRP.varyHunger({user_id,-50}) -- optional
            		vRP.varyThirst({user_id,-30}) -- optional
								vRPclient.ToogleSazio(player,{})
							end)
							vRP.closeMenu({player})
					 end
  	    end
  	  end}
	  return menu
    end,
	weight = 0.1
	},
	["involtino"] = { 
    name = "Involtino primavera",
    desc = "Involtini primavera sono un piatto tipico della cucina cinese, servito come antipasto.'",
    choices = function(args)
	  local menu = {}
      menu["Mangia"] = {function(player,choice)
  	    local user_id = vRP.getUserId({player})
  	    if user_id ~= nil then
  	      if vRP.tryGetInventoryItem({user_id,"involtino",1,false}) then
							vRPclient.notify(player,{"~g~Mangiando Involtino primavera..."})
							Dclient.AttaccaOggettoTemporaneamente(prop_cs_burger_01,1000,true)
                      --      TriggerClientEvent("LSLI:ObjTemp", player, 18905,"prop_cs_burger_01", 12, 0.1, -0.3, 0.07, -100.0, 0.0, 0.0)							
						    local seq = {
								{"mp_player_inteat@burger", "mp_player_int_eat_burger_enter",1},
								{"mp_player_inteat@burger", "mp_player_int_eat_burger",1},
								{"mp_player_inteat@burger", "mp_player_int_eat_burger_fp",1},
								{"mp_player_inteat@burger", "mp_player_int_eat_exit_burger",1}
							}
							vRPclient.playAnim(player,{true,seq,false})
							SetTimeout(3000,function()	
  						    TriggerClientEvent("LSLI:PartTemp", player,"cut_trevor1","cs_meth_pipe_smoke",12844,2.0,5, -0.03, 0.05, 0.0, 0.0, 0.0, 0.0)
							vRP.varyHunger({user_id,-70}) -- optional
            		        vRP.varyThirst({user_id,-5}) -- optional
							vRPclient.ToogleSazio(player,{})
							end)
							vRP.closeMenu({player})
					 end
  	    end
  	  end}
	  return menu
    end,
	weight = 0.1
	},
	["sushi"] = { 
    name = "Sushi",
    desc = "Il sushi è un insieme di piatti tipici della cucina giapponese a base di riso insieme ad altri ingredienti come pesce, alghe, vegetali o uova. Il ripieno può essere crudo, cotto o marinato e può essere servito appoggiato sul riso, arrotolato in una striscia di alga, disposto in rotoli di riso o inserito in una piccola tasca di tofu.",
    choices = function(args)
	  local menu = {}
      menu["Mangia"] = {function(player,choice)
  	    local user_id = vRP.getUserId({player})
  	    if user_id ~= nil then
  	      if vRP.tryGetInventoryItem({user_id,"sushi",1,false}) then
							vRPclient.notify(player,{"~g~Mangiando Sushi..."})
							local seq = {
								{"mp_player_inteat@burger", "mp_player_int_eat_burger_enter",1},
								{"mp_player_inteat@burger", "mp_player_int_eat_burger",1},
								{"mp_player_inteat@burger", "mp_player_int_eat_burger_fp",1},
								{"mp_player_inteat@burger", "mp_player_int_eat_exit_burger",1}
							}
						
							vRPclient.playAnim(player,{true,seq,false})
							SetTimeout(3000,function()
								vRP.varyHunger({user_id,-50}) -- optional
            		vRP.varyThirst({user_id,-50}) -- optional
								vRPclient.ToogleSazio(player,{})
							end)
							vRP.closeMenu({player})
					 end
  	    end
  	  end}
	  return menu
    end,
	weight = 0.1
	},
	["focaccia"] = { 
    name = "Focaccia",
    desc = "La focaccia è un impasto di farina, acqua, sale e lievito, che può essere cotto sia al forno che alla brace.",
    choices = function(args)
	  local menu = {}
      menu["Mangia"] = {function(player,choice)
  	    local user_id = vRP.getUserId({player})
  	    if user_id ~= nil then
  	      if vRP.tryGetInventoryItem({user_id,"focaccia",1,false}) then
							vRPclient.notify(player,{"~g~Mangiando Focaccia..."})
							local seq = {
								{"mp_player_inteat@burger", "mp_player_int_eat_burger_enter",1},
								{"mp_player_inteat@burger", "mp_player_int_eat_burger",1},
								{"mp_player_inteat@burger", "mp_player_int_eat_burger_fp",1},
								{"mp_player_inteat@burger", "mp_player_int_eat_exit_burger",1}
							}
						
							vRPclient.playAnim(player,{true,seq,false})
							SetTimeout(3000,function()
								vRP.varyHunger({user_id,-80}) -- optional
            		vRP.varyThirst({user_id,10}) -- optional
								vRPclient.ToogleSazio(player,{})
							end)
							vRP.closeMenu({player})
					 end
  	    end
  	  end}
	  return menu
    end,
	weight = 0.1
	},
	["pasta"] = { 
    name = "Pasta",
    desc = "La pasta o pasta alimentare è un alimento a base di semola o farina di diversa estrazione, tipico delle varie cucine regionali d'Italia, divisa in piccole forme regolari destinate alla cottura in acqua bollente e sale o con calore umido e salato.",
    choices = function(args)
	  local menu = {}
      menu["Mangia"] = {function(player,choice)
  	    local user_id = vRP.getUserId({player})
  	    if user_id ~= nil then
  	      if vRP.tryGetInventoryItem({user_id,"pasta",1,false}) then
							vRPclient.notify(player,{"~g~Mangiando Pasta..."})
							local seq = {
								{"mp_player_inteat@burger", "mp_player_int_eat_burger_enter",1},
								{"mp_player_inteat@burger", "mp_player_int_eat_burger",1},
								{"mp_player_inteat@burger", "mp_player_int_eat_burger_fp",1},
								{"mp_player_inteat@burger", "mp_player_int_eat_exit_burger",1}
							}
						
							vRPclient.playAnim(player,{true,seq,false})
							SetTimeout(3000,function()
								vRP.varyHunger({user_id,-60}) -- optional
            		vRP.varyThirst({user_id,-20}) -- optional
								vRPclient.ToogleSazio(player,{})
							end)
							vRP.closeMenu({player})
					 end
  	    end
  	  end}
	  return menu
    end,
	weight = 0.1
	},
	["carne"] = { 
    name = "Carne cruda",
    desc = "Carne cruda di un animale selvatico.",
    choices = function(args)
	  local menu = {}
      menu["Mangia"] = {function(player,choice)
  	    local user_id = vRP.getUserId({player})
  	    if user_id ~= nil then
  	      if vRP.tryGetInventoryItem({user_id,"carne",1,false}) then
							vRPclient.notify(player,{"~g~Mangiando carne cruda..."})
							local seq = {
								{"mp_player_inteat@burger", "mp_player_int_eat_burger_enter",1},
								{"mp_player_inteat@burger", "mp_player_int_eat_burger",1},
								{"mp_player_inteat@burger", "mp_player_int_eat_burger_fp",1},
								{"mp_player_inteat@burger", "mp_player_int_eat_exit_burger",1}
							}
						
							vRPclient.playAnim(player,{true,seq,false})
							SetTimeout(3000,function()
								Dclient.playScreenEffect(player, {"RaceTurbo", 600})
							end)
							SetTimeout(120000,function()
							Dclient.resetMovement(player,{false})
							end)
							vRP.closeMenu({player})
					 end
  	    end
  	  end}
	  return menu
    end,
	weight = 0.2
	},
	["erbamedica"] = {
    name = "Erba medica",
    desc = "Dell'ottima Cannabis legale.",
    choices = function(args)
	  local menu = {}
      menu["Fuma"] = {function(player,choice)
  	    local user_id = vRP.getUserId({player})
				if user_id ~= nil then
					vRPclient.isInComa(player,{}, function(in_coma)
						if not in_coma then
							if vRP.tryGetInventoryItem({user_id,"erbamedica",1,false}) then
							vRP.varyhunger({user_id,20})
							vRPclient.notify(player,{"~g~Fumando Erba."})		
  		    			local seq = {
  		      			{"anim@safehouse@bong","bong_stage4",1}
  		    		 	}
							vRPclient.playAnim(player,{true,seq,false})
							vRPclient.varyHealth(player,{20})
							SetTimeout(7500,function()
								TriggerClientEvent("LSLI:PartTemp", player,"cut_trevor1","cs_meth_pipe_smoke",12844,2.0,5, -0.03, 0.05, 0.0, 0.0, 0.0, 0.0)
							end)
							SetTimeout(7500,function()
  		      		Dclient.playMovement(player,{"MOVE_M@DRUNK@SLIGHTLYDRUNK",true,true,false,false})
  		      		Dclient.playScreenEffect(player, {"DrugsMichaelAliensFight", 120})  --- DrugsMichaelAliensFight
  		    		end)
  		    		SetTimeout(60000,function()
  			  		Dclient.resetMovement(player,{false})
  		    		end)
  		    		vRP.closeMenu({player})
						 end
						else
						vRPclient.notify(player,{"~r~Non puoi fumare mentre sei in coma."})
					 end
				  end)
					end
				end}
			return menu
			end,
	 weight = 0.1
	},
	["cocaina_proc"] = {
    name = "Cocaina processata",
    desc = "Sembra talco ma non è.",
    choices = function(args)
	  local menu = {}
      menu["Sniffa"] = {function(player,choice)
  	    local user_id = vRP.getUserId({player})
  	    if user_id ~= nil then
  	      if vRP.tryGetInventoryItem({user_id,"cocaina_proc",1,false}) then
  		    vRPclient.notify(player,{"~g~Sniffando cocaina."})
  		    local seq = {
  		      {"mp_player_int_uppersmoke","mp_player_int_smoke_enter",1},
  		      {"mp_player_int_uppersmoke","mp_player_int_smoke",1},
  		      {"mp_player_int_uppersmoke","mp_player_int_smoke",1},
  		      {"mp_player_int_uppersmoke","mp_player_int_smoke",1},
  		      {"mp_player_int_uppersmoke","mp_player_int_smoke_exit",1}
  		    }
  		    vRPclient.playAnim(player,{true,seq,false})
  		    SetTimeout(3000,function()
  		      Dclient.playMovement(player,{"MOVE_M@DRUNK@MODERATEDRUNK",true,true,false,false})
  		      Dclient.playScreenEffect(player, {"HeistCelebPassBW", 120})
  		    end)
  		    SetTimeout(240000,function()
  			  Dclient.resetMovement(player,{false})
  		    end)
  		    vRP.closeMenu({player})
  		  end
  	    end
  	  end}
	  return menu
    end,
	weight = 0.1
	},
	------------------ALCOLICI
	["birra"] = {
    name = "Birra",
    desc = "Un ottimo bicchiere di birra.",
    choices = function(args)
	  local menu = {}
      menu["Bevi"] = {function(player,choice)
        local user_id = vRP.getUserId({player})
        if user_id ~= nil then
          if vRP.tryGetInventoryItem({user_id,"birra",1,false}) then
			vRP.varyThirst({user_id,-75})
            vRPclient.notify(player,{"~b~Bevendo birra."})
            local seq = {
              {"mp_player_intdrink","intro_bottle",1},
              {"mp_player_intdrink","loop_bottle",1},
              {"mp_player_intdrink","outro_bottle",1}
            }
            vRPclient.playAnim(player,{true,seq,false})
						SetTimeout(5000,function()
			  			Dclient.playMovement(player,{"MOVE_M@DRUNK@MODERATEDRUNK",true,true,false,false})
			  			Dclient.playScreenEffect(player, {"MenuMGSelectionTint", 120})
			  			SetTimeout(120000,function()
			    			Dclient.resetMovement(player,{false})
			  			end)
						end)
            vRP.closeMenu({player})
          end
        end
      end}
	  return menu
    end,
	weight = 0.2
	},
  ["vodka"] = {
    name = "Vodka",
    desc = "La cura per ogni male.",
    choices = function(args)
	  local menu = {}
      menu["Bevi"] = {function(player,choice)
        local user_id = vRP.getUserId({player})
        if user_id ~= nil then
          if vRP.tryGetInventoryItem({user_id,"vodka",1,false}) then
						vRP.varyThirst({user_id,-30})
            vRPclient.notify(player,{"~b~Bevendo vodka."})
            local seq = {
              {"mp_player_intdrink","intro_bottle",1},
              {"mp_player_intdrink","loop_bottle",1},
              {"mp_player_intdrink","outro_bottle",1}
            }
            vRPclient.playAnim(player,{true,seq,false})
			SetTimeout(5000,function()
			  Dclient.playMovement(player,{"MOVE_M@DRUNK@MODERATEDRUNK",true,true,false,false})
			  Dclient.playScreenEffect(player, {"MP_Celeb_Win", 120})
			  SetTimeout(120000,function()
			    Dclient.resetMovement(player,{false})
			  end)
			end)
            vRP.closeMenu({player})
          end
        end
      end}
	  return menu
    end,
	weight = 0.10
	},
	["whisky"] = {
    name = "Whisky",
    desc = "E' forte ehhhhhh??.",
    choices = function(args)
	  local menu = {}
      menu["Bevi"] = {function(player,choice)
        local user_id = vRP.getUserId({player})
        if user_id ~= nil then
          if vRP.tryGetInventoryItem({user_id,"whisky",1,false}) then
						vRP.varyThirst({user_id,-20})
						

            vRPclient.notify(player,{"~b~Bevendo whisky."})
            local seq = {
              {"mp_player_intdrink","intro_bottle",1},
              {"mp_player_intdrink","loop_bottle",1},
              {"mp_player_intdrink","outro_bottle",1}
            }
            vRPclient.playAnim(player,{true,seq,false})
			SetTimeout(5000,function()
			  Dclient.playMovement(player,{"MOVE_M@DRUNK@MODERATEDRUNK_HEAD_UP",true,true,false,false})
			  Dclient.playScreenEffect(player, {"DeathFailMPDark", 120})
			  SetTimeout(120000,function()
			    Dclient.resetMovement(player,{false})
			  end)
			end)
            vRP.closeMenu({player})
          end
        end
      end}
	  return menu
    end,
	weight = 0.10
	},
	["rum"] = {
    name = "Rum",
    desc = "Questa va gustata col ghiaccio.",
    choices = function(args)
	  local menu = {}
      menu["Bevi"] = {function(player,choice)
        local user_id = vRP.getUserId({player})
        if user_id ~= nil then
          if vRP.tryGetInventoryItem({user_id,"rum",1,false}) then
						vRP.varyThirst({user_id,-25})
						

            vRPclient.notify(player,{"~b~Bevendo rum."})
            local seq = {
              {"mp_player_intdrink","intro_bottle",1},
              {"mp_player_intdrink","loop_bottle",1},
              {"mp_player_intdrink","outro_bottle",1}
            }
            vRPclient.playAnim(player,{true,seq,false})
			SetTimeout(5000,function()
			  Dclient.playMovement(player,{"MOVE_M@DRUNK@SLIGHTLYDRUNK",true,true,false,false})
			  Dclient.playScreenEffect(player, {"MP_corona_switch", 120})
			  SetTimeout(120000,function()
			    Dclient.resetMovement(player,{false})
			  end)
			end)
            vRP.closeMenu({player})
          end
        end
      end}
	  return menu
    end,
	weight = 0.10
	},
	["tequila"] = {
    name = "Tequila",
    desc = "Arriba Arriba Andale Arriba.",
    choices = function(args)
	  local menu = {}
      menu["Bevi"] = {function(player,choice)
        local user_id = vRP.getUserId({player})
        if user_id ~= nil then
          if vRP.tryGetInventoryItem({user_id,"tequila",1,false}) then
            vRP.varyThirst({user_id,-35})

            vRPclient.notify(player,{"~b~Bevendo tequila."})
            local seq = {
              {"mp_player_intdrink","intro_bottle",1},
              {"mp_player_intdrink","loop_bottle",1},
              {"mp_player_intdrink","outro_bottle",1}
            }
            vRPclient.playAnim(player,{true,seq,false})
			SetTimeout(5000,function()
			  Dclient.playMovement(player,{"MOVE_M@DRUNK@MODERATEDRUNK_HEAD_UP",true,true,false,false})
			  Dclient.playScreenEffect(player, {"DeathFailMPDark", 120})
			  SetTimeout(120000,function()
			    Dclient.resetMovement(player,{false})
			  end)
			end)
            vRP.closeMenu({player})
          end
        end
      end}
	  return menu
    end,
	weight = 0.5
	},
	["martini"] = {
    name = "Martini",
    desc = "Ti manca solo l'ombrellino.",
    choices = function(args)
	  local menu = {}
      menu["Bevi"] = {function(player,choice)
        local user_id = vRP.getUserId({player})
        if user_id ~= nil then
          if vRP.tryGetInventoryItem({user_id,"martini",1,false}) then
            vRP.varyThirst({user_id,-60})
						
						
            vRPclient.notify(player,{"~b~Bevendo martini."})
            local seq = {
              {"mp_player_intdrink","intro_bottle",1},
              {"mp_player_intdrink","loop_bottle",1},
              {"mp_player_intdrink","outro_bottle",1}
            }
            vRPclient.playAnim(player,{true,seq,false})
			SetTimeout(5000,function()
			  Dclient.playMovement(player,{"MOVE_M@DRUNK@VERYDRUNK",true,true,false,false})
			  Dclient.playScreenEffect(player, {"DeathFailOut", 120})
			  SetTimeout(120000,function()
			    Dclient.resetMovement(player,{false})
			  end)
			end)
            vRP.closeMenu({player})
          end
        end
      end}
	  return menu
    end,
	weight = 0.5
	},
	["paracadute"] = {
    name = "Paracadute",
    desc = "Speriamo si apra.",
    choices = function(args)
	  local menu = {}
      menu["Equipaggia"] = {function(player,choice)
        local user_id = vRP.getUserId({player})
        if user_id ~= nil then
          if vRP.tryGetInventoryItem({user_id,"paracadute",1,false}) then
            vRPclient.notify(player,{"~b~Mettendo il paracadute."})
						vRPclient.playAnim(player,{false,{{"oddjobs@basejump@ig_15","puton_parachute"}},false})
						SetTimeout(2000,function()
							vRPclient.giveWeapons(player,{{["GADGET_PARACHUTE"] = {ammo=1}}})
					 	end)
           vRP.closeMenu({player})
          end
        end
      end}
	  return menu
    end,
	weight = 1.0
	},
	["vino"] = {
    name = "Bicchiere di vino",
    desc = "Del buon vino italiano.",
    choices = function(args)
	  local menu = {}
      menu["Bevi"] = {function(player,choice)
        local user_id = vRP.getUserId({player})
        if user_id ~= nil then
          if vRP.tryGetInventoryItem({user_id,"birra",1,false}) then
						vRP.varyThirst({user_id,-10})
						

            vRPclient.notify(player,{"~b~Bevendo vino..."})
            local seq = {
              {"mp_player_intdrink","intro_bottle",1},
              {"mp_player_intdrink","loop_bottle",1},
              {"mp_player_intdrink","outro_bottle",1}
            }
            vRPclient.playAnim(player,{true,seq,false})
						SetTimeout(5000,function()
			  			Dclient.playMovement(player,{"MOVE_M@DRUNK@MODERATEDRUNK",true,true,false,false})
			  			Dclient.playScreenEffect(player, {"MenuMGSelectionTint", 120})
			  			SetTimeout(120000,function()
			    			Dclient.resetMovement(player,{false})
			  			end)
						end)
            vRP.closeMenu({player})
          end
        end
      end}
	  return menu
    end,
	weight = 0.2
	},
}


return cfg
