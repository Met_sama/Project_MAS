vRP = Proxy.getInterface("vRP")

local Keys = {
  ["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57, 
  ["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177, 
  ["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
  ["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
  ["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
  ["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70, 
  ["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
  ["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
  ["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}


bodybuilderX, bodybuilderY, bodybuilderZ = -1220.4322509766,-1578.2239990234,4.05

function DrawText3D(x,y,z, text, scl) 

    local onScreen,_x,_y=World3dToScreen2d(x,y,z)
    local px,py,pz=table.unpack(GetGameplayCamCoords())
    local dist = GetDistanceBetweenCoords(px,py,pz, x,y,z, 1)
 
    local scale = (1/dist)*scl
    local fov = (1/GetGameplayCamFov())*100
    local scale = scale*fov
   
    if onScreen then
        SetTextScale(0.0*scale, 1.1*scale)
        SetTextFont(1)
        SetTextProportional(1)
        SetTextColour(255, 255, 255, 255)
        SetTextDropshadow(0, 0, 0, 0, 255)
        SetTextEdge(2, 0, 0, 0, 150)
        SetTextDropShadow()
        SetTextOutline()
        SetTextEntry("STRING")
        SetTextCentre(1)
        AddTextComponentString(text)
        DrawText(_x,_y)
    end
end

Citizen.CreateThread(function()
	bodybuilder = 3658575486
	RequestModel( bodybuilder )
	while ( not HasModelLoaded( bodybuilder ) ) do
		Citizen.Wait( 1 )
	end
	thebodybuilder = CreatePed(4, bodybuilder, bodybuilderX, bodybuilderY, bodybuilderZ, 90, false, false)
	SetModelAsNoLongerNeeded(bodybuilder)
	SetEntityHeading(thebodybuilder, -15.0)
	FreezeEntityPosition(thebodybuilder, true)
	SetEntityInvincible(thebodybuilder, true)
	SetBlockingOfNonTemporaryEvents(thebodybuilder, true)
	TaskStartScenarioAtPosition(thebodybuilder, "PROP_HUMAN_SEAT_BENCH", bodybuilderX, bodybuilderY, bodybuilderZ-0.35, GetEntityHeading(thebodybuilder), 0, 0, false)
end)

Citizen.CreateThread(function()
	while true do
		local pos = GetEntityCoords(GetPlayerPed(-1), true)
		if(GetDistanceBetweenCoords(pos.x, pos.y, pos.z, bodybuilderX, bodybuilderY, bodybuilderZ) < 5.5)then
			DrawText3D(bodybuilderX, bodybuilderY, bodybuilderZ+0.8, "~r~Bodybuilder", 1.2)
		end
		Citizen.Wait(0)
	end
end)

function DisplayNotification(string)
	SetTextComponentFormat("STRING")
	AddTextComponentString(string)
	DisplayHelpTextFromStringLabel(0, 0, 2, -1)
end

local chins = {
    {x = -1200.1284,y = -1570.9903,z = 4.6115}
}

local pushup = {
    {x = -1203.3242,y = -1570.6184,z = 4.6115}
}

local arms = {
    {x = -1202.9837,y = -1565.1718,z = 4.6115}
}

local situps = {
    {x = -1206.1055,y = -1565.1589,z = 4.6115}
}

------------------------------------------------------------------------------------------CHINS------------------------------------------------------------------------------------------------------------------------------

Citizen.CreateThread(function()
    while true do
        Citizen.Wait(0)
        for k in pairs(chins) do
            DrawMarker(21, chins[k].x, chins[k].y, chins[k].z, 0, 0, 0, 0, 0, 0, 0.301, 0.301, 0.3001, 0, 255, 50, 200, 0, 0, 0, 0)
        end
    end
end)


Citizen.CreateThread(function()
    while true do
        Citizen.Wait(0)

        for k in pairs(chins) do

            local plyCoords = GetEntityCoords(GetPlayerPed(-1), false)
            local dist = Vdist(plyCoords.x, plyCoords.y, plyCoords.z, chins[k].x, chins[k].y, chins[k].z)

            if dist <= 0.5 then
				DisplayNotification('Premi ~INPUT_CONTEXT~ per fare un pò di ~g~esercizi')
				
				if IsControlJustPressed(0, Keys['E']) then
					
						TriggerServerEvent('funzione_esercizi')
						vRP.notify("Preparandosi a fare esercizi..")
						Citizen.Wait(1000)					
							local playerPed = GetPlayerPed(-1)
							TaskStartScenarioInPlace(playerPed, "prop_human_muscle_chin_ups", 0, true)
							Citizen.Wait(10000)
							ClearPedTasksImmediately(playerPed)
							vRP.notify("Aspetta ~r~60 secondi ~w~prima di fare altri esercizi.")			
					end
				end			
           end
       end
   end)

--------------------------------------------------------------------PUSHUP--------------------------------------------------------------------------------------------------------
Citizen.CreateThread(function()
    while true do
        Citizen.Wait(0)
        for k in pairs(pushup) do
            DrawMarker(21, pushup[k].x, pushup[k].y, pushup[k].z, 0, 0, 0, 0, 0, 0, 0.301, 0.301, 0.3001, 0, 255, 50, 200, 0, 0, 0, 0)
        end
    end
end)


Citizen.CreateThread(function()
    while true do
        Citizen.Wait(0)

        for k in pairs(pushup) do

            local plyCoords = GetEntityCoords(GetPlayerPed(-1), false)
            local dist = Vdist(plyCoords.x, plyCoords.y, plyCoords.z, pushup[k].x, pushup[k].y, pushup[k].z)

            if dist <= 0.5 then
				DisplayNotification('Premi ~INPUT_CONTEXT~ per fare ~g~addominali')
				
				if IsControlJustPressed(0, Keys['E']) then
					
						TriggerServerEvent('funzione_esercizi')
						vRP.notify("Preparandosi a fare ~g~esercizi~w~...")
						Citizen.Wait(1000)							
							local playerPed = GetPlayerPed(-1)
							TaskStartScenarioInPlace(playerPed, "world_human_push_ups", 0, true)
							Citizen.Wait(10000)
							ClearPedTasksImmediately(playerPed)
							vRP.notify("Aspetta ~r~60 secondi ~w~prima di fare altri esercizi.")			
						
					end
				end			
           end
       end
   end)
   
---------------------------------------------------------------------ARMS-------------------------------------------------------------------------------------------------------  
Citizen.CreateThread(function()
    while true do
        Citizen.Wait(0)
        for k in pairs(arms) do
            DrawMarker(21, arms[k].x, arms[k].y, arms[k].z, 0, 0, 0, 0, 0, 0, 0.301, 0.301, 0.3001, 0, 255, 50, 200, 0, 0, 0, 0)
        end
    end
end)


Citizen.CreateThread(function()
    while true do
        Citizen.Wait(0)

        for k in pairs(arms) do

            local plyCoords = GetEntityCoords(GetPlayerPed(-1), false)
            local dist = Vdist(plyCoords.x, plyCoords.y, plyCoords.z, arms[k].x, arms[k].y, arms[k].z)

            if dist <= 0.5 then
				DisplayNotification('Premi ~INPUT_CONTEXT~ per allenare le ~g~braccia')
				
				if IsControlJustPressed(0, Keys['E']) then
					
						TriggerServerEvent('funzione_esercizi')
						vRP.notify("Preparandosi a fare ~g~esercizi~w~...")
						Citizen.Wait(1000)					
					
							local playerPed = GetPlayerPed(-1)
							TaskStartScenarioInPlace(playerPed, "world_human_muscle_free_weights", 0, true)
							Citizen.Wait(10000)
							ClearPedTasksImmediately(playerPed)
							vRP.notify("Aspetta ~r~60 secondi ~w~prima di fare altri esercizi.")			
							
					end
				end			
        end
    end
end)

---------------------------------------------------------------------SITUPS-------------------------------------------------------------------------------------------------------  
Citizen.CreateThread(function()
    while true do
        Citizen.Wait(0)
        for k in pairs(situps) do
            DrawMarker(21, situps[k].x, situps[k].y, situps[k].z, 0, 0, 0, 0, 0, 0, 0.301, 0.301, 0.3001, 0, 255, 50, 200, 0, 0, 0, 0)
        end
    end
end)


Citizen.CreateThread(function()
    while true do
        Citizen.Wait(0)

        for k in pairs(situps) do

            local plyCoords = GetEntityCoords(GetPlayerPed(-1), false)
            local dist = Vdist(plyCoords.x, plyCoords.y, plyCoords.z, situps[k].x, situps[k].y, situps[k].z)

            if dist <= 0.5 then
				DisplayNotification('Premi ~INPUT_CONTEXT~ per fare gli ~g~pushup')
				
				if IsControlJustPressed(0, Keys['E']) then

						TriggerServerEvent('funzione_esercizi')
						vRP.notify("Preparandosi a fare ~g~esercizi~w~...")
						Citizen.Wait(1000)					
					
							local playerPed = GetPlayerPed(-1)
							TaskStartScenarioInPlace(playerPed, "world_human_sit_ups", 0, true)
							Citizen.Wait(10000)
							ClearPedTasksImmediately(playerPed)
							vRP.notify("Aspetta ~r~60 secondi ~w~prima di fare altri esercizi.")			

					end
				end			
        end
    end
end)



