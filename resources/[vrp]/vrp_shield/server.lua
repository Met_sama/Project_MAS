--Settings--
local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")

vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP","vRP_shield")

RegisterServerEvent('shield:command')
AddEventHandler('shield:command', function(dirt)
	local user_id = vRP.getUserId({source})
	local player = vRP.getUserSource({user_id})
	  if vRP.hasPermission({user_id,"police.wanted"}) then
		TriggerClientEvent('shield:enable', player)
	  end	
end)
