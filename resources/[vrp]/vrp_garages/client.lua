--[[Proxy/Tunnel]]--

vRPgt = {}
Tunnel.bindInterface("vRP_garages",vRPgt)
Proxy.addInterface("vRP_garages",vRPgt)
GTserver = Tunnel.getInterface("vrp_garages","vrp_garages")
vRP = Proxy.getInterface("vRP")

--[[Local/Global]]--

GVEHICLES = {}
local inrangeofgarage = false
local currentlocation = nil

local garages = {
    --{name="Garage", colour=3, id=50, x=215.124, y=-791.377, z=30.646, h=0.0},
    --{name="Garage", colour=3, id=50, x=-334.685, y=289.773, z=84.705, h=0.0},
    --{name="Garage", colour=3, id=50, x=-55.272, y=-1838.71, z=25.442, h=0.0},
    --{name="Garage", colour=3, id=50, x=126.434, y=6610.04, z=30.750, h=0.0},
    --{name="Garage", colour=3, id=50, x=-626.11346435547, y=56.508075714111, z=43.727066040039, h=0.0},
	  --{name="Garage", colour=3, id=50, x=1387.8536376953, y=-578.04730224609, z=74.338775634766, h=0.0},
    --{name="Garage", colour=3, id=50, x=288.9680480957, y=-338.53717041016, z=44.919883728027, h=0.0},
	  --{name="Garage", colour=3, id=0, x=-557.45953369141, y=-1663.5657958984, z=19.206163406372, h=0.0},
    --Casa Mafia
    {name="Garage", colour=38, id=50, x=1410.3022460938, y=1117.7930908203, z=114.83800506592, h=0.0},
	  --{name="Garage", colour=3, id=50, x=-189.15110778809, y=501.62377929688, z=134.48828125, h=0.0},
	  --{name="Garage", colour=3, id=50, x=353.81744384766, y=437.37255859375, z=146.6767578125, h=0.0},
	  --{name="Garage", colour=3, id=50, x=391.20950317383, y=430.60192871094, z=143.57209777832, h=0.0},
    --{name="Garage", colour=3, id=50, x=-796.17144775391, y=323.05972290039, z=85.700492858887, h=0.0},
	  --{name="Garage", colour=3, id=0, x=-2596.6850585938, y=1930.0020751953, z=167.30807495117, h=0.0},
	  --{name="Garage", colour=3, id=50, x=484.4739074707, y=-3161.9418945313, z=6.0695567131042, h=0.0},
    --{name="Garage", colour=3, id=50, x=-301.05477905273, y=-986.08465576172, z=31.080602645874, h=0.0},
    --{name="Garage", colour=3, id=50, x=-59.516288757324, y=-1115.2729492188, z=26.435451507568, h=0.0},
	  --{name="Garage", colour=3, id=50, x=899.27801513672, y=-143.61279296875, z=76.753890991211, h=0.0},
	  --{name="Garage", colour=3, id=50, x=1199.2978515625, y=-3228.8532714844, z=5.9218320846558, h=0.0},
	  --{name="Garage", colour=3, id=50, x=-123.53047180176, y=510.26724243164, z=142.79475402832, h=0.0},
    --{name="Garage", colour=3, id=50, x=-76.887107849121, y=496.79095458984, z=144.42790222168, h=0.0},
	  --{name="Garage", colour=3, id=50, x=490.29595947266, y=-1333.6713867188, z=29.330997467041, h=0.0}, 
    --{name="Garage", colour=3, id=50, x=13.651403427124, y=548.30401611328, z=176.13258361816, h=0.0},
    --{name="Garage", colour=3, id=50, x=-821.22534179688, y=183.43142700195, z=72.002914428711, h=0.0},
	  --{name="Garage", colour=3, id=50, x=-297.82165527344, y=-686.20452880859, z=32.817485809326, h=0.0},
	  --{name="Garage", colour=3, id=50, x=-829.45300292969, y=-440.93173217773, z=36.639873504639, h=0.0},
    --{name="Garage", colour=3, id=50, x=-869.73468017578, y=-377.02499389648, z=39.301494598389, h=0.0},
    --{name="Garage", colour=3, id=50, x=-1416.0484619141, y=-956.61761474609, z=7.234516620636, h=0.0},
    --{name="Garage", colour=3, id=50, x=-23.499629974365, y=-1437.7495117188, z=30.654481887817, h=0.0},
	  --{name="Garage", colour=3, id=50, x=504.4176940918, y=-1705.8890380859, z=29.358783721924, h=0.0},
    
    --{name="Garage", colour=3, id=50, x=-260.68518066406, y=-2091.0590820313, z=27.620203018188, h=0.0},
    --Garage Areoporto
    --*{name="Garage", colour=38, id=50, x=-926.25189208984, y=-2440.1579589844, z=13.843800544739, h=0.0},
    --*{name="Garage", colour=38, id=50, x=-809.15930175782, y=-2388.7941894532, z=14.570718765258, h=0.0},
	  --{name="Garage", colour=3, id=50, x=-813.92321777344, y=-1310.6514892578, z=5.0003814697266, h=0.0},
	  --{name="Garage", colour=3, id=50, x=3805.9345703125, y=4462.919921875, z=5.0003814697266, h=0.0},
	  --{name="Garage", colour=3, id=50, x=-772.01452636719, y=-1464.4467773438, z=4.5348782539368, h=0.0},
	  --{name="Garage", colour=3, id=50, x=1981.8479003906, y=3828.0961914063, z=32.433067321777, h=0.0},
	  --{name="Garage", colour=3, id=50, x=2480.5139160156, y=4954.0595703125, z=45.018207550049, h=0.0},
    --{name="Garage", colour=3, id=50, x=-13.111194610596, y=-1103.138305664, z=26.672073364258, h=0.0},	
    --Garage Citta sud
    {name="Garage", colour=38, id=50, x=120.20216369628, y=-1058.4989013672, z=29.192352294922, h=0.0},	
    {name="Garage", colour=38, id=50, x=148.24276733398, y=-1074.228515625, z=29.192352294922, h=0.0},
    {name="Garage", colour=38, id=50, x=238.2312927246, y=-775.71392822266, z=30.696422576904, h=0.0},
    {name="Garage", colour=38, id=50, x=223.58192443848, y=-767.19732666016, z=30.8036403656, h=0.0},
    {name="Garage", colour=38, id=50, x=2643.1547851562, y=3508.7895507812, z=53.66189956665	, h=0.0},
 --   {name="Garage", colour=38, id=50, x=-1173.9313964844, y=-1772.920288086, z=3.8476538658142	, h=0.0},
    --Garage Citta mid
    {name="Garage", colour=38, id=50, x=2012.752319336, y=3060.8959960938, z=47.049983978272, h=0.0},
	
    {name="Garage", colour=0, id=0, x=434.26824951172, y=-1012.8507080078, z=28.717012405396, h=0.0},	
	
    {name="Garage", colour=38, id=50, x=29.012350082398, y=-1585.9881591796, z=29.157842636108, h=0.0},
	
    --Garage Citta nord
    {name="Garage", colour=38, id=50, x=-272.25985717774, y=6061.6845703125, z=31.464525222778, h=0.0},
    {name="Garage", colour=38, id=50, x=-551.78399658204, y=304.61608886718, z=83.21859741211, h=0.0},
    -- Luke
--    {name="Garage", colour=38, id=50, x=2055.6433105468, y=3284.8254394532, z=45.532024383544, h=0.0},		
	
    {name="Garage Concessionario", colour=4, id=50, x=-12.112083435058, y=-1105.4821777344, z=26.67207145691, h=0.0},

	
	
	
}

vehicles = {}
garageSelected = { {x=nil, y=nil, z=nil, h=nil}, }
selectedPage = 0

lang_string = {
menu1 = "Parcheggia Veicolo",
menu2 = "Preleva veicolo",
menu3 = "Chiudi",
menu4 = "Veicoli",
menu5 = "Opzioni",
menu6 = "Prendi",
menu7 = "Indietro",
menu8 = "~g~E~s~ per aprire il menu",
menu9 = "Sell",
menu10 = "~g~E~s~ to sell the vehicle at 50% of the purchase price",
menu11 = "Update the vehicle",
menu12 = "Avanti",
state1 = "Out",
state2 = "In",
text1 = "The area is crowded",
text2 = "Questo veicolo non è nel garage",
text3 = "Veicolo pronto",
text4 = "Non è il tuo veicolo cazzo fai coglione",
text5 = "Veicolo in garage",
text6 = "Nessun veicolo presente",
text7 = "Veicolo venduto",
text8 = "Vehicle bought, good drive",
text9 = "Insufficient funds",
text10 = "Vehicle updated"
}
--[[Functions]]--

function vRPgt.spawnGarageVehicle(vtype, name, vehicle_plate, vehicle_colorprimary, vehicle_colorsecondary, vehicle_pearlescentcolor, vehicle_wheelcolor, vehicle_plateindex, vehicle_neoncolor1, vehicle_neoncolor2, vehicle_neoncolor3, vehicle_windowtint, vehicle_wheeltype, vehicle_mods0, vehicle_mods1, vehicle_mods2, vehicle_mods3, vehicle_mods4, vehicle_mods5, vehicle_mods6, vehicle_mods7, vehicle_mods8, vehicle_mods9, vehicle_mods10, vehicle_mods11, vehicle_mods12, vehicle_mods13, vehicle_mods14, vehicle_mods15, vehicle_mods16, vehicle_turbo, vehicle_tiresmoke, vehicle_xenon, vehicle_mods23, vehicle_mods24, vehicle_neon0, vehicle_neon1, vehicle_neon2, vehicle_neon3, vehicle_bulletproof, vehicle_smokecolor1, vehicle_smokecolor2, vehicle_smokecolor3, vehicle_modvariation) -- vtype is the vehicle type (one vehicle per type allowed at the same time)

  local vehicle = vehicles[vtype]
  if vehicle and not IsVehicleDriveable(vehicle[3]) then -- precheck if vehicle is undriveable
    -- despawn vehicle
    SetVehicleHasBeenOwnedByPlayer(vehicle[3],false)
    Citizen.InvokeNative(0xAD738C3085FE7E11, vehicle[3], false, true) -- set not as mission entity
    SetVehicleAsNoLongerNeeded(Citizen.PointerValueIntInitialized(vehicle[3]))
    Citizen.InvokeNative(0xEA386986E786A54F, Citizen.PointerValueIntInitialized(vehicle[3]))
	TriggerEvent("vrp_garages:setVehicle", vtype, nil)
  end

  vehicle = vehicles[vtype]
  if vehicle == nil then
    -- load vehicle model
    local mhash = GetHashKey(name)

    local i = 0
    while not HasModelLoaded(mhash) and i < 10000 do
      RequestModel(mhash)
      Citizen.Wait(10)
      i = i+1
    end

    -- spawn car
    if HasModelLoaded(mhash) then
      local x,y,z = vRP.getPosition({})
      local nveh = CreateVehicle(mhash, x,y,z+0.5, GetEntityHeading(GetPlayerPed(-1)), true, false) -- added player heading
      SetVehicleOnGroundProperly(nveh)
      SetEntityInvincible(nveh,false)
      SetPedIntoVehicle(GetPlayerPed(-1),nveh,-1) -- put player inside
      SetVehicleNumberPlateText(nveh, "P " .. vRP.getRegistrationNumber({}))
      Citizen.InvokeNative(0xAD738C3085FE7E11, nveh, true, true) -- set as mission entity
      SetVehicleHasBeenOwnedByPlayer(nveh,true)

      vRP.setVehicle({nveh,name,{name,nveh}})
      vehicles[vtype] = {vtype,name,nveh}
      TriggerEvent("vrp_garages:setVehicle", vtype, {vtype,name,nveh})

      SetModelAsNoLongerNeeded(mhash)
	  
	  --grabbing customization
      local plate = plate
      local primarycolor = tonumber(vehicle_colorprimary)
      local secondarycolor = tonumber(vehicle_colorsecondary)
      local pearlescentcolor = vehicle_pearlescentcolor
      local wheelcolor = vehicle_wheelcolor
      local plateindex = tonumber(vehicle_plateindex)
      local neoncolor = {vehicle_neoncolor1,vehicle_neoncolor2,vehicle_neoncolor3}
      local windowtint = vehicle_windowtint
      local wheeltype = tonumber(vehicle_wheeltype)
      local mods0 = tonumber(vehicle_mods0)
      local mods1 = tonumber(vehicle_mods1)
      local mods2 = tonumber(vehicle_mods2)
      local mods3 = tonumber(vehicle_mods3)
      local mods4 = tonumber(vehicle_mods4)
      local mods5 = tonumber(vehicle_mods5)
      local mods6 = tonumber(vehicle_mods6)
      local mods7 = tonumber(vehicle_mods7)
      local mods8 = tonumber(vehicle_mods8)
      local mods9 = tonumber(vehicle_mods9)
      local mods10 = tonumber(vehicle_mods10)
      local mods11 = tonumber(vehicle_mods11)
      local mods12 = tonumber(vehicle_mods12)
      local mods13 = tonumber(vehicle_mods13)
      local mods14 = tonumber(vehicle_mods14)
      local mods15 = tonumber(vehicle_mods15)
      local mods16 = tonumber(vehicle_mods16)
      local turbo = vehicle_turbo
      local tiresmoke = vehicle_tiresmoke
      local xenon = vehicle_xenon
      local mods23 = tonumber(vehicle_mods23)
      local mods24 = tonumber(vehicle_mods24)
      local neon0 = vehicle_neon0
      local neon1 = vehicle_neon1
      local neon2 = vehicle_neon2
      local neon3 = vehicle_neon3
      local bulletproof = vehicle_bulletproof
      local smokecolor1 = vehicle_smokecolor1
      local smokecolor2 = vehicle_smokecolor2
      local smokecolor3 = vehicle_smokecolor3
      local variation = vehicle_modvariation
      local livery = vehicle_livery
      local livery2 = vehicle_livery2
      local extra1 = vehicle_extra1
      local extra2 = vehicle_extra2
      local extra3 = vehicle_extra3
      local extra4 = vehicle_extra4
      local extra5 = vehicle_extra5
      local extra6 = vehicle_extra6
      local extra7 = vehicle_extra7
	  
	  --setting customization
      SetVehicleColours(nveh, primarycolor, secondarycolor)
      SetVehicleExtraColours(nveh, tonumber(pearlescentcolor), tonumber(wheelcolor))
      SetVehicleNumberPlateTextIndex(nveh,plateindex)
      SetVehicleNeonLightsColour(nveh,tonumber(neoncolor[1]),tonumber(neoncolor[2]),tonumber(neoncolor[3]))
      SetVehicleTyreSmokeColor(nveh,tonumber(smokecolor1),tonumber(smokecolor2),tonumber(smokecolor3))
      SetVehicleModKit(nveh,0)
      SetVehicleMod(nveh, 0, mods0)
      SetVehicleMod(nveh, 1, mods1)
      SetVehicleMod(nveh, 2, mods2)
      SetVehicleMod(nveh, 3, mods3)
      SetVehicleMod(nveh, 4, mods4)
      SetVehicleMod(nveh, 5, mods5)
      SetVehicleMod(nveh, 6, mods6)
      SetVehicleMod(nveh, 7, mods7)
      SetVehicleMod(nveh, 8, mods8)
      SetVehicleMod(nveh, 9, mods9)
      SetVehicleMod(nveh, 10, mods10)
      SetVehicleMod(nveh, 11, mods11)
      SetVehicleMod(nveh, 12, mods12)
      SetVehicleMod(nveh, 13, mods13)
      SetVehicleMod(nveh, 14, mods14)
      SetVehicleMod(nveh, 15, mods15)
      SetVehicleMod(nveh, 16, mods16)

      if turbo == "on" then
        ToggleVehicleMod(nveh, 18, true)
      else          
        ToggleVehicleMod(nveh, 18, false)
      end
      if tiresmoke == "on" then
        ToggleVehicleMod(nveh, 20, true)
      else          
        ToggleVehicleMod(nveh, 20, false)
      end
      if xenon == "on" then
        ToggleVehicleMod(nveh, 22, true)
      else          
        ToggleVehicleMod(nveh, 22, false)
      end
		SetVehicleWheelType(nveh, tonumber(wheeltype))
      SetVehicleMod(nveh, 23, mods23)
      SetVehicleMod(nveh, 24, mods24)
      if neon0 == "on" then
        SetVehicleNeonLightEnabled(nveh,0, true)
      else
        SetVehicleNeonLightEnabled(nveh,0, false)
      end
      if neon1 == "on" then
        SetVehicleNeonLightEnabled(nveh,1, true)
      else
        SetVehicleNeonLightEnabled(nveh,1, false)
      end
      if neon2 == "on" then
        SetVehicleNeonLightEnabled(nveh,2, true)
      else
        SetVehicleNeonLightEnabled(nveh,2, false)
      end
      if neon3 == "on" then
        SetVehicleNeonLightEnabled(nveh,3, true)
      else
        SetVehicleNeonLightEnabled(nveh,3, false)
      end
      if bulletproof == "on" then
        SetVehicleTyresCanBurst(nveh, false)
      else
        SetVehicleTyresCanBurst(nveh, true)
      end
      --for i = 1, 30 do 
       -- if DoesExtraExist( nveh, i) then 
       --   SetVehicleExtra( nveh, i, true )
       -- end 
      --end 
      if extra1 == "on" then 
        SetVehicleExtra( nveh, 1, false )
      else
        SetVehicleExtra( nveh, 1, true )
      end 
      if extra2 == "on" then 
        SetVehicleExtra( nveh, 2, false )
      else
        SetVehicleExtra( nveh, 2, true )
      end 
      if extra3 == "on" then 
        SetVehicleExtra( nveh, 3, false )
      else
        SetVehicleExtra( nveh, 3, true )
      end 
      if extra4 == "on" then 
        SetVehicleExtra( nveh, 4, false )
      else
        SetVehicleExtra( nveh, 4, true )
      end 
      if extra5 == "on" then 
        SetVehicleExtra( nveh, 5, false )
      else
        SetVehicleExtra( nveh, 5, true )
      end 
      if extra6 == "on" then 
        SetVehicleExtra( nveh, 6, false )
      else
        SetVehicleExtra( nveh, 6, true )
      end 
      if extra7 == "on" then 
        SetVehicleExtra( nveh, 7, false )
      else
        SetVehicleExtra( nveh, 7, true )
      end 
      --if variation == "on" then
      --  SetVehicleModVariation(nveh,23)
      --else
      --  SetVehicleModVariation(nveh,23, false)
      --end
      SetVehicleMod(nveh, 48, tonumber(livery))
      SetVehicleLivery(nveh, tonumber(livery2))
      SetVehicleWindowTint(nveh,tonumber(windowtint))
    end
  else
    vRP.notify({"Puoi avere solo un veicolo fuori."})
  end
end

function vRPgt.spawnBoughtVehicle(vtype, name)

  local vehicle = vehicles[vtype]
  if vehicle then -- precheck if vehicle is undriveable
    -- despawn vehicle
    SetVehicleHasBeenOwnedByPlayer(vehicle[3],false)
    Citizen.InvokeNative(0xAD738C3085FE7E11, vehicle[3], false, true) -- set not as mission entity
    SetVehicleAsNoLongerNeeded(Citizen.PointerValueIntInitialized(vehicle[3]))
    Citizen.InvokeNative(0xEA386986E786A54F, Citizen.PointerValueIntInitialized(vehicle[3]))
	TriggerEvent("vrp_garages:setVehicle", vtype, nil)
  end

  vehicle = vehicles[vtype]
  if vehicle == nil then
    -- load vehicle model
    local mhash = GetHashKey(name)

    local i = 0
    while not HasModelLoaded(mhash) and i < 10000 do
      RequestModel(mhash)
      Citizen.Wait(10)
      i = i+1
    end

    -- spawn car
    if HasModelLoaded(mhash) then
      local x,y,z = vRP.getPosition({})
      local nveh = CreateVehicle(mhash, x,y,z+0.5, GetEntityHeading(GetPlayerPed(-1)), true, false) -- added player heading
      SetVehicleOnGroundProperly(nveh)
      SetEntityInvincible(nveh,false)
      SetPedIntoVehicle(GetPlayerPed(-1),nveh,-1) -- put player inside
      SetVehicleNumberPlateText(nveh, "P " .. vRP.getRegistrationNumber({}))
      Citizen.InvokeNative(0xAD738C3085FE7E11, nveh, true, true) -- set as mission entity
      SetVehicleHasBeenOwnedByPlayer(nveh,true)
	  vehicle_migration = false
      if not vehicle_migration then
        local nid = NetworkGetNetworkIdFromEntity(nveh)
        SetNetworkIdCanMigrate(nid,false)
      end

	  TriggerEvent("vrp_garages:setVehicle", vtype, {vtype,name,nveh})

      SetModelAsNoLongerNeeded(mhash)
    end
  else
    vRP.notify({"You can only have one "..vtype.." vehicule out."})
  end
end

function vRPgt.despawnGarageVehicle(vtype,max_range)
  local vehicle = vehicles[vtype]
  if vehicle then
    local x,y,z = table.unpack(GetEntityCoords(vehicle[3],true))
    local px,py,pz = vRP.getPosition()

    if GetDistanceBetweenCoords(x,y,z,px,py,pz,true) < max_range then -- check distance with the vehicule
      -- remove vehicle
      SetVehicleHasBeenOwnedByPlayer(vehicle[3],false)
      Citizen.InvokeNative(0xAD738C3085FE7E11, vehicle[3], false, true) -- set not as mission entity
      SetVehicleAsNoLongerNeeded(Citizen.PointerValueIntInitialized(vehicle[3]))
      Citizen.InvokeNative(0xEA386986E786A54F, Citizen.PointerValueIntInitialized(vehicle[3]))
	  TriggerEvent("vrp_garages:setVehicle", vtype, nil)
      vRP.notify({"~g~Veicolo depositato."})
    else
      vRP.notify({"~r~Troppo lontano dal veicolo."})
    end
  else
  vRP.notify({"~r~Non hai un veicolo personale."})
  end
end

function MenuGarage()
    ped = GetPlayerPed(-1)
	selectedPage = 0
    MenuTitle = "Garage"
    ClearMenu()
    Menu.addButton(lang_string.menu1,"StoreVehicle",nil)
    Menu.addButton(lang_string.menu2,"ListVehicle",selectedPage)
    Menu.addButton(lang_string.menu3,"CloseMenu",nil) 
end

function StoreVehicle()
  Citizen.CreateThread(function()
    local caissei = GetClosestVehicle(garageSelected.x, garageSelected.y, garageSelected.z, 3.000, 0, 70)
    SetEntityAsMissionEntity(caissei, true, true)
    local plate = GetVehicleNumberPlateText(caissei)
	local vtype = "car"
	if IsThisModelABike(GetEntityModel(caissei)) then
		vtype = "bike"
	end
    if DoesEntityExist(caissei) then
      TriggerServerEvent('ply_garages:CheckForVeh', plate, vehicles[vtype][2], vtype)
    else
      drawNotification(lang_string.text6)
    end   
  end)
  CloseMenu()
end

function ListVehicle(page)
    ped = GetPlayerPed(-1)
	selectedPage = page
    MenuTitle = lang_string.menu4
    ClearMenu()
	local count = 0
    for ind, value in pairs(GVEHICLES) do
	  if ((count >= (page*10)) and (count < ((page*10)+10))) then
        Menu.addButton(tostring(value.vehicle_name), "OptionVehicle", value.vehicle_name)
	  end
	  count = count + 1
    end   
    Menu.addButton(lang_string.menu12,"ListVehicle",page+1)
	if page == 0 then
      Menu.addButton(lang_string.menu7,"MenuGarage",nil)
	else
      Menu.addButton(lang_string.menu7,"ListVehicle",page-1)
	end
end

function OptionVehicle(vehID)
  local vehID = vehID
    MenuTitle = "Options"
    ClearMenu()
    Menu.addButton(lang_string.menu6, "SpawnVehicle", vehID)
    Menu.addButton(lang_string.menu7, "ListVehicle", selectedPage)
end

function SpawnVehicle(vehID)
  local vehID = vehID
  GTserver.CheckForSpawnVeh({vehID})
  CloseMenu()
end


function drawNotification(text)
  SetNotificationTextEntry("STRING")
  AddTextComponentString(text)
  DrawNotification(false, false)
end

function CloseMenu()
    Menu.hidden = true    
    TriggerServerEvent("ply_garages:CheckGarageForVeh")
end

function LocalPed()
  return GetPlayerPed(-1)
end

function IsPlayerInRangeOfGarage()
  return inrangeofgarage
end

function Chat(debugg)
    TriggerEvent("chatMessage", '', { 0, 0x99, 255 }, tostring(debugg))
end

function ply_drawTxt(text,font,centre,x,y,scale,r,g,b,a)
  SetTextFont(font)
  SetTextProportional(0)
  SetTextScale(scale, scale)
  SetTextColour(r, g, b, a)
  SetTextDropShadow(0, 0, 0, 0,255)
  SetTextEdge(1, 0, 0, 0, 255)
  SetTextDropShadow()
  SetTextOutline()
  SetTextCentre(centre)
  SetTextEntry("STRING")
  AddTextComponentString(text)
  DrawText(x , y)
end

--[[Citizen]]--

Citizen.CreateThread(function()
  while true do
    Citizen.Wait(0)
    for _, garage in pairs(garages) do
      DrawMarker(36, garage.x, garage.y, garage.z, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.5001, 1555, 212, 210,210, true, true, 0,0)	
      if GetDistanceBetweenCoords(garage.x, garage.y, garage.z, GetEntityCoords(LocalPed())) < 3 and IsPedInAnyVehicle(LocalPed(), true) == false then
        ply_drawTxt(lang_string.menu8,0,1,0.5,0.8,0.6,255,255,255,255)
        if IsControlJustPressed(1, 86) then
          garageSelected.x = garage.x
          garageSelected.y = garage.y
          garageSelected.z = garage.z
          MenuGarage()
          Menu.hidden = not Menu.hidden 
        end
        Menu.renderGUI() 
      end
    end
  end
end)

Citizen.CreateThread(function()
  while true do
    local near = false
    Citizen.Wait(0)
    for _, garage in pairs(garages) do    
      if (GetDistanceBetweenCoords(garage.x, garage.y, garage.z, GetEntityCoords(LocalPed())) < 3 and near ~= true) then 
        near = true             
      end
    end
    if near == false then 
      Menu.hidden = true
    end
  end
end)

Citizen.CreateThread(function()
    for _, item in pairs(garages) do
    item.blip = AddBlipForCoord(item.x, item.y, item.z)
    SetBlipSprite(item.blip, item.id)
	SetBlipDisplay(item.blip, 4)
    SetBlipScale(item.blip, 0.8)
    SetBlipAsShortRange(item.blip, true)
    SetBlipColour(item.blip, item.colour)
    BeginTextCommandSetBlipName("STRING")
    AddTextComponentString(item.name)
    EndTextCommandSetBlipName(item.blip)
    end
end)

--[[Events]]--

RegisterNetEvent('vrp_garages:setVehicle')
AddEventHandler('vrp_garages:setVehicle', function(vtype, vehicle)
	vehicles[vtype] = vehicle
end)

RegisterNetEvent('ply_garages:addAptGarage')
AddEventHandler('ply_garages:addAptGarage', function(gx,gy,gz,gh)
local alreadyExists = false
for _, garage in pairs(garages) do
	if garage.x == gx and garage.y == gy then
		alreadyExists = true
	end
end
if not alreadyExists then
	table.insert(garages, #garages + 1, {name="Apartment Garage", colour=3, id=50, x=gx, y=gy, z=gz, h=gh})
end
end)

RegisterNetEvent('ply_garages:getVehicles')
AddEventHandler("ply_garages:getVehicles", function(THEVEHICLES)
    GVEHICLES = {}
    GVEHICLES = THEVEHICLES
end)

AddEventHandler("playerSpawned", function()
    TriggerServerEvent("ply_garages:CheckGarageForVeh")
    TriggerServerEvent("ply_garages:CheckForAptGarages")
end)
