var tableauQuestion = [
    {   question : "Se stai guidando a 80km/h, e sei nelle vicinanze di una zona residenziale, cosa devi fare?",
        propositionA : "Devi aumentare la velocità",
        propositionB : "Puoi mantenere la velocità costante finchè non sono presenti altri veicoli",
        propositionC : "Devi Decellerare la velocità prima di approcciare la zona residenziale",
        propositionD : "Puoi Mantenere la tua velocità",
        reponse : "C"},
		
    {   question : "Mentre stai svoltando a sinistra con il semaforo verde, intravedi un pedone sulle strisce. Chi ha la precedenza?",
        propositionA : "Il Pedone, Perchè possono attraversare indipendentemente dal colore del semafero",
        propositionB : "Il tuo veicolo perchè la luce del semafero è verde",
        propositionC : "Il pedone, perchè lui/lei ha la precedenza avendo il semafero verde",
        propositionD : "Il tuo Veicolo, perchè i guidatori sono sempre nel giusto",
        reponse : "C"},
		
    {   question : "A meno che non sia segnalato, la velocità massima in un area residenziale è di ____ kmh.",
        propositionA : "50",
        propositionB : "55",
        propositionC : "65",
        propositionD : "70",
        reponse : "A"},
	
	{   question : "Prima di Ogni cambio corsia, Dovresti.",
        propositionA : "Controllare gli Specchietti",
        propositionB : "Controllare i punti cechi",
        propositionC : "Segnalare le tue intenzioni",
        propositionD : "Tutte le azioni elencate precedentemente",
        reponse : "D"},
		
	{   question : "Qualè il tasso alcolico consentito per potersi mettere alla guida?",
        propositionA : "0.05%",
        propositionB : "0.18%",
        propositionC : "0.08%",
        propositionD : "0.06%",
        reponse : "C"},
		
	{   question : "Quando un veicolo di emergenza ha le sirene accese dovresti accostarti e fermarti, a meno che:",
        propositionA : "Se sei di fretta",
        propositionB : "C'è abbastanza spazio nelle altre corsie per permettere il passaggio",
        propositionC : "Sei in una zona scolastica",
        propositionD : "Sei in un incrocio",
        reponse : "D"},
	
	{   question : "Il diritto di Precedenza deve essere ceduto ai veicoli di emergenza",
        propositionA : "SEMPRE!",
        propositionB : "In presenza delle luci blu, bianche, rosse",
        propositionC : "Con l'utilizzo delle sirene, luci blu, bianche e rosse",
        propositionD : "Con l'utilizzo delle sirene",
        reponse : "C"},
		
	{   question : "Quale tra queste azioni è consentita quando sorpassi un veicolo?",
        propositionA : "Non Mantenendo la distanza di sicurezza",
        propositionB : "Usciendo dalla strada principale",
        propositionC : "Invadendo il senso opposto della strada se la striscia presente sulla strada è tratteggiata",
        propositionD : "Superando il limite di velocità",
        reponse : "C"},
		
	{   question : "Stai guidando in autostrada con una velocità massima segnalata di 180 kmh. La maggior parte del traffico sta viaggiando a  120 mph, quindi non dovresti guidare più velocemente di?",
        propositionA : "80 mph",
        propositionB : "130 mph",
        propositionC : "100 mph",
        propositionD : "111 mph",
        reponse : "D"},
		
	{   question : "Quando vieni sorpassato da un'altro veicolo, è importante che NON",
        propositionA : "Decelerariate",
        propositionB : "Controlliate gli specchietti",
        propositionC : "Salutiate il conducente che vi sorpassa",
        propositionD : "Acceleriate",
        reponse : "D"},
]
