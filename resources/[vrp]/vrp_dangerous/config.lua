Config = {}
Config.EAS = {}
Config.EAS.Volume = 0.2 --(0.2 = 20% Volume)
Config.EAS.Departments = {
    LSPD    = {
    
        name = "Dipartimento di Polizia"
    
    },
    
    LSSD    = {
    
        name = "Dipartimento degli Sceriffi"
    
    },

    USGVT   = {
    
        name = "Governo"
    
    }
}