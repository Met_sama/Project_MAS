local robbing = false
local isDone = true
local bank = ""
local new_blip = nil
local theBlip = nil
local secondsRemaining = 0
local robbers = {}
theSaw = nil

SawModel = GetHashKey("prop_tool_consaw")
AnimDict = "weapons@heavy@minigun"
AnimName = "idle_2_aim_right_med"
ParticleDict = "des_fib_floor"
ParticleName = "ent_ray_fbi5a_ramp_metal_imp"

function bank_DisplayHelpText(str)
	SetTextComponentFormat("STRING")
	AddTextComponentString(str)
	DisplayHelpTextFromStringLabel(0, 0, 1, -1)
end

function bank_drawTxt(x,y ,width,height,scale, text, r,g,b,a, outline)
    SetTextFont(0)
    SetTextProportional(0)
    SetTextScale(scale, scale)
    SetTextColour(r, g, b, a)
    SetTextDropShadow(0, 0, 0, 0,255)
    SetTextEdge(1, 0, 0, 0, 255)
    SetTextDropShadow()
    if(outline)then
	    SetTextOutline()
	end
    SetTextEntry("STRING")
    AddTextComponentString(text)
    DrawText(x - width/2, y - height/2 + 0.005)
end

banks = {
	["fleeca"] = {
		position = { ['x'] = 148.2932434082, ['y'] = -1050.5938720703, ['z'] = 29.346370697021, ['rot'] = 70.254913330078 },
		nameofbank = "Fleeca Bank",
		lastrobbed = 0,
		from = 1,
		to = 12
	},
	["fleeca2"] = {
		position = { ['x'] = -2952.7429199219, ['y'] = 484.20642089844, ['z'] = 15.675388336182, ['rot'] = 196.53559875488 },
		nameofbank = "Fleeca Bank (Highway)",
		lastrobbed = 0,
		from = 1,
		to = 12
	},
	["blainecounty"] = {
		position = { ['x'] = -107.06505584717, ['y'] = 6474.8012695313, ['z'] = 31.62670135498, ['rot'] = 322.11602783203 },
		nameofbank = "Blaine County Savings",
		lastrobbed = 0,
		from = 13,
		to = 15
	},
	["fleeca3"] = {
		position = { ['x'] = -1206.7172851563, ['y'] = -338.20449829102, ['z'] = 37.759323120117, ['rot'] = 116.47665405273 },
		nameofbank = "Fleeca Bank (Vinewood Hills)",
		lastrobbed = 0,
		from = 1,
		to = 12
	},
	["fleeca4"] = {
		position = { ['x'] = -352.03720092773, ['y'] = -59.957454681396, ['z'] = 49.014862060547, ['rot'] = 74.71410369873 },
		nameofbank = "Fleeca Bank (Burton)",
		lastrobbed = 0,
		from = 1,
		to = 12
	},       	
	["fleeca5"] = {
		position = { ['x'] = 312.64205932617, ['y'] = -288.5231628418, ['z'] = 54.143070220947, ['rot'] = 73.760833740234 },			
		nameofbank = "Fleeca Bank (Alta)",
		lastrobbed = 0,
		from = 1,
		to = 12 
	},
	["fleeca6"] = {
		position = { ['x'] = 1176.255493164, ['y'] = 2711.6381835938, ['z'] = 38.097785949707, ['rot'] = 267.55688476563 },
		nameofbank = "Fleeca Bank (Desert)",
		lastrobbed = 0,
		from = 13,
		to = 15
	},
	["fleeca7"] = { 
		position = { ['x'] = -2071.5158691406, ['y'] = -1022.3671875, ['z'] = 11.91005039215, ['rot'] = 253.90362548828 },
		nameofbank = "Yacht (Spiaggia)",
		lastrobbed = 0,
		from = 16,
		to = 18
	},
	["pacific"] = { 
		position = { ['x'] = 265.55297851563, ['y'] = 213.47087097168, ['z'] = 101.68338012695, ['rot'] = 335.33129882813 },		
		nameofbank = "Pacific Standard PDB (Downtown Vinewood)",
		lastrobbed = 0,
		from = 1,
		to = 12
	}
}

RegisterNetEvent('es_bank:currentlyrobbing')
AddEventHandler('es_bank:currentlyrobbing', function(robb)
	robbing = true
	bank = robb
	secondsRemaining = 720
end)

RegisterNetEvent('es_bank:toofarlocal')
AddEventHandler('es_bank:toofarlocal', function(robb)
	robbing = false
	TriggerEvent('chatMessage', 'SYSTEM', {255, 0, 0}, "Rapina cancellata, non riceverai niente!.")
	robbingName = ""
	secondsRemaining = 0
	incircle = false
end)

RegisterNetEvent('es_bank:playerdiedlocal')
AddEventHandler('es_bank:playerdiedlocal', function(robb)
	robbing = false
	TriggerEvent('chatMessage', 'SYSTEM', {255, 0, 0}, "Sei morto!")
	robbingName = ""
	secondsRemaining = 0
	incircle = false
end)


RegisterNetEvent('es_bank:robberycomplete')
AddEventHandler('es_bank:robberycomplete', function(reward)
	robbing = false
	TriggerEvent('chatMessage', 'SYSTEM', {255, 0, 0}, "Caveau aperto, hai preso ^2" .. reward)
	bank = ""
	secondsRemaining = 0
	incircle = false
end)

Citizen.CreateThread(function()
	while true do
		if robbing then
			Citizen.Wait(1000)
			if(secondsRemaining > 0)then
				secondsRemaining = secondsRemaining - 1
			end
		end

		Citizen.Wait(0)
	end
end)

Citizen.CreateThread(function()
	while true do
		local pos = GetEntityCoords(GetPlayerPed(-1), true)
		for k,v in pairs(banks)do
			local pos2 = v.position

			if(Vdist(pos.x, pos.y, pos.z, pos2.x, pos2.y, pos2.z) < 15.0)then
				if IsPlayerWantedLevelGreater(PlayerId(),0) or ArePlayerFlashingStarsAboutToDrop(PlayerId()) then
					local wanted = GetPlayerWantedLevel(PlayerId())
					Citizen.Wait(5000)
				    SetPlayerWantedLevel(PlayerId(), wanted, 0)
					SetPlayerWantedLevelNow(PlayerId(), 0)
				end
			end
		end
		Citizen.Wait(0)
	end
end)

Citizen.CreateThread(function()
	for k,v in pairs(banks)do
		local ve = v.position

		local blip = AddBlipForCoord(ve.x, ve.y, ve.z)
		SetBlipSprite(blip, 278)
		SetBlipScale(blip, 0.8)
		SetBlipColour(blip, 49)
		SetBlipAsShortRange(blip, true)
		BeginTextCommandSetBlipName("STRING")
		AddTextComponentString("Rapina")
		EndTextCommandSetBlipName(blip)
	end
end)
incircle = false

Citizen.CreateThread(function()
	while true do
		local pos = GetEntityCoords(GetPlayerPed(-1), true)

		for k,v in pairs(banks)do
			local pos2 = v.position

			if(Vdist(pos.x, pos.y, pos.z, pos2.x, pos2.y, pos2.z) < 15.0)then
				if not robbing then
					DrawMarker(29, v.position.x, v.position.y, v.position.z, 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 1.5001, 1555, 0, 0,255, true, true, 0,0)					
					if(Vdist(pos.x, pos.y, pos.z, pos2.x, pos2.y, pos2.z) < 2)then
						if (incircle == false) then
							bank_DisplayHelpText("Premi ~INPUT_CONTEXT~ per rapinare ~b~" .. v.nameofbank .. "~w~. La polizia verrà avvertita!")
						end
						incircle = true
						if(IsControlJustReleased(1, 51))then
							TriggerServerEvent('es_bank:rob', k)
						end
					elseif(Vdist(pos.x, pos.y, pos.z, pos2.x, pos2.y, pos2.z) > 2)then
						incircle = false
					end
				end
			end
		end

		if robbing then
		    SetPlayerWantedLevel(PlayerId(), 0, 0)
            SetPlayerWantedLevelNow(PlayerId(), 0)
			
			bank_drawTxt(0.66, 1.44, 1.0,1.0,0.4, "Rapinando la banca: ~r~" .. secondsRemaining .. "~w~ secondi rimasti", 255, 255, 255, 255)
			
			local pos2 = banks[bank].position
			local ped = GetPlayerPed(-1)
			
            if IsEntityDead(ped) then
			TriggerServerEvent('es_bank:playerdied', bank)
			elseif (Vdist(pos.x, pos.y, pos.z, pos2.x, pos2.y, pos2.z) > 15)then
				TriggerServerEvent('es_bank:toofar', bank)
			end
		end

		Citizen.Wait(0)
	end
end)