-- Remember to use the cop group or this won't work
-- K > Admin > Add Group > User ID > cop

local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")

vRP = Proxy.getInterface("vRP")
vRPnc = Proxy.getInterface("vRP_newcoin")
vRPclient = Tunnel.getInterface("vRP","vRP_bank")

local banks = {
["fleeca"] = {
		position = { ['x'] = 148.2932434082, ['y'] = -1050.5938720703, ['z'] = 29.346370697021, ['rot'] = 70.254913330078 },
		reward = 10000 + math.random(450000,700000),
		nameofbank = "Fleeca Bank",
		lastrobbed = 0
	},
	["fleeca2"] = {
		position = { ['x'] = -2952.7429199219, ['y'] = 484.20642089844, ['z'] = 15.675388336182, ['rot'] = 196.53559875488 },
		reward = 10000 + math.random(400000,600000),
		nameofbank = "Fleeca Bank (Highway)",
		lastrobbed = 0
	},
	["blainecounty"] = {
		position = { ['x'] = -107.06505584717, ['y'] = 6474.8012695313, ['z'] = 31.62670135498, ['rot'] = 322.11602783203 },
		reward = 10000 + math.random(400000,600000),
		nameofbank = "Blaine County Savings",
		lastrobbed = 0
	},
	["fleeca3"] = {
		position = { ['x'] = -1206.7172851563, ['y'] = -338.20449829102, ['z'] = 37.759323120117, ['rot'] = 116.47665405273 },
		reward = 10000 + math.random(400000,600000),
		nameofbank = "Fleeca Bank",
		lastrobbed = 0
	},
	["fleeca4"] = {
		position = { ['x'] = -352.03720092773, ['y'] = -59.957454681396, ['z'] = 49.014862060547, ['rot'] = 74.71410369873 },
		reward = 10000 + math.random(400000,600000),
		nameofbank = "Fleeca Bank (Highway)",
		lastrobbed = 0
	},
	["fleeca5"] = {
		position = { ['x'] = 311.31787109375, ['y'] = -283.26647949218, ['z'] = 53.6745223999023, ['rot'] = 73.760833740234 },
		reward = 10000 + math.random(400000,600000),
		nameofbank = "Fleeca Bank (Alta)",
		lastrobbed = 0
	},	
	["fleeca6"] = {
		position = { ['x'] = 1176.255493164, ['y'] = 2711.6381835938, ['z'] = 38.097785949707, ['rot'] = 267.55688476563 },
		reward = 10000 + math.random(400000,600000),
		nameofbank = "Fleeca Bank",
		lastrobbed = 0
	},
	["fleeca7"] = { 
		position = { ['x'] = -2071.5158691406, ['y'] = -1022.3671875, ['z'] = 11.91005039215, ['rot'] = 253.90362548828 },
		reward = 10000 + math.random(400000,600000),
		nameofbank = "Yacht (Spiaggia)",
		lastrobbed = 0
	},
	["pacific"] = { 
		position = { ['x'] = 265.55297851563, ['y'] = 213.47087097168, ['z'] = 101.68338012695, ['rot'] = 335.33129882813 },		
		reward = 10000 + math.random(500000,800000),
		nameofbank = "Blaine County Savings",
		lastrobbed = 0
	},		
}

local robbers = {}

function get3DDistance(x1, y1, z1, x2, y2, z2)
	return math.sqrt(math.pow(x1 - x2, 2) + math.pow(y1 - y2, 2) + math.pow(z1 - z2, 2))
end

AddEventHandler("playerDropped", function()
	if(robbers[source])then
		local wtf = robbers[source]
		local wtf2 = banks[wtf].nameofbank
		robbers[source] = nil
		TriggerClientEvent('chatMessage', -1, 'NEWS', {255, 0, 0}, "Rapina cancellata: ^2" ..wtf2.."Motivo: [Disconnesso]")
	end
end)

RegisterServerEvent('es_bank:toofar')
AddEventHandler('es_bank:toofar', function(robb)
	if(robbers[source])then
		TriggerClientEvent('es_bank:toofarlocal', source)
		robbers[source] = nil
		TriggerClientEvent('chatMessage', -1, 'NEWS', {255, 0, 0}, "Rapina cancellata: ^2" .. banks[robb].nameofbank)
	end
end)

RegisterServerEvent('es_bank:playerdied')
AddEventHandler('es_bank:playerdied', function(robb)
	if(robbers[source])then
		TriggerClientEvent('es_bank:playerdiedlocal', source)
		robbers[source] = nil
		TriggerClientEvent('chatMessage', -1, 'NEWS', {255, 0, 0}, "Rapina cancellata: ^2" .. banks[robb].nameofbank)
	end
end)

RegisterServerEvent('es_bank:rob')
AddEventHandler('es_bank:rob', function(robb)
  local user_id = vRP.getUserId({source})
  local player = vRP.getUserSource({user_id})
  local cops = vRP.getUsersByGroup({"cop"})
  if vRP.hasGroup({user_id,"cop"}) then
    vRPclient.notify(player,{"~r~Fai attenzione!"})
  else
    if #cops >= 5 then 
	  if banks[robb] then
		  local bank = banks[robb]

		  if (os.time() - bank.lastrobbed) < 600 and bank.lastrobbed ~= 0 then
			  TriggerClientEvent('chatMessage', player, 'RAPINA', {255, 0, 0}, "Questa Banca è stata già rapinata recentemente!: ^2" .. (1200 - (os.time() - bank.lastrobbed)) .. "^0 secondi.")
			  return
		  end
		  TriggerClientEvent('chatMessage', -1, 'NEWS', {255, 0, 0}, "Rapina in corso a ^2" .. bank.nameofbank)
		  TriggerClientEvent('es_bank:currentlyrobbing', player, robb)
		  banks[robb].lastrobbed = os.time()
		  robbers[player] = robb
		  local savedSource = player
		  SetTimeout(720000, function()
			  if(robbers[savedSource])then
				  if(user_id)then
                     -- vRPnc.giveCoins({user_id, bank.reward,true})		
					  vRP.giveInventoryItem({user_id,"dirty_money",bank.reward,true})					 
					  TriggerClientEvent('chatMessage', -1, 'NEWS', {255, 0, 0}, "La rapina è terminata: ^2" .. bank.nameofbank .. "^0!")
					  TriggerClientEvent('es_bank:robberycomplete', savedSource, bank.reward)
				  end
			  end
		  end)		
	  end
    else
      vRPclient.notify(player,{"~r~Non ci sono abbastanza poliziotti."})
    end
	end
end)
