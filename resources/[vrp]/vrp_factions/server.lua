local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")

vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP","vRP_factions")

factions = {
	-- {Grup, Lider Grup, Nume Grup}	
	{"Concessionaria Dal Milanese", "luke", "Concessionario di Camion"},
--	{"cop", "LSPD-Capitano", "Polizia"},
	{"Import & Export", "importcapo", "Import Export"},
	
	{"MotorSport", "capomotorsport", "MotorSport"},
	
	
	
	{"Dipendente Aci","Capo Aci","Aci"}
}

local function ch_inviteInFaction(player,choice)
	local user_id = vRP.getUserId({player})
	for i, v in pairs(factions) do
		group = v[1]
		if(vRP.hasGroup({user_id, group}))then
			groupLider = v[2]
			groupName = v[3]
			theGroup = tostring(group)
			vRP.prompt({player,"User id: ","",function(player,id)
				id = parseInt(id)
				groupName = tostring(groupName)
				local target = vRP.getUserSource({id})
				if(target)then
					if(vRP.hasGroup({id, theGroup}))then
						vRPclient.notify(player,{"~r~Giocatore in fazione ~g~"..groupName.."!"})
					else
						vRP.addUserGroup({id,theGroup})
						local name = GetPlayerName(target)
						vRPclient.notify(player,{"~w~Hai aggiunto ~g~"..name.."~w~ nella fazione ~g~"..groupName.."!"})
						vRPclient.notify(target,{"~w~Sei stato aggiunto nella ~g~"..groupName.."!"})
					end
				else
					vRPclient.notify(player,{"~r~Giocatore non trovato!"})
				end
			end})
		end
	end
end

local function ch_removeFromFaction(player,choice)
	local user_id = vRP.getUserId({player})
	for i, v in pairs(factions) do
		group = v[1]
		if(vRP.hasGroup({user_id, group}))then
			groupLider = v[2]
			groupName = v[3]
			theGroup = tostring(group)
			vRP.prompt({player,"User id: ","",function(player,id)
				groupName = tostring(groupName)
				id = parseInt(id)
				local target = vRP.getUserSource({id})
				if(target)then
					if(vRP.hasGroup({id, theGroup}))then
						vRP.removeUserGroup({id,theGroup})
						local name = GetPlayerName(target)
						vRPclient.notify(player,{"~w~You kicked ~g~"..name.." ~w~out of ~g~"..groupName.."!"})
						vRPclient.notify(target,{"~w~You were kicked out from ~g~"..groupName.."!"})
					else
						vRPclient.notify(player,{"~w~Player is not in ~r~"..groupName.."!"})
					end
				else
					vRPclient.notify(player,{"~r~Player not found!"})
				end
			end})
		end
	end
end

vRP.registerMenuBuilder({"main", function(add, data)
	local user_id = vRP.getUserId({data.player})
	if user_id ~= nil then
		local choices = {}
		for i, v in pairs(factions) do
			groupLider = v[2]
			if(vRP.hasGroup({user_id, groupLider}))then
				group = v[1]
				groupName = v[3]
				choices["Leader Menu"] = {function(player,choice)
					vRP.buildMenu({"Leader Menu", {player = player}, function(menu)
						menu.name = "Leader Menu"
						menu.css={top="75px",header_color="rgba(200,0,0,0.75)"}
						menu.onclose = function(player) vRP.openMainMenu({player}) end
						menu["Invita Membro"] = {ch_inviteInFaction, "Invita membro in "..groupName}
						menu["Caccia Membro"] = {ch_removeFromFaction, "Caccia membro da "..groupName}
						vRP.openMenu({player,menu})
					end})
				end, "Leader menu per la fazione "..groupName}
			end
		end
		add(choices)
	end
end})

local function ch_leaveGroup(player,choice)
	local user_id = vRP.getUserId({player})
	if user_id ~= nil then
		for i, v in pairs(factions) do
			group = v[1]
			if(vRP.hasGroup({user_id, group}))then
				groupLider = v[2]
				groupName = v[3]
				theGroup = tostring(group)
				vRP.removeUserGroup({user_id,theGroup})
				vRPclient.notify(player,{"~w~Hai abbandonato ~r~"..groupName.."!"})
				vRP.openMainMenu({player})
				local fMembers = vRP.getUsersByGroup({tostring(theGroup)})
				for i, v in ipairs(fMembers) do
					local member = vRP.getUserSource({tonumber(v)})
					vRPclient.notify(member,{"~r~"..GetPlayerName(player).." ~w~a ha abbandonato la fazione!"})
				end
			end
		end
	end
end

vRP.registerMenuBuilder({"main", function(add, data)
	local user_id = vRP.getUserId({data.player})
	if user_id ~= nil then
		local choices = {}
		for i, v in pairs(factions) do
			group = v[1]
			if(vRP.hasGroup({user_id, group}))then
				groupLider = v[2]
				groupName = v[3]
				choices["Lascia Fazione"] = {function(player,choice)
					vRP.buildMenu({"Lascia Fazione?", {player = player}, function(menu)
						groupName = tostring(groupName)
						menu.name = "Lascia Fazione?"
						menu.css={top="75px",header_color="rgba(200,0,0,0.75)"}
						menu.onclose = function(player) vRP.openMainMenu({player}) end

						menu["Si"] = {ch_leaveGroup, "Lascia "..groupName}
						menu["No"] = {function(player) vRP.openMainMenu({player}) end}
						vRP.openMenu({player,menu})
					end})
				end, "Lascia "..groupName}
			end
		end
		add(choices)
	end
end})

RegisterCommand('f', function(source, args, rawCommand)
	if(args[1] == nil)then
		TriggerClientEvent('chatMessage', source, "^3SYNTAX: /"..rawCommand.." [Message]") 
	else
		local user_id = vRP.getUserId({source})
		theGroup = ""
		for i, v in pairs(factions) do
			if vRP.hasGroup({user_id,tostring(v)}) then
				theGroup = tostring(v[1])
				theName = tostring(v[3])
			end
		end
		local fMembers = vRP.getUsersByGroup({tostring(theGroup)})
		for i, v in ipairs(fMembers) do
			local player = vRP.getUserSource({tonumber(v)})
			TriggerClientEvent('chatMessage', player, "^5["..theName.."] ^7| " .. GetPlayerName(source) .. ": " ..  rawCommand:sub(3))
		end
	end
end)
