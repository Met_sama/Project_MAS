local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")

vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP","vRP_tvnews")

group = "Weazel News"
camPerm = "weazel.camera"
micPerm = "weazel.mic"

local function cameraSettings(player, choice)
	vRP.closeMenu({player})
	player = player
	SetTimeout(350, function()
		vRP.buildMenu({"Impostazioni Camera", {player = player}, function(menu2)
			menu2.name = "Impostazioni Camera"
			menu2.css={top="75px",header_color="rgba(235,0,0,0.75)"}
			menu2.onclose = function(player) vRP.openMenu({player, menu}) end
			
			menu2["Titolo Notizia"] = {function(player,choice)
				vRP.prompt({player, "Titolo Notizia:", "", function(player,newsTitle)
					newsTitle = newsTitle
					vRPclient.notify(player, {"~g~Titolo Notizia: ~w~"..newsTitle})
					TriggerClientEvent("Cam:SetNewsTitle", player, newsTitle)
				end})
			end,"Imposta il titolo delle notizie"}
			
			menu2["Titolo superiore"] = {function(player,choice)
				vRP.prompt({player, "Titolo Superiore:", "", function(player,topTitle)
					topTitle = topTitle
					vRPclient.notify(player, {"~g~Titolo Superiore: ~w~"..topTitle})
					TriggerClientEvent("Cam:SetTopTitle", player, topTitle)
				end})
			end,"Imposta il titolo più alto"}
			
			menu2["Titolo in basso"] = {function(player,choice)
				vRP.prompt({player, "Titolo in basso:", "", function(player,botTitle)
					botTitle = botTitle
					vRPclient.notify(player, {"~g~Titolo in basso: ~w~"..botTitle})
					TriggerClientEvent("Cam:SetBotTitle", player, botTitle)
				end})
			end,"Imposta il titolo in basso"}
			vRP.openMenu({player, menu2})
		end})
	end)
end

vRP.registerMenuBuilder({"main", function(add, data)
	local user_id = vRP.getUserId({data.player})
	if user_id ~= nil then
		local choices = {}
		if(vRP.hasGroup({user_id, group}))then
			choices["Menu Giornalista"] = {function(player,choice)
				vRP.buildMenu({"Menu Giornalista", {player = player}, function(menu)
					menu.name = "Menu Giornalista"
					menu.css={top="75px",header_color="rgba(235,0,0,0.75)"}
					menu.onclose = function(player) vRP.openMainMenu({player}) end
					
					if(vRP.hasPermission({user_id, camPerm}))then
						menu["Videocamera"] = {function(player,choice)
							TriggerClientEvent("Cam:ToggleCam", player)
						end,"Usa la videocamera"}
						
						menu["Impostazioni videocamera"] = {cameraSettings,"Impostazioni"}
					end
					
					if(vRP.hasPermission({user_id, micPerm}))then
						menu["Microfono"] = {function(player,choice)
							TriggerClientEvent("Mic:ToggleMic", player)
						end,"Usa il microfono"}
					end
					
					vRP.openMenu({player, menu})
				end})
			end, "Weazel News - Menu"}
		end
		add(choices)
	end
end})