
------
-- InteractSound by Scott
-- Verstion: v0.0.1
------

-- Manifest Version
resource_manifest_version '77731fab-63ca-442c-a67b-abc70f28dfa5'

-- Client Scripts
client_script 'client/main.lua'

-- Server Scripts
server_script 'server/main.lua'

-- NUI Default Page
ui_page('client/html/index.html')

-- Files needed for NUI
-- DON'T FORGET TO ADD THE SOUND FILES TO THIS!
files({
    'client/html/index.html',
    -- Begin Sound Files Here...
    -- client/html/sounds/ ... .ogg
    'client/html/sounds/demo.ogg',
    'client/html/sounds/blip_cintura.ogg',
    'client/html/sounds/sms_cellulare.ogg',
    'client/html/sounds/scary1.ogg',
    'client/html/sounds/chiamata_cellulare.ogg',
    'client/html/sounds/handcuff_01.ogg',
    'client/html/sounds/handcuff_02.ogg',
    'client/html/sounds/pesca_01.ogg',
    'client/html/sounds/pesca_02.ogg',
    'client/html/sounds/pesca_03.ogg',
    'client/html/sounds/pesca_04.ogg',
    'client/html/sounds/pesca_05.ogg',
    'client/html/sounds/pesca_06.ogg',
    'client/html/sounds/pesca_07.ogg',
    'client/html/sounds/SodaMachine.ogg',
    'client/html/sounds/bipbip.ogg',
    'client/html/sounds/cintura_on.ogg',
    'client/html/sounds/cintura_off.ogg',
    'client/html/sounds/radio_no.ogg',
    'client/html/sounds/radio_on.ogg',
    'client/html/sounds/radio_off.ogg',
    'client/html/sounds/smokegrenade_esplosione.ogg',
    'client/html/sounds/terremoto.ogg',
    'client/html/sounds/gnome_woo.ogg',
    'client/html/sounds/meme02.ogg'
})
