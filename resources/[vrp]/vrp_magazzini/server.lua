MySQL = module("vrp_mysql", "MySQL")
local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")
local htmlEntities = module("vrp", "lib/htmlEntities")

vRPmz = {}
vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP","vrp_magazzini")
MZclient = Tunnel.getInterface("vrp_magazzini","vrp_magazzini")
Tunnel.bindInterface("vrp_magazzini",vRPmz)


MySQL.createCommand("vRP/maga_tables",[[
CREATE TABLE IF NOT EXISTS vrp_magazzini(
  magazzino VARCHAR(255),
	user_id INTEGER,
	CONSTRAINT pk_business PRIMARY KEY(magazzino)
);
]])

MySQL.createCommand("vRP/get_user_magazzino","SELECT user_id FROM vrp_magazzini WHERE magazzino = @magazzino")
MySQL.createCommand("vRP/set_user_magazzino","UPDATE vrp_magazzini SET user_id = @user_id WHERE magazzino = @magazzino")
MySQL.createCommand("vRP/insert_user_magazzino","INSERT IGNORE INTO vrp_magazzini(magazzino,user_id) VALUES(@magazzino,@user_id)")

MySQL.execute("vRP/maga_tables")

local casse = {
	["magazzinovendita_01"] = {peso = 10000,pre = 1500000,x=909.27172851562,y=-3032.2297363282,z=5.9020409584046},
	["magazzinovendita_02"] = {peso = 10000,pre = 1500000,x=909.30853271484,y=-3023.7736816406,z=5.9020414352416},
	["magazzinovendita_03"] = {peso = 10000,pre = 1500000,x=923.38171386718,y=-3021.0090332032,z=5.9020380973816},
	["magazzinovendita_04"] = {peso = 10000,pre = 1500000,x=923.38214111328,y=-3026.1020507812,z=5.9020380973816},
	["magazzinovendita_05"] = {peso = 10000,pre = 1500000,x=923.3822631836,y=-3031.1696777344,z=5.9020366668702},
	["magazzinovendita_06"] = {peso = 10000,pre = 1500000,x=923.3833618164,y=-3036.419921875,z=5.9020347595214},
	["magazzinovendita_07"] = {peso = 10000,pre = 1500000,x=923.38165283204,y=-3041.4482421875,z=5.9020342826844},
	["magazzinovendita_08"] = {peso = 10000,pre = 1500000,x=923.3814086914,y=-3047.8884277344,z=5.9020400047302},
	["magazzinovendita_09"] = {peso = 10000,pre = 1500000,x=937.2357788086,y=-3047.9580078125,z=5.902042388916},
	["magazzinovendita_10"] = {peso = 10000,pre = 1500000,x=937.3823852539,y=-3040.5734863282,z=5.9020419120788},
	["magazzinovendita_11"] = {peso = 10000,pre = 1500000,x=937.38317871094,y=-3031.6916503906,z=5.9020442962646},
	["magazzinovendita_12"] = {peso = 10000,pre = 1500000,x=937.38269042968,y=-3026.5727539062,z=5.9020419120788},
	["magazzinovendita_13"] = {peso = 10000,pre = 1500000,x=937.3812866211,y=-3021.2768554688,z=5.9020409584046},
	["magazzinovendita_14"] = {peso = 10000,pre = 1500000,x=951.53924560546,y=-3020.2414550782,z=5.9020414352416},
	["magazzinovendita_15"] = {peso = 10000,pre = 1500000,x=951.37768554688,y=-3025.8842773438,z=5.902039527893},
	["magazzinovendita_16"] = {peso = 10000,pre = 1500000,x=951.38104248046,y=-3032.0002441406,z=5.9020419120788},
	["magazzinovendita_17"] = {peso = 10000,pre = 1500000,x=951.17962646484,y=-3039.0390625,z=5.9020404815674},
	["magazzinovendita_18"] = {peso = 10000,pre = 1500000,x=951.1802368164,y=-3044.5712890625,z=5.9020404815674},
	["magazzinovendita_19"] = {peso = 10000,pre = 1500000,x=951.18518066406,y=-2995.2702636718,z=5.9007649421692},
	["magazzinovendita_20"] = {peso = 10000,pre = 1500000,x=951.17553710938,y=-2990.4423828125,z=5.9007635116578},
	["magazzinovendita_21"] = {peso = 10000,pre = 1500000,x=951.18090820312,y=-2984.9104003906,z=5.9007654190064},
	["magazzinovendita_22"] = {peso = 10000,pre = 1500000,x=951.3851928711,y=-2974.1279296875,z=5.9011545181274},
	["magazzinovendita_23"] = {peso = 10000,pre = 1500000,x=951.38568115234,y=-2968.71484375,z=5.9011583328248},
	["magazzinovendita_24"] = {peso = 10000,pre = 1500000,x=937.38348388672,y=-2968.6726074218,z=5.9007635116578},
	["magazzinovendita_25"] = {peso = 10000,pre = 1500000,x=937.38317871094,y=-2994.9545898438,z=5.9007620811462},
	["magazzinovendita_26"] = {peso = 10000,pre = 1500000,x=923.38275146484,y=-2995.1960449218,z=5.900764465332},
	["magazzinovendita_27"] = {peso = 10000,pre = 1500000,x=923.3794555664,y=-2990.3596191406,z=5.900764465332},
	["magazzinovendita_28"] = {peso = 10000,pre = 1500000,x=923.38293457032,y=-2984.9274902344,z=5.9007630348206},
	["magazzinovendita_29"] = {peso = 10000,pre = 1500000,x=923.38885498046,y=-2976.9685058594,z=5.900761127472},
	["magazzinovendita_30"] = {peso = 10000,pre = 1500000,x=923.38232421875,y=-2971.6328125,z=5.9007592201232},
	["magazzinovendita_31"] = {peso = 10000,pre = 1500000,x=909.443359375,y=-2967.6328125,z=5.9007787704468},	
	["magazzinovendita_32"] = {peso = 10000,pre = 1500000,x=909.30731201172,y=-2973.109375,z=5.900779724121},
	["magazzinovendita_33"] = {peso = 10000,pre = 1500000,x=909.30773925782,y=-2979.6030273438,z=5.9007802009582},
	["magazzinovendita_34"] = {peso = 10000,pre = 1500000,x=909.47955322266,y=-2987.4895019532,z=5.9007782936096},
	["magazzinovendita_35"] = {peso = 10000,pre = 1500000,x=909.29461669922,y=-2992.2153320312,z=5.9007806777954},
	["magazzinovendita_36"] = {peso = 10000,pre = 1500000,x=1061.517211914,y=-2995.2451171875,z=5.901038646698},
	["magazzinovendita_37"] = {peso = 10000,pre = 1500000,x=1061.5493164062,y=-2990.1936035156,z=5.9008355140686},
	["magazzinovendita_38"] = {peso = 10000,pre = 1500000,x=1061.573852539,y=-2984.0700683594,z=5.900779724121},
	["magazzinovendita_39"] = {peso = 10000,pre = 1500000,x=1061.791381836,y=-2979.5041503906,z=5.9008283615112},
	["magazzinovendita_40"] = {peso = 10000,pre = 1500000,x=1061.8679199218,y=-2974.1591796875,z=5.9008316993714},
	["magazzinovendita_41"] = {peso = 10000,pre = 1500000,x=1061.8680419922,y=-2969.0310058594,z=5.9008269309998},
	["magazzinovendita_42"] = {peso = 10000,pre = 1500000,x=1047.867553711,y=-2968.1608886718,z=5.9008297920228},
	["magazzinovendita_43"] = {peso = 10000,pre = 1500000,x=1047.8660888672,y=-2973.0676269532,z=5.9010548591614},
	["magazzinovendita_44"] = {peso = 10000,pre = 1500000,x=1047.6174316406,y=-2983.8388671875,z=5.9010400772094},
	["magazzinovendita_45"] = {peso = 10000,pre = 1500000,x=1047.607421875,y=-2989.095703125,z=5.901044845581},
	["magazzinovendita_46"] = {peso = 10000,pre = 1500000,x=1047.6651611328,y=-2992.193359375,z=5.9010443687438},
	["magazzinovendita_47"] = {peso = 10000,pre = 1500000,x=1033.9040527344,y=-2994.955078125,z=5.9010615348816},
	["magazzinovendita_48"] = {peso = 10000,pre = 1500000,x=1033.9040527344,y=-2989.9223632812,z=5.9009947776794},
	["magazzinovendita_49"] = {peso = 10000,pre = 1500000,x=1033.9040527344,y=-2984.9741210938,z=5.9008345603942},
	["magazzinovendita_50"] = {peso = 10000,pre = 1500000,x=1033.8684082032,y=-2974.4030761718,z=5.9008316993714},
	["magazzinovendita_51"] = {peso = 10000,pre = 1500000,x=1033.7860107422,y=-2968.6000976562,z=5.9008283615112},
	["magazzinovendita_52"] = {peso = 10000,pre = 1500000,x=1019.868774414,y=-2968.2038574218,z=5.9008297920228},
	["magazzinovendita_53"] = {peso = 10000,pre = 1500000,x=1019.8663330078,y=-2979.0031738282,z=5.901047706604},
	["magazzinovendita_54"] = {peso = 10000,pre = 1500000,x=1019.8671875,y=-2983.7141113282,z=5.9010472297668},
	["magazzinovendita_55"] = {peso = 10000,pre = 1500000,x=1019.8690185546,y=-2990.072265625,z=5.9010429382324},
	["magazzinovendita_56"] = {peso = 10000,pre = 1500000,x=1019.8682250976,y=-2995.1801757812,z=5.9010419845582},
	["magazzinovendita_57"] = {peso = 10000,pre = 1500000,x=1005.9017944336,y=-2994.0625,z=5.9010376930236},
	["magazzinovendita_58"] = {peso = 10000,pre = 1500000,x=1005.911743164,y=-2989.1647949218,z=5.9008259773254},
	["magazzinovendita_59"] = {peso = 10000,pre = 1500000,x=1005.9606933594,y=-2978.6962890625,z=5.9008259773254},
	["magazzinovendita_60"] = {peso = 10000,pre = 1500000,x=1005.9436645508,y=-2974.5642089844,z=5.9008250236512},
	["magazzinovendita_61"] = {peso = 10000,pre = 1500000,x=1005.901977539,y=-2967.5578613282,z=5.9008255004882},
	["magazzinovendita_62"] = {peso = 10000,pre = 1500000,x=1005.800415039,y=-3020.9133300782,z=5.9010424613952},
	["magazzinovendita_63"] = {peso = 10000,pre = 1500000,x=1005.7991333008,y=-3025.1721191406,z=5.9010424613952},
	["magazzinovendita_64"] = {peso = 10000,pre = 1500000,x=1005.7897949218,y=-3031.0690917968,z=5.9010400772094},
	["magazzinovendita_65"] = {peso = 10000,pre = 1500000,x=1005.9990234375,y=-3039.9909667968,z=5.9010400772094},
	["magazzinovendita_66"] = {peso = 10000,pre = 1500000,x=1006.0509643554,y=-3044.3376464844,z=5.9010410308838},
	["magazzinovendita_67"] = {peso = 10000,pre = 1500000,x=1019.8783569336,y=-3047.5400390625,z=5.9010434150696},
	["magazzinovendita_68"] = {peso = 10000,pre = 1500000,x=1019.8696899414,y=-3042.5971679688,z=5.9010434150696},
	["magazzinovendita_69"] = {peso = 10000,pre = 1500000,x=1019.8682861328,y=-3037.2456054688,z=5.9010434150696},
	["magazzinovendita_61"] = {peso = 10000,pre = 1500000,x=1019.6201171875,y=-3032.1154785156,z=5.9010434150696},
	["magazzinovendita_62"] = {peso = 10000,pre = 1500000,x=1019.6616210938,y=-3026.6696777344,z=5.9010453224182},
	["magazzinovendita_63"] = {peso = 10000,pre = 1500000,x=1019.6572875976,y=-3021.2502441406,z=5.9010424613952},
	["magazzinovendita_64"] = {peso = 10000,pre = 1500000,x=1033.6697998046,y=-3020.3991699218,z=5.9010462760926},
	["magazzinovendita_65"] = {peso = 10000,pre = 1500000,x=1033.6710205078,y=-3026.0947265625,z=5.9010396003724},
	["magazzinovendita_66"] = {peso = 10000,pre = 1500000,x=1033.6684570312,y=-3032.1596679688,z=5.9010429382324},
	["magazzinovendita_67"] = {peso = 10000,pre = 1500000,x=1033.8664550782,y=-3039.4604492188,z=5.9010396003724},
	["magazzinovendita_68"] = {peso = 10000,pre = 1500000,x=1033.865234375,y=-3044.126953125,z=5.9010434150696},
	["magazzinovendita_69"] = {peso = 10000,pre = 1500000,x=1047.646484375,y=-3047.787109375,z=5.9010419845582},
	["magazzinovendita_70"] = {peso = 10000,pre = 1500000,x=1047.896118164,y=-3042.5422363282,z=5.9010572433472},
	["magazzinovendita_71"] = {peso = 10000,pre = 1500000,x=1047.665649414,y=-3037.8186035156,z=5.901038646698},
	["magazzinovendita_72"] = {peso = 10000,pre = 1500000,x=1093.9193115234,y=-3048.0041503906,z=5.9010443687438},
	["magazzinovendita_73"] = {peso = 10000,pre = 1500000,x=1093.9188232422,y=-3043.0727539062,z=5.9010429382324},
	["magazzinovendita_74"] = {peso = 10000,pre = 1500000,x=1093.9187011718,y=-3037.5705566406,z=5.901041507721},
	["magazzinovendita_75"] = {peso = 10000,pre = 1500000,x=1094.0002441406,y=-3032.4533691406,z=5.9010481834412},
	["magazzinovendita_76"] = {peso = 10000,pre = 1500000,x=1093.929321289,y=-3026.7199707032,z=5.901041507721},
	["magazzinovendita_77"] = {peso = 10000,pre = 1500000,x=1094.0219726562,y=-3021.3337402344,z=5.901041507721},
	["magazzinovendita_78"] = {peso = 10000,pre = 1500000,x=1107.9119873046,y=-3021.0119628906,z=5.9010429382324},
	["magazzinovendita_79"] = {peso = 10000,pre = 1500000,x=1107.9696044922,y=-3025.5319824218,z=5.9010438919068},
	["magazzinovendita_80"] = {peso = 10000,pre = 1500000,x=1107.6872558594,y=-3037.1545410156,z=5.9010424613952},
	["magazzinovendita_81"] = {peso = 10000,pre = 1500000,x=1107.6867675782,y=-3041.9582519532,z=5.9010438919068},
	["magazzinovendita_82"] = {peso = 10000,pre = 1500000,x=1107.7438964844,y=-3048.0183105468,z=5.9010438919068},
	["magazzinovendita_83"] = {peso = 10000,pre = 1500000,x=1121.9851074218,y=-3047.345703125,z=5.9010429382324},
	["magazzinovendita_84"] = {peso = 10000,pre = 1500000,x=1121.9890136718,y=-3042.7888183594,z=5.9010410308838},
	["magazzinovendita_85"] = {peso = 10000,pre = 1500000,x=1121.9848632812,y=-3037.0434570312,z=5.9010391235352},
	["magazzinovendita_86"] = {peso = 10000,pre = 1500000,x=1121.984741211,y=-3031.2380371094,z=5.9010400772094},
	["magazzinovendita_87"] = {peso = 10000,pre = 1500000,x=1121.9851074218,y=-3026.6921386718,z=5.9010400772094},
	["magazzinovendita_88"] = {peso = 10000,pre = 1500000,x=1121.984741211,y=-3021.1525878906,z=5.9010443687438},
	["magazzinovendita_89"] = {peso = 10000,pre = 1500000,x=1135.9874267578,y=-3020.2795410156,z=5.9010434150696},
	["magazzinovendita_90"] = {peso = 10000,pre = 1500000,x=1135.9862060546,y=-3025.8979492188,z=5.9010424613952},
	["magazzinovendita_91"] = {peso = 10000,pre = 1500000,x=1135.9846191406,y=-3031.6413574218,z=5.901038646698},
	["magazzinovendita_92"] = {peso = 10000,pre = 1500000,x=1135.8999023438,y=-3036.3898925782,z=5.901038646698},
	["magazzinovendita_93"] = {peso = 10000,pre = 1500000,x=1135.9001464844,y=-3041.47265625,z=5.9010429382324},
	["magazzinovendita_94"] = {peso = 10000,pre = 1500000,x=1135.8990478516,y=-3046.6791992188,z=5.9010462760926},
	["magazzinovendita_95"] = {peso = 10000,pre = 1500000,x=1149.7846679688,y=-3047.4724121094,z=5.9010405540466},
	["magazzinovendita_96"] = {peso = 10000,pre = 1500000,x=1149.7833251954,y=-3042.2131347656,z=5.9010405540466},
	["magazzinovendita_97"] = {peso = 10000,pre = 1500000,x=1149.7844238282,y=-3037.3215332032,z=5.9020805358886},
	["magazzinovendita_98"] = {peso = 10000,pre = 1500000,x=1149.9870605468,y=-3031.6245117188,z=5.9010419845582},
	["magazzinovendita_99"] = {peso = 10000,pre = 1500000,x=1149.9869384766,y=-3026.6364746094,z=5.9010419845582},
	["magazzinovendita_100"] = {peso = 10000,pre = 1500000,x=1149.9865722656,y=-3021.3676757812,z=5.9010419845582},
	["magazzinovendita_101"] = {peso = 10000,pre = 1500000,x=1163.9875488282,y=-3020.8264160156,z=5.9021406173706},
	["magazzinovendita_102"] = {peso = 10000,pre = 1500000,x=1163.9875488282,y=-3026.1118164062,z=5.9010419845582},
	["magazzinovendita_103"] = {peso = 10000,pre = 1500000,x=1163.9876708984,y=-3030.7868652344,z=5.9010443687438},
	["magazzinovendita_104"] = {peso = 10000,pre = 1500000,x=1163.8784179688,y=-3037.125,z=5.9020957946778},
	["magazzinovendita_105"] = {peso = 10000,pre = 1500000,x=1163.8786621094,y=-3041.904296875,z=5.902142047882},
	["magazzinovendita_106"] = {peso = 10000,pre = 1500000,x=1163.8758544922,y=-3047.0598144532,z=5.9010543823242}
}


for nome,_ in pairs(casse) do
	local user_id = 0
	MySQL.execute("vRP/insert_user_magazzino", {magazzino = nome, user_id = json.encode(user_id)})
end

local function build_client_mgazzini(source)
  local user_id = vRP.getUserId({source})
  if user_id ~= nil then
    for nome,dati in pairs(casse) do

      local function chest_enter()
        local user_id = vRP.getUserId({source})
		if user_id ~= nil then
			if not vRP.hasPermission({user_id,"police.fine"}) then
				vRPmz.EntratoMagazzino(source,user_id,nome,dati)
			else
				vRPmz.PulottoEntrato(source,user_id,nome,dati)
			end
        end
      end

      local function chest_leave()
        vRP.closeMenu({source})
        vRP.closeMenu({source})
        SetTimeout(200, function()
          vRP.closeMenu({source})
          vRP.closeMenu({source})
        end)
      end


	  vRP.setArea({source,"vRP:mag"..nome,dati.x,dati.y,dati.z,2,1.5,chest_enter,chest_leave,true})
    end
  end
end

function vRPmz.PulottoEntrato(source,user_id,name,val)
	MySQL.query("vRP/get_user_magazzino", {magazzino = name}, function(rows, affected)
		local puser_id = tonumber(rows[1].user_id)
		if puser_id ~= 0 then
			vRP.request({source, "Vuoi forzare questo magazzino?", 15, function(source,ok)
				if ok then 
					local source_pr = vRP.getUserSource({puser_id})
					if source_pr ~= nil then
						vRP.request({source_pr, "La polizia vuole aprire il tuo magazzino, acconsenti?", 15, function(source_pr,ok)
							if ok then 
								vRP.openChest({source, name, val.peso,false,nil,nil})
							else
								vRPclient.notify(source,{"~r~Il player ha rifiutato.."})
							end
						end})
					else
						vRPclient.notify(source,{"~r~Player offline.."})
					end
				end
			end})
		end
	end)
end

function vRPmz.EntratoMagazzino(source,user_id,name,val)
	MySQL.query("vRP/get_user_magazzino", {magazzino = name}, function(rows, affected)
		local puser_id = tonumber(rows[1].user_id)
	  if puser_id ~= 0 then
		if puser_id == user_id then
			print("Magazzino("..user_id..","..name..")")
			vRP.openChest({source, name, val.peso,false,nil,nil})
		else
			vRPclient.notify(source,{"~r~Questo magazzino è già comprato.."})
		end
	  else
		vRP.request({source, "Vuoi comprare questo magazzino per "..val.pre.."$ PESO("..val.peso..")", 15, function(source,ok)
			if ok then 
				if vRP.tryPayment({user_id,val.pre}) then
					MySQL.execute("vRP/set_user_magazzino", {magazzino = name, user_id = json.encode(user_id)})
				else
					vRPclient.notify(source,{"~r~Non hai abbastanza soldi"})
				end
			end
		end})
	  end
	end)
end

function vRPmz.CambiaMarker(source,user_id,name,val)
	MySQL.query("vRP/get_user_magazzino", {magazzino = name}, function(rows, affected)
		local puser_id = tonumber(rows[1].user_id)
	  if puser_id ~= 0 then
		if puser_id == user_id then
			vRPclient.addMarker(source,{val.x,val.y,val.z-1,0.7,0.7,0.5,  0, 255, 0,  125,150})
		else
			vRPclient.addMarker(source,{val.x,val.y,val.z-1,0.7,0.7,0.5,  230, 255, 0,  125,150})
		end
	  else
		vRPclient.addMarker(source,{val.x,val.y,val.z-1,0.7,0.7,0.5,  230, 255, 0,  125,150})
	  end
	end)
end

AddEventHandler("vRP:playerSpawn",function(user_id, source, first_spawn)
  if first_spawn then
    build_client_mgazzini(source)
  end
end)