vRPsc = {}
Tunnel.bindInterface("vrp_sacco",vRPsc)
vRPserver = Tunnel.getInterface("vRP","vrp_sacco")
SCserver = Tunnel.getInterface("vrp_sacco","vrp_sacco")
vRP = Proxy.getInterface("vRP")

maschera = false
SaccoObj = nil
mask = false

function vRPsc.MettiMascheraClient()
	local playerPed  = GetPlayerPed(PlayerId())
	local coords     = GetEntityCoords(playerPed)
	local bone       = GetPedBoneIndex(playerPed, 12844)
	local SaccoModel = GetHashKey("prop_money_bag_01")

	RequestModel(SaccoModel)
	while not HasModelLoaded(SaccoModel) do
		Citizen.Wait(0)
	end

	SaccoObj = CreateObject(SaccoModel, coords.x, coords.y, coords.z, 1, 1, 0)
	AttachEntityToEntity(SaccoObj, playerPed, bone, 0.2, 0.04, 0.0, -74.0, 269.0, 10.0, 1, 1, 0, 0, 2, 1)
	maschera = true
	vRP.playScreenEffect({"PeyoteEndIn",-1})
end

function vRPsc.TogliMascheraClient()
	DeleteEntity(SaccoObj)
	StopAllScreenEffects()
	maschera = false
end

function vRPsc.ConSaccoInTesta()
	return maschera
end