MySQL = module("vrp_mysql", "MySQL")
local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")
local htmlEntities = module("vrp", "lib/htmlEntities")

vRPsc = {}
vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP","vrp_sacco")
SCclient = Tunnel.getInterface("vrp_sacco","vrp_sacco")
Tunnel.bindInterface("vrp_sacco",vRPsc)

local cfg = module("vrp_sacco", "cfg/attrezzatura")

local choice_mettisacco = {function(player,choice) 
	local user_id = vRP.getUserId({player})
    if user_id ~= nil then
      vRPclient.getNearestPlayer(player,{5},function(nplayer) 
        local nuser_id = vRP.getUserId({nplayer})
		if nuser_id ~= nil then
		  vRP.request({nplayer, "Il player ti vuole mettere un sacco in testa..", 20, function(nplayer,ok)
			if ok then
				SCclient.MettiMascheraClient(nplayer,{})
			else
				vRPclient.notify(player,{"~r~Il player ha rifiutato..."})
			end
		  end})
		else
			vRPclient.notify(player,{"~r~Nessun player vicino a te."})
		end
	  end)
	end
end}
  
local choice_toglisacco = {function(player,choice) 
	local user_id = vRP.getUserId({player})
    if user_id ~= nil then
      vRPclient.getNearestPlayer(player,{5},function(nplayer) 
        local nuser_id = vRP.getUserId({nplayer})
		if nuser_id ~= nil then
		  SCclient.ConSaccoInTesta(nplayer,{}, function(ok)
			if ok then
				SCclient.TogliMascheraClient(nplayer,{})
			else
				vRPclient.notify(player,{"~r~Il player non ha il sacco in testa..."})
			end
		  end)
		else
			vRPclient.notify(player,{"~r~Nessun player vicino a te."})
		end
	  end)
	end
end}

local choice_toglisaccotestesso = {function(player,choice) 
	local user_id = vRP.getUserId({player})
    if user_id ~= nil then
	  SCclient.ConSaccoInTesta(player,{}, function(ok)
		if ok then
			vRPclient.notify(player,{"~b~Ti toglierai il sacco tra 10 secondi..."})
			SetTimeout(10000,function()
				SCclient.TogliMascheraClient(player,{})
			end)
		else
			vRPclient.notify(player,{"~r~Non hai il sacco in testa..."})
		end
	  end)
	end
end}

local maschera_menu = {function(player,choice)
	local user_id = vRP.getUserId({player})
	local menu = {}
	menu.name = "Sacco"
	menu.css = {top="75px",header_color="rgba(0,0,0,0.90)"}
	menu.onclose = function(player) vRP.openMainMenu({player}) end
  
		if vRP.hasPermission({user_id,"player.loot"}) then
			menu["Metti (player)"] = choice_mettisacco
		end  
  
		if vRP.hasPermission({user_id,"player.loot"}) then
			menu["Togli"] = choice_toglisacco
		end  

		if vRP.hasPermission({user_id,"player.loot"}) then
			menu["Togliti sacco"] = choice_toglisaccotestesso
		end  
  
	vRP.openMenu({player,menu})
  end}
  
  vRP.registerMenuBuilder({"main", function(add, data)
	local player = data.player
  
	local user_id = vRP.getUserId({player})
	if user_id ~= nil then
	  local choices = {}
  
	  	if vRP.hasPermission({user_id,"player.loot"}) then
			choices["Sacco"] = maschera_menu
		end
		  
		  add(choices)
	end
  end})
