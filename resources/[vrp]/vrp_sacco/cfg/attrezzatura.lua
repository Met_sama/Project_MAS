local cfg = {}

cfg.attrezzatura = {
	["c4"] = { 
    name = "C4",
    desc = "Un semplice ma efficace esplosivo.",
    choices = function(args)
	  local menu = {}
      menu["Piazza"] = {function(player,choice)
  	    local user_id = vRP.getUserId({player})
				if user_id ~= nil then
					if vRP.hasPermission({user_id,"player.loot"}) then
						C4client.ControllaMacchina(player,{}, function(val)
						if val ~= false then 
							if vRP.tryGetInventoryItem({user_id,"c4",1,false}) then
								vRPclient.playAnim(player,{false,{{"weapons@projectile@sticky_bomb","plant_vertical_far",1}},false})
								vRPc4.SyncC4Server(val)
								vRP.closeMenu({player})
								SetTimeout(500,function()
									vRP.closeMenu({player})
									vRP.closeMenu({player})
								end)
							end
						else
							vRPclient.notify(player,{"~r~Nessuna macchina vicino a te oppure player dentro la macchina..."})
						end
						end)
					else
						vRPclient.notify(player,{"~r~Non sei autorizzato ad utilizzare questo item"})
			   		end
  	    		end
  	  		end}
	    return menu
    end,
	weight = 0.2
	}
}

return cfg