local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")

vRPtow = {}
vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP","vRP_tow")
BMclient = Tunnel.getInterface("vRP_tow","vRP_tow")
Tunnel.bindInterface("vrp_tow",vRPtow)
Proxy.addInterface("vrp_tow",vRPtow)

towGroup = "police.announce"
impoundFee = 0
isTowing = {}

local allowedTowModels = { "flatbed" }

function isPlayerTowTrucker(user_id)
	if(vRP.hasPermission({user_id, towGroup}))then
		return true
	else
		return false
	end
	return false
end

function setPlayerAsNotTowing(thePlayer)
	local user_id = vRP.getUserId({thePlayer})
	isTowing[user_id] = nil
end

RegisterServerEvent("setPlayerAsNotTowing")
AddEventHandler("setPlayerAsNotTowing", function(theVehicle)
	local user_id = vRP.getUserId({source})
	isTowing[user_id] = nil
end)

RegisterServerEvent("setPlayerAsTowing")
AddEventHandler("setPlayerAsTowing", function(theVehicle)
	local user_id = vRP.getUserId({source})
	isTowing[user_id] = theVehicle
end)

local function ch_towVehicle(player, choice)
	TriggerClientEvent('towVehicle', player)
	vRP.closeMenu({player})
end

local function ch_unimpoundVehicle(player, choice)
	setPlayerAsNotTowing(player)
	TriggerClientEvent('towVehicle', player)
	vRP.closeMenu({player})
end

local function ch_impoundVehicle(player, choice)
	TriggerClientEvent('deleteTowedVehicle', player)
	local user_id = vRP.getUserId({player})
	vRP.closeMenu({player})
	setPlayerAsNotTowing(player)
	vRP.giveMoney({user_id, impoundFee})
	vRPclient.notify(player, {"~w~[T.T.C] ~g~Hai sequestrato il veicolo per ~r~$"..impoundFee})
end

local function ch_repair(player,choice)
	local user_id = vRP.getUserId({player})
	if user_id ~= nil then
		vRPclient.getNearestVehicle(player, {7}, function(veh)
			if(veh)then
				vRPclient.playAnim(player,{false,{task="WORLD_HUMAN_WELDING"},false})
				SetTimeout(15000, function()
					vRPclient.fixeNearestVehicle(player,{7})
					vRPclient.stopAnim(player,{false})
				end)
			else
				vRPclient.notify(player, {"~w~[T.O.W] ~r~Nessun veicolo vicino"})
			end
		end)
	end
end

local function ch_towMenu(player, choice)
	vRP.buildMenu({"Menu Veicolo", {player = player}, function(menu)
		menu.name = "Menu Veicolo"
		menu.css={top="75px",header_color="rgba(0,255,213,0.75)"}
		menu.onclose = function(player) vRP.openMainMenu({player}) end -- nest menu
		local user_id = vRP.getUserId({player})
		if(isTowing[user_id] == nil)then
			menu["Traina Veicolo"] = {ch_towVehicle,"Rimorchia il veicolo vicino a te"}
		else
			menu["Rilascia Veicolo"] = {ch_unimpoundVehicle,"Rilascia il veicolo dalla piattaforma"}
		end
		menu["Ripara Veicolo"] = {ch_repair,"Ripara Veicolo"}
		vRP.openMenu({player,menu})
	end})
end

local function impoundVehicle(player)
	local user_id = vRP.getUserId({player})
	if(isTowing[user_id] ~= nil)then
		vRP.buildMenu({"Menu Sequestro", {player = player}, function(menu2)
			menu2.name = "Sequestra Veicolo"
			menu2.css={top="75px",header_color="rgba(0,255,213,0.75)"}
			menu2.onclose = function(player) vRP.closeMenu({player}) end -- nest menu
						
			menu2["Si"] = {ch_impoundVehicle,"Veicolo dalla rampa"}
			menu2["No"] = {function(player) vRP.closeMenu({player}) end}
			vRP.openMenu({player,menu2})
		end})
	else
		vRPclient.notify(player,{"~w~[T.O.W] ~r~Non c'è nessun veicolo nella rampa!"})
	end
end

AddEventHandler("vRP:playerSpawn",function(user_id, source, first_spawn)
	isTowing[user_id] = nil
  
	local function impound_enter()
        local user_id = vRP.getUserId({source})
        if (user_id ~= nil and isPlayerTowTrucker(user_id)) then
			impoundVehicle(source) 
        end
    end

    local function impound_leave()
		vRP.closeMenu({source})
    end
	
vRP.registerMenuBuilder({"main", function(add, data)
	local user_id = vRP.getUserId({data.player})
	if user_id ~= nil then
		choices = {}
		if (isPlayerTowTrucker(user_id) == true) then
			choices["T.O.W"] = {ch_towMenu, "Menu"}
		end
		add(choices)
	end
end})