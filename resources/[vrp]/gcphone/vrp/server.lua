--vrpMySQL = module("vrp_mysql", "MySQL")
local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")
local htmlEntities = module("lib/htmlEntities")

vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP","vRP_gcphone")
local lang = vRP.lang
--[[
--vrpMySQL.createCommand("vRP/get_phones", "SELECT * FROM users WHERE identifier != NULL AND phone_number != NULL")
--vrpMySQL.createCommand("vRP/get_phone", "SELECT * FROM users WHERE identifier = @identifier")
--vrpMySQL.createCommand("vRP/phone_from_id", "SELECT * FROM users WHERE identifier = @id")
--vrpMySQL.createCommand("vRP/id_from_phone", "SELECT * FROM users WHERE phone_number = @number")
--vrpMySQL.createCommand("vRP/police_phone", "UPDATE users SET phone_number = @phone, oldphone = @old WHERE identifier = @identifier")
--vrpMySQL.createCommand("vRP/check_police", "SELECT * FROM users WHERE phone_number = @phone")
--vrpMySQL.createCommand("vRP/update_oldphone", "UPDATE users SET oldphone = @old WHERE identifier = @identifier, phone_number = @phone")
]]
PhoneNumbers        = {}

politicoords = {
  {["x"] = 440.22341918945, ["y"] = -975.72308349609, ["x"] = 30.689586639404}
}

text = {
  servicecall = "Ricevuto {1} chiamata, la prendi? <em>{2}</em>",
  opkaldtaget = "La chiamata è stata presa!"
}

services = {
  ["Polizia"] = {
    blipid = 304,
    blipcolor = 38,
    alert_time = 30, -- 5 minutes
    alert_permission = "police.service",
    alert_notify = "~r~Police alert:~n~~s~",
    notify = "~b~Hai chiamato la polizia.",
    answer_notify = "~b~Una o più volanti sono in arrivo."
  },
  ["Medico"] = {
    blipid = 153,
    blipcolor = 1,
    alert_time = 30, -- 5 minutes
    alert_permission = "emergency.service",
    alert_notify = "~r~Chiamata medica ricevuta:~n~~s~",
    notify = "~b~Hai chiamato un medico.",
    answer_notify = "~b~Chiamata elaborata."
  },
  ["Import&Export"] = {
    blipid = 198,
    blipcolor = 5,
    alert_time = 300,
    alert_permission = "import.whitelisted",
    alert_notify = "~r~Chiamata Import & Export ricevuta:~n~~s~",
    notify = "~b~Hai chiamato un dipendente.",
    answer_notify = "~b~Chiamata elaborata."
  },
  ["Meccanico"] = {
    blipid = 198,
    blipcolor = 1,
    alert_time = 300,
    alert_permission = "dipendente.aci",
    alert_notify = "~y~Chiamata aci ricevuta:~n~~s~",
    notify = "~y~Hai chiamato un aci.",
    answer_notify = "~y~Chiamata elaborata."
  }
--  ["Mekaniker"] = {
--    blipid = 446,
--    blipcolor = 5,
--    alert_time = 300,
--    alert_permission = "repair.service",
--    alert_notify = "~y~Mekaniker alarm:~n~~s~",
--    notify = "~y~Du har ringet efter en mekaniker.",
--    answer_notify = "~y~En mekaniker er sendt ud."
--  }
}


function notifyAlertSMS (number, alert, listSrc)
  if PhoneNumbers[number] ~= nil then
    for k,v in pairs(PhoneNumbers) do
      if k == number then
        local n = getPhoneNumberFromId(v.id)
        if n ~= nil then
          TriggerEvent('gcPhone:_internalAddMessage', number, n, 'SMS fra #' .. alert.numero  .. ' : ' .. alert.message, 0, function (smsMess)
            TriggerClientEvent("gcPhone:receiveMessage", tonumber(k), smsMess)
          end)
          if alert.coords ~= nil then
            TriggerEvent('gcPhone:_internalAddMessage', number, n, 'GPS: ' .. alert.coords.x .. ', ' .. alert.coords.y, 0, function (smsMess)
              TriggerClientEvent("gcPhone:receiveMessage", tonumber(k), smsMess)
            end)
          end
        end
      end
    end
  end
end

RegisterServerEvent('vrp_addons_gcphone:startCallpoli')
AddEventHandler('vrp_addons_gcphone:startCallpoli', function (callnumber, message, coords)
  local source = source
  getPhoneNumber(source, function(plynumber)
    if not callnumber == 112 or not callnumber == "112" then
      local callid = getIdFromPhone(callnumber)
      local callsource = vRP.getUserSource({callid})
      notifyAlertSMS(callnumber, {message = message,coords = coords,numero = plynumber,}, callsource)
    else
      local service_name = "Polizia"
      local service = services[service_name]
      local answered = false
      if service then
        local players = {}
        users = vRP.getUsers({})
        for k,v in pairs(users) do
          local player = vRP.getUserSource({k})
          -- check user
          if vRP.hasPermission({k,service.alert_permission}) and player ~= nil then
            table.insert(players,player)
          end
        end
      
        local msg = message
        local memess = _internalAddMessage(plynumber, "Servizio", msg, 1)
        local memess = _internalAddMessage("Servizio", plynumber, "Grazie per la vostra richiesta. Invieremo qualcuno il prima possibile.", 1)
        -- send notify and alert to all listening players
        for k,v in pairs(players) do 
          vRPclient.notify(v,{service.alert_notify..msg})

          -- add position for service.time seconds
          local x,y,z = coords.x,coords.y,coords.z
          vRPclient.addBlip(v,{x,y,z,service.blipid,service.blipcolor,"("..service_name..") "..msg}, function(bid)
            SetTimeout(service.alert_time*1000,function()
              vRPclient.removeBlip(v,{bid})
            end)
          end)
      
          if service then
            vRP.request({v,"Polizia: ".. msg.. "", 30, function(v,ok)
              if ok then -- take the call
                if not answered then
                  -- answer the call
                  vRPclient.notify(source,{service.answer_notify})
                  vRPclient.setGPS(v,{x,y})
                  answered = true
                else
                  vRPclient.notify(v,{text.opkaldtaget})
                end
              end
            end})
          end
        end
      end
    end
  end)
end)

RegisterServerEvent('vrp_addons_gcphone:startCallambu')
AddEventHandler('vrp_addons_gcphone:startCallambu', function (callnumber, message, coords)
  local source = source
  getPhoneNumber(source, function(plynumber)
    if not callnumber == 114 or not callnumber == "114" then
      local callid = getIdFromPhone(callnumber)
      local callsource = vRP.getUserSource({callid})
      notifyAlertSMS(callnumber, {message = message,coords = coords,numero = plynumber,}, callsource)
    else
      local service_name = "Medico"
      local service = services[service_name]
      local answered = false
      if service then
        local players = {}
        users = vRP.getUsers({})
        for k,v in pairs(users) do
          local player = vRP.getUserSource({k})
          -- check user
          if vRP.hasPermission({k,service.alert_permission}) and player ~= nil then
            table.insert(players,player)
          end
        end
      
        local msg = message
        local memess = _internalAddMessage(plynumber, "Servizio", msg, 1)
        local memess = _internalAddMessage("Servizio", plynumber, "Grazie per la vostra richiesta. Invieremo qualcuno il prima possibile.", 1)
        -- send notify and alert to all listening players
        for k,v in pairs(players) do 
          vRPclient.notify(v,{service.alert_notify..msg})

          -- add position for service.time seconds
          local x,y,z = coords.x,coords.y,coords.z
          vRPclient.addBlip(v,{x,y,z,service.blipid,service.blipcolor,"("..service_name..") "..msg}, function(bid)
            SetTimeout(service.alert_time*1000,function()
              vRPclient.removeBlip(v,{bid})
            end)
          end)
      
          if service then
            vRP.request({v,"Medico: ".. msg.. "", 30, function(v,ok)
              if ok then -- take the call
                if not answered then
                  -- answer the call
                  vRPclient.notify(source,{service.answer_notify})
                  vRPclient.setGPS(v,{x,y})
                  answered = true
                else
                  vRPclient.notify(v,{text.opkaldtaget})
                end
              end
            end})
          end
        end
      end
    end
  end)
end)

RegisterServerEvent('vrp_addons_gcphone:startCallmekan')
AddEventHandler('vrp_addons_gcphone:startCallmekan', function (callnumber, message, coords)
  local source = source
  getPhoneNumber(source, function(plynumber)
    if not callnumber == 120 or not callnumber == "120" then
      local callid = getIdFromPhone(callnumber)
      local callsource = vRP.getUserSource({callid})
      notifyAlertSMS(callnumber, {message = message,coords = coords,numero = plynumber,}, callsource)
    else
      local service_name = "Mekaniker"
      local service = services[service_name]
      local answered = false
      if service then
        local players = {}
        users = vRP.getUsers({})
        for k,v in pairs(users) do
          local player = vRP.getUserSource({k})
          -- check user
          if vRP.hasPermission({k,service.alert_permission}) and player ~= nil then
            table.insert(players,player)
          end
        end
      
        local msg = message
        local memess = _internalAddMessage(plynumber, "Servizio", msg, 1)
        local memess = _internalAddMessage("Servizio", plynumber, "Grazie per la vostra richiesta. Invieremo qualcuno il prima possibile.", 1)
        -- send notify and alert to all listening players
        for k,v in pairs(players) do 
          vRPclient.notify(v,{service.alert_notify..msg})

          -- add position for service.time seconds
          local x,y,z = coords.x,coords.y,coords.z
          vRPclient.addBlip(v,{x,y,z,service.blipid,service.blipcolor,"("..service_name..") "..msg}, function(bid)
            SetTimeout(service.alert_time*1000,function()
              vRPclient.removeBlip(v,{bid})
            end)
          end)
      
          if service then
            vRP.request({v,"Mekaniker opkald: ".. msg.. "", 30, function(v,ok)
              if ok then -- take the call
                if not answered then
                  -- answer the call
                  vRPclient.notify(source,{service.answer_notify})
                  vRPclient.setGPS(v,{x,y})
                  answered = true
                else
                  vRPclient.notify(v,{text.opkaldtaget})
                end
              end
            end})
          end
        end
      end
    end
  end)
end)

RegisterServerEvent('vrp_addons_gcphone:startCalltaxi')
AddEventHandler('vrp_addons_gcphone:startCalltaxi', function (callnumber, message, coords)
  local source = source
  getPhoneNumber(source, function(plynumber)
    if not callnumber == 118 or not callnumber == "118" then
      local callid = getIdFromPhone(callnumber)
      local callsource = vRP.getUserSource({callid})
      notifyAlertSMS(callnumber, {message = message,coords = coords,numero = plynumber,}, callsource)
    else
      local service_name = "Import&Export"
      local service = services[service_name]
      local answered = false
      if service then
        local players = {}
        users = vRP.getUsers({})
        for k,v in pairs(users) do
          local player = vRP.getUserSource({k})
          -- check user
          if vRP.hasPermission({k,service.alert_permission}) and player ~= nil then
            table.insert(players,player)
          end
        end
      
        local msg = message
        local memess = _internalAddMessage(plynumber, "Servizio", msg, 1)
        local memess = _internalAddMessage("Servizio", plynumber, "Grazie per la vostra richiesta. Invieremo qualcuno il prima possibile.", 1)
        -- send notify and alert to all listening players
        for k,v in pairs(players) do 
          vRPclient.notify(v,{service.alert_notify..msg})

          -- add position for service.time seconds
          local x,y,z = coords.x,coords.y,coords.z
          vRPclient.addBlip(v,{x,y,z,service.blipid,service.blipcolor,"("..service_name..") "..msg}, function(bid)
            SetTimeout(service.alert_time*1000,function()
              vRPclient.removeBlip(v,{bid})
            end)
          end)
      
          if service then
            vRP.request({v,"Import & Export: ".. msg.. "", 30, function(v,ok)
              if ok then -- take the call
                if not answered then
                  -- answer the call
                  vRPclient.notify(source,{service.answer_notify})
                  vRPclient.setGPS(v,{x,y})
                  answered = true
                else
                  vRPclient.notify(v,{text.opkaldtaget})
                end
              end
            end})
          end
        end
      end
    end
  end)
end)

RegisterServerEvent('vrp_addons_gcphone:startCalladvo')
AddEventHandler('vrp_addons_gcphone:startCalladvo', function (callnumber, message, coords)
  local source = source
  getPhoneNumber(source, function(plynumber)
    if not callnumber == 116 or not callnumber == "116" then
      local callid = getIdFromPhone(callnumber)
      local callsource = vRP.getUserSource({callid})
      notifyAlertSMS(callnumber, {message = message,coords = coords,numero = plynumber,}, callsource)
    else
      local service_name = "Meccanico"
      local service = services[service_name]
      local answered = false
      if service then
        local players = {}
        users = vRP.getUsers({})
        for k,v in pairs(users) do
          local player = vRP.getUserSource({k})
          -- check user
          if vRP.hasPermission({k,service.alert_permission}) and player ~= nil then
            table.insert(players,player)
          end
        end
      
        local msg = message
        local memess = _internalAddMessage(plynumber, "Servizio", msg, 1)
        local memess = _internalAddMessage("Servizio", plynumber, "Grazie per la vostra richiesta. Invieremo qualcuno il prima possibile.", 1)
        -- send notify and alert to all listening players
        for k,v in pairs(players) do 
          vRPclient.notify(v,{service.alert_notify..msg})

          -- add position for service.time seconds
          local x,y,z = coords.x,coords.y,coords.z
          vRPclient.addBlip(v,{x,y,z,service.blipid,service.blipcolor,"("..service_name..") "..msg}, function(bid)
            SetTimeout(service.alert_time*1000,function()
              vRPclient.removeBlip(v,{bid})
            end)
          end)
      
          if service then
            vRP.request({v,"Meccanico: ".. msg.. "", 30, function(v,ok)
              if ok then -- take the call
                if not answered then
                  -- answer the call
                  vRPclient.notify(source,{service.answer_notify})
                  vRPclient.setGPS(v,{x,y})
                  answered = true
                else
                  vRPclient.notify(v,{text.opkaldtaget})
                end
              end
            end})
          end
        end
      end
    end
  end)
end)

function getPhoneNumber(source, callback) 
  local user_id = vRP.getUserId({source})
  MySQL.Async.fetchAll('SELECT * FROM users WHERE identifier = @identifier',{
    ['@identifier'] = user_id
  }, function(result)
    callback(result[1].phone_number)
  end)
end

function getPhoneNumberFromId(user_id)
  --vrpMySQL.query("vRP/phone_from_id", {id = user_id}, function(rows, affected)
    if #rows > 0 then
      return rows[1].phone_number
    else
      return nil
    end
--end)
end

function getIdFromPhone(number)
  if number ~= nil then
    --vrpMySQL.query("vRP/id_from_phone", {number = number}, function(rows,affected)
      if #rows > 0 then
        return rows[1].identifier
      else
        return nil
      end
    --end)
  end
end

function _internalAddMessage(transmitter, receiver, message, owner)
  local Query = "INSERT INTO phone_messages (`transmitter`, `receiver`,`message`, `isRead`,`owner`) VALUES(@transmitter, @receiver, @message, @isRead, @owner);"
  local Query2 = 'SELECT * from phone_messages WHERE `id` = (SELECT LAST_INSERT_ID());'
  local Parameters = {
      ['@transmitter'] = transmitter,
      ['@receiver'] = receiver,
      ['@message'] = message,
      ['@isRead'] = owner,
      ['@owner'] = owner
  }
  return MySQL.Sync.fetchAll(Query .. Query2, Parameters)[1]
end
