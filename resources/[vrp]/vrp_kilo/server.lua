MySQL = module("vrp_mysql", "MySQL")
local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")
local htmlEntities = module("vrp", "lib/htmlEntities")

vRPkl = {}
vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP","vrp_kilo")
KLclient = Tunnel.getInterface("vrp_kilo","vrp_kilo")
Tunnel.bindInterface("vrp_kilo",vRPkl)

local cfg = module("vrp_kilo", "cfg/attrezzatura")

MySQL.createCommand("vRP/kilo_agg_data", [[
    ALTER TABLE vrp_user_vehicles ADD IF NOT EXISTS vehicle_km varchar(255) NOT NULL DEFAULT '0'
]])

MySQL.createCommand("vRP/kilo_get_vehicle","SELECT vehicle FROM vrp_user_vehicles WHERE user_id = @user_id AND vehicle = @vehicle") -- lscustoms and plygarages
MySQL.createCommand("vRP/kilo_update_vehicle","UPDATE vrp_user_vehicles SET vehicle_km=@vehicle_km WHERE user_id=@user_id AND vehicle=@vehicle") 
MySQL.createCommand("vRP/kilo_get_km","SELECT vehicle_km FROM vrp_user_vehicles WHERE user_id = @user_id AND vehicle = @vehicle") -- lscustoms and plygarages

MySQL.query("vRP/kilo_agg_data")

function vRPkl.ControllaSeSua(veh,vname,vtarga)
    local player = source
    local user_id = vRP.getUserId({player})
    if user_id ~= nil then
        MySQL.query("vRP/kilo_get_vehicle", {user_id = user_id, vehicle = vname}, function(rows, affected)
            if #rows > 0 then -- has vehicle
                KLclient.ControllaSeSuaClient(player,{veh})
                MySQL.query("vRP/kilo_get_km", {user_id = user_id, vehicle = vname}, function(rows, affected)
                    local km = tonumber(rows[1].vehicle_km)
                    KLclient.AggiornaKMclient(player,{km})
                end)
            end
        end)
    end
end

function vRPkl.AggiornaKilometriVeh(vname,km)
    local player = source
    local user_id = vRP.getUserId({player})
    if user_id ~= nil then
        MySQL.query("vRP/kilo_get_km", {user_id = user_id, vehicle = vname}, function(rows, affected)
            km = km+(tonumber(rows[1].vehicle_km))
            KLclient.AggiornaKMclient(player,{km})
            MySQL.query("vRP/kilo_update_vehicle", {user_id = user_id, vehicle = vname, vehicle_km = km})
        end)
    end
end
