vRPkl = {}
Tunnel.bindInterface("vrp_kilo",vRPkl)
vRPserver = Tunnel.getInterface("vRP","vrp_kilo")
KLserver = Tunnel.getInterface("vrp_kilo","vrp_kilo")
vRP = Proxy.getInterface("vRP")

Oldveh = nil
vehName = nil
metri = 0
timer = false
km_hud = 0

Citizen.CreateThread(function()
  	while true do
	Citizen.Wait(100)
	  local PlayerPed = GetPlayerPed(PlayerId())
	  	if IsPedSittingInAnyVehicle(PlayerPed) then
			if Oldveh ~= GetVehiclePedIsIn(PlayerPed, false) then
				Oldveh = nil
				metri = 0
				vRPkl.Controllo()
			else
				timer = true
			end
		else
			timer = false
		end
	end
end)

function vRPkl.Controllo()
	local PlayerPed = GetPlayerPed(PlayerId())
	local veh = GetVehiclePedIsIn(PlayerPed, false)
	vehName = GetDisplayNameFromVehicleModel(GetEntityModel(veh))
	local vehTarga = GetVehicleNumberPlateText(veh)
	KLserver.ControllaSeSua({veh,vehName,vehTarga})
end

function vRPkl.ControllaSeSuaClient(veh)
	Oldveh = veh
	timer = true
end

function vRPkl.AggiornaKMclient(nw_km_hud)
	km_hud = nw_km_hud
end

Citizen.CreateThread(function()
	while true do 
	Citizen.Wait(1)
		if IsPedInAnyVehicle(GetPlayerPed(-1), false) and timer then
			drawTxt(0.61, 1.341, 1.0,1.0,0.55, "~w~" .. km_hud, 255, 255, 255, 255)
			drawTxt(0.640, 1.353, 1.0,1.0,0.4, "~w~ km", 255, 255, 255, 255)
			drawRct(0.11, 0.845, 0.045, 0.037, 0,0,0,150)  
		end
	end
end)



Citizen.CreateThread(function()
	while true do
	Citizen.Wait(0)
		if timer then
			local kmh = GetEntitySpeed(Oldveh)*3.6
			metri = metri + (kmh / 360)
			if metri >= 1000 then
				metri = 0
				KLserver.AggiornaKilometriVeh({vehName,1})
			end 
			--DrawText2D(0.8,0.0,"~b~Kilo = "..math.ceil(metri),0.6 )
		end
	end
end)

function DisplayNotification(string)
  SetTextComponentFormat("STRING")
  AddTextComponentString(string)
  DisplayHelpTextFromStringLabel(0, 0, 1, -1)
end

function drawTxt(x,y ,width,height,scale, text, r,g,b,a)
    SetTextFont(4)
    SetTextProportional(0)
    SetTextScale(scale, scale)
    SetTextColour(r, g, b, a)
    SetTextDropShadow(0, 0, 0, 0,255)
    SetTextEdge(2, 0, 0, 0, 255)
    SetTextDropShadow()
    SetTextOutline()
    SetTextEntry("STRING")
    AddTextComponentString(text)
    DrawText(x - width/2, y - height/2 + 0.005)
end

function drawRct(x,y,width,height,r,g,b,a)
	DrawRect(x + width/2, y + height/2, width, height, r, g, b, a)
end

function DrawText2D(x,y,text,scale)
	SetTextColour(204, 204, 0, 255)
	SetTextFont(0)
	SetTextScale(scale, scale)
	SetTextWrap(0.0, 1.0)
	SetTextCentre(false)
	SetTextDropshadow(2, 2, 0, 0, 0)
	SetTextEdge(1, 255, 255, 255, 205)
	SetTextEntry("STRING")
	AddTextComponentString(text)
	DrawText(x, y)
  end