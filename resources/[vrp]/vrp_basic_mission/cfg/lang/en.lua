
local lang = {
  repair = "Riparazioni {1}.",
  reward = "Compenso: {1} $.",
  bankdriver = "Trasferisci denaro per {1}.",
  reward = "Compenso: {1} $.",
  delivery = {
    title = "Consegna",
    item = "- {2} {1}"
  },
  drugseller = {
	title = "Vendi droghe",
	item = "- {2} {1}"
	},
	drugselle = {
	title = "Vendi alcolici",
	item = "- {2} {1}"
	},
  gioielliere = {
	title = "Vendi gioielli",
	item = "- {2} {1}"
	},
	minatore = {
	title = "Vendi minerali",
	item = "- {2} {1}"
	},
	impresa = {
	title = "Vendi oggetti",
	item = "- {2} {1}"
	},
	agri = {
	title = "Vendi prodotti agricoli",
	item = "- {2} {1}"
	},
	taglia = {
	title = "Vendi derivati del legno",
	item = "- {2} {1}"
	},
	fisherman = {
	title = "Vendi Pesce",
	item = "- {2} {1}"
    },
  weapons_smuggler = {
	title = "Consegna le armi!",
	item = "- {2} {1}"
    },
  medical = {
	title = "Consegna il sangue",
	item = "- {2} {1}"
    },
  hacker = {
	title = "Vendi le info sulle carte di credito",
	item = "- {2} {1}"
	},
	pilot = {
	title = "Consegna il cargo!",
	item = "- {2} {1}"
    }
}

return lang
