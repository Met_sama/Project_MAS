MySQL = module("vrp_mysql", "MySQL")
local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")
local htmlEntities = module("vrp", "lib/htmlEntities")

vRPc4 = {}
vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP","vrp_c4")
C4client = Tunnel.getInterface("vrp_c4","vrp_c4")
Tunnel.bindInterface("vrp_c4",vRPc4)

local cfg = module("vrp_c4", "cfg/attrezzatura")
for k,v in pairs(cfg.attrezzatura) do
	vRP.defInventoryItem({k,v.name,v.desc,v.choices,v.weight})
end

function vRPc4.SyncC4Server(veh)
	C4client.SyncC4Client(-1,{veh})
end
