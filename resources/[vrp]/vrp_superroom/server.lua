--[[
    FiveM Scripts
    Copyright C 2018  Sighmir

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    at your option any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
]]

local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")

MySQL = module("vrp_mysql", "MySQL")

vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP","vrp_superroom")
Gclient = Tunnel.getInterface("vRP_garages","vrp_superroom")

-- vehicle db / garage and lscustoms compatibility
MySQL.createCommand("vRP/super_columns", [[
ALTER TABLE vrp_user_vehicles ADD IF NOT EXISTS veh_type varchar(255) NOT NULL DEFAULT 'default' ;
ALTER TABLE vrp_user_vehicles ADD IF NOT EXISTS sequestro boolean NOT NULL;
ALTER TABLE vrp_user_vehicles ADD IF NOT EXISTS vehicle_plate varchar(255) NOT NULL;
ALTER TABLE vrp_user_vehicles ADD IF NOT EXISTS vehicle_colorprimary varchar(255) NOT NULL;
ALTER TABLE vrp_user_vehicles ADD IF NOT EXISTS vehicle_colorsecondary varchar(255) NOT NULL;
ALTER TABLE vrp_user_vehicles ADD IF NOT EXISTS vehicle_pearlescentcolor varchar(255) NOT NULL;
ALTER TABLE vrp_user_vehicles ADD IF NOT EXISTS vehicle_wheelcolor varchar(255) NOT NULL;
ALTER TABLE vrp_user_vehicles ADD IF NOT EXISTS vehicle_plateindex varchar(255) NOT NULL;
ALTER TABLE vrp_user_vehicles ADD IF NOT EXISTS vehicle_neoncolor1 varchar(255) NOT NULL;
ALTER TABLE vrp_user_vehicles ADD IF NOT EXISTS vehicle_neoncolor2 varchar(255) NOT NULL;
ALTER TABLE vrp_user_vehicles ADD IF NOT EXISTS vehicle_neoncolor3 varchar(255) NOT NULL;
ALTER TABLE vrp_user_vehicles ADD IF NOT EXISTS vehicle_windowtint varchar(255) NOT NULL;
ALTER TABLE vrp_user_vehicles ADD IF NOT EXISTS vehicle_wheeltype varchar(255) NOT NULL;
ALTER TABLE vrp_user_vehicles ADD IF NOT EXISTS vehicle_mods0 varchar(255) NOT NULL;
ALTER TABLE vrp_user_vehicles ADD IF NOT EXISTS vehicle_mods1 varchar(255) NOT NULL;
ALTER TABLE vrp_user_vehicles ADD IF NOT EXISTS vehicle_mods2 varchar(255) NOT NULL;
ALTER TABLE vrp_user_vehicles ADD IF NOT EXISTS vehicle_mods3 varchar(255) NOT NULL;
ALTER TABLE vrp_user_vehicles ADD IF NOT EXISTS vehicle_mods4 varchar(255) NOT NULL;
ALTER TABLE vrp_user_vehicles ADD IF NOT EXISTS vehicle_mods5 varchar(255) NOT NULL;
ALTER TABLE vrp_user_vehicles ADD IF NOT EXISTS vehicle_mods6 varchar(255) NOT NULL;
ALTER TABLE vrp_user_vehicles ADD IF NOT EXISTS vehicle_mods7 varchar(255) NOT NULL;
ALTER TABLE vrp_user_vehicles ADD IF NOT EXISTS vehicle_mods8 varchar(255) NOT NULL;
ALTER TABLE vrp_user_vehicles ADD IF NOT EXISTS vehicle_mods9 varchar(255) NOT NULL;
ALTER TABLE vrp_user_vehicles ADD IF NOT EXISTS vehicle_mods10 varchar(255) NOT NULL;
ALTER TABLE vrp_user_vehicles ADD IF NOT EXISTS vehicle_mods11 varchar(255) NOT NULL;
ALTER TABLE vrp_user_vehicles ADD IF NOT EXISTS vehicle_mods12 varchar(255) NOT NULL;
ALTER TABLE vrp_user_vehicles ADD IF NOT EXISTS vehicle_mods13 varchar(255) NOT NULL;
ALTER TABLE vrp_user_vehicles ADD IF NOT EXISTS vehicle_mods14 varchar(255) NOT NULL;
ALTER TABLE vrp_user_vehicles ADD IF NOT EXISTS vehicle_mods15 varchar(255) NOT NULL;
ALTER TABLE vrp_user_vehicles ADD IF NOT EXISTS vehicle_mods16 varchar(255) NOT NULL;
ALTER TABLE vrp_user_vehicles ADD IF NOT EXISTS vehicle_turbo varchar(255) NOT NULL;
ALTER TABLE vrp_user_vehicles ADD IF NOT EXISTS vehicle_tiresmoke varchar(255) NOT NULL;
ALTER TABLE vrp_user_vehicles ADD IF NOT EXISTS vehicle_xenon varchar(255) NOT NULL;
ALTER TABLE vrp_user_vehicles ADD IF NOT EXISTS vehicle_mods23 varchar(255) NOT NULL;
ALTER TABLE vrp_user_vehicles ADD IF NOT EXISTS vehicle_mods24 varchar(255) NOT NULL;
ALTER TABLE vrp_user_vehicles ADD IF NOT EXISTS vehicle_neon0 varchar(255) NOT NULL;
ALTER TABLE vrp_user_vehicles ADD IF NOT EXISTS vehicle_neon1 varchar(255) NOT NULL;
ALTER TABLE vrp_user_vehicles ADD IF NOT EXISTS vehicle_neon2 varchar(255) NOT NULL;
ALTER TABLE vrp_user_vehicles ADD IF NOT EXISTS vehicle_neon3 varchar(255) NOT NULL;
ALTER TABLE vrp_user_vehicles ADD IF NOT EXISTS vehicle_bulletproof varchar(255) NOT NULL;
ALTER TABLE vrp_user_vehicles ADD IF NOT EXISTS vehicle_smokecolor1 varchar(255) NOT NULL;
ALTER TABLE vrp_user_vehicles ADD IF NOT EXISTS vehicle_smokecolor2 varchar(255) NOT NULL;
ALTER TABLE vrp_user_vehicles ADD IF NOT EXISTS vehicle_smokecolor3 varchar(255) NOT NULL;
ALTER TABLE vrp_user_vehicles ADD IF NOT EXISTS vehicle_modvariation varchar(255) NOT NULL;
]])
MySQL.query("vRP/super_columns")

MySQL.createCommand("vRP/add_custom_vehicle","INSERT IGNORE INTO vrp_user_vehicles(user_id,vehicle,vehicle_plate,sequestro,veh_type,vehicle_colorprimary,vehicle_colorsecondary,vehicle_pearlescentcolor,vehicle_wheelcolor,vehicle_plateindex,vehicle_neoncolor1,vehicle_neoncolor2,vehicle_neoncolor3,vehicle_windowtint,vehicle_wheeltype,vehicle_mods0,vehicle_mods1,vehicle_mods2,vehicle_mods3,vehicle_mods4,vehicle_mods5,vehicle_mods6,vehicle_mods7,vehicle_mods8,vehicle_mods9,vehicle_mods10,vehicle_mods11,vehicle_mods12,vehicle_mods13,vehicle_mods14,vehicle_mods15,vehicle_mods16,vehicle_turbo,vehicle_tiresmoke,vehicle_xenon,vehicle_mods23,vehicle_mods24,vehicle_neon0,vehicle_neon1,vehicle_neon2,vehicle_neon3,vehicle_bulletproof,vehicle_smokecolor1,vehicle_smokecolor2,vehicle_smokecolor3,vehicle_modvariation) VALUES(@user_id,@vehicle,@vehicle_plate,@sequestro,@veh_type,@vehicle_colorprimary,@vehicle_colorsecondary,@vehicle_pearlescentcolor,@vehicle_wheelcolor,@vehicle_plateindex,@vehicle_neoncolor1,@vehicle_neoncolor2,@vehicle_neoncolor3,@vehicle_windowtint,@vehicle_wheeltype,@vehicle_mods0,@vehicle_mods1,@vehicle_mods2,@vehicle_mods3,@vehicle_mods4,@vehicle_mods5,@vehicle_mods6,@vehicle_mods7,@vehicle_mods8,@vehicle_mods9,@vehicle_mods10,@vehicle_mods11,@vehicle_mods12,@vehicle_mods13,@vehicle_mods14,@vehicle_mods15,@vehicle_mods16,@vehicle_turbo,@vehicle_tiresmoke,@vehicle_xenon,@vehicle_mods23,@vehicle_mods24,@vehicle_neon0,@vehicle_neon1,@vehicle_neon2,@vehicle_neon3,@vehicle_bulletproof,@vehicle_smokecolor1,@vehicle_smokecolor2,@vehicle_smokecolor3,@vehicle_modvariation)")

-- SHOWROOM
RegisterServerEvent('super_SR:CheckMoneyForVeh')
AddEventHandler('super_SR:CheckMoneyForVeh', function(vehicle, price ,veh_type)
  local user_id = vRP.getUserId({source})
  local player = vRP.getUserSource({user_id})
  --if vRP.hasGroup(user_id1,"Istruttore1") then	
  MySQL.query("vRP/get_vehicle", {user_id = user_id, vehicle = vehicle}, function(pvehicle, affected)
	if #pvehicle > 0 then
		vRPclient.notify(player,{"~r~Veicolo già posseduto."})
  else
	if vRP.hasPermission({user_id,"luke.concessionaria"}) then		
		if vRP.tryFullPayment({user_id,price}) then
			vRP.getUserIdentity({user_id, function(identity)
				MySQL.query("vRP/add_custom_vehicle", {
					user_id = user_id, 
					vehicle = vehicle,
					 vehicle_plate = "P "..identity.registration,
					 sequestro = false,
					 veh_type = veh_type, 
					 vehicle_colorprimary = 111, 
					 vehicle_colorsecondary = 0, 
					 vehicle_pearlescentcolor = 0, 
					 vehicle_wheelcolor = 0, 
					 vehicle_plateindex = 0, 
					 vehicle_neoncolor1 = 255, 
					 vehicle_neoncolor2 = 0, 
					 vehicle_neoncolor3 = 255, 
					 vehicle_windowtint = -1, 
					 vehicle_wheeltype = 6, 
					 vehicle_mods0 = -1, 
					 vehicle_mods1 = -1, 
					 vehicle_mods2 = -1, 
					 vehicle_mods3 = -1, 
					 vehicle_mods4 = -1, 
					 vehicle_mods5 = -1, 
					 vehicle_mods6 = -1, 
					 vehicle_mods7 = -1, 
					 vehicle_mods8 = -1, 
					 vehicle_mods9 = -1, 
					 vehicle_mods10 = -1, 
					 vehicle_mods11 = -1, 
					 vehicle_mods12 = -1, 
					 vehicle_mods13 = -1, 
					 vehicle_mods14 = -1, 
					 vehicle_mods15 = -1, 
					 vehicle_mods16 = -1, 
					 vehicle_turbo = "off", 
					 vehicle_tiresmoke = "off", 
					 vehicle_xenon = "off", 
					 vehicle_mods23 = -1, 
					 vehicle_mods24 = -1, 
					 vehicle_neon0 = "off", 
					 vehicle_neon1 = "off", 
					 vehicle_neon2 = "off", 
					 vehicle_neon3 = "off", 
					 vehicle_bulletproof = "off", 
					 vehicle_smokecolor1 = 255, 
					 vehicle_smokecolor2 = 255, 
					 vehicle_smokecolor3 = 255, 
					 vehicle_modvariation = "off"
					})
			end})
			
				TriggerClientEvent('super_SR:CloseMenu', player, vehicle, veh_type)
				vRPclient.notify(player,{"Pagato ~r~"..price.."$."})
		 else
			vRPclient.notify(player,{"~r~Non hai abbastanza soldi."})
		end
		
	 else vRPclient.notify(player,{"~r~Non sei autorizzato a comprare veicolo, chiama un dipendente per comprarlo."}) 
	end
	
	end	
  end)
end)

RegisterServerEvent('super_SR:CheckMoneyForBasicVeh')
AddEventHandler('super_SR:CheckMoneyForBasicVeh', function(user_id, vehicle, price ,veh_type)
  local player = vRP.getUserSource({user_id})
  local user_id = vRP.getUserId({source})
  local user_id1 = vRP.getUserId(player)	
  MySQL.query("vRP/get_vehicle", {user_id = user_id, vehicle = vehicle}, function(pvehicle, affected)
	    if #pvehicle > 0 then
		    vRPclient.notify(player,{"~r~Veicolo già posseduto."})
		      vRP.giveMoney({user_id,price})
			  else	
          vRPclient.notify(player,{"Pagato ~r~"..price.."$."})
		    vRP.getUserIdentity({user_id, function(identity)
					MySQL.query("vRP/add_custom_vehicle", {
					user_id = user_id, 
					vehicle = vehicle,
					 vehicle_plate = "P "..identity.registration, 
					 sequestro = false,
					 veh_type = veh_type, 
					 vehicle_colorprimary = 111, 
					 vehicle_colorsecondary = 0, 
					 vehicle_pearlescentcolor = 0, 
					 vehicle_wheelcolor = 0, 
					 vehicle_plateindex = 0, 
					 vehicle_neoncolor1 = 255, 
					 vehicle_neoncolor2 = 0, 
					 vehicle_neoncolor3 = 255, 
					 vehicle_windowtint = -1, 
					 vehicle_wheeltype = 6, 
					 vehicle_mods0 = -1, 
					 vehicle_mods1 = -1, 
					 vehicle_mods2 = -1, 
					 vehicle_mods3 = -1, 
					 vehicle_mods4 = -1, 
					 vehicle_mods5 = -1, 
					 vehicle_mods6 = -1, 
					 vehicle_mods7 = -1, 
					 vehicle_mods8 = -1, 
					 vehicle_mods9 = -1, 
					 vehicle_mods10 = -1, 
					 vehicle_mods11 = -1, 
					 vehicle_mods12 = -1, 
					 vehicle_mods13 = -1, 
					 vehicle_mods14 = -1, 
					 vehicle_mods15 = -1, 
					 vehicle_mods16 = -1, 
					 vehicle_turbo = "off", 
					 vehicle_tiresmoke = "off", 
					 vehicle_xenon = "off", 
					 vehicle_mods23 = -1, 
					 vehicle_mods24 = -1, 
					 vehicle_neon0 = "off", 
					 vehicle_neon1 = "off", 
					 vehicle_neon2 = "off", 
					 vehicle_neon3 = "off", 
					 vehicle_bulletproof = "off", 
					 vehicle_smokecolor1 = 255, 
					 vehicle_smokecolor2 = 255, 
					 vehicle_smokecolor3 = 255, 
					 vehicle_modvariation = "off"
					})
		end})
		 Gclient.spawnBoughtVehicle(player,{veh_type, vehicle})
	  end
  end)
end)