MySQL = module("vrp_mysql", "MySQL")
local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")
local htmlEntities = module("vrp", "lib/htmlEntities")

vRPca = {}
vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP","vrp_car_addon")
CAclient = Tunnel.getInterface("vrp_car_addon","vrp_car_addon")
Tunnel.bindInterface("vrp_car_addon",vRPca)

local cfg = module("vrp_car_addon", "cfg/attrezzatura")

MySQL.createCommand("vRP/get_componenti","SELECT componenti FROM vrp_car_addon WHERE user_id = @user_id AND vehicle = @vehicle")
MySQL.createCommand("vRP/set_componenti","UPDATE vrp_car_addon SET componenti = @componenti WHERE user_id = @user_id AND vehicle = @vehicle")
-- init

function vRPca.setComponenti(user_id,vehicle,comp)
	MySQL.execute("vRP/set_componenti", {user_id = user_id, vehicle = vehicle, componenti = comp})
end

function vRP.getComponenti(user_id, vehicle, cbr)
  local task = Task(cbr, {false})
  MySQL.query("vRP/get_componenti", {user_id = user_id, vehicle = vehicle}, function(rows, affected)
    if #rows > 0 then
      task({rows[1].componenti})
    else
      task()
    end
  end)
end

RegisterServerEvent('Componenti:SpawnVeh')
AddEventHandler('Componenti:SpawnVeh', function(user_id,vehicle)
	vRP.getComponenti(user_id, vehicle, function(dati)
		if dati then 
			CAclient.SetComponente(source,"olio",dati.olio)
		end
	end)
end)
