--Settings--
local Tunnel = module("vrp", "lib/Tunnel")
local Proxy = module("vrp", "lib/Proxy")

vRP = Proxy.getInterface("vRP")
vRPclient = Tunnel.getInterface("vRP","vRP_salar")


salarii = {
--  grup permission     $$$    Message "From bla bla"
	{"police.paycheck", 350, ""},
	{"emergency.paycheck", 400, ""},
	{"repair.paycheck", 300, ""},
	{"delivery.paycheck", 200, ""}
}


RegisterServerEvent('salar')
AddEventHandler('salar', function(salar)
	local user_id = vRP.getUserId({source})
	local player = vRP.getUserSource({user_id})
	pictura = "DIA_CUSTOMER"
	titlu = "Banca"
	mesaj = "Salario ~g~$"
	for i, v in pairs(salarii) do
		permisiune = v[1]
		if(vRP.hasPermission({user_id, permisiune}))then
			salar = v[2]
			deLaGrupa = v[3]
			vRP.giveMoney({user_id,salar})
			vRP.givePuncte({user_id, 1})
			vRPclient.notifyPicture(player,{pictura, 9, titlu, false, mesaj..salar.. "~w~. "..deLaGrupa})
		end
	end
end)