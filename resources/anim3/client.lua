-- Created by Deziel0495 and IllusiveTea --

-- NOTICE
-- This script is licensed under "No License". https://choosealicense.com/no-license/
-- You are allowed to: Download, Use and Edit the Script. 
-- You are not allowed to: Copy, re-release, re-distribute it without our written permission.

--- DO NOT EDIT THIS
local holstered = true

-- RESTRICTED PEDS --
-- I've only listed peds that have a remote speaker mic, but any ped listed here will do the animations.
local skins = {
	"mp_m_freemode_01",
	"mp_f_freemode_01",
	"s_m_y_cop_01",
	"s_f_y_cop_01",
    "s_m_y_swat_01",	
	"s_m_y_hwaycop_01",
	"s_m_y_sheriff_01",
	"s_f_y_sheriff_01",
	"s_m_y_ranger_01",
	"s_m_m_ciasec_01",
	"s_f_y_ranger_01",
}

-- Add/remove weapon hashes here to be added for holster checks.
local weapons = {
	"WEAPON_ASSAULTRIFLE",
	"WEAPON_CARBINERIFL",
	"WEAPON_ADVANCEDRIFLE ",
	"WEAPON_MG",
	"WEAPON_COMBATMG",
	"WEAPON_PUMPSHOTGUN",
	"WEAPON_SAWNOFFSHOTGUN",
	"WEAPON_ASSAULTSHOTGUN",
	"WEAPON_BULLPUPSHOTGUN",
	"WEAPON_SNIPERRIFLE",
	"WEAPON_HEAVYSNIPER",
	"WEAPON_RPG",
	"WEAPON_MICROSMG",
	"WEAPON_SMG",
	"WEAPON_ASSAULTSMG",
	"WEAPON_DBSHOTGUN",
	"WEAPON_DOUBLEACTION",
	"WEAPON_GUSENBERG",
	"WEAPON_ASSAULTRIFLE_MK2",
	"WEAPON_SPECIALCARBINE",
	"WEAPON_COMPACTRIFLE",
	"WEAPON_SWEEPERSHOTGUN",
	"WEAPON_HEAVYSNIPER_MK2",
	"WEAPON_COMPACTLAUNCHER",
	"WEAPON_PUMPSHOTGUN_MK2",
	"WEAPON_BULLPUPRIFLE_MK2",
	"WEAPON_SPECIALCARBINE_MK2",
	"WEAPON_CARBINERIFLE_MK2",
	"WEAPON_MARKSMANRIFLE_MK2",
	
}
--PTTRadioAnim
 
 Citizen.CreateThread(function()
	while true do
		Citizen.Wait(0)
		local ped = PlayerPedId()
		if DoesEntityExist( ped ) and not IsEntityDead( ped ) and not IsPedInAnyVehicle(PlayerPedId(), true) and CheckSkin(ped) then
			loadAnimDict( "combat@combat_reactions_2handed" )
			loadAnimDict( "weapons@pistol@" )
			if CheckWeapon(ped) then
				if holstered then
					TaskPlayAnim(ped, "combat@combat_reactions_2handed", "2handed_react_messy_90", 16.0, 2.0, -1, 48, 10, 0, 0, 0 )
					Citizen.Wait(300)
					holstered = false
				end
			elseif not CheckWeapon(ped) then
				if not holstered then
					TaskPlayAnim(ped, "weapons@rifle@", "0", 8.0, 2.0, -1, 48, 10, 0, 0, 0 )
					Citizen.Wait(300)
					holstered = true
				end
			end
		end
	end
end)

-- DO NOT REMOVE THESE! --

function CheckSkin(ped)
	for i = 1, #skins do
		if GetHashKey(skins[i]) == GetEntityModel(ped) then
			return true
		end
	end
	return false
end

function CheckWeapon(ped)
	for i = 1, #weapons do
		if GetHashKey(weapons[i]) == GetSelectedPedWeapon(ped) then
			return true
		end
	end
	return false
end

function DisableActions(ped)
	DisableControlAction(1, 140, true)
	DisableControlAction(1, 141, true)
	DisableControlAction(1, 142, true)
	DisableControlAction(1, 37, true) --Disables INPUT_SELECT_WEAPON (tab) Actions
	DisablePlayerFiring(ped, true) -- Disable weapon firing
end

function loadAnimDict( dict )
	while ( not HasAnimDictLoaded( dict ) ) do
		RequestAnimDict( dict )
		Citizen.Wait( 0 )
	end
end
