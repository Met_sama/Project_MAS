resource_manifest_version '77731fab-63ca-442c-a67b-abc70f28dfa5'
 
files {
    'vehicles.meta',
    'handling.meta',
    'weapons_file.meta',
    'dlctext.meta',
}

data_file 'HANDLING_FILE' 'handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'vehicles.meta'
data_file 'WEAPONINFO_FILE' 'weapons_file.meta'
data_file 'TEXTFILE_METAFILE' 'dlctext.meta'
