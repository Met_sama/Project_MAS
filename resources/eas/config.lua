Config = {}
Config.EAS = {}
Config.EAS.Departments = {
    LSPD    = {
    
        name = "Dipartimento Di Polizia"
    
    },
    
    LSSD    = {
    
        name = "Los Santos Sheriff's Department"
    
    },

    USGVT   = {
    
        name = "United States Government"
    
    }
}