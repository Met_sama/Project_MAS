if(globalConf["SERVER"].enableGiveKey)then
    RegisterCommand('givekey', function(source, args, rawCommand)
        local src = source
        local identifier = GetPlayerIdentifiers(src)[1]

        if(args[1])then
            local targetId = args[1]
            local targetIdentifier = GetPlayerIdentifiers(targetId)[1]
            if(targetIdentifier)then
                if(targetIdentifier ~= identifier)then
                    if(args[2])then
                        local plate = string.lower(args[2])
                        if(owners[plate])then
                            if(owners[plate] == identifier)then
                                alreadyHas = false
                                for k, v in pairs(secondOwners) do
                                    if(k == plate)then
                                        for _, val in ipairs(v) do
                                            if(val == targetIdentifier)then
                                                alreadyHas = true
                                            end
                                        end
                                    end
                                end

                                if(not alreadyHas)then
                                    TriggerClientEvent("ls:giveKeys", targetIdentifier, plate)
                                    TriggerEvent("ls:addSecondOwner", targetIdentifier, plate)

                                    TriggerClientEvent("ls:notify", targetId, "Hai ricevuto le chiavi del veicolo " .. plate .. " da " .. GetPlayerName(src))
                                    TriggerClientEvent("ls:notify", src, "Hai dato le chiavi del veicolo " .. plate .. " to " .. GetPlayerName(targetId))
                                else
                                    TriggerClientEvent("ls:notify", src, "Il giocatore ha già le chaivi")
                                    TriggerClientEvent("ls:notify", targetId, GetPlayerName(src) .. " ho cercato di darti le sue chiavi, ma le hai già")
                                end
                            else
                                TriggerClientEvent("ls:notify", src, "Questo non è il tuo veicolo")
                            end
                        else
                            TriggerClientEvent("ls:notify", src, "Questo veicolo ha qualcosa che non va..")
                        end
                    else
                        TriggerClientEvent("ls:notify", src, "Prova così : /givekey <id> <plate>")
                    end
                else
                    TriggerClientEvent("ls:notify", src, "Non puoi dare le chiavi a te stesso")
                end
            else
                TriggerClientEvent("ls:notify", src, "Giocatore non trovato")
            end
        else
            TriggerClientEvent("ls:notify", src, 'Prova così : /givekey <id> <plate>')
        end

        CancelEvent()
    end)
end
