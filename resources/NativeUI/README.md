# NativeUILua-Reloaded

## Warning, NativeUI-Shared.lua will not be kept update, I invite the community to update it with the changes. 

#### ⚠️Following a change and for more understanding I decided to remove the fork and to dedicate a repository to this magnificent project that will be named NativeUI Reloaded , previously based on [FrazzIe](https://github.com/FrazzIe) work . ⚠️

Original code / concept
 
 | name                         	| author                      	| development status   	| link                                   	| language 	|
 |------------------------------	|-----------------------------	|----------------------	|----------------------------------------	|----------	|
 | NativeUILua                  	| FrazzIe                     	| Discontinued (maybe) 	| https://github.com/FrazzIe/NativeUILua 	| Lua      	|
 | NativeUI ported to CitizenFX 	| Guard (ported by CitizenFX) 	| Discontinued         	| https://github.com/citizenfx/NativeUI  	| C#       	|
 | NativeUI                     	| Guard                       	| Continued            	| https://github.com/Guad/NativeUI       	| C#       	|

### How to use NativeUI-Shared.lua cross resource ?
1) Download project
2) Drag the file you have downloaded into the resource folder of your FXServer  
3) Start the resource in your server.cfg `start NativeUILua-Reloaded` (Be careful to load the resource before using it in another)
4) Add this line in the resource or you want to use NativeUILua-Reloaded-Shared `client_script '@NativeUILua-Reloaded/NativeUI-Shared.lua'`
Everything is ready !

### Warning, NativeUI-Shared.lua will not be kept update, I invite the community to update it with the changes. 


### Thanks to all who follow this project ! 🖤
