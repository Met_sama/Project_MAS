local holstered = true

local weapons = {
	"WEAPON_KNIFE", 
	"WEAPON_NIGHTSTICK", 
	"WEAPON_HAMMER", 
	"WEAPON_CROWBAR",
	"WEAPON_DAGGER", 
	"WEAPON_KNUCKLE", 
	"WEAPON_BOTTLE", 
	"WEAPON_FLARE", 
	"WEAPON_FLASHLIGHT", 
}

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(0)
		local ped = PlayerPedId()
		if DoesEntityExist( ped ) and not IsEntityDead( ped ) and not IsPedInAnyVehicle(PlayerPedId(), true) then
			loadAnimDict( "anim@melee@switchblade@holster" )
			if CheckWeapon(ped) then
				if holstered then
					TaskPlayAnim(ped, "anim@melee@switchblade@holster", "unholster", 8.0, 2.0, -1, 48, 10, 0, 0, 0 )
					Citizen.Wait(600)
					ClearPedTasks(ped)
					holstered = false
				end
			elseif not CheckWeapon(ped) then
				if not holstered then
					TaskPlayAnim(ped, "anim@melee@switchblade@holster", "holster", 8.0, 2.0, -1, 48, 10, 0, 0, 0 )
					Citizen.Wait(500)
					ClearPedTasks(ped)
					holstered = true
				end
			end
		end
	end
end)

function CheckWeapon(ped)
	for i = 1, #weapons do
		if GetHashKey(weapons[i]) == GetSelectedPedWeapon(ped) then
			return true
		end
	end
	return false
end

function loadAnimDict( dict )
	while ( not HasAnimDictLoaded( dict ) ) do
		RequestAnimDict( dict )
		Citizen.Wait( 0 )
	end
end
