dependency "vrp"

client_scripts {
    "lib/Proxy.lua",
    "lib/Tunnel.lua",
    "infinite_stamina/client.lua",
    "timeAndDateDisplay_client.lua",
    "neverwanted/nowanted.lua",
    "DriveByLimit/client.lua",
    "point/pointfinger-client.lua",
    "sigte/scope.lua",
    "carhud.lua",
	"deletepoliceweapons-client.lua",
    "stungun/stungun-client.lua",
    "weapondrop/noweapondrops-client.lua",
    "policevehiclesweapondeleter/client.lua",
    "armeria1.lua",	
    "holograms.lua",	
    "animstop/client.lua",
}

server_scripts {
    "@vrp/lib/utils.lua",
    "policevehiclesweapondeleter/server.lua",
	
}