Config = {}

----------------------------------------------------
-------- Intervalles en secondes -------------------
----------------------------------------------------

-- Temps d'attente Antispam / Waiting time for antispam
Config.AntiSpamTimer = 20

-- Vérification et attribution d'une place libre / Verification and allocation of a free place
Config.TimerCheckPlaces = 3

-- Mise à jour du message (emojis) et accès à la place libérée pour l'heureux élu / Update of the message (emojis) and access to the free place for the lucky one
Config.TimerRefreshClient = 3

-- Mise à jour du nombre de points / Number of points updating
Config.TimerUpdatePoints = 6

----------------------------------------------------
------------ Nombres de points ---------------------
----------------------------------------------------

-- Nombre de points gagnés pour ceux qui attendent / Number of points earned for those who are waiting
Config.AddPoints = 1

-- Nombre de points perdus pour ceux qui sont entrés dans le serveur / Number of points lost for those who entered the server
Config.RemovePoints = 1

-- Nombre de points gagnés pour ceux qui ont 3 emojis identiques (loterie) / Number of points earned for those who have 3 identical emojis (lottery)
Config.LoterieBonusPoints = 25

-- Accès prioritaires / Priority access
Config.Points = {
	-- {'steamID', points},
	--met 
	{'steam:11000010ec255e5', 100000},
	--stronght 
	{'steam:1100001171eae66', 100000},	
	--simo
	{'steam:110000108a9f4d8', 10000},
	--cousik
	{'steam:1100001154a8b08', 10000},
	--luke
	{'steam:11000010568639d', 10000},
	--chip
	{'steam:110000107263553', 5000},
	--zioluke
	{'steam:11000010adab606', 1000},
	{'steam:11000010aca6584', 1000},
	{'steam:11000010adab606', 1000},
	--vicuz
	{'steam:110000123a2c6b7', 2000},
	--JC
	{'steam:11000010667815f', 5000},
	--Rykkuchan
	{'steam:11000010902bbeb', 900},
	--Max
	{'steam:110000105564b1a', 900},
	--xFisbo
	{'steam:11000011a2050c9', 900},
	--Sax
	{'steam:11000010a35ac28', 900},
	--genny mafia
	{'steam:11000010442c714', 900},
	--Capuano Mafia
	{'steam:1100001093f03fc', 900},
	--Terror
	{'steam:11000010544ec60', 900},
	--FgFoto Crew
	{'steam:11000010380e224', 900},
	{'steam:110000101d45b86', 900},
	{'steam:110000117025c27', 900},	
	--- Helper
	
	{'steam:1100001342abb52', 900},
	{'steam:1100001140abc97', 900},
	{'steam:11000011456206f', 900},

	--loli
	{'steam:1100001072996c7', 999},	
	--Holazzoso id 22
	{'steam:1100001086c166c', 999},
	--VanCulo
	{'steam:11000010626184f', 999},
	
	--marcuskron
	{'steam:1100001065e24e1', 999},	
	
	--ilchina
	{'steam:110000104758631', 999},	
	
	--davidhaus
	{'steam:1100001132ac0d9', 999},	
	-- Pula
	{'steam:110000111513ac8', 800},
	{'steam:110000107654936', 800},
	{'steam:1100001118ab755', 800},
	{'steam:11000010d57d5e9', 800},
	
	--medici
	{'steam:11000010afcc432', 800},
	
	--meccanici
	{'steam:110000137a79d68', 800},
	{'steam:11000010a291624', 800},
	{'steam:110000137a79d68', 800}
}

----------------------------------------------------
------------- Textes des messages ------------------
----------------------------------------------------

-- Si steam n'est pas détecté / If steam is not detected
Config.NoSteam = "Steam non è stato rilevato. Per favore (ri) lancia Steam e FiveM, e riprova."
-- Config.NoSteam = "Steam was not detected. Please (re)launch Steam and FiveM, and try again."

-- Message d'attente / Waiting text
Config.EnRoute = "Stai arrivando. Hai completato il  viaggio"
-- Config.EnRoute = "You are on the road. You have already traveled"

-- "points" traduits en langage RP / "points" for RP purpose
Config.PointsRP = "chilometri"
-- Config.PointsRP = "kilometers"

-- Position dans la file / position in the queue
Config.Position = "Sei in posizione "
-- Config.Position = "You are in position "

-- Texte avant les emojis / Text before emojis
Config.EmojiMsg = "Se gli emoji sono fermi, riavvia il tuo client : "
-- Config.EmojiMsg = "If the emojis are frozen, restart your client: "

-- Quand le type gagne à la loterie / When the player win the lottery
Config.EmojiBoost = "!!! Urrà!, " .. Config.LoterieBonusPoints .. " " .. Config.PointsRP .. " guadagnato !!!"
-- Config.EmojiBoost = "!!! Yippee, " .. Config.LoterieBonusPoints .. " " .. Config.PointsRP .. " won !!!"

-- Anti-spam message / anti-spam text
Config.PleaseWait_1 = "Per favore, aspetta "
Config.PleaseWait_2 = " secondi. La connessione verrà avviata automaticamente !"
-- Config.PleaseWait_1 = "Please wait "
-- Config.PleaseWait_2 = " seconds. The connection will start automatically!"

-- Me devrait jamais s'afficher / Should never be displayed
Config.Accident = "Oops, hai appena avuto un incidente ... Se succede di nuovo, puoi informare lo staff :)"
-- Config.Accident = "Oops, you just had an accident ... If it happens again, you can inform the support :)"

-- En cas de points négatifs / In case of negative points
Config.Error = " ERRORE: RIPETERE LA ROCADE E CONTATTARE IL SUPPORTO DEL SERVER "
-- Config.Error = " ERROR : RESTART THE QUEUE SYSTEM AND CONTACT THE SUPPORT "


Config.EmojiList = {
	'🐌', 
	'🐍',
	'🐎', 
	'🐑', 
	'🐒',
	'🐘', 
	'🐙', 
	'🐛',
	'🐜',
	'🐝',
	'🐞',
	'🐟',
	'🐠',
	'🐡',
	'🐢',
	'🐤',
	'🐦',
	'🐧',
	'🐩',
	'🐫',
	'🐬',
	'🐲',
	'🐳',
	'🐴',
	'🐅',
	'🐈',
	'🐉',
	'🐋',
	'🐀',
	'🐇',
	'🐏',
	'🐐',
	'🐓',
	'🐕',
	'🐖',
	'🐪',
	'🐆',
	'🐄',
	'🐃',
	'🐂',
	'🐁',
	'🔥'
}
