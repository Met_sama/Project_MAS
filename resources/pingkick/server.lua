-- CONFIG --

-- Ping Limit
pingLimit = 250

-- CODE --

RegisterServerEvent("checkMyPingBro")
AddEventHandler("checkMyPingBro", function()
	ping = GetPlayerPing(source)
	if ping >= pingLimit then
		DropPlayer(source, "Ping troppo alto (Limite: " .. pingLimit .. " Il tuo Ping: " .. ping .. ")")
	end
end)