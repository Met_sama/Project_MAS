Citizen.CreateThread(function()
    while true do
        Citizen.Wait(0)
		if IsControlJustPressed(1, 288) then
			TriggerEvent('pv:setCruiseSpeed')
		end
	end
end)

local cruise = 0

AddEventHandler('pv:setCruiseSpeed', function()
	if cruise == 0 and IsPedInAnyVehicle(GetPlayerPed(-1), false) then
		if GetEntitySpeedVector(GetVehiclePedIsIn(GetPlayerPed(-1), false), true)['y'] > 0 then
			cruise = GetEntitySpeed(GetVehiclePedIsIn(GetPlayerPed(-1), false))
			local cruiseKm = math.floor(cruise * 3.6 + 0.5)
			local cruiseMph = math.floor(cruise * 2.23694 + 0.5)
			
				TriggerEvent("pNotify:SendNotification",{
				text = "Fartpilot: <b style='color:#00c800'> TÆNDT - <b style='color:#ffffff'> " .. cruiseKm .." km/t",
				type = "success",
				timeout = (3000),
				layout = "bottomCenter",
				queue = "global",
				killer=true,
				animation = {
				open = "gta_effects_fade_in", 
				close = "gta_effects_fade_out",
				}
				})

			Citizen.CreateThread(function()
				while cruise > 0 and GetPedInVehicleSeat(GetVehiclePedIsIn(GetPlayerPed(-1), false), -1) == GetPlayerPed(-1) do
					local cruiseVeh = GetVehiclePedIsIn(GetPlayerPed(-1), false)
					if IsVehicleOnAllWheels(cruiseVeh) and GetEntitySpeed(GetVehiclePedIsIn(GetPlayerPed(-1), false)) > (cruise - 3.0) then
						SetVehicleForwardSpeed(GetVehiclePedIsIn(GetPlayerPed(-1), false), cruise)
					else
						cruise = 0
						TriggerEvent("pNotify:SendNotification",{
							text = "Fartpilot: <b style='color:#ff0000'>SLUKKET",
							type = "success",
							timeout = (3000),
							layout = "bottomCenter",
							queue = "global",
							killer=true,
							animation = {
							open = "gta_effects_fade_in", 
							close = "gta_effects_fade_out"
							}
							})

						break
					end
					if IsControlPressed(1, 8) then
						cruise = 0
						TriggerEvent("pNotify:SendNotification",{
							text = "Fartpilot: <b style='color:#ff0000'>SLUKKET",
							type = "success",
							timeout = (3000),
							layout = "bottomCenter",
							queue = "global",
							killer=true,
							animation = {
							open = "gta_effects_fade_in", 
							close = "gta_effects_fade_out"
							}
							})

					end
					if IsControlPressed(1, 32) then
						cruise = 0
						TriggerEvent('pv:setNewSpeed')
					end
					if cruise > 100 then
						cruise = 0
						TriggerEvent("pNotify:SendNotification",{
							text = "Fartpilot: <b style='color:#ff0000'>Kan ikke sættes så højt!",
							type = "success",
							timeout = (3000),
							layout = "bottomCenter",
							queue = "global",
							killer=true,
							animation = {
							open = "gta_effects_fade_in", 
							close = "gta_effects_fade_out"
							}
							})

						break
					end
					Wait(200)
				end
				cruise = 0
			end)
		else
			cruise = 0
			TriggerEvent("pNotify:SendNotification",{
				text = "Fartpilot: <b style='color:#ff0000'>SLUKKET",
				type = "success",
				timeout = (3000),
				layout = "bottomCenter",
				queue = "global",
				killer=true,
				animation = {
				open = "gta_effects_fade_in", 
				close = "gta_effects_fade_out"
				}
				})

		end
	else
		if cruise > 0 then
						TriggerEvent("pNotify:SendNotification",{
							text = "Fartpilot: <b style='color:#ff0000'>SLUKKET",
							type = "success",
							timeout = (3000),
							layout = "bottomCenter",
							queue = "global",
							killer=true,
							animation = {
							open = "gta_effects_fade_in", 
							close = "gta_effects_fade_out"
							}
							})

		end
		cruise = 0
	end
end)

AddEventHandler('pv:setNewSpeed', function()
	Citizen.CreateThread(function()
		while IsControlPressed(1, 32) do
			Wait(1)
		end
		TriggerEvent('pv:setCruiseSpeed')
	end)
end)