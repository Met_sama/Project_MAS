version '0.1.0'

client_scripts {
	'cruise.lua',
	'handsup.lua',
	'nodriveby.lua',
	'noweapondrop.lua',
	'novintageshoot.lua',
	'nowanted.lua',
	'stamina.lua',
	'drowning.lua',
	'flipoff.lua'
}