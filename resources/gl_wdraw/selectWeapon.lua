local allWeapons = {
  'WEAPON_KNIFE',             
  'WEAPON_NIGHTSTICK',        
  'WEAPON_HAMMER',            
  'WEAPON_BAT',               
  'WEAPON_GOLFCLUB',          
  'WEAPON_CROWBAR',           
  'WEAPON_BOTTLE',            
  'WEAPON_KNUCKLE',           
  'WEAPON_HATCHET',           
  'WEAPON_MACHETE',           
  'WEAPON_SWITCHBLADE',       
  'WEAPON_FLASHLIGHT',        
  'WEAPON_PISTOL',            
  'WEAPON_COMBATPISTOL',      
  'WEAPON_APPISTOL',          
  'WEAPON_PISTOL50',          
  'WEAPON_VINTAGEPISTOL',     
  'WEAPON_HEAVYPISTOL',       
  'WEAPON_SNSPISTOL',         
  'WEAPON_FLAREGUN',          
  'WEAPON_MARKSMANPISTOL',    
  'WEAPON_REVOLVER',          
  'WEAPON_STUNGUN',           
  'WEAPON_MICROSMG',          
  'WEAPON_SMG',               
  'WEAPON_MG',                
  'WEAPON_COMBATMG',          
  'WEAPON_GUSENBERG',         
  'WEAPON_COMBATPDW',         
  'WEAPON_MACHINEPISTOL',     
  'WEAPON_ASSAULTSMG',        
  'WEAPON_MINISMG',           
  'WEAPON_ASSAULTRIFLE',      
  'WEAPON_CARBINERIFLE',      
  'WEAPON_ADVANCEDRIFLE',     
  'WEAPON_SPECIALCARBINE',    
  'WEAPON_BULLPUPRIFLE',      
  'WEAPON_COMPACTRIFLE',      
  'WEAPON_PUMPSHOTGUN',       
  'WEAPON_SAWNOFFSHOTGUN',    
  'WEAPON_BULLPUPSHOTGUN',    
  'WEAPON_ASSAULTSHOTGUN',    
  'WEAPON_MUSKET',            
  'WEAPON_HEAVYSHOTGUN',      
  'WEAPON_DBSHOTGUN',         
  'WEAPON_AUTOSHOTGUN',       
  'WEAPON_SNIPERRIFLE',       
  'WEAPON_HEAVYSNIPER',       
  'WEAPON_MARKSMANRIFLE',     
  'WEAPON_REMOTESNIPER',      
  'WEAPON_STINGER',           
  'WEAPON_GRENADELAUNCHER',   
  'WEAPON_RPG',               
  'WEAPON_MINIGUN',           
  'WEAPON_FIREWORK',          
  'WEAPON_RAILGUN',           
  'WEAPON_HOMINGLAUNCHER',    
  'WEAPON_COMPACTLAUNCHER',   
  'WEAPON_STICKYBOMB',        
  'WEAPON_MOLOTOV',           
  'WEAPON_FIREEXTINGUISHER',  
  'WEAPON_PETROLCAN',         
  'WEAPON_PROXMINE',          
  'WEAPON_SNOWBALL',          
  'WEAPON_BALL',              
  'WEAPON_GRENADE',           
  'WEAPON_SMOKEGRENADE',      
  'WEAPON_BZGAS',             
  'WEAPON_DIGISCANNER',       
  'WEAPON_DAGGER',            
  'WEAPON_GARBAGEBAG',        
  'WEAPON_HANDCUFFS',         
  'WEAPON_BATTLEAXE',         
  'WEAPON_PIPEBOMB',          
  'WEAPON_POOLCUE',           
  'WEAPON_WRENCH',            
  'GADGET_NIGHTVISION',       
  'GADGET_PARACHUTE',
  'WEAPON_UNARMED'
}

local WEAPON_HASHES = {
    ["-1716189206"] = "Knife",
    ["1737195953"] = "Nightstick",
    ["1317494643"] = "Hammer",
    ["-1786099057"] = "Bat",
    ["1141786504"] = "Golf Club",
    ["-2067956739"] = "Crowbar",
    ["-102323637"] = "Bottle",
    ["-538741184"] = "Switch Blade",
    ["453432689"] = "Pistol",
    ["1593441988"] = "Combat Pistol",
    ["584646201"] = "AP Pistol",
    ["-1716589765"] = "Pistol 50",
    ["1198879012"] = "Flare Gun",
    ["-598887786"] = "Marksman Pistol",
    ["-1045183535"] = "Revolver",
    ["324215364"] = "Micro SMG",
    ["736523883"] = "SMG",
    ["-270015777"] = "Assault SMG",
    ["171789620"] = "Combat PDW",
    ["-1074790547"] = "Assault Rifle",
    ["-2084633992"] = "Carbine Rifle",
    ["-1357824103"] = "Advanced Rifle",
    ["1649403952"] = "Compact Rifle",
    ["-1660422300"] = "MG",
    ["2144741730"] = "Combat MG",
    ["487013001"] = "Pump Shotgun",
    ["2017895192"] = "Sawn Off Shotgun",
    ["-494615257"] = "Assault Shotgun",
    ["-1654528753"] = "Bullpup Shotgun",
    ["-275439685"] = "Double Barrel Shotgun",
    ["911657153"] = "Stun Gun",
    ["100416529"] = "Sniper Rifle",
    ["205991906"] = "Heavy Sniper",
    ["-1568386805"] = "Grenade Launcher",
    ["1305664598"] = "Grenade Launcher Smoke",
    ["-1834847097"] = "Antique Cavalry Dagger",
    ["-1312131151"] = "RPG",
    ["1119849093"] = "Minigun",
    ["-1813897027"] = "Grenade",
    ["741814745"] = "Sticky Bomb",
    ["4256991824"] = "Smoke Grenade",
    ["-37975472"] = "Tear Gas",
    ["-1600701090"] = "BZ Gas",
    ["615608432"] = "Molotov",
    ["101631238"] = "Fire Extinguisher",
    ["883325847"] = "Petrol Can",
    ["-1076751822"] = "SNS Pistol",
    ["-1063057011"] = "Special Carbine",
    ["-771403250"] = "Heavy Pistol",
    ["2132975508"] = "Bullpup Rifle",
    ["1672152130"] = "Homing Launcher",
    ["-1420407917"] = "Proximity Mine",
    ["126349499"] = "Snowball",
    ["137902532"] = "Vintage Pistol",
    ["2460120199"] = "Dagger",
    ["2138347493"] = "Firework",
    ["-1466123874"] = "Musket",
    ["-952879014"] = "Marksman Rifle",
    ["984333226"] = "Heavy Shotgun",
    ["1627465347"] = "Gusenberg",
    ["-102973651"] = "Hatchet",
    ["1834241177"] = "Railgun",
    ["2725352035"] = "Unarmed",
    ["-656458692"] = "Knuckle Duster",
    ["-581044007"] = "Machete",
    ["-619010992"] = "Machine Pistol",
    ["-1951375401"] = "Flashlight",
    ["600439132"] = "Ball",
    ["1233104067"] = "Flare",
    ["2803906140"] = "Night Vision",
    ["4222310262"] = "Parachute",
    ["317205821"] = "Sweeper Shotgun",
    ["3441901897"] = "Battle Axe",
    ["125959754"] = "Compact Grenade Launcher",
    ["3173288789"] = "Mini SMG",
    ["3125143736"] = "Pipe Bomb",
    ["2484171525"] = "Pool Cue",
    ["921050412"] = "Radar Gun",
    ["419712736"] = "Wrench",
    ["-1569615261"] = "Unarmed"
}

local getOutWeapon = -1

local selectKey = 73

local function cycleWeapon()
  if getOutWeapon == #allWeapons then
    getOutWeapon = -1
  end
  for k = getOutWeapon,#allWeapons do
    local v = allWeapons[k]
    if getOutWeapon ~= k then
      if HasPedGotWeapon(PlayerPedId(), GetHashKey(v), false) then
        getOutWeapon = k
        break
      end
    end
  end
end

local function weaponLoop()

  if IsPedInAnyVehicle(PlayerPedId(), false) then
    DisableControlAction(0, selectKey, true)

    if IsDisabledControlJustPressed(0, selectKey) then
      cycleWeapon()
    end
  end

end

local function drawText()
  if getOutWeapon ~= -1 then
    exports.GTALife:drawTxt("Exit Weapon: ~b~"..WEAPON_HASHES[tostring(GetHashKey(allWeapons[getOutWeapon]))],0,false,0.25,0.001,0.5,255,255,255,255)
  end
end

local function checkOutWeapon()
  if getOutWeapon ~= -1 then
    if not IsPedInAnyVehicle(PlayerPedId(), true) then
      TriggerEvent("weapondraw:pauseWeaponDraw", 1000)
      GiveWeaponToPed(PlayerPedId(), GetHashKey(allWeapons[getOutWeapon]), 0, false, true)
      getOutWeapon = -1
    end
  end
end

Citizen.CreateThread(function()
  while true do
    Wait(5)
    weaponLoop()
    drawText()
    checkOutWeapon()
  end
end)