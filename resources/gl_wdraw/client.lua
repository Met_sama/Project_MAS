local pauseWeaponDraw = -1

local lastAnimWeapon = -1
local delayEquipWeapon = -1

local delayHideWeapon = -1
local removeWeaponOnHide = false

local dropWeapon = -1

local animPlaying = false
local stopTime = -1

local copHolster = false

local meleeAnim = {dict="anim@melee@switchblade@holster",anim="unholster"}
local pistolAnim = {dict="reaction@intimidation@1h",anim="intro"}
local rifleAnim = {dict="anim@heists@money_grab@duffel",anim="enter"}

local melee = {
	"WEAPON_KNIFE", 
	"WEAPON_NIGHTSTICK", 
	"WEAPON_HAMMER", 
	"WEAPON_CROWBAR",
	"WEAPON_DAGGER", 
	"WEAPON_KNUCKLE", 
	"WEAPON_BOTTLE", 
	"WEAPON_FLARE", 
	"WEAPON_FLASHLIGHT", 
}

local pistols = {
	"WEAPON_PISTOL", 
	"WEAPON_COMBATPISTOL", 
	"WEAPON_APPISTOL", 
	"WEAPON_PISTOL50",
	"WEAPON_STUNGUN", 
	"WEAPON_SNSPISTOL", 
	"WEAPON_HEAVYPISTOL", 
	"WEAPON_FLAREGUN", 
	"WEAPON_VINTAGEPISTOL", 
  	"WEAPON_REVOLVER", 
  	"WEAPON_DOUBLEACTION"
}

local rifles = {
	"WEAPON_BAT", 
  	"WEAPON_GOLFCLUB", 
  	"WEAPON_FIREEXTINGUISHER",  
	--"WEAPON_NIGHTSTICK", 
  	"WEAPON_PETROLCAN",
  	"WEAPON_MACHETE", 
  	"WEAPON_DBSHOTGUN", 
  	"WEAPON_COMPACTRIFLE",
  	"WEAPON_HEAVYSHOTGUN", 
  	"WEAPON_GUSENBERG", 
  	"WEAPON_COMBATPDW",
  	"WEAPON_MUSKET",
  	"WEAPON_BULLPUPSHOTGUN",  
  	"WEAPON_PUMPSHOTGUN", 
  	"WEAPON_SAWNOFFSHOTGUN", 
  	"WEAPON_MICROSMG", 
  	"WEAPON_SMG", 
  	"WEAPON_ASSAULTSMG", 
  	"WEAPON_ASSAULTRIFLE", 
  	"WEAPON_CARBINERIFLE", 
}

local function checkMelee()
	for _, v in ipairs(melee) do
		if GetHashKey(v) == GetSelectedPedWeapon(PlayerPedId()) then
			if v ~= lastAnimWeapon then
				return true, GetSelectedPedWeapon(PlayerPedId())
			end
		end
	end
	return false
end

local function checkPistol()
	for _, v in pairs(pistols) do
		if GetHashKey(v) == GetSelectedPedWeapon(PlayerPedId()) then
			if v ~= lastAnimWeapon then
				return true, GetSelectedPedWeapon(PlayerPedId())
			end
		end
	end
	return false
end

local function checkRifle()
	for _, v in ipairs(rifles) do
		if GetHashKey(v) == GetSelectedPedWeapon(PlayerPedId()) then
			if v ~= lastAnimWeapon then
				return true, GetSelectedPedWeapon(PlayerPedId())
			end
		end
	end
	return false
end

local function PlayAnim(dict, anim, time)
	animPlaying = true
	RequestAnimDict(dict)

	while not HasAnimDictLoaded(dict) do Wait(5) end
	TaskPlayAnim(PlayerPedId(), dict, anim, 8.0, -8, -1, 50, 0, false, false, false)
	stopTime = GetGameTimer() + time
end

local function delayHide(time, remove, weapon)
	delayHideWeapon = GetGameTimer() + time

	if remove then
		removeWeaponOnHide = weapon
	else
		removeWeaponOnHide = false
	end
end

local function pullMelee()
	RequestAnimDict(meleeAnim.dict)

	while not HasAnimDictLoaded(meleeAnim.dict) do
		Wait(5)
	end

	animPlaying = true
	TaskPlayAnim(PlayerPedId(), meleeAnim.dict, meleeAnim.anim, 3.0, 0.0, -1, 48, 0, 0, 0, 0)
	stopTime = GetGameTimer() + 2000
	--Wait(1000)
end

local function putAwayMelee()
	RequestAnimDict(meleeAnim.dict)

	while not HasAnimDictLoaded(meleeAnim.dict) do
		Wait(5)
	end

	animPlaying = true
	TaskPlayAnim(PlayerPedId(), meleeAnim.dict, "holster", 3.0, 0.0, -1, 48, 0, 0, 0, 0)
	stopTime = GetGameTimer() + 2000
end

local function pullPistol(weapon)
	SetPedCurrentWeaponVisible(PlayerPedId(), false, false, 0, 0)
	PlayAnim(pistolAnim.dict, pistolAnim.anim, 2500)
	delayEquipWeapon = 1200 + GetGameTimer()
end

local function putAwayPistol(weapon)
	PlayAnim(pistolAnim.dict, "outro", 2500)
	if HasPedGotWeapon(PlayerPedId(), weapon, 0) then
		delayHide(1200, false)
	else
		delayHide(1200, true, weapon)
	end
	GiveWeaponToPed(PlayerPedId(), weapon, 0, false, true)
end

local function pullRifle()
	SetPedCurrentWeaponVisible(PlayerPedId(), false, false, 0, 0)
	PlayAnim(rifleAnim.dict, rifleAnim.anim, 1200)
	delayEquipWeapon = 1200
end

local function putAwayRifle()
	SetPedCurrentWeaponVisible(PlayerPedId(), false, true, 0, 0)
	PlayAnim(rifleAnim.dict, rifleAnim.anim, 1000)
end

local function checkCopHolsterAnim()

	if not IsEntityPlayingAnim(PlayerPedId(), "reaction@intimidation@cop@unarmed", "intro", 3) then
		RequestAnimDict("reaction@intimidation@cop@unarmed")

		while not HasAnimDictLoaded("reaction@intimidation@cop@unarmed") do Wait(0) end

		TaskPlayAnim(PlayerPedId(), "reaction@intimidation@cop@unarmed", "intro", 8.0, 1.0, -1, 50, 2.0, 0, 0, 0)
	end

	if DoesEntityExist(GetVehiclePedIsTryingToEnter(PlayerPedId())) then
		copHolster = false
		ClearPedSecondaryTask(PlayerPedId())
	end
end

local function checkUnarmedSelected()
	if GetSelectedPedWeapon(PlayerPedId()) ~= GetHashKey("WEAPON_UNARMED") then
		GiveWeaponToPed(PlayerPedId(), GetHashKey("WEAPON_UNARMED"), 0, false, true)
	end
end

local function checkDrawKeys()
	if IsControlJustPressed(0, 24) or IsControlJustPressed(0, 25) then
		copHolster = false
		ClearPedSecondaryTask(PlayerPedId())
		lastAnimWeapon = GetHashKey("WEAPON_COMBATPISTOL")
		GiveWeaponToPed(PlayerPedId(), GetHashKey("WEAPON_COMBATPISTOL"), 0, false, true)
	end
end

local function checkStartCopHolster()
	if not IsPedInAnyVehicle(PlayerPedId(), true) then
		if IsControlJustPressed(0, 74) then
			copHolster = not copHolster
		end
	end
end

local function checkAnimEnd()

	if delayEquipWeapon < GetGameTimer() and delayEquipWeapon ~= -1 then
		SetPedCurrentWeaponVisible(PlayerPedId(), true, false, 0, 0)
		--TriggerEvent("ShowInformation", 1000, "Show Weapon")
		delayEquipWeapon = -1
	end

	if delayHideWeapon < GetGameTimer() and delayHideWeapon ~= -1 then
		--SetPedCurrentWeaponVisible(PlayerPedId(), false, true, 0, 0)
		GiveWeaponToPed(PlayerPedId(), GetHashKey("WEAPON_UNARMED"), 0, false, true)
		--TriggerEvent("ShowInformation", 1000, "Hide Weapon")
		if removeWeaponOnHide then
			RemoveWeaponFromPed(PlayerPedId(), removeWeaponOnHide)
			removeWeaponOnHide = false
		end
		delayHideWeapon = -1
	end

	if stopTime < GetGameTimer() and stopTime ~= -1 then
		--ClearPedTask(PlayerPedId())
		ClearPedTasks(PlayerPedId())
		print("Clear")
		stopTime = -1
		animPlaying = false
		--TriggerEvent("ShowInformation", 1000, "Anim Done")
	end
end

local function disableFireActions()
	DisableControlAction(0, 24, true)
	DisableControlAction(0, 25, true)
	DisablePlayerFiring(PlayerPedId(), true)
end

function copHolsterLoop()
	if copHolster then
		checkCopHolsterAnim()
		checkUnarmedSelected()
		checkDrawKeys()
	else
		if IsEntityPlayingAnim(PlayerPedId(), "reaction@intimidation@cop@unarmed", "intro", 3) then
			ClearPedSecondaryTask(PlayerPedId())
		end
	end
end

function PlayDropAnim()
	RequestAnimDict("MP_weapon_drop")

	while not HasAnimDictLoaded("MP_weapon_drop") do Wait(0) end

	TaskPlayAnim(PlayerPedId(), "MP_weapon_drop", "drop_bh", 8.0, 1.0, -1, 50, 2.0, 0, 0, 0)
	dropWeapon = -1
	stopTime = GetGameTimer() + 450
end

local function checkPaused()
	if pauseWeaponDraw == -1 then
		return true
	else
		if pauseWeaponDraw < GetGameTimer() then
			lastAnimWeapon = GetSelectedPedWeapon(PlayerPedId())
			pauseWeaponDraw = -1
		end
		return false
	end
end


Citizen.CreateThread(function()
	while true do
		Wait(5)

		if checkPaused() then
			if exports.GTALife:isPlayerCop() or copHolster then
				copHolsterLoop()
				checkStartCopHolster()
			end

			if not IsPedInAnyVehicle(PlayerPedId(), true) and animPlaying == false and lastAnimWeapon ~= GetSelectedPedWeapon(PlayerPedId()) then
				if checkMelee() then
					local _, w = checkMelee()
					lastAnimWeapon = w
					pullPistol(w)
				elseif checkPistol() then
					local _, w = checkPistol()
					lastAnimWeapon = w
					pullPistol(w)
				elseif checkRifle() then
					local _, w = checkRifle()
					lastAnimWeapon = w
					pullRifle()
				end
			end

			if GetSelectedPedWeapon(PlayerPedId()) == GetHashKey("WEAPON_UNARMED") and lastAnimWeapon ~= -1 and animPlaying == false and dropWeapon == -1 and not IsPedInAnyVehicle(PlayerPedId(), true) then
				for _, v in ipairs(melee) do
					if lastAnimWeapon == GetHashKey(v) then
						putAwayPistol()
						break
					end
				end
				for _, v in ipairs(pistols) do
					if lastAnimWeapon == GetHashKey(v) then
						putAwayPistol(GetHashKey(v))
						break
					end
				end

				for _, v in ipairs(rifles) do
					if lastAnimWeapon == GetHashKey(v) then
						putAwayRifle()
						break
					end
				end
				lastAnimWeapon = -1
			end

			if dropWeapon ~= -1 then
				PlayDropAnim()
				dropWeapon = -1
				lastAnimWeapon = -1
			end

			checkAnimEnd()
			if animPlaying then
				disableFireActions()
			end
		end
	end
end)

function setLastAnimWeapon(weapon)
	dropWeapon = weapon
end

RegisterNetEvent("dropWeapon")
AddEventHandler("dropWeapon", setLastAnimWeapon)

RegisterNetEvent("weapondraw:pauseWeaponDraw")
AddEventHandler("weapondraw:pauseWeaponDraw", function(time)
	pauseWeaponDraw = GetGameTimer() + time
end)